/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.bean;

import java.util.Collection;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class SpecificationInfoBean.
 */
public class SpecificationInfoBean extends CoreInfoBean<SpecificationInfoBean> {

    /** The reference. */
    private String reference;

    /** The version. */
    private String version;

    /** The revision. */
    private Long revision;

    /** The description. */
    private String description;

    /** The tagged. */
    private boolean tagged;

    /** The action. */
    private String action;

    /** The group **/
    private String groupe;

    /** The size. */
    private long size;

    private JsonNode details;

    /** The reference id. */
    private Long referenceId;

    /** The metadata **/
    private Map<String, Collection<String>> metadata;

    /** The color **/
    private String color;

    /** The icon **/
    private String icon;

    /** The priority *. */
    private Long priority;

    /**
     * Gets the reference.
     *
     * @return the reference
     */
    public String getReference() {
        return this.reference;
    }

    /**
     * Sets the reference.
     *
     * @param reference
     *            the new reference
     * @return the specification info bean
     */
    public SpecificationInfoBean setReference(final String reference) {
        this.reference = reference;
        return this;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the new version
     * @return the specification info bean
     */
    public SpecificationInfoBean setVersion(final String version) {
        this.version = version;
        return this;
    }

    /**
     * Gets the revision.
     *
     * @return the revision
     */
    public Long getRevision() {
        return this.revision;
    }

    /**
     * Sets the revision.
     *
     * @param revision
     *            the new revision
     * @return the specification info bean
     */
    public SpecificationInfoBean setRevision(final Long revision) {
        this.revision = revision;
        return this;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     * @return the specification info bean
     */
    public SpecificationInfoBean setDescription(final String description) {
        this.description = description;
        return this;
    }

    /**
     * Checks if is tagged.
     *
     * @return true, if is tagged
     */
    public boolean isTagged() {
        return this.tagged;
    }

    /**
     * Sets the tagged.
     *
     * @param tagged
     *            the new tagged
     * @return the specification info bean
     */
    public SpecificationInfoBean setTagged(final boolean tagged) {
        this.tagged = tagged;
        return this;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public String getAction() {
        return this.action;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            the new action
     * @return the specification info bean
     */
    public SpecificationInfoBean setAction(final String action) {
        this.action = action;
        return this;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public long getSize() {
        return this.size;
    }

    /**
     * Sets the size.
     *
     * @param size
     *            the new size
     * @return the specification info bean
     */
    public SpecificationInfoBean setSize(final long size) {
        this.size = size;
        return this;
    }

    /**
     * Gets the reference id.
     *
     * @return the reference id
     */
    public Long getReferenceId() {
        return this.referenceId;
    }

    /**
     * Sets the reference id.
     *
     * @param referenceId
     *            the new reference id
     * @return the specification info bean
     */
    public SpecificationInfoBean setReferenceId(final Long referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    /**
     * @return the deatils
     */
    public JsonNode getDetails() {
        return details;
    }

    /**
     * @param deatils
     *            the deatils to set
     */
    public SpecificationInfoBean setDetails(JsonNode details) {
        this.details = details;
        return this;
    }

    /**
     * @return the group
     */
    public String getGroupe() {
        return groupe;
    }

    /**
     * @param group
     *            the group to set
     */
    public SpecificationInfoBean setGroupe(final String groupe) {
        this.groupe = groupe;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #metadata}.
     *
     * @return Map<String,Collection<String>> metadata
     */
    public Map<String, Collection<String>> getMetadata() {
        return metadata;
    }

    /**
     * Mutateur sur l'attribut {@link #metadata}.
     *
     * @param metadata
     *            la nouvelle valeur de l'attribut metadata
     */
    public SpecificationInfoBean setMetadata(final Map<String, Collection<String>> metadata) {
        this.metadata = metadata;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #color}.
     *
     * @return String color
     */
    public String getColor() {
        return color;
    }

    /**
     * Mutateur sur l'attribut {@link #color}.
     *
     * @param color
     *            la nouvelle valeur de l'attribut color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Accesseur sur l'attribut {@link #icon}.
     *
     * @return String icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Mutateur sur l'attribut {@link #icon}.
     *
     * @param icon
     *            la nouvelle valeur de l'attribut icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * Accesseur sur l'attribut {@link #priority}.
     *
     * @return Long priority
     */
    public Long getPriority() {
        return priority;
    }

    /**
     * Mutateur sur l'attribut {@link #priority}.
     *
     * @param priority
     *            la nouvelle valeur de l'attribut priority
     */
    public void setPriority(Long priority) {
        this.priority = priority;
    }

}
