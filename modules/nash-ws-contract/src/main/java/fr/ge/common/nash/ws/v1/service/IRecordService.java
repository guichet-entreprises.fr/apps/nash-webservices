/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.ws.v1.bean.ProxyResultBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;

/**
 * Service interface to handle records.
 *
 * @author Christian Cougourdan
 */
@Path("/v1/Record")
public interface IRecordService extends ICoreService<RecordInfoBean> {

    /**
     * Creates a record.
     *
     * @param resourceAsBytes
     *            the uploaded record
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param roles
     *            the roles using "[entity path]:[role]" format
     * @return the response
     */
    @POST
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Créer un dossier", //
            description = "Création d'un dossier assigné à un auteur" //
    )
    Response upload( //
            @Parameter(description = "Contenu du modèle") byte[] resourceAsBytes, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Droits par défaut à positionner sur le dossier") @QueryParam("roles") @DefaultValue("*:*") List<SearchQueryFilter> roles);

    /**
     * Creates a record.
     *
     * @param resourceAsBytes
     *            the uploaded record
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param context
     *            the context
     * @param roles
     *            the roles using "[entity path]:[role]" format
     * @return the response
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Créer un dossier avec contexte", //
            description = "Création d'un dossier avec contexte assigné à un auteur" //
    )
    Response upload( //
            @Parameter(description = "Contenu du modèle") @Multipart(value = "record") byte[] resourceAsBytes, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Droits par défaut à positionner sur le dossier") @QueryParam("roles") @DefaultValue("") List<SearchQueryFilter> roles, //
            @Parameter(description = "Contenu du dossier") @Multipart(value = "context") byte[] context //
    );

    /**
     * Creates a record by model reference with input context.
     *
     * @param reference
     *            the reference
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param context
     *            the context
     * @param lang
     *            the lang
     * @param roles
     *            the roles using "[entity path]:[role]" format
     * @return the response
     * @throws JsonParseException
     *             the json parse exception
     * @throws JsonMappingException
     *             the json mapping exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @POST
    @Path("/ref/{reference:.*}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Créer un dossier avec contexte", //
            description = "Création d'un dossier avec contexte assigné à un auteur" //
    )
    Response uploadWithContext( //
            @Parameter(description = "Référentiel métier du modèle de formalité") @PathParam("reference") String reference, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Contenu du modèle") @Multipart(value = "context") byte[] context, //
            @Parameter(description = "Langue par défaut") @QueryParam("lang") @DefaultValue("") String lang //
    ) throws JsonParseException, JsonMappingException, IOException;

    /**
     * Updates or creates a file in a record.
     *
     * @param code
     *            the record identifier
     * @param resourcePath
     *            the path of the file to update within the record
     * @param resourceAsBytes
     *            the uploaded file
     * @return the response
     */
    @PUT
    @Path("/code/{code}/{resourcePath:.*}")
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Créer ou mettre à jour un ressource d'un dossier", //
            description = "Création ou mise à jour d'un fichier de ressource existant d'un dossier crée" //
    )
    Response upload( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") String code, //
            @Parameter(description = "Chemin absolu de la ressource") @PathParam("resourcePath") String resourcePath, //
            @Parameter(description = "Contenu du modèle") byte[] resourceAsBytes);

    /**
     * Removes a file in a record.
     *
     * @param code
     *            the record identifier
     * @param resourcePath
     *            the path of the file to remove within the record
     * @return the response
     */
    @DELETE
    @Path("/code/{code}/{resourcePath:.*}")
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Supprimer une ressource d'un dossier", //
            description = "Suppression d'un fichier de ressource existant d'un dossier crée" //
    )
    Response remove( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") String code, //
            @Parameter(description = "Chemin absolu de la ressource") @PathParam("resourcePath") String resourcePath);

    /**
     * Removes a record.
     *
     * @param code
     *            the record identifier
     * @return the response
     */
    @DELETE
    @Path("/code/{code}")
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Supprimer un dossier", //
            description = "Suppression d'un dossier crée" //
    )
    Response remove( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") String code);

    /**
     * Search for records.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return issues search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Rechercher des dossiers", //
            description = "Recherche de dossiers à partir des critères de tri et filtre" //
    )
    RecordSearchResult search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    /**
     * Search for records using metas also.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @param searchTerms
     *            terms as string
     * @return issues search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Rechercher des dossiers", //
            description = "Recherche full-text de dossiers à partir des critères de tri et filtre" //
    )
    RecordSearchResult search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Mots-clés") @QueryParam("q") String searchTerms //
    );

    /**
     * Transform a Guichet-Entreprises record to a Guichet-Partenaires record.
     *
     * @param formsRecordName
     *            The FORMS record name
     * @param formsRecordType
     *            The FORMS record type
     * @param resourceAsBytes
     *            the Guichet-Entreprises record
     * @param author
     *            The author identifier
     * @param roles
     *            the roles using "[entity path]:[role]" format
     * @return The record identifier
     * @throws FunctionalException
     *             the functional exception
     */
    @POST
    @Path("/transform")
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Générer un dossier Guichet-Entreprises (ancien moteur de formalités) en un dossier Guichet-Partenaires (nouveau moteur)", //
            description = "Génération de dossier (ancien moteur de formalités) en dossier (nouveau moteur)" //
    )
    String transform( //
            @Parameter(description = "Nom du dossier déclarant") @QueryParam("formsRecordName") String formsRecordName, //
            @Parameter(description = "Type du dossier déclarant") @QueryParam("formsRecordType") String formsRecordType, //
            @Parameter(description = "Contenu du dossier") byte[] resourceAsBytes, //
            @Parameter(description = "Auteur du dossier") @QueryParam("author") String author,
            @Parameter(description = "Droits par défaut") @QueryParam("roles") @DefaultValue("") List<SearchQueryFilter> roles) throws FunctionalException;

    /**
     * Execute an identified action for a record.
     *
     * @param code
     *            the record identifier
     * @param action
     *            the action identifier
     * @param engineUser
     *            the engine user
     * @return the response
     */
    @POST
    @Path("/code/{code}/action/{action}/execute")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Exécuter une action sur un dossier", //
            description = "Exécution d'une étape sur un dossier" //
    )
    Response executeAction( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") String code, //
            @Parameter(description = "Type d'action") @PathParam("action") String action, //
            @Parameter(description = "Type d'utilisateur") @QueryParam("engineUser") @DefaultValue("") String engineUser);

    /**
     * Upload proxy result.
     *
     * @param recordUid
     *            the record uid
     * @param stepId
     *            the step id
     * @param processPhase
     *            the process phase
     * @param processId
     *            the process id
     * @param resultFileName
     *            the result file name
     * @param proxyResult
     *            the proxy result
     * @return the response
     */
    @POST
    @Path("/code/{recordUid}/step/{stepId}/phase/{processPhase}/process/{processId}/{resultFileName}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Générer un fichier du résultat dans un dossier", //
            description = "Génération d'un rapport suite à l'exécution du proxy suite à un paiement ou une signature électronique (proxy) dans un dossier" //
    )
    Response uploadProxyResult( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("recordUid") String recordUid, //
            @Parameter(description = "Identifiant technique de l'étape en cours") @PathParam("stepId") String stepId, //
            @Parameter(description = "Phase technique du process en cours") @PathParam("processPhase") String processPhase, //
            @Parameter(description = "Identifiant technique du (pre|post)process en cours") @PathParam("processId") String processId,
            @Parameter(description = "Nom du fichier de résultat") @PathParam("resultFileName") String resultFileName, ProxyResultBean proxyResult);

    /**
     * Assign all records from owner to assignee.
     *
     * @param owner
     *            The owner identifier
     * @param assignee
     *            The assignee identifier
     * @return the response
     */
    @PUT
    @Path("/assign")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Résssigner tous les dossiers d'un utilisateur à un autre", //
            description = "Résssigner en masse de tous les dossiers d'un utilisateur à un autre" //
    )
    Response assign( //
            @Parameter(description = "Identifiant technique du compte source") @QueryParam("owner") @DefaultValue("") String owner, //
            @Parameter(description = "Identifiant technique du compte cible") @DefaultValue("") @QueryParam("assignee") String assignee);

    /**
     * Check if the author is the owner record.
     *
     * @param code
     * @param author
     * @return
     */
    @GET
    @Path("/code/{code}/author/{author}/own")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Vérifier l'appartenance d'un dossier à un utilisateur", //
            description = "Résssigner en masse de tous les dossiers d'un utilisateur à un autre" //
    )
    boolean own( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") String code, //
            @Parameter(description = "Identifiant technique du compte source") @PathParam("author") String author);

    /**
     * Remove all records whose update date is less than X days.
     * 
     * @param days
     *            the number of days before removing
     * @return
     */
    @DELETE
    @Path("/purge")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Supprimer tous les dossiers antérieurs à un nombre de jours", //
            description = "Suppression en masse de tous les dossiers dont la date de création est antérieur à un nombre de jours" //
    )
    Response purge( //
            @Parameter(description = "Nombre de jours de rétention") @QueryParam("retentionDays") @DefaultValue("1") long days);

    /**
     * Lock a record.
     *
     * @param code
     *            the record code
     * @return the response
     */
    @POST
    @Path("/code/{code}/lock")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Verrouiller un dossier", //
            description = "Verrouillage d'un dossier. Le dossier ne pourra être modifié par son propriétaire." //

    )
    Response lock( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") final String code);

    /**
     * Unlock a record.
     *
     * @param code
     *            the record code
     * @return the response
     */
    @POST
    @Path("/code/{code}/unlock")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Déverrouiller un dossier", //
            description = "Déverrouillage d'un dossier. Le dossier pourra être alors modifié par son propriétaire." //

    )
    Response unlock( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") final String code);

    /**
     * Reset the current step.
     *
     * @param code
     *            the record code
     * @return the response
     */
    @POST
    @Path("/code/{code}/step/reset")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Réinitialiser l'étape courante d'un dossier", //
            description = "Réinitialisation de l'étape courante d'un dossier." //

    )
    Response reset( //
            @Parameter(description = "Identifiant technique du dossier") @PathParam("code") final String code);

    /**
     * Remove records based on criteria.
     *
     * @param code
     *            the record identifier
     * @return the response
     */
    @DELETE
    @Path("/remove")
    @Operation( //
            tags = "Service de gestion des dossiers", //
            summary = "Supprimer en masse les dossiers à partir des critères de filtre.", //
            description = "Suppression en masse les dossiers à partir des critères de filtre." //
    )
    Response remove( //
            @Parameter(description = "Critrèes de filtre") @QueryParam("filters") final List<SearchQueryFilter> filters);
}
