/**
 * 
 */
package fr.ge.common.nash.ws.v1.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author bsadil
 *
 */
public class ListOfSpecificationGroupe {

    private List<GroupInfoSpecificationBean> groups;

    /**
     * @return the groupe
     */
    public List<GroupInfoSpecificationBean> getGroups() {
        if (groups == null) {
            groups = new ArrayList<GroupInfoSpecificationBean>();
        }
        return this.groups;
    }

    /**
     * @param groupe
     *            the groupe to set
     */
    public void setGroups(List<GroupInfoSpecificationBean> groups) {
        this.groups = groups;
    }

}
