/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Service interface to handle specifications.
 *
 * @author Christian Cougourdan
 */
@Path("/v1/Specification")
public interface ISpecificationService extends ICoreService<SpecificationInfoBean> {

    /**
     * Creates a specification specifying group.
     *
     * @param resourceAsBytes
     *            the uploaded specification
     * @param published
     *            must the specification be published ?
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param group
     *            the group
     * @return the response
     */
    @POST
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Créer modèle de formalité", //
            description = "Création d'un modèle de formalité publié ou non à partir d'une archive au format zip." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès, le modèle de formalités a été bien créé.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject("{ \"created\": 1592566846682, \"updated\": 1592566855142, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 4, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 0, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1101, \"metadata\": null, \"color\": \"#0092BC\", \"icon\": \"desktop\", \"priority\": null }") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Le modèle de formalité en entrée est invalide.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response upload( //
            @Parameter(description = "Contenu du modèle de formalité") byte[] resourceAsBytes, //
            @Parameter(description = "Publié ou non le modèle de formalité") @QueryParam("published") @DefaultValue("no") boolean published, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Groupe par défaut") @QueryParam("group") @DefaultValue("all") String group, //
            @Parameter(description = "Création à blanc ou non") @QueryParam("dry") @DefaultValue("no") boolean dry //
    );

    /**
     * Updates a specification.
     *
     * @param code
     *            the specification identifier
     * @param resourceAsBytes
     *            the uploaded specification
     * @param published
     *            must the specification be published ?
     * @param author
     *            the author identifier provided by the "Account" service header
     * @return the response
     */
    @POST
    @Path("/code/{code}")
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Mettre à jour un modèle de formalité existant", //
            description = "Mise à jour d'un modèle de formalité publié ou non à partir d'une archive au format zip." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès, le modèle de formalités a été bien créé.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject("{ \"created\": 1592566846682, \"updated\": 1592566855142, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 4, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 0, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1101, \"metadata\": null, \"color\": \"#0092BC\", \"icon\": \"desktop\", \"priority\": null }") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Le modèle de formalité en entrée est invalide.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response upload( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") String code, //
            @Parameter(description = "Contenu du modèle de formalité") byte[] resourceAsBytes, //
            @Parameter(description = "Publié ou non le modèle de formalité") @QueryParam("published") @DefaultValue("no") boolean published, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author //
    );

    /**
     * Updates a specification.
     *
     * @param code
     *            the specification identifier
     * @param resourceAsBytes
     *            the uploaded specification
     * @param published
     *            must the specification be published ?
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param group
     *            the group
     * @return the response
     */
    @POST
    @Path("/code/{code}")
    @Consumes({ "application/zip", MediaType.APPLICATION_OCTET_STREAM })
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Mettre à jour un modèle de formalité existant", //
            description = "Mise à jour d'un modèle de formalité publié ou non à partir d'une archive au format zip." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès, le modèle de formalités a été bien créé.", //
                    content = @Content( //
                            mediaType = MediaType.TEXT_PLAIN, //
                            examples = @ExampleObject("{ \"created\": 1592566846682, \"updated\": 1592566855142, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 4, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 0, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1101, \"metadata\": null, \"color\": \"#0092BC\", \"icon\": \"desktop\", \"priority\": null }") //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "400", //
                    description = "Le modèle de formalité en entrée est invalide.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response upload( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") String code, //
            @Parameter(description = "Contenu du modèle de formalité") byte[] resourceAsBytes, //
            @Parameter(description = "Publié ou non le modèle de formalité") @QueryParam("published") @DefaultValue("no") boolean published, //
            @Parameter(description = "Auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Groupe du modèle") @QueryParam("group") @DefaultValue("all") String group, //
            @Parameter(description = "Création à blanc ou non") @QueryParam("dry") @DefaultValue("no") boolean dry //
    );

    /**
     * Loads an item.
     *
     * @param reference
     *            the item identifier
     * @return the item
     */
    @GET
    @Path("/ref/{reference:.*}")
    @Produces("application/zip")
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Télécharger un modèle de formalité", //
            description = "Téléchargement d'un modèle de formalité publié ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = "application/zip" //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "204", //
                    description = "Le modèle de formalité n'est pas trouvé.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response downloadByReference( //
            @Parameter(description = "Référentiel métier du modèle de formalité") @PathParam("reference") String reference);

    /**
     * Updates a specification metadata.
     *
     * @param code
     *            the specification identifier
     * @param bean
     *            the new specification metadata
     * @return the specification metadata
     */
    @PUT
    @Path("/code/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Mettre à jour les métadonnées d'un modèle de formalité", //
            description = "Mise à jour des métadonnées d'un modèle de formalité" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
            @ApiResponse( //
                    responseCode = "500", //
                    description = "Le modèle de formalité n'est pas trouvé.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    SpecificationInfoBean persist( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") String code, SpecificationInfoBean bean);

    /**
     * Removes a specification.
     *
     * @param code
     *            the specification code
     * @return the response
     */
    @DELETE
    @Path("/code/{code}")
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Supprimer un modèle de formalité", //
            description = "Supppression d'un modèle de formalité publié ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON //
                    ) //
            ), //
    })
    Response remove( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") String code);

    /**
     * Searches for specifications.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param searchTerms
     *            the search terms
     * @return the specification search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/search")
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Rechercher des modèles de formalités", //
            description = "Recherche full-text de modèles de formalité publiés ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Les modèles de formalités sont retournés.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 1, \"totalResults\": 1, \"content\": [ { \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specificationt\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 } ] }") //
                    ) //
            ), //
    })
    SpecificationSearchResult fullTextSearch( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Mots-clés") @QueryParam("q") String searchTerms);

    /**
     * Searches for specifications.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @return the specification search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Rechercher des modèles de formalités", //
            description = "Recherche de modèles de formalité publiés ou non (non full-text)" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Les modèles de formalités sont retournés.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 1, \"totalResults\": 1, \"content\": [ { \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specificationt\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 } ] }") //
                    ) //
            ), //
    })
    SpecificationSearchResult search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    /**
     * Loads an item.
     *
     * @param reference
     *            the item identifier
     * @return the item
     */
    @GET
    @Path("/ref/{reference:.*}/info")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Rechercher les inforamtions d'un modèle de formalités", //
            description = "Rechercher les inforamtions de modèles de formalités publiés ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Les informations du modèle sont retournés.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 }") //
                    ) //
            ), //
    })
    SpecificationInfoBean loadByReference( //
            @Parameter(description = "Référence métier du modèle de formalité") @PathParam("reference") String reference);

    @GET
    @Path("/template/code/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Charger le template d'un modèle de formalités à partir d'un identifiant", //
            description = "Chargement du template d'un modèle de formalités publiés ou non (au format JSON)" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Le template est bien retourné.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"context\": { \"identity\": { \"lastname\": null, \"firstname\": null, \"age\": null } } }") //
                    ) //
            ), //
    })
    Response templateByCode( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") String code);

    /**
     * Loads an item.
     *
     * @param reference
     *            the item identifier
     * @return the item
     */
    @GET
    @Path("/template/ref/{reference:.*}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Charger le template d'un modèle de formalités à partir d'une référence métier", //
            description = "Chargement du template d'un modèle de formalités publiés ou non (au format JSON)" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Le template est bien retourné.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"context\": { \"identity\": { \"lastname\": null, \"firstname\": null, \"age\": null } } }") //
                    ) //
            ), //
    })
    Response templateByReference( //
            @Parameter(description = "Référence métier du modèle de formalité") @PathParam("reference") String reference);

    /**
     * Update specification priority by reference.
     *
     * @param reference
     *            the reference identifier
     * @param priority
     *            the priority
     * @return the item
     */
    @PUT
    @Path("/ref/{reference:.*}/priority/{priority}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Mettre à jour le poids d'un modèle de formalités à partir d'une référence métier", //
            description = "Mise à jour du poids d'un modèle de formalités publiés ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Le poids a bien été mis à jour.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 }") //
                    ) //
            ), //
    })
    SpecificationInfoBean updatePriorityByReference( //
            @Parameter(description = "Référence métier du modèle de formalité") @PathParam("reference") final String reference, //
            @Parameter(description = "Poids du modèle de formalité") @PathParam("priority") int priority);

    /**
     * Update specification priority by code.
     *
     * @param code
     *            the item identifier
     * @param priority
     *            the priority
     * @return the item
     */
    @PUT
    @Path("/code/{code}/priority/{priority}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Mettre à jour le poids d'un modèle de formalités à partir d'un identifiant", //
            description = "Mise à jour du poids d'un modèle de formalités publiés ou non" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Le poids a bien été mis à jour.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specification\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 }") //
                    ) //
            ), //
    })
    SpecificationInfoBean updatePriorityByCode( //
            @Parameter(description = "Identifiant technique du modèle de formalité") @PathParam("code") final String code, //
            @Parameter(description = "Poids du modèle de formalité") @PathParam("priority") int priority);
}
