/**
 * 
 */
package fr.ge.common.nash.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.nash.ws.v1.bean.ListOfSpecificationGroupe;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * @author bsadil
 *
 */

@Path("/public/v1/Specification")
public interface ISpecificationPublicService {

    /**
     * Searches for specifications.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param searchTerms
     *            the search terms
     * @return the specification search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/search")
    @Operation( //
            tags = "Service de gestion des modèles de formalités", //
            summary = "Rechercher des modèles de formalités", //
            description = "Recherche de modèles de formalité publiés ou non (full-text)" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès. Les modèles de formalités sont retournés.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 1, \"totalResults\": 1, \"content\": [ { \"created\": 1587115530192, \"updated\": 1587115534679, \"code\": \"2020-01-MOD-ELE-01\", \"title\": \"Specification\", \"author\": \"2020-01-AUT-HOR-01\", \"reference\": \"Tools/Specification\", \"version\": \"1.2\", \"revision\": 2, \"description\": null, \"tagged\": true, \"action\": null, \"groupe\": \"all\", \"size\": 22166, \"details\": { \"description\": \"Specificationt\" }, \"referenceId\": 1, \"metadata\": null, \"color\": \"#A9CFF1\", \"icon\": \"check-square-o\", \"priority\": 0 } ] }") //
                    ) //
            ), //
    })
    SpecificationSearchResult fullTextSearch( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders, //
            @Parameter(description = "Mots-clés") @QueryParam("q") String searchTerms //
    );

    /**
     * Loads an item.
     *
     * @return the item
     */
    @GET
    @Path("/group")
    @Produces(MediaType.APPLICATION_JSON)
    ListOfSpecificationGroupe findAllGroupe();

}
