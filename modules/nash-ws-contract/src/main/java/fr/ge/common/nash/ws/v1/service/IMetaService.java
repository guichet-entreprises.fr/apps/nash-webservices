/**
 *
 */
package fr.ge.common.nash.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.nash.ws.v1.bean.search.MetaSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Service interface to handle meta of specifications.
 *
 * @author Adil BSIBISS
 */
@Path("/v1/Meta")
public interface IMetaService {

    /**
     * Search for specification metas.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @return the meta search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des métadonnées", //
            summary = "Rechercher dans les métadonnées d'un dossier ou d'un modèle de formalités", //
            description = "Recherche paginée et indexée avec critères de filtres et de tri" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 1, \"totalResults\": 1, \"content\": [ { \"created\": 1584879923900, \"updated\": 1584879923900, \"key\": \"reference\", \"value\": \"Specification\", \"uid\": \"2020-01-MOD-ELE-01\" } ] }") //
                    ) //
            ), //
    })
    MetaSearchResult search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders //
    );

}
