/**
 *
 */
package fr.ge.common.nash.ws.v1.bean;

/**
 * The Class ReferenceInfoBean.
 *
 * @author bsadil
 */
public class ReferenceInfoBean {

    /** The id. */
    private Long id;

    /** the category of reference. */
    private String category;

    /** the parent of reference. */
    private Long parent;

    /** the content of reference. */
    private String content;

    /** The total specifications. */
    private Long totalSpecifications;

    /**
     * Gets the category.
     *
     * @return the category
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * Sets the category.
     *
     * @param category
     *            the category to set
     * @return the reference info bean
     */
    public ReferenceInfoBean setCategory(final String category) {
        this.category = category;
        return this;
    }

    /**
     * Gets the parent.
     *
     * @return the parent
     */
    public Long getParent() {
        return this.parent;
    }

    /**
     * Sets the parent.
     *
     * @param parent
     *            the parent to set
     * @return the reference info bean
     */
    public ReferenceInfoBean setParent(final Long parent) {
        this.parent = parent;
        return this;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content to set
     * @return the reference info bean
     */
    public ReferenceInfoBean setContent(final String content) {
        this.content = content;
        return this;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     * @return the reference info bean
     */
    public ReferenceInfoBean setId(final Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets the total specifications.
     *
     * @return the total specifications
     */
    public Long getTotalSpecifications() {
        return this.totalSpecifications;
    }

    /**
     * Sets the total specifications.
     *
     * @param totalSpecifications
     *            the total specifications
     * @return the reference info bean
     */
    public ReferenceInfoBean setTotalSpecifications(final Long totalSpecifications) {
        this.totalSpecifications = totalSpecifications;
        return this;
    }

}
