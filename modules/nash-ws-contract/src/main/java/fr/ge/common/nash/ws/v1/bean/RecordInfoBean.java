/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The Class RecordBean.
 *
 * @author Christian Cougourdan
 */
public class RecordInfoBean extends CoreInfoBean<RecordInfoBean> {

    /** The origin. */
    private String origin;

    /** The properties. */
    private byte[] properties;

    /** The entries. */
    private List<EntryInfoBean> entries;

    /** The record details **/
    private JsonNode details;

    /**
     * Gets the origin.
     *
     * @return the origin
     */
    public String getOrigin() {
        return this.origin;
    }

    /**
     * Sets the origin.
     *
     * @param origin
     *            the new origin
     * @return the record info bean
     */
    public RecordInfoBean setOrigin(final String origin) {
        this.origin = origin;
        return this;
    }

    /**
     * Getter on attribute {@link #properties}.
     *
     * @return byte[] properties
     */
    public byte[] getProperties() {
        return this.properties == null ? null : this.properties.clone();
    }

    /**
     * Setter on attribute {@link #properties}.
     *
     * @param properties
     *            the new value of attribute properties
     */
    public RecordInfoBean setProperties(final byte[] properties) {
        this.properties = null == properties ? null : properties.clone();
        return this;
    }

    /**
     * Gets the entries.
     *
     * @return the entries
     */
    public List<EntryInfoBean> getEntries() {
        return this.entries;
    }

    /**
     * Sets the entries.
     *
     * @param entries
     *            the new entries
     * @return the record info bean
     */
    public RecordInfoBean setEntries(final List<EntryInfoBean> entries) {
        if (null == entries) {
            this.entries = null;
        } else {
            this.entries = new ArrayList<>(entries);
        }
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #details}.
     *
     * @return JsonNode details
     */
    public JsonNode getDetails() {
        return this.details;
    }

    /**
     * Mutateur sur l'attribut {@link #details}.
     *
     * @param details
     *            la nouvelle valeur de l'attribut details
     */
    public RecordInfoBean setDetails(final JsonNode details) {
        this.details = details;
        return this;
    }

}
