/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service;

import java.io.IOException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Encoded;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Service interface to handle records.
 *
 * @author Christian Cougourdan
 */
@Path("/v2/record")
public interface IRecordManagedService {

    /**
     * Creates a record.
     *
     * @param resourceAsBytes
     *            the uploaded record
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param context
     *            the context
     * @return the response
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @POST
    @Path("/ref/{reference:.*}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers (managed)", //
            summary = "Créer un dossier avec contexte", //
            description = "Création d'un dossier avec contexte au format JSON à partir d'une référence métier d'un modèle de formalité publié" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("2020-01-REC-ORD-01") //
                    ) //
            ), //
    })
    Response uploadWithContext( //
            @Parameter(description = "Référence métier du modèle de formalité") @PathParam("reference") String reference, //
            @Parameter(description = "Identifiant technique de l'auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Contenu du modèle") @Multipart(value = "context") byte[] context, //
            @Parameter(description = "Langue par défaut") @QueryParam("lang") @DefaultValue("") String lang) throws JsonParseException, JsonMappingException, IOException;

    /**
     * Creates a record.
     * 
     * @param reference
     *            the model reference
     * @param author
     *            the author identifier provided by the "Account" service header
     * @param context
     *            the record context
     * @param lang
     *            the default record language
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    @PUT
    @Consumes(MediaType.APPLICATION_OCTET_STREAM)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des dossiers (managed)", //
            summary = "Créer un dossier avec contexte", //
            description = "Création d'un dossier avec contexte au format JSON à partir d'une référence métier d'un modèle de formalité publié" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("2020-01-REC-ORD-01") //
                    ) //
            ), //
    })
    Response uploadWithContextAndReference( //
            @Parameter(description = "Contenu du modèle") byte[] context, //
            @Parameter(description = "Référence métier du modèle de formalité") @Encoded @QueryParam("reference") String reference, //
            @Parameter(description = "Identifiant technique de l'auteur du modèle") @QueryParam("author") @DefaultValue("") String author, //
            @Parameter(description = "Langue par défaut") @QueryParam("lang") @DefaultValue("") String lang) throws JsonParseException, JsonMappingException, IOException;
}
