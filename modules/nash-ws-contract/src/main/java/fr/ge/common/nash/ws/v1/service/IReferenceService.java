/**
 *
 */
package fr.ge.common.nash.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.nash.ws.v1.bean.ReferenceInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.ReferenceSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Service interface to handle reference of specifications.
 *
 * @author Adil BSIBISS
 */
@Path("/v1/Reference")
public interface IReferenceService {

    /**
     * Search for specification references.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @return the reference search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des références métiers des formalités", //
            summary = "Rechercher une référence de modèles de formalités", //
            description = "Recherche de références de modèles de formalités" //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"startIndex\": 0, \"maxResults\": 1, \"totalResults\": 1, \"content\": [ { \"id\": 1, \"category\": \"Specification\", \"parent\": null, \"content\": \"Specification\", \"totalSpecifications\": 0 } ] }") //
                    ) //
            ), //
    })
    ReferenceSearchResult search( //
            @Parameter(description = "Début de l'index") @QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @Parameter(description = "Nombre de résultats maximal") @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @Parameter(description = "Critères de recherche") @QueryParam("filters") List<SearchQueryFilter> filters, //
            @Parameter(description = "Critères de tri") @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    /**
     * Search for full path element for specified reference.
     *
     * @param referenceId
     *            the reference id
     * @return the list of path element
     */
    @GET
    @Path("/{refId:[0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation( //
            tags = "Service de gestion des références métiers des formalités", //
            summary = "Rechercher l'élément de chemin complet pour la référence spécifiée.", //
            description = "Recherche un élément de chemin complet pour la référence spécifiée." //
    )
    @ApiResponses({ //
            @ApiResponse( //
                    responseCode = "200", //
                    description = "Opération avec succès.", //
                    content = @Content( //
                            mediaType = MediaType.APPLICATION_JSON, //
                            examples = @ExampleObject("{ \"id\": 1, \"category\": \"Specification\", \"parent\": null, \"content\": \"Specification\", \"totalSpecifications\": 0 }") //
                    ) //
            ), //
    })
    List<ReferenceInfoBean> path( //
            @Parameter(description = "Index technique de la référence") @PathParam("refId") long referenceId);

}
