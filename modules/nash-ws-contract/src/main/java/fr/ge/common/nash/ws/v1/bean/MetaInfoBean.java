/**
 * 
 */
package fr.ge.common.nash.ws.v1.bean;

/**
 * @author bsadil
 *
 */
public class MetaInfoBean extends DatedInfoBean<MetaInfoBean> {

    /**
     * the key
     */
    private String key;
    /**
     * the value
     */
    private String value;
    /**
     * the uid
     */
    private String uid;

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key
     *            the key to set
     */
    public MetaInfoBean setKey(String key) {
        this.key = key;
        return this;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public MetaInfoBean setValue(String value) {
        this.value = value;
        return this;

    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public MetaInfoBean setUid(String uid) {
        this.uid = uid;
        return this;
    }

}
