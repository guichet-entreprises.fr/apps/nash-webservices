/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.ws.v1.bean;

import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Christian Cougourdan
 */

@XmlRootElement(name = "proxyResult")
public class ProxyResultBean {

    /**
     * the status of proxy treatment
     */
    private String status;

    /**
     * the message of proxy treatment
     */
    private String message;

    /**
     * the list of documents
     */
    private Map<String, byte[]> documents;

    /**
     *
     */
    public ProxyResultBean() {
        super();
        // Nothing to do
    }

    /**
     * @param status
     * @param message
     * @param documents
     */
    public ProxyResultBean(final String status, final String message, final Map<String, byte[]> documents) {
        super();
        this.status = status;
        this.message = message;
        this.documents = documents;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param status
     *            the status to set
     */
    public ProxyResultBean setStatus(final String status) {
        this.status = status;
        return this;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param message
     *            the message to set
     */
    public ProxyResultBean setMessage(final String message) {
        this.message = message;
        return this;
    }

    /**
     * @return the documents
     */
    public Map<String, byte[]> getDocuments() {
        return this.documents;
    }

    /**
     * @param documents
     *            the documents to set
     */
    public ProxyResultBean setDocuments(final Map<String, byte[]> documents) {
        this.documents = documents;
        return this;
    }

}
