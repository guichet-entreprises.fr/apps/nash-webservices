/**
 * 
 */
package fr.ge.common.nash.ws.v1.bean;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author bsadil
 *
 */
public class GroupInfoSpecificationBean {

    /**
     * the groupName of sepcification
     */
    @Schema(description =  "Group name for record models.")
    private String groupName;

    /**
     * the number total of specifications by group
     */
    private Long totaleByGroup;

    /**
     * @param groupName
     * @param totaleByGroup
     * @return
     */
    public GroupInfoSpecificationBean(String groupName, Long totaleByGroup) {
        this.groupName = groupName;
        this.totaleByGroup = totaleByGroup;
    }

    /**
     * 
     */
    public GroupInfoSpecificationBean() {
        super();
        // Nothing to do
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName
     *            the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the totaleByGroup
     */
    public Long getTotaleByGroup() {
        return totaleByGroup;
    }

    /**
     * @param totaleByGroup
     *            the totaleByGroup to set
     */
    public void setTotaleByGroup(Long totaleByGroup) {
        this.totaleByGroup = totaleByGroup;
    }

}
