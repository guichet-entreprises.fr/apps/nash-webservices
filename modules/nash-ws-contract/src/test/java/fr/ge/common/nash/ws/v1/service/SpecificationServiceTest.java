/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service;

import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Class SpecificationServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/nash-ws-context.xml" })
public class SpecificationServiceTest extends AbstractRestTest {

    /** rest service. */
    @Autowired
    @Qualifier("specificationServiceClient")
    private ISpecificationService service;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        reset(this.specificationService);
    }

    /**
     * Test search.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearch() throws Exception {
        when(this.specificationService.search(any(long.class), any(long.class), any(), any())).thenReturn(this.buildSpecificationSearchResult(0L, 20L));
        final SearchResult<SpecificationInfoBean> searchResult = this.service.search(0L, 20L, null, null);

        assertThat(searchResult, notNullValue());
    }

    /**
     * Test search with order.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchWithOrder() throws Exception {
        when(this.specificationService.search(any(long.class), any(long.class), any(), any())).thenReturn(this.buildSpecificationSearchResult(1L, 5L, this.buildSpecificationInfoBean()));
        final SearchResult<SpecificationInfoBean> searchResult = this.service.search(1L, 5L, Arrays.asList(new SearchQueryFilter("name", "*")), Arrays.asList(new SearchQueryOrder("name", "asc")));

        assertThat(searchResult, notNullValue());
        assertThat(searchResult.getContent().get(0), isA(SpecificationInfoBean.class));
    }

    /**
     * Build specification search result.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results
     * @param beans
     *            beans
     * @return specification search result
     */
    private SpecificationSearchResult buildSpecificationSearchResult(final long startIndex, final long maxResults, final SpecificationInfoBean... beans) {
        final SpecificationSearchResult searchResult = new SpecificationSearchResult(startIndex, maxResults);

        if (null != beans && beans.length > 0) {
            searchResult.setContent(Arrays.asList(beans));
        }

        return searchResult;
    }

    /**
     * Build specification info bean.
     *
     * @return specification info bean
     */
    private SpecificationInfoBean buildSpecificationInfoBean() {
        final Calendar now = Calendar.getInstance();

        return new SpecificationInfoBean() //
                .setCode("2016-11-AAA-AAA-42") //
                .setTitle("Test Specification") //
                .setTagged(false) //
                .setCreated(now) //
                .setUpdated(now);
    }

}
