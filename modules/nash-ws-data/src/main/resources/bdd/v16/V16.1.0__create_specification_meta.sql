-- Drop table specification_meta
DROP TABLE IF EXISTS specification_meta;

-- Create table specification_meta
CREATE TABLE specification_meta
(
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    author character varying(18),
    key character varying(255) NOT NULL,
    uid character varying(18) NOT NULL,
    value character varying(255) NOT NULL,
    tsv tsvector,
    CONSTRAINT specification_meta_pkey PRIMARY KEY (id),
    CONSTRAINT fk_specification_meta_specification_uid FOREIGN KEY (uid) REFERENCES "specification" (uid) ON DELETE CASCADE
);

-- Drop sequence specification_meta
DROP SEQUENCE IF EXISTS sq_specification_meta;

-- Create sequence specification_meta
CREATE SEQUENCE sq_specification_meta INCREMENT 50 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

-- Create indexes to optimise query search
CREATE INDEX specification_meta_ts_idx ON specification_meta USING gin (to_tsvector('french'::regconfig, value::text || ' '::text));
CREATE INDEX idx_specification_meta_uid ON specification_meta USING hash(uid);
CREATE INDEX idx_specification_meta_key ON specification_meta USING hash(key);
