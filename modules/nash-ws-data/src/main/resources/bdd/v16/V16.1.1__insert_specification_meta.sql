-- insert lines related to specifications
INSERT INTO specification_meta (id, created, updated, author, key, uid, value, tsv)
SELECT nextval('sq_specification_meta'), created, updated, author, key, uid, value, tsv FROM meta WHERE uid in (SELECT uid FROM specification);

-- delete lines relate to specifications
DELETE FROM meta WHERE uid IN (SELECT uid FROM specification);

-- delete lines related to non records
DELETE FROM meta WHERE uid NOT IN (SELECT uid FROM record);
