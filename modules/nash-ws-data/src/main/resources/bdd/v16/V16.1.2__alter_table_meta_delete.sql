-- create a foreign key between meta and record
ALTER TABLE meta ADD CONSTRAINT fk_meta_record_uid FOREIGN KEY (uid) REFERENCES "record" (uid) ON DELETE CASCADE;
