-- Table: meta

-- DROP TABLE meta;

CREATE TABLE meta
(
  id bigint NOT NULL,
  created timestamp without time zone NOT NULL,
  updated timestamp without time zone NOT NULL,
  author character varying(18),
  key character varying(255) NOT NULL,
  uid character varying(18) NOT NULL,
  value character varying(255) NOT NULL,
  tsv tsvector,
  CONSTRAINT meta_pkey PRIMARY KEY (id)
);

-- Index: meta_ts_idx

-- DROP INDEX meta_ts_idx;


CREATE INDEX meta_ts_idx
  ON meta
  USING gin
  (to_tsvector('french'::regconfig, value::text || ' '::text));
