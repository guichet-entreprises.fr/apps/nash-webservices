CREATE INDEX idx_record_uid_entity_role ON record_entity_role (uid, entity, role);
CREATE INDEX idx_specification_uid_entity_role ON specification_entity_role (uid, entity, role);