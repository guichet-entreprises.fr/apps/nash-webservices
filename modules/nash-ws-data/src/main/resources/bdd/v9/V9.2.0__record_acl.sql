CREATE TABLE record_entity_role (
    id bigint NOT NULL,
    uid VARCHAR(18) NOT NULL,
    entity VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    CONSTRAINT pk_record_entity_role PRIMARY KEY (id)
);

CREATE SEQUENCE sq_record_entity_role INCREMENT 1;

INSERT INTO record_entity_role (id, uid, entity, role)
SELECT nextval('sq_record_entity_role'), r.uid, r.author, '*' FROM record r;