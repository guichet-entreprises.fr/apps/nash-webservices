CREATE TABLE specification_entity_role
(
  id bigint NOT NULL,
  uid character varying(18) NOT NULL,
  entity character varying(255) NOT NULL,
  role character varying(255) NOT NULL,
  CONSTRAINT specification_entity_role_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE sq_specification_entity_role
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
