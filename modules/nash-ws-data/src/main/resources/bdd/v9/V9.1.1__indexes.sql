CREATE INDEX idx_specification_uid ON specification USING hash(uid);
CREATE INDEX idx_specification_hash ON specification USING hash(hash);
CREATE INDEX idx_specification_tagged_hash ON specification (hash, tagged);
CREATE INDEX idx_specification_reference_id ON specification USING hash(reference_id);

CREATE INDEX idx_record_uid ON record USING hash(uid);
CREATE INDEX idx_record_author ON record USING hash(author);

CREATE INDEX idx_record_file_name ON record_file USING hash(name);
CREATE INDEX idx_record_file_record_id ON record_file (record_id);
CREATE INDEX idx_record_file_record_id_name ON record_file (record_id, name);

CREATE INDEX idx_meta_uid ON meta USING hash(uid);
CREATE INDEX idx_meta_key ON meta USING hash(key);

CREATE INDEX idx_reference_parent ON reference USING hash(parent);
CREATE INDEX idx_reference_category ON reference USING hash(category);
CREATE INDEX idx_reference_parent_category ON reference (parent, category);
