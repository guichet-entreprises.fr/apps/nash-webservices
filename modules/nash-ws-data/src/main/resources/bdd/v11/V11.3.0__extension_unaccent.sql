-- ----------------------------------------------
-- Ajout de l'extension unaccent qui permet la recherche de dossiers par texte avec et sans accents
-- ----------------------------------------------

CREATE EXTENSION IF NOT EXISTS unaccent;

DO
$$BEGIN
   	CREATE TEXT SEARCH CONFIGURATION fr ( COPY = french );
   	ALTER TEXT SEARCH CONFIGURATION fr
	ALTER MAPPING FOR hword, hword_part, word
	WITH unaccent, french_stem;
EXCEPTION
   WHEN unique_violation THEN
      NULL;  -- ignore error
END;$$;

