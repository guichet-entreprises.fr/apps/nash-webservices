--
-- Create extension must be done by superuser or database owner
--
CREATE EXTENSION IF NOT EXISTS lo;

DROP TRIGGER IF EXISTS tg_record_properties ON record;
CREATE TRIGGER tg_record_properties BEFORE UPDATE OF properties OR DELETE ON record FOR EACH ROW EXECUTE PROCEDURE lo_manage(properties);

DROP TRIGGER IF EXISTS tg_record_file_content ON record_file;
CREATE TRIGGER tg_record_file_content BEFORE UPDATE OF content OR DELETE ON record_file FOR EACH ROW EXECUTE PROCEDURE lo_manage(content);

DROP TRIGGER IF EXISTS tg_specification_file_content ON specification_file;
CREATE TRIGGER tg_specification_file_content BEFORE UPDATE OF content OR DELETE ON specification_file FOR EACH ROW EXECUTE PROCEDURE lo_manage(content);

