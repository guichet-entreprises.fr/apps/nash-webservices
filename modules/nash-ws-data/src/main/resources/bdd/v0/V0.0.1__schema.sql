-- ----------------------------------------------
-- table "specification_file"
-- ----------------------------------------------

-- DROP TABLE "specification_file";

CREATE TABLE IF NOT EXISTS "specification_file" (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    content oid NOT NULL,
    name varchar(255) COLLATE pg_catalog."default" NOT NULL,
    size bigint NOT NULL,
    type varchar(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_specification_file PRIMARY KEY (id)
)
    WITH (OIDS = FALSE)
    TABLESPACE pg_default;


-- DROP SEQUENCE IF EXISTS sq_specification_file;

CREATE SEQUENCE "sq_specification_file"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;





-- ----------------------------------------------
-- table "specification"
-- ----------------------------------------------

-- DROP TABLE "specification";

CREATE TABLE "specification" (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    author char(18) COLLATE pg_catalog."default",
    hash bytea,
    name varchar(255) COLLATE pg_catalog."default" NOT NULL,
    reference varchar(255) COLLATE pg_catalog."default" NOT NULL,
    revision bigint NOT NULL,
    tagged boolean NOT NULL,
    uid char(18) COLLATE pg_catalog."default" NOT NULL,
    version varchar(255) COLLATE pg_catalog."default" NOT NULL,
    source_id bigint NOT NULL,
    CONSTRAINT pk_specification PRIMARY KEY (id),
    CONSTRAINT uk_specification_uid UNIQUE (uid),
    CONSTRAINT uk_specification_source_id UNIQUE (source_id),
    CONSTRAINT fk_specification_source_id FOREIGN KEY (source_id)
        REFERENCES "specification_file" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
    WITH (OIDS = FALSE)
    TABLESPACE pg_default;


-- DROP SEQUENCE IF EXISTS "sq_specification";

CREATE SEQUENCE "sq_specification"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;





-- ----------------------------------------------
-- table "record"
-- ----------------------------------------------

-- DROP TABLE "record";

CREATE TABLE "record" (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    author char(18) COLLATE pg_catalog."default",
    spec char(18) COLLATE pg_catalog."default" NOT NULL,
    uid char(18) COLLATE pg_catalog."default" NOT NULL,
    title varchar(255) COLLATE pg_catalog."default",
    CONSTRAINT record_pkey PRIMARY KEY (id)
)
    WITH (OIDS = FALSE)
    TABLESPACE pg_default;


-- DROP SEQUENCE IF EXISTS "sq_record";

CREATE SEQUENCE "sq_record"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;





-- ----------------------------------------------
-- table "record_file"
-- ----------------------------------------------

-- DROP TABLE "record_file";

CREATE TABLE IF NOT EXISTS "record_file" (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    record_id bigint NOT NULL,
    content oid NOT NULL,
    name varchar(255) COLLATE pg_catalog."default" NOT NULL,
    size bigint NOT NULL,
    type varchar(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT pk_record_file PRIMARY KEY (id),
    CONSTRAINT fk_record_file_record_id FOREIGN KEY  (record_id)
        REFERENCES "record" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
    WITH (OIDS = FALSE)
    TABLESPACE pg_default;


DROP SEQUENCE IF EXISTS "sq_record_file";

CREATE SEQUENCE "sq_record_file"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;


