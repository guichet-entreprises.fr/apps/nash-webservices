ALTER TABLE "record" ALTER COLUMN author TYPE  VARCHAR(18);
ALTER TABLE "specification" ADD COLUMN reference_id  bigint NOT NULL;
CREATE TABLE reference
(
  id bigint NOT NULL,
  created timestamp without time zone NOT NULL,
  updated timestamp without time zone NOT NULL,
  category character varying(255) NOT NULL,
  content character varying(255) NOT NULL,
  parent bigint,
  CONSTRAINT reference_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

DROP SEQUENCE IF EXISTS "sq_reference";

CREATE SEQUENCE "sq_reference"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;



ALTER table "specification" ADD CONSTRAINT fk_specification_reference_id FOREIGN KEY (reference_id) REFERENCES reference (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
