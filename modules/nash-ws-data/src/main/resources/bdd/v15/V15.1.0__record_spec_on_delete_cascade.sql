-- ----------------------------------------------
-- Constraints on delete cascade for records
-- ----------------------------------------------
--record_file
ALTER TABLE record_file DROP CONSTRAINT fk_record_file_record_id;
ALTER TABLE record_file ADD CONSTRAINT fk_record_file_record_id FOREIGN KEY (record_id) REFERENCES "record" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

--DELETE record_entity_role
DELETE FROM record_entity_role WHERE uid not in (SELECT uid FROM record);

--record
ALTER TABLE record ADD UNIQUE (uid);

--record_entity_role
ALTER TABLE record_entity_role ADD CONSTRAINT fk_record_entity_role_uid FOREIGN KEY (uid) REFERENCES record (uid) ON DELETE CASCADE;

-- ----------------------------------------------
-- Constraints on delete cascade for specifications
-- ----------------------------------------------
--specification_file
ALTER TABLE specification_file ADD COLUMN specification_id bigint;
DELETE FROM specification_file WHERE id NOT IN (SELECT source_id FROM specification);
UPDATE specification_file SET specification_id = s.id FROM specification s WHERE specification_file.id = s.source_id;
ALTER TABLE specification_file ALTER COLUMN specification_id SET NOT NULL;
ALTER TABLE specification_file ADD CONSTRAINT fk_specification_file_specification_id FOREIGN KEY (specification_id) REFERENCES "specification" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;

--specification
ALTER TABLE specification DROP CONSTRAINT fk_specification_source_id;
ALTER TABLE specification DROP COLUMN source_id;

--specification_entity_role
DELETE FROM specification_entity_role WHERE uid not in (SELECT uid FROM specification);
ALTER TABLE specification ADD UNIQUE (uid);
ALTER TABLE specification_entity_role ADD CONSTRAINT fk_specification_entity_role_uid FOREIGN KEY (uid) REFERENCES "specification" (uid) ON DELETE CASCADE;
