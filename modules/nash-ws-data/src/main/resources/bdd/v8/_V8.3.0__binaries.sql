-- ----------------------------------------------------------------------------
-- OID columns conversion for table 'record'
-- ----------------------------------------------------------------------------
ALTER TABLE record ALTER COLUMN properties SET DATA TYPE BYTEA USING lo_get(properties);

-- ----------------------------------------------------------------------------
-- OID columns conversion for table 'record_file'
-- ----------------------------------------------------------------------------
ALTER TABLE record_file ALTER COLUMN content SET DATA TYPE BYTEA USING lo_get(content);

-- ----------------------------------------------------------------------------
-- OID columns conversion for table 'record_file'
-- ----------------------------------------------------------------------------
ALTER TABLE specification_file ALTER COLUMN content SET DATA TYPE BYTEA USING lo_get(content);

