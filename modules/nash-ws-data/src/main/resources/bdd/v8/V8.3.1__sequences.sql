CREATE SEQUENCE sq_meta INCREMENT BY 1;

SELECT setval('sq_meta', (SELECT MAX(id) FROM meta));
SELECT setval('sq_reference', (SELECT MAX(id) FROM reference));
SELECT setval('sq_record', (SELECT MAX(id) FROM record));
SELECT setval('sq_record_file', (SELECT MAX(id) FROM record_file));
SELECT setval('sq_specification', (SELECT MAX(id) FROM specification));
SELECT setval('sq_specification_file', (SELECT MAX(id) FROM specification_file));
