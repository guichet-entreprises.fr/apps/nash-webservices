-- ----------------------------------------------
-- Defaulting records roles if necessary
-- ----------------------------------------------

WITH records AS (
    SELECT r.uid, r.author
    FROM record r
    WHERE r.author is NULL AND NOT EXISTS (SELECT 1 FROM record_entity_role rer WHERE rer.uid = r.uid )
)
INSERT INTO record_entity_role (id, uid, entity, role)
SELECT nextval('sq_record_entity_role'), uid, 'GE', 'support' FROM records;