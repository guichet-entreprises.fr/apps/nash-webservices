-- ----------------------------------------------
-- Defaulting records roles if necessary
-- ----------------------------------------------
ALTER TABLE "specification" ADD COLUMN icon varchar(255);
ALTER TABLE "specification" ADD COLUMN color varchar(255);