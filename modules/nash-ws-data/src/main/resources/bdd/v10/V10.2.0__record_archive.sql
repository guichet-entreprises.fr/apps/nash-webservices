-- ----------------------------------------------
-- table "record_archive"
-- ----------------------------------------------

-- DROP TABLE "record_archive";

CREATE TABLE "record_archive" (
    id bigint NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL,
    author char(18) COLLATE pg_catalog."default",
    spec char(18) COLLATE pg_catalog."default" NOT NULL,
    uid char(18) COLLATE pg_catalog."default" NOT NULL,
    content oid NOT NULL,
    archived timestamp without time zone default CURRENT_TIMESTAMP,
    CONSTRAINT record_archive_pkey PRIMARY KEY (id)
)
    WITH (OIDS = FALSE)
    TABLESPACE pg_default;


-- DROP SEQUENCE IF EXISTS "sq_record_archive";

CREATE SEQUENCE "sq_record_archive"
    INCREMENT 50
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
