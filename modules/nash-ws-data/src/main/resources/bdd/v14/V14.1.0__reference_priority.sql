-- ----------------------------------------------
-- Add column priority with default value
-- ----------------------------------------------
alter table reference add column priority bigint default 0;
