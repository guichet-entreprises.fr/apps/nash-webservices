# Create Record with context
Upload record with context,this rest api is used by proxies.
# Description
This REST API is used by proxies for the creation of records via a backend without going through the interface of the website.

**URL** : /api/managed/v2/record/ref/{reference}

**Method** : `POST`

**Auth required** : YES

**Permissions required** : None

**Consumes** : multipart/form-data

**Produces** : application/json

**Parameters**:

***Reference***
```json
    name: "reference",
    in: "path",
    description: "the reference of specification Example:reference='Formalités SCN/ENT/Création/Déclaration de début d'activité libérale/Micro-entrepreneur'",
    required: true,
    type: "string",
    pattern: ".*"
```
***Author***
```json
    name: "author",
    in: "query",
    description: "the author of record Example: author='2018-01-DYE-XCH-26'",
    required: false,
    type: "string"
```

***lang***
```json
    name: "lang",
    in: "query",
    description: "the language of record by default lang ='FR'",
    required: false,
    type: "string"
```
***context***
```json
    in: "body",
    name: "context",
    description: "the context is json file content all information about record,the structure of json is provided by Guichet-entreprises",
    required: false
```
