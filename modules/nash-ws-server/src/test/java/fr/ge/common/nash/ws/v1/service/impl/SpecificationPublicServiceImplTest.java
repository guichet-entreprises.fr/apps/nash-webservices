/**
 *
 */
package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.reset;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.data.bean.GroupSpecificationBean;
import fr.ge.common.nash.ws.data.service.IMetaDataService;
import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.v1.bean.ListOfSpecificationGroupe;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * @author bsadil
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class SpecificationPublicServiceImplTest extends AbstractRestTest {

    @Autowired
    private ISpecificationDataService specificationDataService;

    @Autowired
    private IReferenceDataService referenceDataService;

    @Autowired
    private IMetaDataService metaDataService;

    private static String ROOT_DIRECTORY = "target/ge/records";

    private static String ERROR_DIRECTORY = "target/errors";

    private File rootdirectory = null;

    private File errorDirectory = null;

    @Value("${nash.directory.watcher:exclude}")
    private String useWatcher;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.specificationDataService, this.referenceDataService, this.metaDataService);

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();

        this.errorDirectory = new File(ERROR_DIRECTORY);
        this.errorDirectory.mkdirs();
    }

    @Test
    public void testFindAllGroup() throws Exception {

        final GroupSpecificationBean group = new GroupSpecificationBean("all", 4L);
        final GroupSpecificationBean group1 = new GroupSpecificationBean("referent", 3L);

        final List<GroupSpecificationBean> groups = new ArrayList<>();
        groups.add(group);
        groups.add(group1);

        Mockito.when(this.specificationDataService.findAllGroupe()).thenReturn(groups);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/public/v1/Specification/group").get();

        assertThat(response, hasProperty("status", equalTo(Status.OK.getStatusCode())));

        final ListOfSpecificationGroupe actual = this.readAsBean(response, ListOfSpecificationGroupe.class);

        assertThat(actual.getGroups(), hasSize(2));
    }

}
