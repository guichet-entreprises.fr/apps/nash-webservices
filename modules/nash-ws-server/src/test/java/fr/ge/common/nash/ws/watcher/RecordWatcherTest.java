/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.watcher;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.ws.v1.service.internal.RecordInternalService;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml",
        "classpath:spring/record-watcher-context.xml" })
public class RecordWatcherTest extends AbstractTest {

    @Value("${nash.support.directory.input}")
    private String ROOT_DIRECTORY;

    private File rootdirectory = null;

    @Autowired
    private RecordWatcher watcher;

    @Autowired
    private Engine engine;

    private RecordInternalService service;

    @Before
    public void setUp() throws IOException {
        this.service = mock(RecordInternalService.class);
        this.rootdirectory = new File(this.ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();
        this.watcher.setRecordService(this.service);

        this.watcher = spy(this.watcher);

        reset(this.engine);
        when(this.engine.loader(any())).thenCallRealMethod();
    }

    @Test
    public void testUploadRecord() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("form.zip");
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testUploadRecord" + ".zip");
        FileUtils.writeByteArrayToFile(file, resourceAsBytes);

        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testUploadRecord" + ".sha1"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testUploadRecord" + ".md5"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "MD5")).getBytes());

        when(this.service.upload(any(byte[].class), any(), any())).thenReturn(Response.created(URI.create("testUploadRecord")).build());

        this.watcher.processZipFile(Paths.get(this.ROOT_DIRECTORY + File.separator + "testUploadRecord" + ".sha1"));

        verify(this.service).upload(any(byte[].class), any(), any());

        file.delete();
    }

    @Test
    public void testNoUploadRecord() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("form.zip");
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testNoUploadRecord" + ".zip");
        FileUtils.writeByteArrayToFile(file, resourceAsBytes);
        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testNoUploadRecord" + ".sha1"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testNoUploadRecord" + ".md5"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "MD5")).getBytes());
        file.delete();

        this.watcher.processZipFile(Paths.get(this.ROOT_DIRECTORY + File.separator + "testNoUploadRecord" + ".sha1"));

        verify(this.service, never()).upload(any(byte[].class), any(String.class), any());

    }

    @Test
    // @Ignore("the checksums are not equal !")
    public void testCreateChecksumEmptyFile() throws Exception {

        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testCreateChecksumEmptyFile" + ".zip");
        file.createNewFile();

        final byte[] readAllBytes = Files.readAllBytes(file.toPath());

        final MessageDigest md = MessageDigest.getInstance("SHA1");
        md.update(readAllBytes);

        final String expected = new HexBinaryAdapter().marshal(md.digest());
        final String actual = new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1"));

        file.delete();
        assertEquals(expected, actual);
    }

    @Test
    // @Ignore("the checksums are not equal !")
    public void testCreateChecksum() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("form.zip");
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testCreateChecksum" + ".zip");
        FileUtils.writeByteArrayToFile(file, resourceAsBytes);

        final MessageDigest md = MessageDigest.getInstance("SHA1");
        md.update(resourceAsBytes);

        final String expected = new HexBinaryAdapter().marshal(md.digest());
        final String actual = new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1"));

        assertEquals(expected, actual);
    }

    @Test(expected = TechnicalException.class)
    public void testCreateChecksum_throwsNoSuchAlgorithmException() throws Exception {
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "throwsNoSuchAlgorithmException" + ".zip");
        file.createNewFile();
        this.watcher.createChecksum(file, "");
    }

    @Test(expected = TechnicalException.class)
    public void testCreateChecksum_throwsFileNotFoundException() throws Exception {
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testCreateChecksum_throwsFileNotFoundException" + ".zip");
        file.createNewFile();
        file.delete();
        this.watcher.createChecksum(file, "SHA1");
    }

    @Test
    public void testSendRecordToNash() throws Exception {

        final File folder = new File(this.ROOT_DIRECTORY);

        final byte[] resourceAsBytes = this.resourceAsBytes("form.zip");
        final File file = new File(this.ROOT_DIRECTORY + File.separator + "testSendRecordToNash" + ".zip");
        FileUtils.writeByteArrayToFile(file, resourceAsBytes);

        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testSendRecordToNash" + ".sha1"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "SHA1")).getBytes());
        FileUtils.writeByteArrayToFile(new File(this.ROOT_DIRECTORY + File.separator + "testSendRecordToNash" + ".md5"),
                new HexBinaryAdapter().marshal(this.watcher.createChecksum(file, "MD5")).getBytes());

        this.watcher.sendRecordToNash(folder);
        verify(this.watcher).processZipFile(new File(this.ROOT_DIRECTORY + File.separator + "testSendRecordToNash" + ".sha1").toPath());

        file.delete();
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.deleteDirectory(this.rootdirectory);
    }
}
