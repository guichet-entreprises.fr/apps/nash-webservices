/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.GroupElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.ValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.value.ListValueElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.util.QueryStringUtil;
import fr.ge.common.nash.ws.v1.bean.ProxyResultBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * Class RecordServiceImplTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class RecordServiceImplTest extends AbstractRestTest {

    /** La constante UID_AUTHOR. */
    private static final String UID_AUTHOR = "2016-11-USR-AAA-42";

    /** La constante UID_RECORD. */
    private static final String UID_RECORD = "2016-11-ESG-YTW-37";

    /** data service. */
    @Autowired
    private IRecordDataService dataService;

    /** spec data service. */
    @Autowired
    private ISpecificationDataService specDataService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /** The engine. */
    @Autowired
    private Engine engine;

    private SpecificationLoader loader;

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.dataService, this.specDataService);

        this.loader = mock(SpecificationLoader.class);
        final FormSpecificationDescription fsd = mock(FormSpecificationDescription.class);
        when(fsd.getAuthor()).thenReturn(UID_AUTHOR);
        when(this.loader.description()).thenReturn(fsd);
        when(this.engine.loader(any(IProvider.class))).thenCallRealMethod();
    }

    /**
     * Test info.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInfo() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);

        final Response response = this.client().accept("application/json").path("/v1/Record/code/{code}", dataBean.getUid()).get();
        final RecordInfoBean actual = this.readAsBean(response, RecordInfoBean.class);

        verify(this.dataService).load(eq(dataBean.getUid()), eq(RecordBean.class));

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(actual, allOf( //
                Arrays.asList( //
                        hasProperty("author", equalTo(dataBean.getAuthor())), //
                        hasProperty("code", equalTo(dataBean.getUid())), //
                        hasProperty("title", equalTo(dataBean.getTitle())), //
                        hasProperty("origin", equalTo(dataBean.getSpec())) //
                ) //
        ) //
        );

    }

    /**
     * Test info unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInfoUnknown() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(null);

        final Response response = this.client().accept("application/json").path("/v1/Record/code/{code}", dataBean.getUid()).get();

        verify(this.dataService).load(eq(dataBean.getUid()), eq(RecordBean.class));

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));

    }

    /**
     * Tests {@link RecordServiceImpl#upload(byte[], String, List)}.
     *
     * @throws FunctionalException
     *             a {@link FunctionalException}
     */
    @Test
    public void testUploadRecord() throws FunctionalException {
        // prepare
        final byte[] recordAsBytes = "record zip content".getBytes(StandardCharsets.UTF_8);
        final InputStream recordAsStream = new ByteArrayInputStream(recordAsBytes);

        // call
        final Response response = this.client().accept("application/json").path("/v1/Record").query("author", "2017-05-JON-ATH-AN") //
                .type(MediaType.APPLICATION_OCTET_STREAM).post(recordAsStream);

        // verify
        verify(this.dataService).fromFile(any(), any(), eq(recordAsBytes), eq("2017-05-JON-ATH-AN"), any(), eq(RecordInfoBean.class));
        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));
    }

    /**
     * Tests {@link RecordServiceImpl#upload(byte[], String, List, Attachment)}.
     *
     * @throws FunctionalException
     *             a {@link FunctionalException}
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testUploadRecordContext() throws FunctionalException {
        // prepare
        final byte[] recordAsBytes = this.resourceAsBytes("simple.zip");
        final InputStream recordAsStream = new ByteArrayInputStream(recordAsBytes);
        final Attachment attachment1 = new Attachment("record", MediaType.APPLICATION_OCTET_STREAM, recordAsStream);

        final byte[] jsonAsBytes = "{\"data01\":{\"grp01\":{\"fld01x02\":\"42\",\"fld01x03\":\"test\"}}}".getBytes(StandardCharsets.UTF_8);
        final InputStream jsonAsStream = new ByteArrayInputStream(jsonAsBytes);
        final Attachment attachment2 = new Attachment("context", MediaType.APPLICATION_JSON, jsonAsStream);

        // call
        final Response response = this.client().accept("application/json").path("/v1/Record").query("author", "2017-05-JON-ATH-AN") //
                .type(MediaType.MULTIPART_FORM_DATA).post(new MultipartBody(Arrays.asList(attachment1, attachment2)));

        // verify
        final ArgumentCaptor<byte[]> savedRecordAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).fromFile(any(), any(), savedRecordAsBytes.capture(), eq("2017-05-JON-ATH-AN"), any(), eq(RecordInfoBean.class));
        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));
        final SpecificationLoader loader = this.engine.loader(new ZipProvider(savedRecordAsBytes.getValue()));
        final FormSpecificationData formSpecificationData = loader.data(0);
        assertThat(formSpecificationData, //
                hasProperty("groups", //
                        hasItems( //
                                allOf( //
                                        hasProperty("id", equalTo("grp01")), //
                                        hasProperty("data", //
                                                hasItems( //
                                                        hasProperty("id", equalTo("fld01x01")), //
                                                        allOf( //
                                                                hasProperty("id", equalTo("fld01x02")), //
                                                                hasProperty("value", hasProperty("content", equalTo("42"))) //
                                                        ), //
                                                        allOf( //
                                                                hasProperty("id", equalTo("fld01x03")), //
                                                                hasProperty("value", hasProperty("content", equalTo("test"))) //
                                                        ) //
                                                ) //
                                        ) //
                                ) //
                        ) //
                ) //
        );
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testUploadRecordContextWithAttachment() throws FunctionalException, IOException {
        // prepare
        final String reference = "QP039/REF";
        final SpecificationBean specBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.specDataService.loadByReference(reference)).thenReturn(specBean);
        when(this.specDataService.resource(specBean.getUid())).thenReturn(new FileEntry(specBean.getUid() + ".zip").asBytes(this.resourceAsBytes("specWithAttachment.zip")));

        final Attachment jsonAsAttachment = new Attachment("context", MediaType.APPLICATION_JSON, this.resourceAsStream("withAttachment.json"));

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN).path("/v1/Record/ref/{reference}", reference) //
                .query("author", "2017-05-JON-ATH-AN") //
                .type(MediaType.MULTIPART_FORM_DATA).post(new MultipartBody(Arrays.asList(jsonAsAttachment)));

        // verify
        final ArgumentCaptor<byte[]> savedRecordAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).fromFile(any(), any(), savedRecordAsBytes.capture(), eq("2017-05-JON-ATH-AN"), any(), eq(RecordInfoBean.class));
        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));
        final SpecificationLoader loader = this.engine.loader(new ZipProvider(savedRecordAsBytes.getValue()));
        final String dataFileValue = ((ValueElement) loader.data(0).getGroups().get(0).getData().get(0).getValue()).getContent().toString();

        assertEquals("{ \"id\": null, \"value\": [ { \"id\": null, \"ref\": \"1\", \"label\": \"test.jpg\" } ] }", dataFileValue);

    }

    /**
     * Test upload on unknown record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadOnUnknownRecord() throws Exception {
        final String dstResourcePath = "1-data/data.xml";
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("updated/1-data/data.xml");

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordInfoBean.class))).thenReturn(null);
        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", dataBean.getUid(), dstResourcePath).put(updatedResourceAsBytes);

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
    }

    @Test
    public void testTransform() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("forms.zip");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        // when(this.dataService.transform("Dossier FORMS", "Dossier de
        // création", updatedResourceAsBytes,
        // "2017-05-JON-ATH-AN")).thenReturn(dataBean);
        when(this.dataService.fromFile(anyString(), anyString(), any(byte[].class), eq(UID_AUTHOR), any(List.class), eq(RecordBean.class))).thenReturn(buildRecordBean());

        final byte[] decriptionBoAsBytes = this.resourceAsBytes("simple/description.xml");
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(decriptionBoAsBytes));
        // call
        final Response response = this.client().accept("application/json").path("/v1/Record/transform") //
                .query("author", UID_AUTHOR) //
                .query("formsRecordName", "Dossier FORMS") //
                .query("formsRecordType", "Dossier de création") //
                .query("roles", Arrays.asList(new SearchQueryFilter("GE/CA/2019-01-ABC-DEF-01:referent"), new SearchQueryFilter("GE/CA/2019-01-ABC-DEF-01:user"))) //
                .type(MediaType.APPLICATION_OCTET_STREAM).post(updatedResourceAsBytes);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Test upload updated resource.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadUpdatedResource() throws Exception {
        final String dstResourcePath = "1-data/data.xml";
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("updated/1-data/data.xml");

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.resourceExists(dataBean.getUid(), dstResourcePath)).thenReturn(true);
        when(this.dataService.addOrUpdate(dataBean.getUid(), dstResourcePath, "text/xml", updatedResourceAsBytes)).thenReturn(this.buildFileBean(dstResourcePath, "text/xml"));

        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", dataBean.getUid(), dstResourcePath).put(updatedResourceAsBytes);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(response.getHeaderString(HttpHeaders.LOCATION), equalTo(ENDPOINT + "/v1/Record/code/" + dataBean.getUid() + "/" + dstResourcePath));

        verify(this.dataService).addOrUpdate(dataBean.getUid(), "1-data/data.xml", "text/xml", updatedResourceAsBytes);
    }

    /**
     * Test upload new resource.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadNewResource() throws Exception {
        final String dstResourcePath = "2-data/data 02.xml";
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("updated/1-data/data.xml");

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.addOrUpdate(dataBean.getUid(), dstResourcePath, "text/xml", updatedResourceAsBytes)).thenReturn(this.buildFileBean(dstResourcePath, "text/xml"));

        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", dataBean.getUid(), dstResourcePath).put(updatedResourceAsBytes);

        verify(this.dataService).addOrUpdate(dataBean.getUid(), dstResourcePath, "text/xml", updatedResourceAsBytes);

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
        assertThat(response.getHeaderString(HttpHeaders.LOCATION), equalTo(ENDPOINT + "/v1/Record/code/" + dataBean.getUid() + "/" + QueryStringUtil.encodeResourcePath(dstResourcePath)));
    }

    /**
     * Build file bean.
     *
     * @param resourcePath
     *            resource path
     * @param resourceType
     *            resource type
     * @return file bean
     */
    private FileBean buildFileBean(final String resourcePath, final String resourceType) {
        final DateTime now = DateTime.now();
        final FileBean bean = new FileBean();

        bean.setName(resourcePath);
        bean.setType(resourceType);
        bean.setCreated(now.toCalendar(Locale.getDefault()));
        bean.setUpdated(now.toCalendar(Locale.getDefault()));

        return bean;
    }

    /**
     * Test upload unknown type.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUploadUnknownType() throws Exception {
        final String dstResourcePath = "readme.xyz";
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final byte[] updatedResourceAsBytes = "Strange file type content".getBytes(StandardCharsets.UTF_8);

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.addOrUpdate(dataBean.getUid(), dstResourcePath, "application/octet-stream", updatedResourceAsBytes)).thenReturn(this.buildFileBean(dstResourcePath, "text/xml"));

        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", dataBean.getUid(), dstResourcePath).put(updatedResourceAsBytes);

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
        assertThat(response.getHeaderString(HttpHeaders.LOCATION), equalTo(ENDPOINT + "/v1/Record/code/" + dataBean.getUid() + "/" + dstResourcePath));

        verify(this.dataService).addOrUpdate(dataBean.getUid(), dstResourcePath, "application/octet-stream", updatedResourceAsBytes);
    }

    /**
     * Test zip upload.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipUpload() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.dataService.resource(any(), eq(Engine.FILENAME_DESCRIPTION)))
                .thenReturn(new FileEntry(Engine.FILENAME_DESCRIPTION, "text/xml").asBytes(this.resourceAsBytes("simple/description.xml")));
        when(this.dataService.fromFile(eq(null), eq(null), eq(resourceAsBytes), eq(UID_AUTHOR), any(), eq(RecordInfoBean.class))).thenReturn(buildRecordInfoBean());

        final Response response = this.client().accept("application/json").path("/v1/Record") //
                .header("Content-Type", "application/zip") //
                .query("author", UID_AUTHOR) //
                .post(resourceAsBytes);

        verify(this.dataService).fromFile(eq(null), eq(null), eq(resourceAsBytes), eq(UID_AUTHOR), any(), eq(RecordInfoBean.class));

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
        assertThat(response.getHeaderString("Location"), equalTo(ENDPOINT + "/v1/Record/code/" + UID_RECORD));
    }

    // @Test
    // public void testUploadError() throws Exception {
    // final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");
    //
    // when(this.dataService.fromFile("specification.zip", "application/zip",
    // resourceAsBytes)).thenThrow(new FunctionalException("Oops"));
    //
    // final Response response =
    // this.client().accept("application/json").path("/v1/Specification").header("Content-Type",
    // "application/zip").post(resourceAsBytes);
    //
    // verify(this.dataService).fromFile("specification.zip", "application/zip",
    // resourceAsBytes);
    //
    // assertThat(response, hasProperty("status", equalTo(500)));
    // assertThat(this.readAsString(response), equalTo("Unable to create
    // resource from uploaded content"));
    // }
    //
    // @Test
    // public void testUploadEmpty() throws Exception {
    // final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");
    //
    // when(this.dataService.fromFile("specification.zip", "application/zip",
    // resourceAsBytes)).thenReturn(null);
    //
    // final Response response =
    // this.client().accept("application/json").path("/v1/Specification").header("Content-Type",
    // "application/zip").post(resourceAsBytes);
    //
    // verify(this.dataService).fromFile("specification.zip", "application/zip",
    // resourceAsBytes);
    //
    // assertThat(response, hasProperty("status", equalTo(204)));
    // assertThat(response, hasProperty("entity", nullValue()));
    // }

    /**
     * Test zip download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownload() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final SpecificationBean specBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specDataService.load(dataBean.getSpec())).thenReturn(specBean);
        when(this.specDataService.resource(specBean.getUid())).thenReturn(new FileEntry(specBean.getUid() + ".zip").asBytes(this.resourceAsBytes("simple.zip")));

        final Collection<FileBean> recordResourceList = buildRecordResourceList();
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.dataService.resource(dataBean.getUid(), fileBean.getName())).thenReturn( //
                    new FileEntry(fileBean.getName(), fileBean.getType())//
                            .setContent(this.resourceAsStream("simple/" + fileBean.getName())) //
            );
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file", dataBean.getUid()) //
                .header("X-Roles", "{\"A/B/2018-12-ENTITY\":[\"ROL1\",\"ROL1\"]}") //
                .get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        final byte[] asBytes = this.readAsBytes(response);
        assertThat(ZipUtil.entries(asBytes), containsInAnyOrder("description.xml", "1-data/data.xml", //
                "css/common.css", "css/data.css", "css/types.css", "css/referential.css", "css/history.css", //
                "css/common.xslt", "css/description.xslt", "css/process.xslt" //
        ));

        final byte[] emptyBytes = new byte[] {};
        for (final FileBean fileBean : recordResourceList) {
            if (fileBean.getName().startsWith("__cache__/")) {
                assertThat("Resource " + fileBean.getName(), ZipUtil.entryAsBytes(fileBean.getName(), asBytes), equalTo(emptyBytes));
            } else {
                assertThat("Resource " + fileBean.getName(), ZipUtil.entryAsBytes(fileBean.getName(), asBytes), equalTo(this.resourceAsBytes("simple/" + fileBean.getName())));
            }
        }
    }

    /**
     * Test zip download no spec.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadNoSpec() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final Collection<FileBean> recordResourceList = buildRecordResourceList();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.dataService.resource(dataBean.getUid(), fileBean.getName())).thenReturn( //
                    new FileEntry(fileBean.getName(), fileBean.getType())//
                            .setContent(this.resourceAsStream("simple/" + fileBean.getName())) //
            );
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        final byte[] asBytes = this.readAsBytes(response);
        assertThat(ZipUtil.entries(asBytes), containsInAnyOrder("description.xml", "1-data/data.xml", //
                "css/common.css", "css/data.css", "css/types.css", "css/referential.css", "css/history.css", //
                "css/common.xslt", "css/description.xslt", "css/process.xslt" //
        ));

        final byte[] emptyBytes = new byte[] {};
        for (final FileBean fileBean : recordResourceList) {
            if (fileBean.getName().startsWith("__cache__/")) {
                assertThat("Resource " + fileBean.getName(), ZipUtil.entryAsBytes(fileBean.getName(), asBytes), equalTo(emptyBytes));
            } else {
                assertThat("Resource " + fileBean.getName(), ZipUtil.entryAsBytes(fileBean.getName(), asBytes), equalTo(this.resourceAsBytes("simple/" + fileBean.getName())));
            }
        }
    }

    /**
     * Test zip download unknown record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadUnknownRecord() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download no file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadNoFile() throws Exception {
        final Collection<FileBean> recordResourceList = buildRecordResourceList();
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.dataService.resource(dataBean.getUid(), fileBean.getName())).thenReturn(null);
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())));
        assertThat(this.readAsString(response), equalTo("No content for file 'description.xml'"));
    }

    /**
     * Test zip download no source.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadNoSource() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        final byte[] asBytes = this.readAsBytes(response);
        assertThat(ZipUtil.entries(asBytes), containsInAnyOrder( //
                "css/common.css", "css/data.css", "css/types.css", "css/referential.css", "css/history.css", //
                "css/common.xslt", "css/description.xslt", "css/process.xslt" //
        ));
    }

    /**
     * Test download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownload() throws Exception {
        final Collection<FileBean> recordResourceList = buildRecordResourceList();
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.dataService.resource(dataBean.getUid(), fileBean.getName())).thenReturn( //
                    new FileEntry(fileBean.getName().replaceFirst("^.*/", ""), fileBean.getType()) //
                            .lastModified(fileBean.getUpdated()) //
                            .asBytes(this.resourceAsBytes("simple/" + fileBean.getName())));
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "description.xml").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(this.resourceAsBytes("simple/description.xml")));
    }

    /**
     * Test download sub file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadSubFile() throws Exception {
        final Collection<FileBean> recordResourceList = buildRecordResourceList();
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.dataService.resource(dataBean.getUid(), fileBean.getName())).thenReturn( //
                    new FileEntry(fileBean.getName().replaceFirst("^.*/", ""), fileBean.getType()) //
                            .lastModified(fileBean.getUpdated()) //
                            .asBytes(this.resourceAsBytes("simple/" + fileBean.getName())));
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "1-data/data.xml").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(this.resourceAsBytes("simple/1-data/data.xml")));
    }

    /**
     * Test download unknown record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadUnknownRecord() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "description.xml").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download unknown sub file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadUnknownSubFile() throws Exception {
        final Collection<FileBean> recordResourceList = buildRecordResourceList();
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        when(this.dataService.resources(dataBean.getUid())).thenReturn(recordResourceList);
        for (final FileBean fileBean : recordResourceList) {
            when(this.specDataService.resource(dataBean.getSpec(), fileBean.getName())).thenReturn( //
                    new FileEntry(fileBean.getName(), fileBean.getType())//
                            .setContent(this.resourceAsStream("simple/" + fileBean.getName())) //
            );
        }

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "meta.xml").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download sub file no binary.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadSubFileNoBinary() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "description.xml").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download default file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadDefaultFile() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();

        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);

        final Response response = this.client().accept("application/zip").path("/v1/Record/code/{code}/file/{path}", dataBean.getUid(), "css/data.css").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), notNullValue());
    }

    /**
     * Test search.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearch() throws Exception {
        final RecordInfoBean dataBean = buildRecordInfoBean();
        final SearchResult<RecordInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()) //
                .addOrder("name", "desc");
        when(this.dataService.search(any(long.class), any(long.class), any(), any(), any(), eq(null), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept("application/json").path("/v1/Record") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .query("orders", "name:desc") //
                .get();

        final SearchResult<RecordInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<RecordInfoBean>>() {
        });

        verify(this.dataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), eq(searchQuery.getOrders()), any(), eq(null), eq(RecordInfoBean.class));

        assertThat(actualSearchResult, allOf( //
                hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
        ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo("QP039")), //
                                        hasProperty("origin", equalTo(dataBean.getOrigin())) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test search no orders.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchNoOrders() throws Exception {
        final RecordInfoBean dataBean = buildRecordInfoBean();
        final SearchResult<RecordInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults());
        when(this.dataService.search(any(long.class), any(long.class), any(), any(), any(), eq(null), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept("application/json").path("/v1/Record") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .get();

        final SearchResult<RecordInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<RecordInfoBean>>() {
        });

        verify(this.dataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), any(), any(), eq(null), eq(RecordInfoBean.class));

        assertThat(actualSearchResult, allOf( //
                hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
        ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo("QP039")), //
                                        hasProperty("origin", equalTo(dataBean.getOrigin())) //
                                ) //
                        ) //
                ) //
        );

    }

    /**
     * Test search order field conversion.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchOrderFieldConversion() throws Exception {
        final RecordInfoBean dataBean = buildRecordInfoBean();
        final SearchResult<RecordInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()) //
                .addOrder("origin", "desc").addOrder("code", "asc");
        when(this.dataService.search(any(long.class), any(long.class), any(), any(), any(), eq(null), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept("application/json").path("/v1/Record") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .query("orders", "origin:desc") //
                .query("orders", "code:asc") //
                .get();

        final SearchResult<RecordInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<RecordInfoBean>>() {
        });

        verify(this.dataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), eq(searchQuery.getOrders()), any(), eq(null), eq(RecordInfoBean.class));

        assertThat(actualSearchResult, allOf( //
                hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
        ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo("QP039")), //
                                        hasProperty("origin", equalTo(dataBean.getOrigin())) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveUid() throws Exception {

        when(this.dataService.remove(UID_RECORD)).thenReturn(1L);

        final Response response = this.client().path("/v1/Record/code/{code}", UID_RECORD).delete();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        verify(this.dataService).remove(UID_RECORD);
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemove() throws Exception {
        final String resourcePath = "toDelete.txt";

        when(this.dataService.remove(UID_RECORD, resourcePath)).thenReturn(1L);

        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", UID_RECORD, resourcePath).delete();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        verify(this.dataService).remove(UID_RECORD, resourcePath);
    }

    /**
     * Test remove unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveUnknown() throws Exception {
        final String resourcePath = "toDelete.txt";

        when(this.dataService.remove(UID_RECORD, resourcePath)).thenReturn(0L);

        final Response response = this.client().path("/v1/Record/code/{code}/{resourcePath}", UID_RECORD, resourcePath).delete();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NOT_MODIFIED.getStatusCode())));

        verify(this.dataService).remove(UID_RECORD, resourcePath);
    }

    /**
     * Build record bean.
     *
     * @return record bean
     */
    public static RecordBean buildRecordBean() {
        final Calendar now = Calendar.getInstance();

        return new RecordBean() //
                .setAuthor("the author id") //
                .setUid(UID_RECORD) //
                .setSpec("2016-10-XRT-BYD-35") //
                .setTitle("QP039") //
                .setCreated(now) //
                .setUpdated(now) //
        ;
    }

    public static Collection<FileBean> buildRecordResourceList() {
        final Calendar now = Calendar.getInstance();

        return Arrays.asList( //
                new FileBean() //
                        .setId(42L) //
                        .setName("description.xml") //
                        .setType("text/xml") //
                        .setCreated(now) //
                        .setUpdated(now), //
                new FileBean() //
                        .setId(51L) //
                        .setName("1-data/data.xml") //
                        .setType("text/xml") //
                        .setCreated(now) //
                        .setUpdated(now), //
                new FileBean() //
                        .setId(69L) //
                        .setName("__cache__/tpl-0-0.html") //
                        .setType("text/html") //
                        .setCreated(now) //
                        .setUpdated(now) //
        );
    }

    /**
     * Build record info bean.
     *
     * @return record info bean
     */
    public static RecordInfoBean buildRecordInfoBean() {
        final RecordBean bean = buildRecordBean();

        return new RecordInfoBean() //
                .setCode(bean.getUid()) //
                .setOrigin(bean.getSpec()) //
                .setTitle(bean.getTitle()) //
                .setCreated(bean.getCreated()) //
                .setUpdated(bean.getUpdated()) //
                .setAuthor(bean.getAuthor());
    }

    /**
     * Execute both processed and archived steps in separate executions.
     *
     * @throws FunctionalException
     */
    @Test
    public void testExecuteActionProcessedArchived() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("processed/description.xml");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        Response response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "exec:processed").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "exec:archived").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Execute both processed and archived steps in one single execution.
     *
     * @throws FunctionalException
     */
    @Test
    public void testExecuteActionArchived() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("processed/description.xml");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));
        final Response response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "exec:archived").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Execute only bo steps with failure.
     *
     * @throws FunctionalException
     */
    @Test
    public void testExecuteBoSteps() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("execute-step/description.xml");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));
        final Response response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "exec:archived").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Execute no steps.
     *
     * @throws FunctionalException
     */
    @Test
    public void testExecuteNoStep() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("done-steps/description.xml");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));
        final Response response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "exec:archived").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Get summary URL redirection.
     *
     * @throws FunctionalException
     */
    @Test
    public void testGetSummaryUrlRedirection() throws FunctionalException {
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("done-steps/description.xml");
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));
        final Response response = this.client().path("/v1/Record/code/{code}/action/{action}/execute", UID_RECORD, "view").query("engineUser", "bo").post(null);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // -->Try to execute an unknown action
        // response =
        // this.client().path("/v1/Record/code/{code}/action/{action}/execute",
        // UID_RECORD, "checkout").query("engineUser", "bo").post(null);
        // assertThat(response, hasProperty("status",
        // equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())));
    }

    @Test
    public void testGetProxyResult() {

        final Map<String, byte[]> documents = new HashMap<>();
        documents.put("test", new byte[50]);
        documents.put("test1", new byte[50]);
        final ProxyResultBean proxy = new ProxyResultBean("ok", "test ok", documents);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("proxy/description.xml");
        when(this.dataService.resource(UID_RECORD, "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final byte[] resourceAsBytes = this.resourceAsBytes("proxy/5-proxy/preprocess.xml");
        when(this.dataService.resource(UID_RECORD, "5-proxy/preprocess.xml")).thenReturn(new FileEntry("5-proxy/preprocess.xml").asBytes(resourceAsBytes));

        final Response response = this.client()
                .path("/v1/Record/code/{recordUid}/step/{stepId}/phase/{processPhase}/process/{processId}/{resultFileName}", UID_RECORD, "proxy", "pre", "proxyOut", "data-generated.xml")
                .type(MediaType.APPLICATION_JSON).post(proxy);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);

        verify(this.dataService).addOrUpdate(eq(UID_RECORD), eq("5-proxy/data-generated.xml"), any(), captor.capture());

        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationData.class);
        final GroupElement root = actual.getGroups().get(0);

        assertThat(root.getData().get(0), //
                allOf( //
                        hasProperty("id", equalTo("status")), //
                        hasProperty("value", hasProperty("content", equalTo("ok"))) //
                ) //
        );

        assertThat(root.getData().get(1), //
                allOf( //
                        hasProperty("id", equalTo("message")), //
                        hasProperty("value", hasProperty("content", equalTo("test ok"))) //
                ) //
        );

        assertThat(root.getData().get(2), hasProperty("id", equalTo("files")));
        assertThat(((ListValueElement) ((ValueElement) root.getData().get(2).getValue()).getContent()).getElements(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("label", equalTo("test")), //
                                hasProperty("label", equalTo("test1")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testGetProxyResultError() {

        final Map<String, byte[]> documents = new HashMap<>();
        documents.put("test", new byte[50]);
        documents.put("test1", new byte[50]);
        final ProxyResultBean proxy = new ProxyResultBean("ok", "test ok", documents);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("proxy/description.xml");
        when(this.dataService.resource(UID_RECORD, "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final byte[] resourceAsBytes = this.resourceAsBytes("proxy/5-proxy/preprocess.xml");
        when(this.dataService.resource(UID_RECORD, "5-proxy/preprocess.xml")).thenReturn(new FileEntry("5-proxy/preprocess.xml").asBytes(resourceAsBytes));

        final Response response = this.client()
                .path("/v1/Record/code/{recordUid}/step/{stepId}/phase/{processPhase}/process/{processId}/{resultFileName}", UID_RECORD, "proxy", "pre", "script", "data-generated.xml")
                .type(MediaType.APPLICATION_JSON).post(proxy);
        assertThat(response, hasProperty("status", equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())));
    }

    /**
     * Test assign record.
     *
     * @throws Exception
     *             exception
     */
    // @Test
    // public void testAssign() throws Exception {
    // final RecordInfoBean dataBean = buildRecordInfoBean();
    // final SearchResult<RecordInfoBean> dataSearchResult = new
    // SearchResult<>(1L,
    // 5L);
    // dataSearchResult.setTotalResults(1L);
    // dataSearchResult.setContent(Arrays.asList(dataBean));
    //
    // when(this.dataService.searchInternal(any(long.class), any(long.class),
    // any(),
    // any(), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);
    //
    // Mockito.doNothing().when(this.dataService).assign(dataBean.getCode(),
    // "2016-11-USR-AAA-99");
    //
    // final Response response = this.client().accept("application/json") //
    // .path("/v1/Record/assign") //
    // .query("owner", UID_AUTHOR) //
    // .query("assignee", "2016-11-USR-AAA-99") //
    // .put(null);
    //
    // assertThat(response, hasProperty("status",
    // equalTo(Response.Status.OK.getStatusCode())));
    // }

    @Test
    public void testUploadRecordContextAndDefaultLanguage() throws FunctionalException {
        // prepare
        final String defaultLang = "en";
        final String reference = "QP039/REF";
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        final SpecificationBean specBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.specDataService.loadByReference(reference)).thenReturn(specBean);
        when(this.specDataService.resource(specBean.getUid())).thenReturn(new FileEntry(specBean.getUid() + ".zip").asBytes(this.resourceAsBytes("translation.zip")));

        final byte[] recordAsBytes = this.resourceAsBytes("translation.zip");
        final InputStream recordAsStream = new ByteArrayInputStream(recordAsBytes);
        final Attachment attachment1 = new Attachment("record", MediaType.APPLICATION_OCTET_STREAM, recordAsStream);

        final byte[] jsonAsBytes = "{\"data01\":{\"grp01\":{\"fld01x02\":\"42\",\"fld01x03\":\"test\"}}}".getBytes(StandardCharsets.UTF_8);
        final InputStream jsonAsStream = new ByteArrayInputStream(jsonAsBytes);
        final Attachment attachment2 = new Attachment("context", MediaType.APPLICATION_JSON, jsonAsStream);

        final List<SearchQueryFilter> listSearchQueryFilters = new ArrayList<SearchQueryFilter>();
        listSearchQueryFilters.add(new SearchQueryFilter("all:*"));

        // call
        final Response response = this.client().accept(MediaType.TEXT_PLAIN).path("/v1/Record/ref/{reference}", reference) //
                .query("author", "2017-05-JON-ATH-AN") //
                .query("lang", defaultLang) //
                .type(MediaType.MULTIPART_FORM_DATA).post(new MultipartBody(Arrays.asList(attachment1, attachment2)));

        // verify
        final ArgumentCaptor<byte[]> savedRecordAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).fromFile(any(), any(), savedRecordAsBytes.capture(), eq("2017-05-JON-ATH-AN"), any(), eq(RecordInfoBean.class));
        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));
        final SpecificationLoader loader = this.engine.loader(new ZipProvider(savedRecordAsBytes.getValue()));
        final FormSpecificationDescription formSpecificationDescription = loader.description();
        assertThat(formSpecificationDescription, hasProperty("lang", equalTo(defaultLang)));

        final TranslationsElement translationsElement = formSpecificationDescription.getTranslations();
        assertThat(translationsElement, hasProperty("defaultLang", equalTo(defaultLang)));
    }

    @Test
    public void testUploadWithContextMultipleOccurrences() throws Exception {
        final String reference = "R&D/WithContext";

        final RecordInfoBean recBean = new RecordInfoBean().setCode(UID_RECORD).setAuthor(UID_AUTHOR);

        final SpecificationBean specBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.dataService.resource(any(), eq(Engine.FILENAME_DESCRIPTION)))
                .thenReturn(new FileEntry(Engine.FILENAME_DESCRIPTION, "text/xml").asBytes(this.resourceAsBytes("simple/description.xml")));
        when(this.specDataService.loadByReference(reference)).thenReturn(specBean);
        when(this.specDataService.resource(specBean.getUid())).thenReturn(new FileEntry("multi.zip").asBytes(this.resourceAsBytes("multi.zip")));
        // when(this.specificationFileDataService.fileContent(specBean.getSourceFile().getId())).thenReturn(this.resourceAsBytes("multi.zip"));
        when(this.dataService.fromFile(eq(null), eq(null), any(byte[].class), eq(UID_AUTHOR), any(), eq(RecordInfoBean.class))).thenReturn(recBean);

        final Attachment contextAttachment = new Attachment("context", MediaType.APPLICATION_JSON, this.resourceAsStream("multi.json"));

        final Response response = this.client().accept(MediaType.TEXT_PLAIN).path("/v1/Record/ref/{reference}", reference) //
                .query("author", UID_AUTHOR) //
                .type(MediaType.MULTIPART_FORM_DATA).post(new MultipartBody(contextAttachment));

        final ArgumentCaptor<byte[]> savedRecordAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).fromFile(any(), any(), savedRecordAsBytes.capture(), eq(UID_AUTHOR), any(), eq(RecordInfoBean.class));

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));

        final IProvider provider = new ZipProvider(savedRecordAsBytes.getValue());
        final SpecificationLoader loader = this.engine.loader(provider);

        final FormSpecificationData spec = loader.data(0);
        final GroupElement page = spec.getGroups().get(0);

        assertThat(page.getData(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("fld01")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #01 value"))) //
                                ), //
                                hasProperty("id", equalTo("grp02[0]")), //
                                hasProperty("id", equalTo("grp02[1]")) //
                        ) //
                ) //
        );

        assertThat(page.getData().get(1).getData(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("fld02")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #02 value (1)"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[0]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03a value (1)"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[1]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03b value (1)"))) //
                                ) //
                        ) //
                ) //
        );

        assertThat(page.getData().get(2).getData(), //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo("fld02")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #02 value (2)"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[0]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03a value (2)"))) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("fld03[1]")), //
                                        hasProperty("value", hasProperty("content", equalTo("Field #03b value (2)"))) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test own.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testOwn() throws Exception {
        final RecordInfoBean dataBean = buildRecordInfoBean();
        final SearchResult<RecordInfoBean> dataSearchResult = new SearchResult<>(0L, 1L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults());
        when(this.dataService.search(any(long.class), any(long.class), any(), any(), any(), eq(null), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept("application/json").path("/v1/Record/code/{code}/author/{author}/own", UID_RECORD, UID_AUTHOR).get();

        verify(this.dataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), eq(searchQuery.getOrders()), any(), eq(null), eq(RecordInfoBean.class));

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final Boolean actual = this.readAsBean(response, Boolean.class);
        assertTrue(actual);
    }

    /**
     * Test own.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPurgeRecords() throws Exception {
        final Long days = 1L;
        final String dateBefore = LocalDateTime.now().minusDays(days).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        final List<SearchQueryFilter> filters = Arrays.asList(new SearchQueryFilter("updated", "<=", dateBefore));

        when(this.dataService.remove(filters)).thenReturn(1L);

        final Response response = this.client().accept("application/json").path("/v1/Record/purge") //
                .query("days", days) //
                .delete();

        verify(this.dataService).remove(eq(filters));

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    /**
     * Test lock record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testLockRecord() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("lock/lock-description.xml");
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final Response response = this.client().accept("application/json") //
                .path("/v1/Record/code/{code}/lock", dataBean.getUid()) //
                .post(null);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // verify
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).addOrUpdate(eq(dataBean.getUid()), eq("description.xml"), any(), captor.capture());

        final FormSpecificationDescription description = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationDescription.class);
        assertThat(description, notNullValue());
        assertThat(description.getSteps().getElements(), //
                contains( //
                        Arrays.asList( // , //
                                allOf( //
                                        hasProperty("id", equalTo("summary")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("processed")), //
                                        hasProperty("status", equalTo("error")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("archived")), //
                                        hasProperty("status", equalTo("todo")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test unlock record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void tesUnlockRecord() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("lock/unlock-description.xml");

        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final Response response = this.client().accept("application/json") //
                .path("/v1/Record/code/{code}/unlock", dataBean.getUid()) //
                .post(null);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // verify
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService, times(2)).addOrUpdate(eq(dataBean.getUid()), eq("description.xml"), any(), captor.capture());

        final FormSpecificationDescription description = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationDescription.class);
        assertThat(description, notNullValue());
        assertThat(description.getSteps().getElements(), //
                contains( //
                        Arrays.asList( // , //
                                allOf( //
                                        hasProperty("id", equalTo("summary")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("processed")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("archived")), //
                                        hasProperty("status", equalTo("todo")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test unlock record.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void tesUnlockLastStepRecord() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("lock/unlock-last-step-description.xml");

        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final Response response = this.client().accept("application/json") //
                .path("/v1/Record/code/{code}/unlock", dataBean.getUid()) //
                .post(null);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // verify
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService, times(2)).addOrUpdate(eq(dataBean.getUid()), eq("description.xml"), any(), captor.capture());

        final FormSpecificationDescription description = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationDescription.class);
        assertThat(description, notNullValue());
        assertThat(description.getSteps().getElements(), //
                contains( //
                        Arrays.asList( // , //
                                allOf( //
                                        hasProperty("id", equalTo("summary")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("processed")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("archived")), //
                                        hasProperty("status", equalTo("done")) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test unlock payment step.
     *
     * @throws Exception
     *             exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testUnlockPaymentStep() throws Exception {
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        final byte[] updatedResourceAsBytes = this.resourceAsBytes("lock/unlock-payment-description.xml");

        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(updatedResourceAsBytes));

        final byte[] resourceAsBytes = this.resourceAsBytes("lock/5-payment/preprocess.xml");
        when(this.dataService.resource(dataBean.getUid(), "5-payment/preprocess.xml")).thenReturn(new FileEntry("5-payment/preprocess.xml").asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/json") //
                .path("/v1/Record/code/{code}/unlock", dataBean.getUid()) //
                .post(null);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // verify
        final ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService, times(2)).addOrUpdate(eq(dataBean.getUid()), eq("description.xml"), any(), captor.capture());

        final FormSpecificationDescription description = JaxbFactoryImpl.instance().unmarshal(captor.getValue(), FormSpecificationDescription.class);
        assertThat(description, notNullValue());
        assertThat(description.getSteps().getElements(), //
                contains( //
                        Arrays.asList( // , //
                                allOf( //
                                        hasProperty("id", equalTo("summary")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("payment")), //
                                        hasProperty("status", equalTo("done")) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo("archived")), //
                                        hasProperty("status", equalTo("todo")) //
                                ) //
                        ) //
                ) //
        );

        final ArgumentCaptor<byte[]> captorData = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).addOrUpdate(eq(dataBean.getUid()), eq("5-payment/data.xml"), any(), captorData.capture());
        final FormSpecificationData data = JaxbFactoryImpl.instance().unmarshal(captorData.getValue(), FormSpecificationData.class);
        assertThat(data, notNullValue());
        assertThat(data.getGroups().get(0), //
                allOf( //
                        hasProperty("description", equalTo("Cette étape a été débloquée suite à une intervention sur votre dossier")) //
                ) //
        );
    }

    /**
     * Test reset step.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testResetStep() throws Exception {
        // prepare
        final RecordBean dataBean = RecordServiceImplTest.buildRecordBean();
        when(this.dataService.exists(dataBean.getUid())).thenReturn(true);
        when(this.dataService.load(eq(dataBean.getUid()), eq(RecordBean.class))).thenReturn(dataBean);
        when(this.dataService.resource(dataBean.getUid(), "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(this.resourceAsBytes("step-reset/description.xml")));

        when(this.dataService.resource(dataBean.getUid(), "0-data/data.xml")).thenReturn(new FileEntry("data.xml").asBytes(this.resourceAsBytes("step-reset/0-data/data.xml")));

        for (final String name : new String[] { "preprocess.xml", "postprocess.xml", "data-generated.xml", "proxy-calling.xml" }) {
            when(this.dataService.resource(dataBean.getUid(), "1-payment/" + name)).thenReturn(new FileEntry("1-payment/" + name).asBytes(this.resourceAsBytes("step-reset/1-payment/" + name)));
        }

        when(this.dataService.remove(dataBean.getUid(), "1-payment/data-generated.xml")).thenReturn(1L);
        when(this.dataService.remove(dataBean.getUid(), "1-payment/proxy-calling.xml")).thenReturn(1L);

        final Response response = this.client().accept("application/json") //
                .path("/v1/Record/code/{code}/step/reset", dataBean.getUid(), 1) //
                .post(null);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        // verify
    }

    /**
     * Test remove all records.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemoveAll() throws Exception {
        final List<SearchQueryFilter> filters = Arrays.asList(new SearchQueryFilter("uid", ":", Arrays.asList("2020-01-REC-ORD-01")));
        when(this.dataService.remove(filters)).thenReturn(1L);

        final Response response = this.client().path("/v1/Record/remove").query("filters", filters).delete();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        verify(this.dataService).remove(filters);
    }
}
