package fr.ge.common.nash.ws.v1.service.impl.dashboard;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Properties;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;
import fr.ge.record.ws.v1.model.RecordActionTypeEnum;
import fr.ge.record.ws.v1.model.RecordResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class RecordDashboardServiceImplTest extends AbstractRestTest {

    /** La constante UID_RECORD. */
    private static final String UID_RECORD = "2016-11-ESG-YTW-37";

    /** La constante UID_AUTHOR. */
    private static final String UID_AUTHOR = "2018-11-JON-DO7515";

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServer;

    /** data service. */
    @Autowired
    private IRecordDataService dataService;

    @Autowired
    private Engine engine;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServer;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.dataService, this.engine);
    }

    @Test
    public void testDefaultStepLabel() throws Exception {
        final SpecificationLoader loader = SpecificationLoader.create(new ZipProvider(this.resourceAsBytes("test.zip")));
        final String stepLabel = CoreUtil.coalesce(loader.stepsMgr().current().getLabel(), RecordActionTypeEnum.READ.name());
        assertEquals("step data label", stepLabel);
    }

    /**
     * Build record bean.
     *
     * @return record bean
     */
    public static RecordBean buildRecordBean() {
        final Calendar now = Calendar.getInstance();

        return new RecordBean() //
                .setAuthor(UID_AUTHOR) //
                .setUid(UID_RECORD) //
                .setSpec("2016-10-XRT-BYD-35") //
                .setTitle("QP039") //
                .setCreated(now) //
                .setUpdated(now) //
        ;
    }

    /**
     * Build record info bean.
     *
     * @return record info bean
     */
    public static RecordInfoBean buildRecordInfoBean() {
        final RecordBean bean = buildRecordBean();

        return new RecordInfoBean() //
                .setCode(bean.getUid()) //
                .setOrigin(bean.getSpec()) //
                .setTitle(bean.getTitle()) //
                .setCreated(bean.getCreated()) //
                .setUpdated(bean.getUpdated()) //
                .setAuthor(bean.getAuthor());
    }

    private void prepareSearch(final String resourceName) throws Exception {
        // final SpecificationLoader loader = this.engine.loader(new
        // ZipProvider(this.resourceAsBytes(resourceName)));
        final SpecificationLoader loader = SpecificationLoader.create(new ZipProvider(this.resourceAsBytes(resourceName)));
        when(this.engine.loader(any())).thenReturn(loader);

        final RecordInfoBean dataBean = buildRecordInfoBean();
        final SearchResult<RecordInfoBean> dataSearchResult = new SearchResult<>(1L, 10L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        when(this.dataService.search(any(long.class), any(long.class), any(), any(), any(), eq(null), eq(RecordInfoBean.class))).thenReturn(dataSearchResult);

        final Properties properties = new Properties();
        properties.put("engine.user.type", "user");
        final Configuration configuration = new Configuration(properties);
        when(this.engine.getEngineProperties()).thenReturn(configuration);
    }

    private Response callSearch() throws Exception {
        return this.client() //
                .accept("application/json") //
                .path("record/author/{authorId}/search", UID_AUTHOR) //
                .query("maxResults", 10L) //
                .get();
    }

    private void verifySearch(final RecordResult actual) throws Exception {
        verify(this.dataService).search(//
                eq(0L), //
                eq(10L), //
                eq(Arrays.asList(new SearchQueryFilter("author:" + UID_AUTHOR))), //
                eq(Arrays.asList(new SearchQueryOrder("created:desc"))), //
                eq(new ArrayList<SearchQueryFilter>()), //
                eq(null), //
                eq(RecordInfoBean.class));

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(actual.getStartIndex())), //
                hasProperty("nbResults", equalTo(actual.getNbResults())), //
                hasProperty("totalResults", equalTo(actual.getTotalResults())) //
        ) //
        );
    }

    /**
     * Test search with removable flag.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchRemovableRecord() throws Exception {
        // prepare
        this.prepareSearch("removable.zip");

        // call
        final Response response = this.callSearch();

        // verify
        final RecordResult actual = this.readAsBean(response, new TypeReference<RecordResult>() {
        });
        this.verifySearch(actual);

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo(UID_AUTHOR)), //
                        hasProperty("recordId", equalTo(UID_RECORD)), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD)) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/download.zip")) //
                                        )))) //
                        ) //
                )) //
        );
    }

    /**
     * Test search without removable flag.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchWithoutRemovableFlag() throws Exception {
        // prepare
        this.prepareSearch("without-removable.zip");

        // call
        final Response response = this.callSearch();

        // verify
        final RecordResult actual = this.readAsBean(response, new TypeReference<RecordResult>() {
        });
        this.verifySearch(actual);

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo(UID_AUTHOR)), //
                        hasProperty("recordId", equalTo(UID_RECORD)), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD)) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DELETE)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/remove")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/download.zip")) //
                                        )) //
                                )) //
                        ) //
                )) //
        );
    }

    /**
     * Test search record with all steps done.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchStepsDone() throws Exception {
        // prepare
        this.prepareSearch("done.zip");

        // call
        final Response response = this.callSearch();

        // verify
        final RecordResult actual = this.readAsBean(response, new TypeReference<RecordResult>() {
        });
        this.verifySearch(actual);

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo(UID_AUTHOR)), //
                        hasProperty("recordId", equalTo(UID_RECORD)), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD)) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/" + UID_RECORD + "/download.zip")) //
                                        )) //
                                )) //
                        ) //
                )) //
        );
    }
}