package fr.ge.common.nash.ws.event.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.RecordOpenEvent;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Record open listener tests.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordOpenListenerTest {

    /** The event. */
    @Mock
    private RecordOpenEvent event;

    /** The listener. */
    @InjectMocks
    private RecordOpenListener listener;

    /** Loader. */
    @Mock
    private SpecificationLoader loader;

    /** Tracker facade. */
    @Mock
    private ITrackerFacade trackerFacade;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnEvent() {
        // mock
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setRecordUid("My record id");
        description.setFormUid("My form id");
        when(this.loader.description()).thenReturn(description);
        when(this.event.getLoader()).thenReturn(this.loader);

        // call
        this.listener.onRecordOpenEvent(this.event);

        // check
        final String messagePattern = "My record id";
        final ArgumentCaptor<String> message = ArgumentCaptor.forClass(String.class);
        verify(this.trackerFacade).post(eq("GE-OPEN-My form id"), message.capture());
        final String actual = message.getValue();

        assertNotNull(actual);
        assertEquals(actual, messagePattern);
    }

    @Test
    public void testOnEventNoMessage() {
        // mock
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setRecordUid(null);
        when(this.loader.description()).thenReturn(description);
        when(this.event.getLoader()).thenReturn(this.loader);

        // call
        this.listener.onRecordOpenEvent(this.event);

        // check
        verify(this.trackerFacade, never()).post(anyString(), anyString());
    }
}
