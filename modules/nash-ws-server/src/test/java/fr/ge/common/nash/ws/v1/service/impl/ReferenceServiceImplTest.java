/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.nash.ws.v1.bean.ReferenceInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.ReferenceSearchResult;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class ReferenceServiceImplTest extends AbstractRestTest {

    @Autowired
    private IReferenceDataService dataService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        reset(this.dataService);
    }

    class IsFilterCompleted implements ArgumentMatcher<List<SearchQueryFilter>> {
        public boolean matches(List<SearchQueryFilter> list) {
            if ((list.size() != 2) || !list.get(0).getColumn().equals("groupe") || !list.get(0).getOperator().equals(":") || !list.get(1).getColumn().equals("nb_specs")
                    || !list.get(1).getOperator().equals(">")) {
                return false;
            }

            return true;
        }
    }

    @Test
    public void testSearchAddingNewFilterParam() throws Exception {

        SearchQuery query = new SearchQuery(0L, 10L);
        Set<String> groups = new HashSet<>();
        groups.add("referent");
        query.addOrder("id", "desc");
        query.addFilter(new SearchQueryFilter("groupe", ":", groups));

        when(this.dataService.search(eq(0L), eq(10L), argThat(new IsFilterCompleted()), anyList(), isNull(), isNull(), eq(ReferenceInfoBean.class))).thenReturn(new SearchResult<ReferenceInfoBean>());
        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Reference").query("startIndex", query.getStartIndex()).query("maxResults", query.getMaxResults())
                .query("filters", query.getFilters()).query("orders", query.getOrders()).get();
        final ReferenceSearchResult actual = this.readAsBean(response, new TypeReference<ReferenceSearchResult>() {
        });

        assertNotNull(actual);

    }

    @Test
    public void testPath() throws Exception {
        when(this.dataService.load(1001L, ReferenceInfoBean.class)).thenReturn(new ReferenceInfoBean().setId(1001L).setCategory("SCN").setContent("SCN"));
        when(this.dataService.load(1002L, ReferenceInfoBean.class)).thenReturn(new ReferenceInfoBean().setId(1002L).setParent(1001L).setCategory("GQ").setContent("SCN/GQ"));
        when(this.dataService.load(1006L, ReferenceInfoBean.class)).thenReturn(new ReferenceInfoBean().setId(1006L).setParent(1002L).setCategory("PE04").setContent("SCN/GQ/PE04"));

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Reference/{refId}", 1006).get();
        final List<ReferenceInfoBean> actual = this.readAsBean(response, new TypeReference<List<ReferenceInfoBean>>() {
        });

        assertThat(actual, hasSize(3));
        assertThat(actual, //
                contains( //
                        Arrays.asList( //
                                allOf( //
                                        hasProperty("id", equalTo(1001L)), //
                                        hasProperty("category", equalTo("SCN")), //
                                        hasProperty("content", equalTo("SCN")), //
                                        hasProperty("parent", nullValue()) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo(1002L)), //
                                        hasProperty("category", equalTo("GQ")), //
                                        hasProperty("content", equalTo("SCN/GQ")), //
                                        hasProperty("parent", equalTo(1001L)) //
                                ), //
                                allOf( //
                                        hasProperty("id", equalTo(1006L)), //
                                        hasProperty("category", equalTo("PE04")), //
                                        hasProperty("content", equalTo("SCN/GQ/PE04")), //
                                        hasProperty("parent", equalTo(1002L)) //
                                ) //
                        ) //
                ) //
        );
    }

}
