/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.bean.Configuration;
import fr.ge.common.nash.ws.data.bean.MetaBean;
import fr.ge.common.nash.ws.data.service.IMetaDataService;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.support.context.AclContext;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;
import fr.ge.record.ws.v1.model.RecordActionTypeBOEnum;
import fr.ge.record.ws.v1.model.RecordActionTypeEnum;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.model.RecordResultBO;

/**
 * Tests {@link RecordDisplayServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class RecordDisplayServiceImplTest extends AbstractRestTest {

    /** Data service. */
    @Autowired
    private IRecordDataService recordDataService;

    /** Engine. */
    @Autowired
    private Engine engineMock;

    @Autowired
    private IMetaDataService metaDataService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    @Autowired
    private AclContext aclContext;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        reset(this.recordDataService, this.metaDataService, this.aclContext, this.engineMock);
        when(this.aclContext.asList()).thenReturn(new ArrayList<>());

        final SearchResult<MetaBean> metaSearchResult = new SearchResult<>(0, 1);
        metaSearchResult.setTotalResults(0);

        when(this.metaDataService.search(eq(0L), eq(1L), any(), eq(null), eq(null), eq(null), eq(MetaBean.class))).thenReturn(metaSearchResult);
        Arrays.asList("description.xml").forEach(elm -> {
            when(this.recordDataService.resource(any(), eq(elm))).thenReturn(new FileEntry(elm).asBytes(this.resourceAsBytes("completed/" + elm)));
        });

        final Properties properties = new Properties();
        properties.put("engine.user.type", "user");
        final Configuration configuration = new Configuration(properties);
        when(this.engineMock.getEngineProperties()).thenReturn(configuration);
        when(this.engineMock.loader(any())).thenCallRealMethod();

    }

    /**
     * Tests
     * {@link RecordDisplayServiceImpl#getRecords(String, int, int, String)}.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetRecords() throws Exception {

        // prepare
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));
        filters.add(new SearchQueryFilter("stepId", ":", "archived"));

        final Collection<String> status = new ArrayList<String>();
        status.add(StepStatusEnum.TO_DO.getStatus());
        status.add(StepStatusEnum.IN_PROGRESS.getStatus());
        filters.add(new SearchQueryFilter("stepStatus", ":", status));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("created", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), null, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-01"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta01.xml")));
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN").query("startIndex", 3).query("maxResults", 10).query("orders", "created").get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        final RecordResult actual = this.readAsBean(response, RecordResult.class);

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), null, RecordInfoBean.class);
        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo("3")), //
                        hasProperty("nbResults", equalTo("10")), //
                        hasProperty("totalResults", equalTo("2")), //
                        hasProperty("records", hasSize(2)) //
                ) //
        );

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-01")), //
                        hasProperty("title", equalTo("record 1")), //
                        hasProperty("completedStep", equalTo("2")), //
                        hasProperty("totalStep", equalTo("5")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/download.zip")) //
                                        )))) //
                        ), //
                        hasProperty("metadata"))//
                ) //
        );

        assertThat(actual.getRecords().get(0).getMetadata(), IsMapContaining.hasKey("mKey"));

        assertThat(actual.getRecords().get(1), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-02")), //
                        hasProperty("title", equalTo("record 2")), //
                        hasProperty("completedStep", equalTo("4")), //
                        hasProperty("totalStep", equalTo("4")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-02/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-02/documents.zip")) //
                                        )))) //
                        ), //
                        hasProperty("metadata")//
                )) //
        );

        assertThat(actual.getRecords().get(1).getMetadata(), IsMapContaining.hasKey("document"));
    }

    /**
     * Tests
     * {@link RecordDisplayServiceImpl#searchRecords(String, int, int, String, String)}.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchRecords() throws Exception {
        // prepare
        final String searchText = "record";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));
        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("updated", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-01"), eq("description.xml"))).thenReturn(new FileEntry("description.xml").asBytes(this.resourceAsBytes("spec/description-label.xml")));
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-01"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta01.xml")));
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "update")
                .query("searchText", searchText).get();

        // verify
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final RecordResult actual = this.readAsBean(response, RecordResult.class);

        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo("3")), //
                        hasProperty("nbResults", equalTo("10")), //
                        hasProperty("totalResults", equalTo("2")), //
                        hasProperty("records", hasSize(2)) //
                ) //
        );

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-01")), //
                        hasProperty("title", equalTo("record 1")), //
                        hasProperty("completedStep", equalTo("2")), //
                        hasProperty("totalStep", equalTo("5")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01")), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("label03"))) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DELETE)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/remove")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/download.zip")) //
                                        )))) //
                        ), //
                        hasProperty("metadata")//
                )) //
        );

        assertThat(actual.getRecords().get(1), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-02")), //
                        hasProperty("title", equalTo("record 2")), //
                        hasProperty("completedStep", equalTo("4")), //
                        hasProperty("totalStep", equalTo("4")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-02/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-02/documents.zip")) //
                                        )))) //
                        ), //
                        hasProperty("metadata")//
                )) //
        );

        when(this.recordDataService.resource(eq("2017-04-REC-ORD-01"), eq("description.xml")))
                .thenReturn(new FileEntry("description.xml").asBytes(this.resourceAsBytes("spec/description-nolabel.xml")));
        // call
        final Response response2 = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "update")
                .query("searchText", searchText).get();

        // verify
        assertThat(response2, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final RecordResult actual2 = this.readAsBean(response2, RecordResult.class);

        verify(this.recordDataService, times(2)).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo("3")), //
                        hasProperty("nbResults", equalTo("10")), //
                        hasProperty("totalResults", equalTo("2")), //
                        hasProperty("records", hasSize(2)) //
                ) //
        );

        assertThat(actual2.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-01")), //
                        hasProperty("title", equalTo("record 1")), //
                        hasProperty("completedStep", equalTo("2")), //
                        hasProperty("totalStep", equalTo("5")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.READ)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01")), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("Consulter"))) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.HISTORY)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/history")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DELETE)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/remove")) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("type", equalTo(RecordActionTypeEnum.DOWNLOAD)), //
                                                hasProperty("uri", equalTo("/record/2017-04-REC-ORD-01/download.zip")) //
                                        )))) //
                        ), //
                        hasProperty("metadata")//
                )) //
        );
    }

    /**
     * Tests
     * {@link RecordDisplayServiceImpl#searchRecords(String, int, int, String, String)}.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchRecordsBO() throws Exception {

        // prepare
        final SearchResult<RecordInfoBean> searchResultRecord = this.buildSearchResultRecord();

        final String searchString = "record";
        final List<SearchQueryFilter> recordFilters = new ArrayList<>();

        recordFilters.add(new SearchQueryFilter("stepId", ":", "archived"));

        final Collection<String> status = new ArrayList<String>();
        status.add(StepStatusEnum.TO_DO.getStatus());
        status.add(StepStatusEnum.IN_PROGRESS.getStatus());
        status.add(StepStatusEnum.DONE.getStatus());
        recordFilters.add(new SearchQueryFilter("stepStatus", ":", status));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("created", "desc"));

        when(this.recordDataService.search(3, 10, recordFilters, orders, new ArrayList<>(), searchString, RecordInfoBean.class)).thenReturn(searchResultRecord);
        when(this.recordDataService.resource("2017-04-REC-ORD-01", "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(this.resourceAsBytes("spec/description-01.xml")));
        when(this.recordDataService.resource("2017-04-REC-ORD-02", "description.xml")).thenReturn(new FileEntry("description.xml").asBytes(this.resourceAsBytes("spec/description-02.xml")));
        when(this.recordDataService.resource("2017-04-REC-ORD-02", "meta.xml")).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/v2/search") //
                .query("startIndex", 3) //
                .query("maxResults", 10) //
                .query("searchText", searchString) //
                .query("archived", true) //
                .get();

        assertThat(response.getStatus(), equalTo(200));

        final RecordResultBO actual = this.readAsBean(response, RecordResultBO.class);

        // verify
        verify(this.recordDataService).search(3, 10, recordFilters, orders, new ArrayList<>(), searchString, RecordInfoBean.class);

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo("3")), //
                        hasProperty("nbResults", equalTo("10")), //
                        hasProperty("totalResults", equalTo("2")), //
                        hasProperty("records", hasSize(2)) //
                ) //
        );

        assertThat(actual.getRecords().get(0), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-01")), //
                        hasProperty("title", equalTo("record 1")), //
                        hasProperty("completedStep", equalTo("2")), //
                        hasProperty("totalStep", equalTo("5")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("code", equalTo(RecordActionTypeBOEnum.VIEW.getTypeActionName())), //
                                                hasProperty("type", equalTo(RecordActionTypeBOEnum.VIEW.getTypeActionId())), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("Consulter"))) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("code", equalTo(RecordActionTypeBOEnum.DOWNLOAD.getTypeActionName())), //
                                                hasProperty("type", equalTo(RecordActionTypeBOEnum.DOWNLOAD.getTypeActionId())), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("Télécharger"))) //
                                        )) //
                                )) //
                        ) //
                )) //
        );

        assertThat(actual.getRecords().get(1), //
                allOf(Arrays.asList( //
                        hasProperty("authorId", equalTo("2017-04-JON-ATH-AN")), //
                        hasProperty("recordId", equalTo("2017-04-REC-ORD-02")), //
                        hasProperty("title", equalTo("record 2")), //
                        hasProperty("completedStep", equalTo("4")), //
                        hasProperty("totalStep", equalTo("4")), //
                        hasProperty("actions", //
                                contains(Arrays.asList( //
                                        allOf(Arrays.asList( //
                                                hasProperty("code", equalTo(RecordActionTypeBOEnum.VIEW.getTypeActionName())), //
                                                hasProperty("type", equalTo(RecordActionTypeBOEnum.VIEW.getTypeActionId())), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("Consulter"))) //
                                        )), //
                                        allOf(Arrays.asList( //
                                                hasProperty("code", equalTo(RecordActionTypeBOEnum.DOCUMENTS.getTypeActionName())), //
                                                hasProperty("type", equalTo(RecordActionTypeBOEnum.DOWNLOAD.getTypeActionId())), //
                                                hasProperty("label", hasProperty("actualLabel", equalTo("Télécharger"))) //
                                        )) //
                                )) //
                        ) //
                )) //
        );
    }

    @Test
    public void testSearchRecordsByUnknownTitle() throws Exception {
        // prepare
        final String searchText = "alambic";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("title", "asc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "title")
                .query("searchText", searchText).get();

        // verify
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);

    }

    @Test
    public void testSearchRecordsByEmpty() throws Exception {
        // prepare
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("created", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), null, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).get();

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), null, RecordInfoBean.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }

    @Test
    public void testSearchRecordsByTitleWildcardStart() throws Exception {
        // prepare
        final String searchText = "*cord*";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("updated", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "update")
                .query("searchText", searchText).get();

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }

    @Test
    public void testSearchRecordsByEmptyTitle() throws Exception {
        // prepare
        final String searchText = "**";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("created", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "creation")
                .query("searchText", searchText).get();

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }

    @Test
    public void testSearchRecordsByTitleWildcardEnd() throws Exception {
        // prepare
        final String searchText = "record";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("created", "desc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "")
                .query("searchText", searchText).get();

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }

    @Test
    public void testSearchRecordsByTitleWildcards() throws Exception {
        // prepare
        final String searchText = "*cord*";
        final List<SearchQueryFilter> filters = new ArrayList<>();
        filters.add(new SearchQueryFilter("author", ":", "2017-04-JON-ATH-AN"));

        final List<SearchQueryOrder> orders = new ArrayList<>();
        orders.add(new SearchQueryOrder("title", "asc"));

        final SearchResult<RecordInfoBean> searchResult = this.buildSearchResultRecord();

        when(this.recordDataService.search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class)).thenReturn(searchResult);
        when(this.recordDataService.resource(eq("2017-04-REC-ORD-02"), eq("meta.xml"))).thenReturn(new FileEntry("meta.xml").asBytes(this.resourceAsBytes("spec/meta02.xml")));

        // call
        final Response response = this.client().accept("application/json").path("/record/author/2017-04-JON-ATH-AN/search").query("startIndex", 3).query("maxResults", 10).query("orders", "title")
                .query("searchText", "*cord*").get();

        // verify
        verify(this.recordDataService).search(3, 10, filters, orders, new ArrayList<>(), searchText, RecordInfoBean.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

    }

    protected SearchResult<RecordInfoBean> buildSearchResultRecord() {
        final Calendar now = new GregorianCalendar();

        final SearchResult<RecordInfoBean> searchResult = new SearchResult<>();
        searchResult.setStartIndex(3);
        searchResult.setMaxResults(10);
        searchResult.setTotalResults(2);
        searchResult.setContent( //
                Arrays.asList( //
                        new RecordInfoBean() //
                                .setAuthor("2017-04-JON-ATH-AN") //
                                .setCode("2017-04-REC-ORD-01") //
                                .setOrigin("2017-04-SPE-CIF-01") //
                                .setTitle("record 1") //
                                .setCreated(now) //
                                .setUpdated(now) //
                                .setProperties("{\"nbSteps\":5,\"nbCompletedSteps\":2}".getBytes()) //
                        , new RecordInfoBean() //
                                .setAuthor("2017-04-JON-ATH-AN") //
                                .setCode("2017-04-REC-ORD-02") //
                                .setOrigin("2017-04-SPE-CIF-02") //
                                .setTitle("record 2") //
                                .setCreated(now) //
                                .setUpdated(now) //
                                .setProperties("{\"nbSteps\":4,\"nbCompletedSteps\":4}".getBytes()) //
                ) //
        );

        return searchResult;
    }

}
