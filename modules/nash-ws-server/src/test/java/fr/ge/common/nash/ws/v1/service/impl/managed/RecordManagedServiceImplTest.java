package fr.ge.common.nash.ws.v1.service.impl.managed;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.impl.SpecificationServiceImplTest;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.test.AbstractRestTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class RecordManagedServiceImplTest extends AbstractRestTest {

    /** La constante UID_AUTHOR. */
    private static final String UID_AUTHOR = "2016-11-USR-AAA-42";

    /** La constante UID_RECORD. */
    private static final String UID_RECORD = "2016-11-ESG-YTW-37";

    /** spec data service. */
    @Autowired
    private ISpecificationDataService specDataService;

    /** data service. */
    @Autowired
    private IRecordDataService dataService;

    @Autowired
    @Qualifier("restManagedServer")
    private JAXRSServerFactoryBean restServerFactory;

    /** The engine. */
    @Autowired
    private Engine engine;

    private SpecificationLoader loader;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.specDataService, this.dataService);

        this.loader = mock(SpecificationLoader.class);
        final FormSpecificationDescription fsd = mock(FormSpecificationDescription.class);
        when(fsd.getAuthor()).thenReturn(UID_AUTHOR);
        when(this.loader.description()).thenReturn(fsd);
        when(this.engine.loader(any(IProvider.class))).thenCallRealMethod();
        // when(this.engine.loader(any(IProvider.class))).thenReturn(this.loader);

        when(this.dataService.resource(eq(UID_RECORD), eq(Engine.FILENAME_DESCRIPTION))).thenReturn(new FileEntry(Engine.FILENAME_DESCRIPTION).asBytes(JaxbFactoryImpl.instance().asByteArray(fsd)));
    }

    /**
     * Build record bean.
     *
     * @return record bean
     */
    public static RecordBean buildRecordBean() {
        final Calendar now = Calendar.getInstance();

        return new RecordBean() //
                .setAuthor("the author id") //
                .setUid(UID_RECORD) //
                .setSpec("2016-10-XRT-BYD-35") //
                .setTitle("QP039") //
                .setCreated(now) //
                .setUpdated(now) //
        ;
    }

    public static Collection<FileBean> buildRecordResourceList() {
        final Calendar now = Calendar.getInstance();

        return Arrays.asList( //
                new FileBean() //
                        .setId(42L) //
                        .setName("description.xml") //
                        .setType("text/xml") //
                        .setCreated(now) //
                        .setUpdated(now), //
                new FileBean() //
                        .setId(51L) //
                        .setName("1-data/data.xml") //
                        .setType("text/xml") //
                        .setCreated(now) //
                        .setUpdated(now) //
        );
    }

    /**
     * Build record info bean.
     *
     * @return record info bean
     */
    public static RecordInfoBean buildRecordInfoBean() {
        final RecordBean bean = buildRecordBean();

        return new RecordInfoBean() //
                .setCode(bean.getUid()) //
                .setOrigin(bean.getSpec()) //
                .setTitle(bean.getTitle()) //
                .setCreated(bean.getCreated()) //
                .setUpdated(bean.getUpdated()) //
                .setAuthor(bean.getAuthor());
    }

    /**
     * Test health check.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testHealthcheck() {
        // call
        final Response response = this.client().accept("text/plain") //
                .path("/v2/record/healthcheck") //
                .get();
        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
    }

    private Map<String, Object> prepareCreDo(final String reference, final String jsonAsString) throws FunctionalException {
        // prepare
        final SpecificationBean specBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.specDataService.loadByReference(reference)).thenReturn(specBean);
        when(this.specDataService.resource(specBean.getUid())).thenReturn(new FileEntry(specBean.getUid() + ".zip").asBytes(this.resourceAsBytes("translation.zip")));

        when(this.dataService.fromFile(eq(null), eq(null), any(), eq(UID_AUTHOR), any(), eq(RecordInfoBean.class))).thenReturn(buildRecordInfoBean());

        final byte[] jsonAsBytes = jsonAsString.getBytes(StandardCharsets.UTF_8);
        final InputStream jsonAsStream = new ByteArrayInputStream(jsonAsBytes);
        final Attachment attachment = new Attachment("context", MediaType.APPLICATION_JSON, jsonAsStream);

        final Map<String, Object> parameters = new HashMap<>();
        parameters.put("context", attachment);
        parameters.put("author", UID_AUTHOR);
        parameters.put("reference", reference);

        return parameters;
    }

    /**
     * Test upload with context.
     *
     * @throws Exception
     */
    @Test
    public void testUploadWithContext() throws Exception {
        // prepare
        final String jsonAsString = "{\"data01\":{\"grp01\":{\"fld01x02\":\"42\",\"fld01x03\":\"test\"}}}";
        final Map<String, Object> parameters = this.prepareCreDo("QP039/REF", jsonAsString);

        // call
        final Response response = this.client().path("/v2/record/ref/{reference}", (String) parameters.get("reference")) //
                .query("author", (String) parameters.get("author")) //
                .type(MediaType.MULTIPART_FORM_DATA) //
                .post(new MultipartBody((Attachment) parameters.get("context")));

        // verify
        final List<SearchQueryFilter> listSearchQueryFilters = new ArrayList<SearchQueryFilter>();
        listSearchQueryFilters.add(new SearchQueryFilter("GE/" + UID_AUTHOR + ":*"));

        final ArgumentCaptor<byte[]> savedRecordAsBytes = ArgumentCaptor.forClass(byte[].class);
        verify(this.dataService).fromFile(any(), any(), savedRecordAsBytes.capture(), eq(UID_AUTHOR), eq(listSearchQueryFilters), eq(RecordInfoBean.class));
        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
    }

    /**
     * Test upload with context and reference as parameters.
     *
     * @throws Exception
     */
    @Test
    public void testUploadWithContextAndReference() throws Exception {
        // prepare
        final String jsonAsString = "{\"data01\":{\"grp01\":{\"fld01x02\":\"42\",\"fld01x03\":\"test\"}}}";
        final Map<String, Object> parameters = this.prepareCreDo("QP039/REF", jsonAsString);

        // call
        final Response response = this.client().path("/v2/record") //
                .query("reference", (String) parameters.get("reference")) //
                .query("author", (String) parameters.get("author")) //
                .type(MediaType.APPLICATION_OCTET_STREAM) //
                .put(jsonAsString.getBytes());

        // verify
        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
    }
}
