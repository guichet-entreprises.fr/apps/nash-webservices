/**
 *
 */
package fr.ge.common.nash.ws.util;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import fr.ge.common.utils.test.AbstractTest;

/**
 * Class ResponseUtilTest.
 *
 * @author bsadil
 */
public class ResponseUtilTest extends AbstractTest {

    /** La constante Not_Found. */
    private static final String Not_Found = "not_found";

    /** La constante DESCRIPTION. */
    private static final String DESCRIPTION = "css/description.css";

    /**
     * Response default test.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void responseDefaultTest() throws Exception {
        assertEquals(Response.noContent().build().getLength(), ResponseUtil.defaultResponse(Not_Found).getLength());

        final Response response = ResponseUtil.defaultResponse(DESCRIPTION);
        final byte[] asBytes = (byte[]) response.getEntity();

        MatcherAssert.assertThat(asBytes, Matchers.equalTo(this.resourceAsBytes("/css/description.css")));

    }

}
