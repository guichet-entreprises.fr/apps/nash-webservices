package fr.ge.common.nash.ws.v1.service.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.ws.data.service.IMetaDataService;
import fr.ge.common.nash.ws.v1.bean.MetaInfoBean;
import fr.ge.common.utils.bean.search.SearchResult;

public class MetaServiceImplTest {

    @InjectMocks
    private MetaServiceImpl metaService;

    @Mock
    private IMetaDataService metaDataService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSearch() throws Exception {
        when(metaDataService.search(0, 1, null, null, null, null, MetaInfoBean.class)).thenReturn(new SearchResult<MetaInfoBean>());

        this.metaService.search(0, 1, null, null);

        verify(this.metaDataService).search(0, 1, null, null, null, null, MetaInfoBean.class);

    }

}
