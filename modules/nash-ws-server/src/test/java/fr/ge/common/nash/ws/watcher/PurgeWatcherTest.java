package fr.ge.common.nash.ws.watcher;

import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.ws.v1.service.IRecordService;

/**
 * @author aakkou
 *
 */
public class PurgeWatcherTest {

    @InjectMocks
    private PurgeWatcher watcher;

    @Mock
    private IRecordService service;

    protected static final String RECORD_UID = "2016-03-AAA-AAA-27";

    protected static final Integer DAYS = 1;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        this.watcher.setThreadSleep(0); //
        this.watcher.setRetentionDays(DAYS); //
    }

    @Test
    public void testPurgeRecords() throws Exception {
        final Response response = Mockito.mock(Response.class);
        when(this.service.purge(DAYS)).thenReturn(response);

        final Thread t = new Thread(PurgeWatcherTest.this.watcher);
        t.start();
        t.interrupt();
    }
}
