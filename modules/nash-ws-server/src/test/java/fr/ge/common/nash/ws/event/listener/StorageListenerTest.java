package fr.ge.common.nash.ws.event.listener;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.StorageEvent;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferenceElement;
import fr.ge.common.nash.ws.v1.service.IRecordService;

public class StorageListenerTest {

    /** The event. */
    @Mock
    private StorageEvent event;

    /** The listener. */
    @InjectMocks
    private StorageListener listener;

    /** Loader. */
    @Mock
    private SpecificationLoader loader;

    /** The record service. */
    @Mock
    private IRecordService recordService;

    /** The record service. */
    @Mock
    private IRecordStorage recordStorage;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.listener.setStorageRecordRoot("test");
    }

    @Test
    public void testOnEvent() throws TemporaryException {
        // mock
        final FormSpecificationDescription description = new FormSpecificationDescription();
        description.setRecordUid("ABC");
        final ReferenceElement reference = new ReferenceElement();
        reference.setCode("test");
        description.setReference(reference);

        doNothing().when(this.recordStorage).storeRecord(any(String.class), any(byte[].class), any(String.class));
        when(this.recordService.download("ABC")).thenReturn(Response.ok("ABC".getBytes()).build());
        when(this.loader.description()).thenReturn(description);
        when(this.event.getLoader()).thenReturn(this.loader);

        // call
        this.listener.onStorageEvent(this.event);
    }
}
