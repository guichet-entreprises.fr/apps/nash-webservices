/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.dozer.Mapper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.validation.Error;
import fr.ge.common.nash.engine.validation.Error.Level;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.service.IMetaDataService;
import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * Class SpecificationServiceImplTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource(value = "classpath:application.properties")
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class SpecificationServiceImplTest extends AbstractRestTest {

    /** La constante UID_AUTHOR. */
    private static final String UID_AUTHOR = "2016-11-USR-AAA-42";

    /** La constante UID_SPEC. */
    private static final String UID_SPEC = "2016-10-XRT-BYD-35";

    @Autowired
    private ISpecificationDataService specificationDataService;

    @Autowired
    private IReferenceDataService referenceDataService;

    @Autowired
    private IMetaDataService metaDataService;

    /** dozer. */
    @Autowired
    private Mapper dozerMapper;

    private static String ROOT_DIRECTORY = "target/ge/records";

    private static String ERROR_DIRECTORY = "target/errors";

    private File rootdirectory = null;

    private File errorDirectory = null;

    @Value("${nash.directory.watcher:exclude}")
    private String useWatcher;

    @Autowired
    private Engine engine;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.specificationDataService, this.referenceDataService, this.metaDataService, this.engine);

        this.rootdirectory = new File(ROOT_DIRECTORY);
        this.rootdirectory.mkdirs();

        this.errorDirectory = new File(ERROR_DIRECTORY);
        this.errorDirectory.mkdirs();

        when(this.engine.loader(any())).thenCallRealMethod();
    }

    /**
     * Test conversion.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testConversion() throws Exception {
        final SpecificationBean dataBean = buildFormSpecificationBean();
        final SpecificationInfoBean actual = this.dozerMapper.map(dataBean, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(dataBean.getUid())), //
                                hasProperty("title", equalTo(dataBean.getName())), //
                                hasProperty("description", equalTo(dataBean.getDescription())), //
                                hasProperty("reference", equalTo(dataBean.getReference())), //
                                hasProperty("revision", equalTo(dataBean.getRevision())), //
                                hasProperty("version", equalTo(dataBean.getVersion())), //
                                hasProperty("tagged", equalTo(dataBean.isTagged())), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );
    }

    /**
     * Test info.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInfo() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        Mockito.when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification/code/{code}", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Status.OK.getStatusCode())));

        final SpecificationInfoBean actual = this.readAsBean(response, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(dataBean.getUid())), //
                                hasProperty("title", equalTo(dataBean.getName())), //
                                hasProperty("description", equalTo(dataBean.getDescription())), //
                                hasProperty("reference", equalTo(dataBean.getReference())), //
                                hasProperty("revision", equalTo(dataBean.getRevision())), //
                                hasProperty("version", equalTo(dataBean.getVersion())), //
                                hasProperty("tagged", equalTo(dataBean.isTagged())), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );

    }

    /**
     * Test info unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testInfoUnknown() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        Mockito.when(this.specificationDataService.load(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification/code/{code}", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Status.NO_CONTENT.getStatusCode())));

        verify(this.specificationDataService).load(dataBean.getUid());

        assertThat(response, hasProperty("entity", nullValue()));

    }

    /**
     * Test upload.
     *
     * @throws Exception
     *             exception
     */
    @Test
    @Ignore
    public void testUpload() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"))).thenReturn(SpecificationServiceImplTest.buildFormSpecificationBean());

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .type("application/zip") //
                .query("author", UID_AUTHOR) //
                .query("group", "all") //
                .post(resourceAsBytes);

        verify(this.specificationDataService).fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"));

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
        assertThat(response.getHeaderString("Location"), equalTo(ENDPOINT + "/v1/Specification/2016-10-XRT-BYD-35"));
    }

    /**
     * Test upload error.
     *
     * @throws Exception
     *             exception
     */
    @Test
    @Ignore
    public void testUploadError() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"))).thenThrow(new FunctionalException("Oops"));

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .type("application/zip") //
                .query("author", UID_AUTHOR) //
                .query("group", "all") //
                .post(resourceAsBytes);

        verify(this.specificationDataService).fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"));

        assertThat(response, hasProperty("status", equalTo(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())));
        assertThat(this.readAsString(response), equalTo("Unable to create resource from uploaded content"));
    }

    /**
     * Tests {@link SpecificationServiceImpl}.
     */
    @Test
    @Ignore
    public void testUploadValidation() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("validation.zip");

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .type("application/zip") //
                .query("author", UID_AUTHOR) //
                .post(resourceAsBytes);

        verify(this.specificationDataService, never()).fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"));

        final Errors errors = this.readAsBean(response, Errors.class);
        assertThat(response, hasProperty("status", equalTo(Response.Status.BAD_REQUEST.getStatusCode())));
        assertThat(errors.getGlobalErrors(), hasSize(0));
        assertThat(errors.getObjectErrors().values(), hasSize(1));
        final Object[] objectErrors = errors.getObjectErrors("1-data/data.xml").toArray();
        assertThat(objectErrors.length, equalTo(2));
        assertThat((Error) objectErrors[0], notNullValue());
        final Error error = (Error) objectErrors[1];
        assertThat(error.getLevel(), equalTo(Level.FATAL));
        assertThat(error.getMessage(), equalTo("[47,7] Le type d'élément \"grrRRrrRrrroup\" doit se terminer par la balise de fin correspondante \"</grrRRrrRrrroup>\"."));
    }

    /**
     * Test upload empty.
     *
     * @throws Exception
     *             exception
     */
    @Test
    @Ignore
    public void testUploadEmpty() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");
        when(this.specificationDataService.fromFile(eq(null), any(byte[].class), eq(true), eq(UID_AUTHOR), eq("all"))).thenReturn(null);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .type("application/zip") //
                .query("author", UID_AUTHOR) //
                .query("group", "all") //
                .post(resourceAsBytes);

        verify(this.specificationDataService).fromFile(eq(null), any(byte[].class), eq(false), eq(UID_AUTHOR), eq("all"));

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(response, hasProperty("entity", nullValue()));
    }

    /**
     * Test upload and publish.
     *
     * @throws Exception
     *             exception
     */
    @Test
    @Ignore
    public void testUploadAndPublish() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.fromFile(eq(null), any(byte[].class), eq(true), eq(UID_AUTHOR), eq("all"))).thenReturn(SpecificationServiceImplTest.buildFormSpecificationBean());

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .query("published", "yes") //
                .type("application/zip") //
                .query("author", UID_AUTHOR) //
                .query("group", "all") //
                .post(resourceAsBytes);

        final ArgumentCaptor<byte[]> resourceCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(this.specificationDataService).fromFile(eq(null), resourceCaptor.capture(), eq(true), eq(UID_AUTHOR), eq("all"));

        assertThat(response, hasProperty("status", equalTo(Response.Status.CREATED.getStatusCode())));
        assertThat(response.getHeaderString("Location"), equalTo(ENDPOINT + "/v1/Specification/2016-10-XRT-BYD-35"));
    }

    /**
     * Test zip download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownload() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").lastModified(Instant.now()).asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(resourceAsBytes));
    }

    /**
     * Test zip download unknown spec.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadUnknownSpec() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download no file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadNoFile() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download no source.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testZipDownloadNoSource() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        dataBean.setSourceId(null);

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownload() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/description.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(this.resourceAsBytes("simple/description.xml")));
    }

    /**
     * Test download sub file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadSubFile() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/1-data/data.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(this.resourceAsBytes("simple/1-data/data.xml")));
    }

    /**
     * Test download unknown spec.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadUnknownSpec() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/description.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download no zip file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadNoZipFile() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/description.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download no source.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadNoSource() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        dataBean.setSourceId(null);

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/description.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test download no sub file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadNoSubFile() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/zip").path("/v1/Specification/code/{code}/file/meta.xml", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download by reference.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadByReference() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");

        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").lastModified(Instant.now()).asBytes(resourceAsBytes));

        final Response response = this.client().accept("application/zip").path("/v1/Specification/ref/{reference}", dataBean.getReference()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        assertThat(this.readAsBytes(response), equalTo(resourceAsBytes));
    }

    /**
     * Test zip download by reference and no file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadByReferenceNoFile() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/ref/{reference}", dataBean.getReference()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download by reference and no source.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadByReferenceNoSource() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        dataBean.setSourceId(null);

        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(dataBean);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/ref/{reference}", dataBean.getReference()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test zip download by reference and unknown spec.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadByReferenceUnknownSpec() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(null);

        final Response response = this.client().accept("application/zip").path("/v1/Specification/ref/{reference}", dataBean.getReference()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test search.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearch() throws Exception {
        final SpecificationInfoBean dataBean = this.buildSpecificationInfoBean();
        final SearchResult<SpecificationInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()).addOrder("name", "desc");
        when(this.specificationDataService.search(any(long.class), any(long.class), any(), any(), any(), any(), eq(SpecificationInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .query("orders", "name:desc") //
                .get();

        final SearchResult<SpecificationInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<SpecificationInfoBean>>() {
        });

        verify(this.specificationDataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), any(), any(), any(), eq(SpecificationInfoBean.class));

        assertThat(actualSearchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                        hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                        hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
                ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo(dataBean.getTitle())), //
                                        hasProperty("description", equalTo(dataBean.getDescription())), //
                                        hasProperty("reference", equalTo(dataBean.getReference())), //
                                        hasProperty("revision", equalTo(dataBean.getRevision())), //
                                        hasProperty("version", equalTo(dataBean.getVersion())), //
                                        hasProperty("tagged", equalTo(dataBean.isTagged())) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test search no orders.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchNoOrders() throws Exception {
        final SpecificationInfoBean dataBean = this.buildSpecificationInfoBean();
        final SearchResult<SpecificationInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults());
        when(this.specificationDataService.search(any(long.class), any(long.class), any(), any(), any(), any(), eq(SpecificationInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .get();

        final SearchResult<SpecificationInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<SpecificationInfoBean>>() {
        });

        verify(this.specificationDataService).search(eq(searchQuery.getStartIndex()), eq(searchQuery.getMaxResults()), any(), any(), any(), any(), eq(SpecificationInfoBean.class));

        assertThat(actualSearchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                        hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                        hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
                ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo(dataBean.getTitle())), //
                                        hasProperty("description", equalTo(dataBean.getDescription())), //
                                        hasProperty("reference", equalTo(dataBean.getReference())), //
                                        hasProperty("revision", equalTo(dataBean.getRevision())), //
                                        hasProperty("version", equalTo(dataBean.getVersion())), //
                                        hasProperty("tagged", equalTo(dataBean.isTagged())), //
                                        hasProperty("size", equalTo(21748L)) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test search filter operator.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchFilterOperator() throws Exception {
        final SpecificationInfoBean dataBean = this.buildSpecificationInfoBean();

        final SearchResult<SpecificationInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()) //
                .addFilter("created", ">=", new DateTime(dataBean.getCreated()).toString("yyyy-MM-dd")) //
                .addOrder("name", "desc");
        when(this.specificationDataService.search(any(long.class), any(long.class), any(), any(), any(), any(), eq(SpecificationInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .query("filters", searchQuery.getFilters().get(0)) //
                .query("orders", searchQuery.getOrders().get(0)) //
                .get();

        final SearchResult<SpecificationInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<SpecificationInfoBean>>() {
        });

        verify(this.specificationDataService).search(1L, 5L, searchQuery.getFilters(), searchQuery.getOrders(), Collections.emptyList(), null, SpecificationInfoBean.class);

        assertThat(actualSearchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                        hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                        hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
                ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo(dataBean.getTitle())), //
                                        hasProperty("description", equalTo(dataBean.getDescription())), //
                                        hasProperty("reference", equalTo(dataBean.getReference())), //
                                        hasProperty("revision", equalTo(dataBean.getRevision())), //
                                        hasProperty("version", equalTo(dataBean.getVersion())), //
                                        hasProperty("tagged", equalTo(dataBean.isTagged())) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testRemove() throws Exception {
        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC).delete();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));

        verify(this.specificationDataService).remove(UID_SPEC);
    }

    /**
     * Test persist tagged.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistTagged() throws Exception {
        final SpecificationBean previousDataBean = buildFormSpecificationBean();
        previousDataBean.setTagged(false);

        final SpecificationBean updatedDataBean = buildFormSpecificationBean();
        updatedDataBean.setTagged(true);

        final SpecificationInfoBean bean = this.buildSpecificationInfoBean().setTagged(true);

        when(this.specificationDataService.load(updatedDataBean.getUid())).thenReturn(previousDataBean, updatedDataBean);

        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC) //
                .type(MediaType.APPLICATION_JSON) //
                .accept(MediaType.APPLICATION_JSON) //
                .put(bean);

        final SpecificationInfoBean actual = this.readAsBean(response, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(updatedDataBean.getUid())), //
                                hasProperty("title", equalTo(updatedDataBean.getName())), //
                                hasProperty("description", equalTo(updatedDataBean.getDescription())), //
                                hasProperty("reference", equalTo(updatedDataBean.getReference())), //
                                hasProperty("revision", equalTo(updatedDataBean.getRevision())), //
                                hasProperty("version", equalTo(updatedDataBean.getVersion())), //
                                hasProperty("tagged", equalTo(true)), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );

        verify(this.specificationDataService).publish(previousDataBean.getUid());
        verify(this.specificationDataService, times(0)).unpublish(previousDataBean.getUid());
    }

    /**
     * Test persist not tagged.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistNotTagged() throws Exception {
        final SpecificationBean previousDataBean = buildFormSpecificationBean();
        previousDataBean.setTagged(true);

        final SpecificationBean updatedDataBean = buildFormSpecificationBean();
        updatedDataBean.setTagged(false);

        final SpecificationInfoBean bean = this.buildSpecificationInfoBean().setTagged(false);

        when(this.specificationDataService.load(previousDataBean.getUid())).thenReturn(previousDataBean, updatedDataBean);

        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC) //
                .type(MediaType.APPLICATION_JSON) //
                .accept(MediaType.APPLICATION_JSON) //
                .put(bean);

        final SpecificationInfoBean actual = this.readAsBean(response, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(previousDataBean.getUid())), //
                                hasProperty("title", equalTo(previousDataBean.getName())), //
                                hasProperty("description", equalTo(previousDataBean.getDescription())), //
                                hasProperty("reference", equalTo(previousDataBean.getReference())), //
                                hasProperty("revision", equalTo(previousDataBean.getRevision())), //
                                hasProperty("version", equalTo(previousDataBean.getVersion())), //
                                hasProperty("tagged", equalTo(false)), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );

        verify(this.specificationDataService, times(0)).publish(previousDataBean.getUid());
        verify(this.specificationDataService).unpublish(previousDataBean.getUid());
    }

    /**
     * Test persist unknown.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistUnknown() throws Exception {
        final SpecificationInfoBean bean = this.buildSpecificationInfoBean();

        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC) //
                .type(MediaType.APPLICATION_JSON) //
                .accept(MediaType.APPLICATION_JSON) //
                .put(bean);

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));

        verify(this.specificationDataService, times(0)).publish(bean.getCode());
        verify(this.specificationDataService, times(0)).unpublish(bean.getCode());
    }

    /**
     * Test persist tagged unmodified.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistTaggedUnmodified() throws Exception {
        final SpecificationBean previousDataBean = buildFormSpecificationBean();
        previousDataBean.setTagged(true);

        final SpecificationBean updatedDataBean = buildFormSpecificationBean();
        updatedDataBean.setTagged(true);

        final SpecificationInfoBean bean = this.buildSpecificationInfoBean().setTagged(true);

        when(this.specificationDataService.load(previousDataBean.getUid())).thenReturn(previousDataBean, updatedDataBean);

        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC) //
                .type(MediaType.APPLICATION_JSON) //
                .accept(MediaType.APPLICATION_JSON) //
                .put(bean);

        final SpecificationInfoBean actual = this.readAsBean(response, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(previousDataBean.getUid())), //
                                hasProperty("title", equalTo(previousDataBean.getName())), //
                                hasProperty("description", equalTo(previousDataBean.getDescription())), //
                                hasProperty("reference", equalTo(previousDataBean.getReference())), //
                                hasProperty("revision", equalTo(previousDataBean.getRevision())), //
                                hasProperty("version", equalTo(previousDataBean.getVersion())), //
                                hasProperty("tagged", equalTo(true)), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );

        verify(this.specificationDataService, times(0)).publish(previousDataBean.getUid());
        verify(this.specificationDataService, times(0)).unpublish(previousDataBean.getUid());
    }

    /**
     * Test persist not tagged unmodified.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testPersistNotTaggedUnmodified() throws Exception {
        final SpecificationBean previousDataBean = buildFormSpecificationBean();
        previousDataBean.setTagged(false);

        final SpecificationBean updatedDataBean = buildFormSpecificationBean();
        updatedDataBean.setTagged(false);

        final SpecificationInfoBean bean = this.buildSpecificationInfoBean().setTagged(false);

        when(this.specificationDataService.load(previousDataBean.getUid())).thenReturn(previousDataBean, updatedDataBean);

        final Response response = this.client().path("/v1/Specification/code/{code}", UID_SPEC) //
                .type(MediaType.APPLICATION_JSON) //
                .accept(MediaType.APPLICATION_JSON) //
                .put(bean);

        final SpecificationInfoBean actual = this.readAsBean(response, SpecificationInfoBean.class);

        assertThat(actual, //
                allOf( //
                        Arrays.asList( //
                                hasProperty("code", equalTo(previousDataBean.getUid())), //
                                hasProperty("title", equalTo(previousDataBean.getName())), //
                                hasProperty("description", equalTo(previousDataBean.getDescription())), //
                                hasProperty("reference", equalTo(previousDataBean.getReference())), //
                                hasProperty("revision", equalTo(previousDataBean.getRevision())), //
                                hasProperty("version", equalTo(previousDataBean.getVersion())), //
                                hasProperty("tagged", equalTo(false)), //
                                hasProperty("size", equalTo(21748L)) //
                        ) //
                ) //
        );

        verify(this.specificationDataService, times(0)).publish(previousDataBean.getUid());
        verify(this.specificationDataService, times(0)).unpublish(previousDataBean.getUid());
    }

    /**
     * Build specification info bean.
     *
     * @return specification info bean
     */
    private SpecificationInfoBean buildSpecificationInfoBean() {
        return this.dozerMapper.map(buildFormSpecificationBean(), SpecificationInfoBean.class);
    }

    /**
     * Build form specification bean.
     *
     * @return specification bean
     */
    public static SpecificationBean buildFormSpecificationBean() {
        final Calendar now = Calendar.getInstance();

        return new SpecificationBean() //
                .setGroupe("all") //
                .setName("QP039") //
                .setDescription("QP039 registering form") //
                .setUid(UID_SPEC) //
                .setReference("QP039/REF") //
                .setRevision(1L) //
                .setVersion("1.2") //
                .setTagged(false) //
                .setCreated(now) //
                .setUpdated(now) //
                .setSize(21748L) //
                .setSourceId(42L) //
        ;
    }

    /**
     * Test search filter operator.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchWithFilterValues() throws Exception {
        final SpecificationInfoBean dataBean = this.buildSpecificationInfoBean();

        final SearchResult<SpecificationInfoBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()) //
                .addOrder("name", "desc") //
                .addFilter("groupe", ":", Arrays.asList("all")); //
        when(this.specificationDataService.search(any(long.class), any(long.class), any(), any(), any(), any(), eq(SpecificationInfoBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept(MediaType.APPLICATION_JSON).path("/v1/Specification") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .query("filters", searchQuery.getFilters().get(0)) //
                .query("orders", searchQuery.getOrders().get(0)) //
                .get();

        final SearchResult<SpecificationInfoBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<SpecificationInfoBean>>() {
        });

        verify(this.specificationDataService).search(1L, 5L, searchQuery.getFilters(), searchQuery.getOrders(), Collections.emptyList(), null, SpecificationInfoBean.class);

        assertThat(actualSearchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                        hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                        hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
                ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("groupe", equalTo(dataBean.getGroupe())), //
                                        hasProperty("code", equalTo(dataBean.getCode())), //
                                        hasProperty("title", equalTo(dataBean.getTitle())), //
                                        hasProperty("description", equalTo(dataBean.getDescription())), //
                                        hasProperty("reference", equalTo(dataBean.getReference())), //
                                        hasProperty("revision", equalTo(dataBean.getRevision())), //
                                        hasProperty("version", equalTo(dataBean.getVersion())), //
                                        hasProperty("tagged", equalTo(dataBean.isTagged())) //
                                ) //
                        ) //
                ) //
        );
    }

    /**
     * Test template.json download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testTemplateDownloadByCode() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceZipAsBytes = this.resourceAsBytes("simple.zip");
        final String resourceJsonAsString = this.resourceAsString("simple.json");

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").lastModified(Instant.now()).asBytes(resourceZipAsBytes));

        final Response response = this.client().accept("application/json").path("/v1/Specification//template/code/{code}", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final String resourceResponseAsString = new String(this.readAsBytes(response));
        assertThat(resourceResponseAsString, equalTo(resourceJsonAsString));
    }

    /**
     * Test download unknown template.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testDownloadUnknownTemplateByCode() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();

        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(null);

        final Response response = this.client().accept("application/json").path("/v1/Specification/template/code/{code}", dataBean.getUid()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.NO_CONTENT.getStatusCode())));
        assertThat(this.readAsBytes(response), nullValue());
    }

    /**
     * Test template.json download.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testTemplateDownloadByReference() throws Exception {
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        final byte[] resourceZipAsBytes = this.resourceAsBytes("simple.zip");
        final String resourceJsonAsString = this.resourceAsString("simple.json");

        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(dataBean);
        when(this.specificationDataService.resource(dataBean.getUid())).thenReturn(new FileEntry(dataBean.getUid() + ".zip").lastModified(Instant.now()).asBytes(resourceZipAsBytes));

        final Response response = this.client().accept("application/json").path("/v1/Specification/template/ref/{reference}", dataBean.getReference()).get();

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final String resourceResponseAsString = new String(this.readAsBytes(response));
        assertThat(resourceResponseAsString, equalTo(resourceJsonAsString));
    }

    /**
     * Test update priority by specification code.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpdatePriorityByCode() throws Exception {
        // -->prepare
        final Long priority = 10L;
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.specificationDataService.load(dataBean.getUid())).thenReturn(dataBean);
        when(this.specificationDataService.updatePriorityByCode(dataBean.getUid(), priority.intValue())).thenReturn(dataBean.setPriority(priority));

        // -->call
        final Response response = this.client().accept("application/json").path("/v1/Specification/code/{code}/priority/{priority}", dataBean.getUid(), priority.intValue()).put(null);

        // -->verify
        verify(this.specificationDataService).load(eq(dataBean.getUid()));
        verify(this.specificationDataService).updatePriorityByCode(eq(dataBean.getUid()), eq(priority.intValue()));

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final SpecificationInfoBean specificationInfoBean = this.readAsBean(response, SpecificationInfoBean.class);
        assertThat(specificationInfoBean, //
                hasProperty("priority", equalTo(priority)) //
        );
    }

    /**
     * Test update priority by specification reference.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testUpdatePriorityByReference() throws Exception {
        // -->prepare
        final Long priority = 10L;
        final SpecificationBean dataBean = SpecificationServiceImplTest.buildFormSpecificationBean();
        when(this.specificationDataService.loadByReference(dataBean.getReference())).thenReturn(dataBean);
        when(this.specificationDataService.updatePriorityByReference(dataBean.getReference(), priority.intValue())).thenReturn(dataBean.setPriority(priority));

        // -->call
        final Response response = this.client().accept("application/json").path("/v1/Specification/ref/{reference}/priority/{priority}", dataBean.getReference(), priority.intValue()).put(null);

        // -->verify
        verify(this.specificationDataService).loadByReference(eq(dataBean.getReference()));
        verify(this.specificationDataService).updatePriorityByReference(eq(dataBean.getReference()), eq(priority.intValue()));

        assertThat(response, hasProperty("status", equalTo(Response.Status.OK.getStatusCode())));
        final SpecificationInfoBean specificationInfoBean = this.readAsBean(response, SpecificationInfoBean.class);
        assertThat(specificationInfoBean, //
                hasProperty("priority", equalTo(priority)) //
        );
    }
}
