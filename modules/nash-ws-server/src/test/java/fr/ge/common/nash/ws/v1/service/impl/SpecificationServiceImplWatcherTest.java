/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.utils.test.AbstractRestTest;
import fr.ge.common.utils.watcher.IProcessEvent;
import fr.ge.common.utils.watcher.ProcessEventResult;

/**
 * Class SpecificationServiceImplTest.
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/engine-context.xml", "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/rest-services-context.xml" })
public class SpecificationServiceImplWatcherTest extends AbstractRestTest {

    /** La constante UID_AUTHOR. */
    private static final String UID_AUTHOR = "2016-11-USR-AAA-42";

    /** data service. */
    @Autowired
    private ISpecificationDataService dataService;

    /** The service. */
    @Autowired
    private IProcessEvent service;

    @Autowired
    private Engine engine;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        reset(this.dataService, this.engine);
    }

    /**
     * Test process.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testProcess() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("simple.zip");
        when(this.dataService.fromFile(any(), any(byte[].class), eq(false), eq(UID_AUTHOR), any())).thenReturn(SpecificationServiceImplTest.buildFormSpecificationBean());
        when(this.engine.loader(any())).thenCallRealMethod();
        final ProcessEventResult processEventResult = this.service.process(resourceAsBytes, UID_AUTHOR);
        assertThat(processEventResult, hasProperty("status", equalTo(ProcessEventResult.Result.SUCCESS)));
    }

}
