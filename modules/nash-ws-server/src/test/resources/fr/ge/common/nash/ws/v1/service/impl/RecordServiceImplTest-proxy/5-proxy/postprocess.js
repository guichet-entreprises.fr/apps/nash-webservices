// create input.xml for finFuncIdReceiver

var type_auth = "Conseil en propriété industrielle";
var codePostalAuth = $qp083PE1.adresse.adresseContact.codePostalAdresseDeclarant

var attachment = "/3-review/generated/generated.formulaire-1-Conseil en propriete industrielle.pdf";


return spec.create({
		id: 'prepareFindfuncIdReceiver',
		label: "Conseil en propriété industrielle - établissement de l'autorité compétente",
		groups: [spec.createGroup({
				id: 'authority',
				label: "Authority",
				data: [spec.createData({
						id: 'authorityType',
						label: "Le type d'autorité",
						type: 'String',
						mandatory: true,
						value: type_auth
					}), spec.createData({
						id: 'zipCode',
						label: "Le code postal",
						type: 'String',
						mandatory: true,
						value: codePostalAuth
					}), spec.createData({
						id: 'attachment',
						label: "Le chemin de la pièce jointe",
						type: 'String',
						mandatory: true,
						value: attachment
					})
				]
			})]
});

