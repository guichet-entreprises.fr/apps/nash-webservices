var lastName = ($p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != "" && $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  != null)  ? $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomUsage  :  $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiqueNomNaissance;
var firstName = $p2p4modif.cadre1RappelIdentificationGroup.cadre1RappelIdentification.personneLieePersonnePhysiquePrenom1[0];			
var description = nash.record.description().title; 
var recordUid = nash.record.description().recordUid; 
var civility = " ";

//Récupérer les informations de l'utilisateur connecté

var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author) //
.accept('text') //
.get();

var user = userResponse.asObject();
_log.info("user  is {}", user);

var email = user.email;
_log.info("user  is {}", user);
_log.info("user email is {}", email);
_log.info("civility is {}", civility);
_log.info("lastName is {}", lastName);
_log.info("firstName is {}", firstName);
_log.info("description is {}", description);
_log.info("recordUid is {}", recordUid);

var items = [];

for(var i=0; i<_INPUT_.fraisInformation.fraisDestinations.size(); i++){
	
	//Récupérer les informations Autorité + Frais

	var coupleFraisDestination = _INPUT_.fraisInformation.fraisDestinations[i];
	_log.info("coupleFraisDestination  is {}", coupleFraisDestination);

    var authorityCode = coupleFraisDestination.authorityCode;
	_log.info("authorityCode  is {}", authorityCode);	
	
	var authorityLabel = coupleFraisDestination.authorityLabel;
	_log.info("authorityLabel  is {}", authorityLabel);	
		
	var fraisCode = coupleFraisDestination.fraisCode;
	_log.info("fraisCode  is {}", fraisCode);	
		
	var fraisLabel = coupleFraisDestination.fraisLabel;
	_log.info("fraisLabel  is {}", fraisLabel);	
		
	var	fraisPrix = coupleFraisDestination.fraisPrix;
	_log.info("fraisPrix  is {}", fraisPrix);

	//Vérification du partner ID pour l'autorité 
	
	var partnerID = null;

	// call directory with funcId to find all information of Authorithy
	var response = nash.service.request('${directory.baseUrl}/v1/authority/{funcId}', authorityCode) //
				   .connectionTimeout(10000) //
				   .receiveTimeout(10000) //
				   .accept('json') //
				   .get();

	//result			     
	var receiverInfo = response.asObject();
	_log.info("receiverInfo is  {}",receiverInfo);
	// prepare all information of receiver to create data.xml

	var funcId = !receiverInfo.entityId ? null : receiverInfo.entityId;
	var funcLabel = !receiverInfo.label ? null :receiverInfo.label;
	var details = !receiverInfo.details ? null : receiverInfo.details;
	_log.info("details is  {}",details);
	var ediCode = !details.ediCode ? null : details.ediCode;
	var email = !details.profile.email ? null : details.profile.email;
	var tel = !details.profile.tel ? null : details.profile.tel;

	_log.info("adress is  {}",details.profile.address);
	_log.info("email is  {}", email);
	_log.info("recipientName is  {}",details.profile.address.recipientName);
	_log.info("addressNameCompl is  {}",details.profile.address.addressNameCompl);
	_log.info("cityNumber is  {}",details.profile.address.cityNumber);

	var nomAuth = !details.profile.address.recipientName ? null : details.profile.address.recipientName;
	var authCP = !details.profile.address.cityNumber ? null : details.profile.address.cityNumber;

	var authAddressNameCompl = !details.profile.address.addressNameCompl ? null : details.profile.address.addressNameCompl;
	var authSpecial = !details.profile.address.special ? null : details.profile.address.special;
	var authCity = !details.profile.address.cityName ? null : details.profile.address.cityName;
	var postalCode = !details.profile.address.postalCode ? null : details.profile.address.postalCode;
	var authAdress = authAddressNameCompl + " " + authSpecial;
	var attachment = attachment ? null : attachment;
	
var item=	{  
				"name":fraisCode,
				"price": fraisPrix,
				"description":"Paiement du service :"+fraisLabel,
				"partnerLabel" : funcLabel,
				"ediCode" : ediCode	
			};

items.push(item);

}
_log.info("items is {}", items);

//TODO : Les informations du payeur devront être issues de la formalité

var cart = {    

				"recordUid" : recordUid,  
				"dossierId" : recordUid,
				"email" : user.email,
				"userCivility": ""+civility,
				"userLastName": lastName,
				"userFirstName": firstName,
				"description": description,
				"paymentType":"DB",
				"items":items,	                    
				"reference": recordUid
		};

_log.info("cart is {}", cart);
		
var nfo = nash.hangout.pay({'cart' : JSON.stringify(cart)});

return nfo;