var recordUid = nash.record.description().recordUid;

_log.info("************INPUT : {}************", _INPUT_);
	
_log.info("************Fromalité payante!************");	
// check inout information
var proxyResult = _INPUT_.proxyResult;
_log.info("proxyResult is {}", proxyResult);
_log.info("proxyResult status  is {}", proxyResult.status);	
	


var nameToCheck = recordUid+"_recu.pdf";
	_log.info("nameToCheck is {}", nameToCheck);
	var fileExists = false ;
	if (null != proxyResult.files) {
		for(var i=0; i<proxyResult.files.length; i++){
		var indice = i+1;
		_log.info("label is {}", proxyResult.files[i].label);
		_log.info("proxyResult {} element is {}", i, proxyResult.files[i]);
			if(proxyResult.files[i].label.indexOf(nameToCheck)!=-1){
				var	fileName = "proxyResult.files-"+indice+"-"+proxyResult.files[i].label;
				fileExists = true ;
			}
		}
		_log.info("file name is {}", fileName);
	}
	
			
if (proxyResult.status == 'SUCCESS') {
	
	if (fileExists){

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Génération du ticket de paiement",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Récapitulatif de votre paiement",
									description: "Votre dossier va être transmis au service compétent.",
									data: [
										spec.createData({
											id: 'files',
											description:'Veuillez trouver ci-dessous le récapitulatif de votre paiement.',
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	}else{
		return spec.create({
						id: 'review',
						label: "Génération du ticket de paiement",
						groups: [spec.createGroup({
								id: 'generatedFile',
								label: "Récapitulatif de votre paiement",
								description: "Votre dossier va être transmis au service compétent.",
								data: [	]
							})]
					});	
	}
	

} else {
	
	if (fileExists){

		var metas = [];
		var proxyFiles = proxyResult.files;
		for(var i=0; i<proxyResult.files.size(); i++){
			if (!proxyResult.files.get(i).getLabel().endsWith(".pdf")) {
				proxyFiles.remove(i);
			} else {
				metas.push({'name':'document', 'value': '/'+proxyFiles.get(i).getAbsolutePath()});
			}
		}
		//-->Ajout du recu du paiement dans les méta
		if (metas.length > 0) {
			nash.record.meta(metas);
		}
		
		return spec.create({
							id: 'review',
							label: "Génération du ticket de paiement",
							groups: [spec.createGroup({
									id: 'proxyResult',
									label: "Récapitulatif de votre paiement",
									data: [
										spec.createData({
											id: 'files',
											label : 'Résultat de la tentative de paiement',
											description : "Un problème est survenu lors de la tentative du paiement, veuillez contacter votre support.",
											type: 'FileReadOnly',
											value:  proxyFiles
										})
									]
								})]
						});	
					
	}else{
		return spec.create({
						id: 'paymentResult',
						label: "Paiement en erreur",
						groups: [spec.createGroup({
								id : 'proxyResult',
								label : 'Résultat de la tentative de paiement',
								description : "Un problème est survenu lors de la tentative du paiement, veuillez contacter votre support.",
								data : []
							})]
					});	
	}
}
