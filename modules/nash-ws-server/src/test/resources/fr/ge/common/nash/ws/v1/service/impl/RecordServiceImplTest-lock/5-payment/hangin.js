var recordUid = nash.record.description().recordUid;

_log.info("************INPUT : {}************", _INPUT_);
	
_log.info("************Fromalité payante!************");	
// check inout information
var proxyResult = _INPUT_.proxyResult;
_log.info("proxyResult is {}", proxyResult);
_log.info("proxyResult status  is {}", proxyResult.status);	
	
if( /^(000.000.|000.100.1|000.[36]).*/g.test(proxyResult.status)== true || /^(000.400.0|000.400.100).*/g.test(proxyResult.status)==true || /^(000.200).*/g.test(proxyResult.status)==true || proxyResult.status == 'OK'){

if(proxyResult.status != 'OK'){
	var nameToCheck = recordUid+"_reçu.pdf";
	_log.info("nameToCheck is {}", nameToCheck);
	for(var i=0; i<proxyResult.files.length; i++){
	var indice = i+1;
	_log.info("label is {}", proxyResult.files[i].label);
	_log.info("proxyResult {} element is {}", i, proxyResult.files[i]);
		if(proxyResult.files[i].label.indexOf(nameToCheck)!=-1){
			var	fileName = "proxyResult.files-"+indice+"-"+proxyResult.files[i].label;
		}
	}
	_log.info("file name is {}", fileName);

	var file = nash.doc //
			.load('/5-payment/generated/'+fileName);
	var finalDoc = file.save(fileName);

//try{
			_log.info("Sending Mail....");
			var userResponse = nash.service.request('${account.ge.baseUrl.read}/private/users/{id}', nash.record.description().author).accept('text').get();
			var user = userResponse.asObject();
			_log.info("user object is {}", user);
			
			if(user != null && user.email !=null){
				var sendMail = true;
			}else{
				var sendMail = false;
			}
			_log.info("Send mail --> {}", sendMail);
			
			// Paramétrage du mail 
			if (sendMail == true) {
					_log.info("fileName sendMail is {}", fileName);
					var exchangeSender = _CONFIG_.get('exchange.email.sender');
					_log.info("exchangeSender is {}", exchangeSender);
					var emailTo = user.email;
					_log.info("sending mail to{}", emailTo);
					var prenomDestinataire = user.firstName;
					var nomDestinataire = user.lastName;
					var civiliteDestinataire = user.civility;

					if (prenomDestinataire == null) {prenomDestinataire="";}
					_log.info("prenomDestinataire is {} ", prenomDestinataire);
					if (nomDestinataire == null) {nomDestinataire="";}
					_log.info("nomDestinataire is {} ", nomDestinataire);
					if (civiliteDestinataire == null) {civiliteDestinataire="";}
					_log.info("civiliteDestinataire is {} ", civiliteDestinataire);


					var emailObject = "Reçu de paiement pour le dossier: "+recordUid;
					var attachementFileName="Reçu de paiement.pdf"
					var attachement= "/5-payment/generated/"+fileName;
					_log.info("attachement is {}", attachement);
					var emailContent ="<html> <body style='font-family: Times New Roman,serif;'> <div class='head'> <img style='display: block; margin: 0 auto;' src='https://account.guichet-entreprises.fr/assets/images/guichet-entreprises/logo/long-base.png'> </div> <div class='content' style='color: #555555; width: 900px; margin: 0 auto;'> <div class='dear' style='padding-left: 40px; padding-bottom: 20px;text-transform: capitalize;'>Bonjour "+civiliteDestinataire+" "+prenomDestinataire+" "+nomDestinataire+"</div> <div class='message' style='padding-left: 40px; text-indent: 20px; line-height: 2em;'>Nous vous confirmons la prise en compte de votre paiement sur <a href='https://www.guichet-entreprises.fr/'>www.guichet-entreprises.fr</a>, dans le cadre de votre dossier "+recordUid+". Vous en trouverez le reçu en pièce jointe de ce courriel.</br></br>Nous vous remercions de votre confiance,</br></br>L’équipe Guichet Entreprises </div> </div> </body> </html>";
					// call exchange service to send mail with pj
					_log.info("send mail .......");
					var response = nash.service.request('${exchange.baseUrl}/email/send') //
						.connectionTimeout(2000) //
						.receiveTimeout(5000) //
						.dataType("form") //
						.param("sender", [ exchangeSender ]) //
						.param("recipient", [ emailTo ]) //
						.param("object", [ emailObject ]) //
						.param("content", [ emailContent ]) //
						.param("attachmentPDFName", [ attachementFileName ]) //
						.post({ "file" : nash.util.resourceFromPath(attachement) });

					_log.info("response send mail .......");
					// output to confirm
					_log.info("status is {}", response.status);
			}
			
	return spec.create({
						id: 'review',
						label: "Génération du reçu de paiement",
						groups: [spec.createGroup({
								id: 'generatedFile',
								label: "Récapitulatif de votre paiement",
								description: "Votre dossier va être transmis au service compétent.",
								data: [ spec.createData({
										id: 'reviewMsg',
										description:'Veuillez trouver ci-dessous le récapitulatif de votre paiement.',
										type: 'FileReadOnly',
										value: [ finalDoc ]
									})]
							})]
					});	
}


	
	return spec.create({
			id : 'payment',
			label : 'Paiement électronique',
			groups : [ spec.createGroup({
				id : 'proxyResult',
				label : 'Paiement électronique',
				description : "La piement électronique s'est déroulée avec succès. Veuillez passer à l'étape suivante de votre dossier",
				data : []
			}) ]
		});	
}else if (proxyResult.status == 'CANCEL') {
	return spec.create({
		id : 'payment',
		label : 'Paiement électronique',
		groups : [ spec.createGroup({
			id : 'proxyResult',
			label : 'Résultat',
			warnings : [ { description : 'Une erreur est survenue lors de la piement électronique. En validant cette étape, vous allez re-exécuter la procédure de paiement électronique.' } ],
			data : []
		}) ]
	});	
}else {
		return spec.create({
				id : "paymentResult",
				label : "Payment en erreur",
				groups : [ spec.createGroup({
					id : 'result',
					label : 'Résultat de la tentative de paiement',
					description : "Un problème est survenu lors de la tentative du paiement, veuillez contacter votre support.",
					data : []
				}) ]
			});			
}
