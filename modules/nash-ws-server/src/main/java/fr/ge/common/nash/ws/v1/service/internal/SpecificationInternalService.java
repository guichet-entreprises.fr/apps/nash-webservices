/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.internal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.provider.SpecificationDataServiceProvider;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.ApiParam;

/**
 * The Class SpecificationInternalService.
 *
 * @author Christian Cougourdan
 */
@Service
public class SpecificationInternalService {

    @Autowired
    private ISpecificationDataService service;

    @Autowired
    private Engine engine;

    public SpecificationSearchResult fullTextSearch(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, //
            @ApiParam("roles as \"&lt;entity&gt;&lt;:&gt;&lt;value&gt;\"") final List<SearchQueryFilter> roles, final String searchTerms) {

        final SpecificationSearchResult searchResult = new SpecificationSearchResult(startIndex, maxResults);

        final SearchQueryFilter taggedFilter = new SearchQueryFilter("tagged", ":", "true");
        filters.add(taggedFilter);

        final SearchResult<SpecificationInfoBean> rawSearchResult = this.service.search(startIndex, maxResults, filters, orders, roles, searchTerms, SpecificationInfoBean.class);

        searchResult.setTotalResults(rawSearchResult.getTotalResults());
        searchResult.setContent(rawSearchResult.getContent());

        searchResult.setContent( //
                rawSearchResult.getContent() //
                        .stream() //
                        .map(nfo -> this.map(nfo)) //
                        .collect(Collectors.toList()) //
        );
        return searchResult;
    }

    private SpecificationInfoBean map(final SpecificationInfoBean specificationInfo) {
        return specificationInfo.setMetadata(this.map(this.engine.loader(SpecificationDataServiceProvider.create(this.service, specificationInfo.getCode())).meta()));
    }

    protected Map<String, Collection<String>> map(final FormSpecificationMeta metas) {
        final Map<String, Collection<String>> result = new HashMap<>();

        Optional.ofNullable(metas) //
                .map(FormSpecificationMeta::getMetas) //
                .orElseGet(Collections::emptyList) //
                .forEach(meta -> {
                    Collection<String> values = result.get(meta.getName());
                    if (null == values) {
                        values = new ArrayList<>();
                        result.put(meta.getName(), values);
                    }
                    values.add(meta.getValue());
                });

        return result.isEmpty() ? null : result;
    }
}
