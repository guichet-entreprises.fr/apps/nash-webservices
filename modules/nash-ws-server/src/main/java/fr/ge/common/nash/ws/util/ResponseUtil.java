/**
 *
 */
package fr.ge.common.nash.ws.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.ArrayUtils;

import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.utils.ResourceUtil;

/**
 * Class ResponseUtil.
 *
 * @author bsadil
 */
public final class ResponseUtil {

    /** The Constant DEFAULT_FILE_MAPPING. */
    private static final Map<String, String> DEFAULT_FILE_MAPPING;

    static {
        final Map<String, String> map = new HashMap<>();

        map.put("css/common.css", "/css/common.css");
        map.put("css/description.css", "/css/description.css");
        map.put("css/data.css", "/css/data.css");
        map.put("css/types.css", "/css/types.css");
        map.put("css/process.css", "/css/process.css");
        map.put("css/referential.css", "/css/referential.css");
        map.put("css/history.css", "/css/history.css");
        map.put("css/common.xslt", "/css/common.xslt");
        map.put("css/description.xslt", "/css/description.xslt");
        map.put("css/process.xslt", "/css/process.xslt");

        DEFAULT_FILE_MAPPING = Collections.unmodifiableMap(map);
    }

    /**
     * default constructor.
     */
    private ResponseUtil() {
        // Nothing to do
    }

    /**
     * Default response.
     *
     * @param resourcePath
     *            resource path
     * @return response
     */
    public static Response defaultResponse(final String resourcePath) {
        final byte[] defaultContent = defaultFile(resourcePath);

        if (ArrayUtils.isEmpty(defaultContent)) {
            return Response.noContent().build();
        }

        return Response.ok(defaultContent) //
                .header(HttpHeaders.CONTENT_TYPE, MimeTypeUtil.type(resourcePath)) //
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + resourcePath) //
                .build();
    }

    /**
     * Default file.
     *
     * @param recordFileName
     *            the record file name
     * @return the byte[]
     */
    public static byte[] defaultFile(final String recordFileName) {
        final String defaultFilename = DEFAULT_FILE_MAPPING.get(recordFileName);

        if (null == defaultFilename) {
            return new byte[] {};
        } else {
            return ResourceUtil.resourceAsBytes(defaultFilename, Engine.class);
        }
    }
}
