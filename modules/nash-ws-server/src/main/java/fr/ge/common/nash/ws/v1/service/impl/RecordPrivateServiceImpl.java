/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IRecordPrivateService;
import fr.ge.common.nash.ws.v1.service.internal.RecordInternalService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Class RecordServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Api("private Record services")
@Path("/private/v1/Record")
public class RecordPrivateServiceImpl implements IRecordPrivateService {

    @Autowired
    private RecordInternalService internalService;

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Create a record.")
    public Response upload(@ApiParam("the uploaded record") final byte[] resourceAsBytes, @ApiParam("the author identifier provided by the \"Account\" service header") final String author,
            @ApiParam("ACLs information using \"[entity path]:[role]\" format") final List<SearchQueryFilter> roles) {
        return this.internalService.upload(resourceAsBytes, author, roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RecordInfoBean load(final String code) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(final String code) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response download(final String code, final String resourcePath) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @ApiOperation(value = "upload record with context,this rest api is used by proxies(mandataires)", notes = "this rest api is used by proxies for the creation of records via a backend without going through the interface of the website ")
    public Response uploadWithContext(
            @ApiParam("the reference of specification Example:reference='Formalités SCN/ENT/Création/Déclaration de début d'activité libérale/Micro-entrepreneur'") final String reference,
            @ApiParam("the author of record Example: author='2018-01-DYE-XCH-26'") final String author,
            @ApiParam("the context is json file content all information about record,the structure of json is provided by Guichet-entreprises") final byte[] context,
            @ApiParam("the language of record by default lang ='FR'") final String lang) throws JsonParseException, JsonMappingException, IOException {

        return this.internalService.uploadWithContext(reference, author, Arrays.asList(new SearchQueryFilter(defaultPrefixAuthor + author, "*")), context, lang);
    }

}