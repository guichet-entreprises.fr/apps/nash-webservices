/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.provider;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.FileEntry;

/**
 * Provider for record operations.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordDataServiceProvider implements IProvider {

    /** The files cache. */
    private final Map<String, FileEntry> filesCache = new HashMap<>();

    /** The mapper files cache. */
    private final Map<String, Object> mappedFilescache = new HashMap<>();

    /** The service. */
    private final IRecordDataService dataService;

    /** The record identifier. */
    private final String code;

    /**
     * Instantiates a new record service provider.
     *
     * @param service
     *            the service
     * @param code
     *            the code
     */
    public RecordDataServiceProvider(final IRecordDataService dataService, final String code) {
        this.dataService = dataService;
        this.code = code;
    }

    /**
     * Creates the provider.
     *
     * @param service
     *            the service
     * @param code
     *            the code
     * @return the i specification provider
     */
    public static IProvider create(final IRecordDataService dataService, final String code) {
        return new RecordDataServiceProvider(dataService, code);
    }

    @Override
    public FileEntry load(final String name) {
        final String key = name.startsWith("/") ? name.substring(1) : name;
        FileEntry entry = this.filesCache.get(key);

        if (null == entry) {
            entry = this.dataService.resource(this.code, key);
            this.filesCache.put(key, entry);
        }

        return entry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T asBean(final String name, final Class<T> clazz) {
        T asBean = null;
        if (this.mappedFilescache.containsKey(name)) {
            asBean = CoreUtil.cast(this.mappedFilescache.get(name));
        } else {
            final byte[] asBytes = this.asBytes(name);
            if (ArrayUtils.isNotEmpty(asBytes)) {
                asBean = JaxbFactoryImpl.instance().unmarshal(asBytes, clazz);
            }
            this.mappedFilescache.put(name, asBean);
        }
        return asBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save(final String name, final String mimetype, final byte[] content) {
        final FileBean fileBean = this.dataService.addOrUpdate(this.code, name, mimetype, content);

        final FileEntry entry = this.filesCache.get(name);
        if (null != entry && null != fileBean) {
            entry.asBytes(content);
            entry.lastModified(fileBean.getUpdated().getTime());
        }
        this.mappedFilescache.remove(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(final String name) {
        this.filesCache.remove(name);
        this.mappedFilescache.remove(name);
        final long deleted = this.dataService.remove(this.code, name);
        return deleted > 0;
    }
}