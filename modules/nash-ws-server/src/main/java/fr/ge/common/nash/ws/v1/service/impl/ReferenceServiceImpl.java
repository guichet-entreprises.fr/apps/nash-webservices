/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.nash.ws.v1.bean.ReferenceInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.ReferenceSearchResult;
import fr.ge.common.nash.ws.v1.service.IReferenceService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class ReferenceServiceImpl.
 *
 * @author Adil BSIBISS
 */
@Api
@Path("/v1/Reference")
public class ReferenceServiceImpl implements IReferenceService {

    @Autowired
    private IReferenceDataService service;

    @Override
    @ApiOperation(value = "Search for specification references", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public ReferenceSearchResult search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        final ReferenceSearchResult searchResult = new ReferenceSearchResult(startIndex, maxResults);

        filters.add(new SearchQueryFilter("nb_specs>0"));

        final SearchResult<ReferenceInfoBean> rawSearchResult = this.service.search(startIndex, maxResults, filters, orders, null, null, ReferenceInfoBean.class);

        searchResult.setTotalResults(rawSearchResult.getTotalResults());
        searchResult.setContent(rawSearchResult.getContent());

        return searchResult;
    }

    @Override
    @ApiOperation("Search for full path element for specified reference.")
    public List<ReferenceInfoBean> path(@ApiParam("reference id") final long referenceId) {
        final List<ReferenceInfoBean> lst = new ArrayList<>();

        for (Long refId = referenceId; null != refId;) {
            final ReferenceInfoBean bean = this.service.load(refId, ReferenceInfoBean.class);
            lst.add(0, bean);
            refId = bean.getParent();
        }

        return lst;
    }

}
