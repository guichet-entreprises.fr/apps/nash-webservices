/**
 *
 */
package fr.ge.common.nash.ws.support.dozer;

import java.io.IOException;

import org.dozer.DozerConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author bsadil
 *
 */
public class JsonNodeConverter extends DozerConverter<ObjectNode, JsonNode> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonNodeConverter.class);

    private final ObjectMapper mapper = new ObjectMapper();

    /**
     * @param prototypeA
     * @param prototypeB
     */
    public JsonNodeConverter() {
        super(ObjectNode.class, JsonNode.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JsonNode convertTo(final ObjectNode source, final JsonNode destination) {
        if (source != null) {
            try {
                return this.mapper.readTree(source.toString());
            } catch (final IOException ex) {
                LOGGER.info("Error while converting ObjectNode to JsonNode");
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ObjectNode convertFrom(final JsonNode source, final ObjectNode destination) {
        if (source != null) {
            try {
                return (ObjectNode) this.mapper.readTree(source.asText());
            } catch (final IOException ex) {
                LOGGER.info("Error while converting JsonNode to ObjectNode");
            }
        }

        return null;
    }

}
