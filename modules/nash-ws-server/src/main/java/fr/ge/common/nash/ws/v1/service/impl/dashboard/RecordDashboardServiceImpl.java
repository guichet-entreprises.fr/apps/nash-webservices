/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl.dashboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.ws.data.bean.RecordPropertiesBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.provider.RecordDataServiceProvider;
import fr.ge.common.utils.CoreUtil;
import fr.ge.record.ws.v1.model.LocalizedLabel;
import fr.ge.record.ws.v1.model.LocalizedString;
import fr.ge.record.ws.v1.model.RecordAction;
import fr.ge.record.ws.v1.model.RecordActionTypeEnum;
import fr.ge.record.ws.v1.model.RecordDisplay;
import fr.ge.record.ws.v1.model.RecordResult;
import fr.ge.record.ws.v1.rest.IRecordDisplayRestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 *
 * @author Christian Cougourdan
 */
@Path("/")
@Api("Record display services")
public class RecordDashboardServiceImpl extends AbstractDashboardServiceImpl<RecordResult> implements IRecordDisplayRestService {

    private static final Map<RecordActionTypeEnum, LocalizedLabel> actionLabels;

    static {
        final Map<RecordActionTypeEnum, LocalizedLabel> m = new EnumMap<>(RecordActionTypeEnum.class);
        m.put(RecordActionTypeEnum.READ, getActionLabel("Access file"));
        m.put(RecordActionTypeEnum.HISTORY, getActionLabel("History"));
        m.put(RecordActionTypeEnum.DELETE, getActionLabel("Delete"));
        m.put(RecordActionTypeEnum.DOWNLOAD, getActionLabel("Download"));
        m.put(RecordActionTypeEnum.DOCUMENTS, getActionLabel("Document(s)"));
        actionLabels = Collections.unmodifiableMap(m);
    }

    /** Data service. */
    @Autowired
    private IRecordDataService dataService;

    @Autowired
    private Engine engine;

    @Override
    @ApiOperation("Gets records to display a dashboard.")
    public RecordResult getRecords(@ApiParam("the author identifier") final String authorId, @ApiParam("the index of the element to display") final int startIndex,
            @ApiParam("the maximum number of results per page") final int maxResults, @ApiParam("the sort orders") final String orders) {

        return this.search(authorId, startIndex, maxResults, orders, null, null, null, false, null);
    }

    @Override
    @ApiOperation("Gets records to display a dashboard with full text search on record title.")
    public RecordResult searchRecords(@ApiParam("the author identifier") final String authorId, @ApiParam("the index of the element to display") final int startIndex,
            @ApiParam("the maximum number of results per page") final int maxResults, @ApiParam("the sort orders") final String orders,
            @ApiParam("the full text search query") final String fullTextSearchCriteria) {

        return this.search(authorId, startIndex, maxResults, orders, fullTextSearchCriteria, null, null, null, null);
    }

    @Override
    protected RecordResult buildResult(final String startIndex, final String maxResults, final String totalResults, final Collection<RecordInfoBean> beans, final String engineUserType) {
        final RecordResult recordDashboardResult = new RecordResult();
        recordDashboardResult.setNbResults(maxResults);
        recordDashboardResult.setStartIndex(startIndex);
        recordDashboardResult.setTotalResults(totalResults);

        recordDashboardResult.setRecords( //
                beans //
                        .stream() //
                        .map(this::map) //
                        .collect(Collectors.toList()) //
        );

        return recordDashboardResult;
    }

    private RecordDisplay map(final RecordInfoBean recordInfo) {
        final RecordPropertiesBean properties = this.readProperties(recordInfo.getProperties());

        final RecordDisplay recordDashboardDisplay = new RecordDisplay();
        recordDashboardDisplay.setAuthorId(recordInfo.getAuthor());
        recordDashboardDisplay.setRecordId(recordInfo.getCode());
        recordDashboardDisplay.setTitle(recordInfo.getTitle());
        recordDashboardDisplay.setCreationDate(recordInfo.getCreated().getTime());
        recordDashboardDisplay.setUpdateDate(recordInfo.getUpdated().getTime());
        recordDashboardDisplay.setCompletedStep(String.valueOf(properties.getNbCompletedSteps()));
        recordDashboardDisplay.setTotalStep(String.valueOf(properties.getNbSteps()));
        recordDashboardDisplay.setError(false);

        final SpecificationLoader loader = this.engine.loader(RecordDataServiceProvider.create(this.dataService, recordInfo.getCode()));
        recordDashboardDisplay.setMetadata(this.map(loader.meta()));

        final boolean error = loader.hasErrorState();

        final List<RecordAction> actions = new ArrayList<>();
        if (properties.getNbSteps() == 0 || properties.getNbCompletedSteps() < properties.getNbSteps()) {
            final String stepLabel = CoreUtil.coalesce(loader.stepsMgr().current().getLabel(), RecordActionTypeEnum.READ.name());
            if (stepLabel.equals(RecordActionTypeEnum.READ.name())) {
                actions.add(this.getAction(RecordActionTypeEnum.READ, recordInfo.getCode(), actionLabels.get(RecordActionTypeEnum.READ)));
            } else {
                final LocalizedLabel localizedReadLabel = new LocalizedLabel();
                localizedReadLabel.setActualLabel(stepLabel);
                actions.add(this.getAction(RecordActionTypeEnum.READ, recordInfo.getCode(), localizedReadLabel));
            }
        }

        actions.add(this.getAction(RecordActionTypeEnum.HISTORY, recordInfo.getCode(), actionLabels.get(RecordActionTypeEnum.HISTORY)));

        if (error) {
            recordDashboardDisplay.setError(true);
        }
        if (!error && this.isRemovable(loader)) {
            actions.add(this.getAction(RecordActionTypeEnum.DELETE, recordInfo.getCode(), actionLabels.get(RecordActionTypeEnum.DELETE)));
        }

        this.getDocumentsDownloadAction(recordDashboardDisplay, loader).ifPresent(actions::add);
        recordDashboardDisplay.setActions(actions);

        recordDashboardDisplay.setDetails(recordInfo.getDetails());

        return recordDashboardDisplay;
    }

    private boolean isRemovable(final SpecificationLoader loader) {
        // -->Check if current step is done
        if (Optional.of(loader.stepsMgr().find(loader.currentStepPosition())) //
                .filter(s -> s.getStatus().equals(StepStatusEnum.DONE.getStatus()) && //
                        s.getUser().equals(this.engine.getEngineProperties().get("engine.user.type"))) //
                .map(s -> Boolean.TRUE) //
                .orElse(false)) {
            return false;
        }

        // -->Cannot remove record if all steps are done
        if (Optional.of(loader.stepsMgr().findAll()) //
                .orElse(new ArrayList<>()) //
                .stream() //
                .allMatch(s -> s.getPosition() < loader.currentStepPosition() && //
                        s.getUser().equals(this.engine.getEngineProperties().get("engine.user.type")) && //
                        s.getStatus().equals(StepStatusEnum.DONE.getStatus()) //
                )) {
            return false;
        }

        return Optional.of(loader.stepsMgr().find(loader.currentStepPosition())) //
                .filter(s -> s.getUser().equals(this.engine.getEngineProperties().get("engine.user.type")) && //
                        (null == s.getRemovable() || (null != s.getRemovable() && s.getRemovable() == true)) //
                ) //
                .map(s -> Boolean.TRUE) //
                .orElse(false);
    }

    private Optional<RecordAction> getDocumentsDownloadAction(final RecordDisplay record, final SpecificationLoader loader) {
        final FormSpecificationMeta recordMetas = loader.meta();
        if (null != recordMetas && null != recordMetas.getMetas() && recordMetas.getMetas().stream().map(MetaElement::getName).anyMatch("document"::equals)) {
            return Optional.ofNullable(this.getAction(RecordActionTypeEnum.DOCUMENTS, record.getRecordId(), actionLabels.get(RecordActionTypeEnum.DOWNLOAD)));
        }

        final int nbCompletedSteps = Integer.parseInt(record.getCompletedStep());
        final int nbTotalSteps = Integer.parseInt(record.getTotalStep());

        if (nbCompletedSteps == nbTotalSteps) {
            for (int stepIndex = nbTotalSteps - 1; stepIndex >= 0; stepIndex -= 1) {
                if (loader.addDocumentMeta(stepIndex)) {
                    return Optional.ofNullable(this.getAction(RecordActionTypeEnum.DOCUMENTS, record.getRecordId(), actionLabels.get(RecordActionTypeEnum.DOWNLOAD)));
                }
            }
        }

        return Optional.ofNullable(this.getAction(RecordActionTypeEnum.DOWNLOAD, record.getRecordId(), actionLabels.get(RecordActionTypeEnum.DOWNLOAD)));
    }

    /**
     * Gets an action.
     *
     * @param actionType
     *            the action type
     * @param recordCode
     *            the record code
     * @param localizedLabel
     *            the localized label
     * @return the action
     */
    private RecordAction getAction(final RecordActionTypeEnum actionType, final String recordCode, final LocalizedLabel localizedLabel) {
        final RecordAction action = new RecordAction();
        action.setType(actionType);
        action.setLabel(localizedLabel);
        switch (actionType) {
        case READ:
            action.setUri("/record/" + recordCode);
            break;
        case HISTORY:
            action.setType(RecordActionTypeEnum.HISTORY);
            action.setUri("/record/" + recordCode + "/history");
            break;
        case DOWNLOAD:
            action.setUri("/record/" + recordCode + "/download.zip");
            break;
        case DOCUMENTS:
            action.setType(RecordActionTypeEnum.DOWNLOAD);
            action.setUri("/record/" + recordCode + "/documents.zip");
            break;
        case DELETE:
            action.setType(RecordActionTypeEnum.DELETE);
            action.setUri("/record/" + recordCode + "/remove");
            break;
        default:
            break;
        }
        return action;
    }

    /**
     * Gets an action localized label.
     *
     * @param label
     *            the input label;
     * @return the localized label
     */
    private static LocalizedLabel getActionLabel(final String label) {
        final LocalizedLabel localizedLabel = new LocalizedLabel();
        final List<LocalizedString> localizedStrings = new ArrayList<>();
        localizedStrings.add(new LocalizedString("fr", NashMessageReader.getReader(Locale.FRENCH).getFormatter().format(label)));
        localizedStrings.add(new LocalizedString("en", NashMessageReader.getReader(Locale.ENGLISH).getFormatter().format(label)));
        localizedLabel.setValues(localizedStrings);
        localizedLabel.setDefaultLocale("fr");
        return localizedLabel;
    }

}
