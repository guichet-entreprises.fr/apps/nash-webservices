/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl.dashboard;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.ws.data.bean.RecordPropertiesBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.support.context.AclContext;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.service.IAclService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 *
 * @author Christian Cougourdan
 */
public abstract class AbstractDashboardServiceImpl<T> implements IAclService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDashboardServiceImpl.class);

    private AclContext acls;

    /** Data service. */
    @Autowired
    private IRecordDataService dataService;

    @Autowired
    @Qualifier("fmRestObjectMapper")
    private ObjectMapper objectMapper;

    @Context
    @Override
    public void setAcls(final AclContext acls) {
        this.acls = acls;
    }

    protected T search(final String authorId, final int startIndex, final int maxResults, final String orders, final String fullTextSearchCriteria, final String beginDate, String endDate,
            Boolean archived, String engineUserType) {
        final int max = maxResults < 1 ? Integer.MAX_VALUE : maxResults;

        final List<SearchQueryFilter> searchFilters = new ArrayList<>();
        if (StringUtils.isNotEmpty(authorId)) {
            searchFilters.add(new SearchQueryFilter("author", ":", authorId));
        }
        if (StringUtils.isNotEmpty(beginDate)) {
            searchFilters.add(new SearchQueryFilter("created", ">=", beginDate));
        }
        if (StringUtils.isNotEmpty(endDate)) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                c.setTime(sdf.parse(endDate));
                c.add(Calendar.DATE, 1);
                endDate = sdf.format(c.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            searchFilters.add(new SearchQueryFilter("created", "<=", endDate));
        }

        if (null != archived) {
            searchFilters.add(new SearchQueryFilter("stepId", ":", "archived"));

            Collection<String> status = new ArrayList<String>();
            status.add(StepStatusEnum.TO_DO.getStatus());
            status.add(StepStatusEnum.IN_PROGRESS.getStatus());

            if (archived) {
                status.add(StepStatusEnum.DONE.getStatus());
            }
            searchFilters.add(new SearchQueryFilter("stepStatus", ":", status));
        }

        Optional.ofNullable(engineUserType).filter(StringUtils::isNotEmpty).map(user -> new SearchQueryFilter("stepUser", ":", user)).map(searchFilters::add);

        final List<SearchQueryOrder> searchOrders = new ArrayList<>();
        if ("update".equals(orders)) {
            searchOrders.add(new SearchQueryOrder("updated", "desc"));
        } else if ("title".equals(orders)) {
            searchOrders.add(new SearchQueryOrder("title", "asc"));
        } else if ("createdAsc".equals(orders)) {
            searchOrders.add(new SearchQueryOrder("created", "asc"));
        } else {
            searchOrders.add(new SearchQueryOrder("created", "desc"));
        }

        final String searchTerms = StringUtils.isNotEmpty(fullTextSearchCriteria) ? fullTextSearchCriteria.trim() : null;
        final SearchResult<RecordInfoBean> rawSearchResultRecord = this.dataService.search(startIndex, max, searchFilters, searchOrders, this.acls.asList(), searchTerms, RecordInfoBean.class);

        return this.buildResult( //
                String.valueOf(rawSearchResultRecord.getStartIndex()), //
                String.valueOf(rawSearchResultRecord.getMaxResults()), //
                String.valueOf(rawSearchResultRecord.getTotalResults()), //
                rawSearchResultRecord.getContent(), //
                engineUserType //
        );
    }

    protected abstract T buildResult(final String startIndex, final String maxResults, final String totalResults, final Collection<RecordInfoBean> beans, final String engineUserType);

    /**
     * Read serialized record properties.
     *
     * @param recordProperties
     * @return
     */
    protected RecordPropertiesBean readProperties(final byte[] recordProperties) {
        RecordPropertiesBean properties = null;
        if (recordProperties != null) {
            try {
                properties = this.objectMapper.readValue(recordProperties, RecordPropertiesBean.class);
            } catch (final IOException e) {
                LOGGER.warn("Unable to read record properties", e);
            }
        }

        if (properties == null) {
            return new RecordPropertiesBean();
        } else {
            return properties;
        }
    }

    protected Map<String, Collection<String>> map(final FormSpecificationMeta metas) {
        final Map<String, Collection<String>> result = new HashMap<>();

        Optional.ofNullable(metas) //
                .map(FormSpecificationMeta::getMetas) //
                .orElseGet(Collections::emptyList) //
                .forEach(meta -> {
                    Collection<String> values = result.get(meta.getName());
                    if (null == values) {
                        values = new ArrayList<>();
                        result.put(meta.getName(), values);
                    }
                    values.add(meta.getValue());
                });

        return result.isEmpty() ? null : result;
    }
}
