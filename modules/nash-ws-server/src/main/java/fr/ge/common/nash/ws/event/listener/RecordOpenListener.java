/**
 *
 */
package fr.ge.common.nash.ws.event.listener;

import java.text.MessageFormat;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.ServiceUnavailableException;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.engine.loader.event.RecordOpenEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Tracker history listener.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordOpenListener {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordOpenListener.class);

    /** Tracker facade. */
    @Autowired
    private ITrackerFacade trackerFacade;

    private static final String GE_OPEN_PREFIX = "GE-OPEN-";

    /**
     * Mutateur sur l'attribut {@link #trackerFacade}.
     *
     * @param trackerFacade
     *     la nouvelle valeur de l'attribut trackerFacade
     */
    public void setTrackerFacade(final ITrackerFacade trackerFacade) {
        this.trackerFacade = trackerFacade;
    }

    @Subscribe
    public void onRecordOpenEvent(final RecordOpenEvent event) {
        LOGGER.debug("Trigger RecordOpenListener event");

        final String code = GE_OPEN_PREFIX + event.getLoader().description().getFormUid();
        final String enhancedMessage = event.getLoader().description().getRecordUid();

        try {
            this.trackerFacade.post(code, enhancedMessage);
        } catch (ServiceUnavailableException | NotFoundException | ProcessingException | Fault e) {
            LOGGER.warn(MessageFormat.format("Error calling Tracker with code \"{0}\" and message \"{1}\" : {2}", code, enhancedMessage, e.getMessage()));
            LOGGER.warn(StringUtils.EMPTY, e);
        }
    }

}
