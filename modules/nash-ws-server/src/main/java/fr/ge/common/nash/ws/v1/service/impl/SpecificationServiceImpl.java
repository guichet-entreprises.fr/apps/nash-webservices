/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import java.io.File;
import java.net.URI;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.ArrayUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.i18n.LocaleContextHolder;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.SpecificationPages;
import fr.ge.common.nash.engine.manager.extractor.RecursiveDataModelExtractor;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.util.StylesheetUtil;
import fr.ge.common.nash.engine.validation.Errors;
import fr.ge.common.nash.engine.validation.FormSpecificationValidator;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.support.context.AclContext;
import fr.ge.common.nash.ws.util.ResponseUtil;
import fr.ge.common.nash.ws.v1.bean.SpecificationInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.service.IAclService;
import fr.ge.common.nash.ws.v1.service.ISpecificationService;
import fr.ge.common.nash.ws.v1.service.internal.SpecificationInternalService;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.watcher.IProcessEvent;
import fr.ge.common.utils.watcher.ProcessEventResult;
import fr.ge.common.utils.watcher.ProcessEventResult.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class SpecificationServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Path("/v1/Specification")
@Api("Specification Services")
public class SpecificationServiceImpl implements ISpecificationService, IProcessEvent, IAclService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationServiceImpl.class);

    /** The dozer. */
    @Autowired
    private Mapper dozerMapper;

    /** The data service. */
    @Autowired
    private ISpecificationDataService dataService;

    @Autowired
    private SpecificationInternalService internalService;

    @Autowired
    private Engine engine;

    private AclContext acls;

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * @param acls
     *            the acls to set
     */
    @Context
    @Override
    public void setAcls(final AclContext acls) {
        this.acls = acls;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Creates a specification.")
    public Response upload(@ApiParam("the uploaded specification") final byte[] resourceAsBytes, @ApiParam("must the specification be published ?") final boolean published,
            @ApiParam("the author identifier provided by the \"Account\" service header") final String author, @ApiParam("the group of specification by default group = ALL") final String group,
            @ApiParam("must the specification be persisted ?") final boolean dry) {

        return this.upload(null, resourceAsBytes, published, author, group, dry);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Updates a specification.")
    public Response upload(@ApiParam("the specification identifier") final String code, @ApiParam("the uploaded specification") final byte[] resourceAsBytes,
            @ApiParam("must the specification be published ?") final boolean published, @ApiParam("the author identifier provided by the \"Account\" service header") final String author) {

        return this.upload(code, resourceAsBytes, published, author, null, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Updates a specification.")
    public Response upload(@ApiParam("the specification identifier") final String code, @ApiParam("the uploaded specification") final byte[] resourceAsBytes,
            @ApiParam("must the specification be published ?") final boolean published, @ApiParam("the author identifier provided by the \"Account\" service header") final String author,
            final String group, @ApiParam("must the specification be persisted ?") final boolean dry) {

        final FormSpecificationValidator validator = this.applicationContext.getBean(FormSpecificationValidator.class, LocaleContextHolder.getLocale(), new ZipProvider(resourceAsBytes));
        final Errors errors = validator.validate();
        if (errors.hasErrors()) {
            LOGGER.warn("Specification file is invalid.");
            return Response.status(Status.BAD_REQUEST).entity(errors).build();
        }

        if (dry) {
            return Response.ok().build();
        }

        final SpecificationBean bean;

        try {
            final IProvider provider = new ZipProvider(resourceAsBytes);
            this.insertStaticFiles(provider);
            this.sanitize(this.engine.loader(provider).buildEngineContext());

            bean = this.dataService.fromFile(code, provider.asBytes(), published, author, group);
        } catch (final FunctionalException ex) {
            LOGGER.warn("", ex);
            return Response.serverError().entity("Unable to create resource from uploaded content").build();
        }

        if (null == bean) {
            return Response.noContent().build();
        }

        return Response.created(URI.create("/v1/Specification/" + bean.getUid())).entity(this.dozerMapper.map(bean, SpecificationInfoBean.class)).build();
    }

    /**
     * Insert static files, ie CSS and XSLT.
     *
     * @param provider
     */
    private void insertStaticFiles(final IProvider provider) {
        for (final String resourcePath : StylesheetUtil.stylesheets()) {
            provider.save(resourcePath, ResourceUtil.resourceAsBytes("/" + resourcePath));
        }
    }

    private void sanitize(final EngineContext<SpecificationLoader> engineContext) {
        /*
         * Sanitize root file (description.xml)
         */
        final FormSpecificationDescription description = engineContext.getRecord().description();
        engineContext.getRecord().flush();

        final SpecificationPages pages = engineContext.getRecord().pages();

        /*
         * Sanitize each step
         */
        final List<StepElement> steps = Optional.ofNullable(description) //
                .map(FormSpecificationDescription::getSteps) //
                .map(StepsElement::getElements) //
                .orElseGet(Collections::emptyList);

        for (final StepElement step : steps) {
            final EngineContext<FormSpecificationData> stepDataContext = engineContext.getRecord().buildEngineContext(step);

            this.sanitizeStepData(stepDataContext);
            this.sanitizeProcesses(engineContext.target(step.getPreprocess(), FormSpecificationProcess.class));
            this.sanitizeProcesses(engineContext.target(step.getPostprocess(), FormSpecificationProcess.class));

            pages.create(stepDataContext);
        }
    }

    private void sanitizeStepData(final EngineContext<FormSpecificationData> engineContext) {
        if (null == engineContext) {
            return;
        }

        final FormSpecificationData root = engineContext.getElement();

        engineContext.getBaseProvider().save(root.getResourceName(), root);
    }

    private void sanitizeProcesses(final EngineContext<FormSpecificationProcess> engineContext) {
        if (null == engineContext) {
            return;
        }

        final FormSpecificationProcess root = engineContext.getElement();

        for (final IProcess process : root.getProcesses()) {
            this.sanitizeProcess(engineContext.target(process));
        }

        engineContext.getBaseProvider().save(root.getResourceName(), root);
    }

    private void sanitizeProcess(final EngineContext<IProcess> engineContext) {
        if (null == engineContext) {
            return;
        }

        final IProcess root = engineContext.getElement();

        if (engineContext.getElement() instanceof ProcessElement) {
            final ProcessElement elm = (ProcessElement) engineContext.getElement();
            this.sanitizeStepData(engineContext.target(elm.getInput(), FormSpecificationData.class));
            this.sanitizeStepData(engineContext.target(elm.getOutput(), FormSpecificationData.class));
        }

        if (null != root.getChildren()) {
            for (final IProcess process : root.getChildren()) {
                this.sanitizeProcess(engineContext.target(process));
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieve specification metadata.")
    public SpecificationInfoBean load(@ApiParam("the specification identifier") final String code) {
        final SpecificationBean bean = this.dataService.load(code);

        if (null == bean) {
            return null;
        }

        return this.dozerMapper.map(bean, SpecificationInfoBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download specification complete file.")
    public Response download(@ApiParam("the specification identifier") final String code) {
        final FileEntry entry = this.dataService.resource(code);

        if (null == entry || null == entry.getContentAsStream()) {
            return Response.noContent().build();
        }

        return Response.ok(entry.getContentAsStream()) //
                .header(HttpHeaders.CONTENT_TYPE, entry.type()) //
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + entry.name()) //
                .lastModified(Date.valueOf(LocalDateTime.ofInstant(entry.lastModified(), ZoneId.systemDefault()).toLocalDate())) //
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download specific file from specification.")
    public Response download(@ApiParam("the specification identifier") final String code, final String resourcePath) {
        final FileEntry resource = this.dataService.resource(code);
        if (null == resource) {
            return Response.noContent().build();
        }

        final FileEntry entry = ZipUtil.entry(resource.getContentAsStream(), resourcePath);
        if (null == entry) {
            return ResponseUtil.defaultResponse(resourcePath);
        }

        return Response.ok(entry.getContentAsStream()) //
                .header(HttpHeaders.CONTENT_TYPE, MimeTypeUtil.type(resourcePath)) //
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + new File(resourcePath).getName()) //
                .lastModified(Date.from(entry.lastModified())) //
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download specification by reference.")
    public Response downloadByReference(final String reference) {
        final SpecificationBean bean = this.dataService.loadByReference(reference);

        if (null == bean || null == bean.getUid()) {
            return Response.noContent().build();
        } else {
            return this.download(bean.getUid());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for records", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SpecificationSearchResult fullTextSearch(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, //
            final String searchTerms) {

        return this.internalService.fullTextSearch(startIndex, maxResults, filters, orders, this.acls.asList(), searchTerms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for records", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SpecificationSearchResult search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        final SpecificationSearchResult searchResult = new SpecificationSearchResult(startIndex, maxResults);

        final SearchResult<SpecificationInfoBean> rawSearchResult = this.dataService.search(startIndex, maxResults, filters, orders, this.acls.asList(), null, SpecificationInfoBean.class);

        searchResult.setTotalResults(rawSearchResult.getTotalResults());
        searchResult.setContent(rawSearchResult.getContent());

        return searchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Updates a specification metadata.")
    public SpecificationInfoBean persist(@ApiParam("the specification identifier") final String code, @ApiParam("the new specification metadata") final SpecificationInfoBean bean) {
        final SpecificationBean dataBean = this.dataService.load(code);

        if (null == dataBean) {
            return null;
        }

        if (!dataBean.isTagged() && bean.isTagged()) {
            this.dataService.publish(code);
        } else if (dataBean.isTagged() && !bean.isTagged()) {
            this.dataService.unpublish(code);
        }

        final SpecificationBean updatedDataBean = this.dataService.load(code);

        return this.dozerMapper.map(updatedDataBean, SpecificationInfoBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Removes a specification.")
    public Response remove(@ApiParam("the specification identifier") final String code) {
        this.dataService.remove(code);

        return Response.ok().build();
    }

    @Override
    public ProcessEventResult process(final byte[] resourceAsBytes, final String author) {
        if (ArrayUtils.isEmpty(resourceAsBytes)) {
            return new ProcessEventResult(Result.ERROR, null);
        }

        final Response responseUpload = this.upload(resourceAsBytes, false, author, null, false);
        if (responseUpload.getStatus() == Status.CREATED.getStatusCode()) {
            final SpecificationInfoBean spec = responseUpload.readEntity(SpecificationInfoBean.class);
            spec.setTagged(true);
            this.persist(spec.getCode(), spec);
            return new ProcessEventResult(Result.SUCCESS, null);
        } else {
            return new ProcessEventResult(Result.ERROR, null);
        }
    }

    @Override
    public SpecificationInfoBean loadByReference(final String reference) {
        final SpecificationBean bean = this.dataService.loadByReference(reference);

        if (null == bean) {
            return null;
        }

        return this.dozerMapper.map(bean, SpecificationInfoBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download template specification by code.")
    public Response templateByCode(final String code) {
        final SpecificationBean bean = this.dataService.load(code);

        if (null == bean || null == bean.getUid()) {
            return Response.noContent().build();
        }

        return this.template(bean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download template specification by reference.")
    public Response templateByReference(final String reference) {
        final SpecificationBean bean = this.dataService.loadByReference(reference);

        if (null == bean || null == bean.getUid()) {
            return Response.noContent().build();
        }

        return this.template(bean);
    }

    /**
     * Get template for specification.
     *
     * @param bean
     *            the specification input.
     * @return response containing template as json
     */
    private Response template(final SpecificationBean bean) {
        final byte[] specContent = Optional.ofNullable(bean).map(SpecificationBean::getUid).map(this.dataService::resource).map(FileEntry::asBytes).orElse(null);

        if (null == specContent) {
            return Response.noContent().build();
        }

        final IProvider zipProvider = new ZipProvider(specContent);
        final SpecificationLoader loader = this.engine.loader(zipProvider);
        final Map<String, Object> model = new HashMap<>();
        for (int i = 0; i < loader.stepsMgr().findAll().size(); i++) {
            final StepElement step = loader.stepsMgr().find(i);
            if ("user".equals(step.getUser())) {
                final Map<String, Object> contextModel = RecursiveDataModelExtractor.create(zipProvider).extract(loader.data(step));
                model.putAll(contextModel);
            }
        }
        if (model.isEmpty()) {
            return Response.noContent().build();
        }

        return Response.ok(model) //
                .header(HttpHeaders.CONTENT_TYPE, "application/json") //
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + bean.getUid() + ".json") //
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Update priority specification by reference.")
    public SpecificationInfoBean updatePriorityByReference(final String reference, final int priority) {
        final SpecificationBean bean = this.dataService.loadByReference(reference);

        if (null == bean || null == bean.getUid()) {
            return null;
        }
        return this.dozerMapper.map(this.dataService.updatePriorityByReference(reference, priority), SpecificationInfoBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Update priority specification by code.")
    public SpecificationInfoBean updatePriorityByCode(final String code, final int priority) {
        final SpecificationBean bean = this.dataService.load(code);

        if (null == bean || null == bean.getUid()) {
            return null;
        }
        return this.dozerMapper.map(this.dataService.updatePriorityByCode(code, priority), SpecificationInfoBean.class);
    }
}