/**
 *
 */
package fr.ge.common.nash.ws.watcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.watcher.IWatcher;

/**
 * Delete records older than x days using a watcher.
 *
 * @author aakkou
 */
public class PurgeWatcher implements IWatcher {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(PurgeWatcher.class);

    /** The form spec service. */
    @Autowired
    private IRecordService service;

    /** The thread sleep. */
    @Value("${watcher.purge.thread.sleep:#{null}}")
    private Long threadSleep;

    /** The minimum age of a record to be eligible for deletion. */
    @Value("${watcher.purge.retention.days:#{null}}")
    private Integer retentionDays;

    /**
     * Sets the thread sleep.
     *
     * @param threadSleep
     *            the threadSleep to set
     * @return the profiler watcher
     */
    public PurgeWatcher setThreadSleep(final long threadSleep) {
        this.threadSleep = threadSleep;
        return this;
    }

    /**
     * Sets the service.
     *
     * @param service
     *            the service to set
     * @return the profiler watcher
     */
    public PurgeWatcher setService(final IRecordService service) {
        this.service = service;
        return this;
    }

    /**
     * Mutateur sur l'attribut {@link #retentionDays}.
     *
     * @param retentionDays
     *            la nouvelle valeur de l'attribut retentionDays
     */
    public PurgeWatcher setRetentionDays(final int retentionDays) {
        this.retentionDays = retentionDays;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive() {
        return null != this.retentionDays && null != this.threadSleep;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean processEvents() throws TechnicalException {
        LOGGER.info("Purge records since {} days", this.retentionDays);

        this.service.purge(this.retentionDays);

        try {
            LOGGER.info("Purge watcher sleeping for {} ms", this.threadSleep);
            Thread.sleep(this.threadSleep);
        } catch (final InterruptedException e) {
            return false;
        }
        return true;
    }
}
