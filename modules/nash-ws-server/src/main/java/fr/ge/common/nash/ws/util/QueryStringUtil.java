/**
 *
 */
package fr.ge.common.nash.ws.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class QueryStringUtil.
 *
 * @author Christian Cougourdan
 */
public final class QueryStringUtil {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(QueryStringUtil.class);

    /**
     * Instantie un nouveau query string util.
     */
    private QueryStringUtil() {
        // Nothing to do
    }

    /**
     * Encode resource path.
     *
     * @param resourcePath          resource path
     * @return string
     */
    public static String encodeResourcePath(final String resourcePath) {
        String fullyEncodedResourcePath = resourcePath;

        try {
            fullyEncodedResourcePath = URLEncoder.encode(resourcePath, "UTF-8");
        } catch (final UnsupportedEncodingException ex) {
            LOGGER.warn("Unable to encode resource path '{}'", resourcePath, ex);
        }

        return fullyEncodedResourcePath.replaceAll("%2F", "/");
    }

}
