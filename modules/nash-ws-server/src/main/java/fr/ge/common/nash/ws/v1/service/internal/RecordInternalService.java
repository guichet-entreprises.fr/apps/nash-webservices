/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.internal;

import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.ge.common.nash.core.bean.EventTypeEnum;
import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.loader.event.MessageEvent;
import fr.ge.common.nash.engine.loader.event.RecordOpenEvent;
import fr.ge.common.nash.engine.loader.event.ScriptEvent;
import fr.ge.common.nash.engine.manager.DataModelRequestUpdater;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.FormContentReader;
import fr.ge.common.nash.engine.manager.processor.IProcessResult;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.TranslationsElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.provider.RecordDataServiceProvider;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class RecordInternalService.
 *
 * @author Christian Cougourdan
 */
@Service
public class RecordInternalService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordInternalService.class);

    /** The record service. */
    @Autowired
    private IRecordDataService recordDataService;

    /** The specification service. */
    @Autowired
    private ISpecificationDataService specificationDataService;

    /** data service. */
    @Autowired
    private IRecordDataService dataService;

    /** The engine. */
    @Autowired
    private Engine engine;

    /**
     * Removes the.
     *
     * @param uid
     *     the uid
     * @return the long
     */
    public Response remove(final String uid) {
        final long deleted = this.dataService.remove(uid);

        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

    /**
     * Upload.
     *
     * @param resourceAsBytes
     *     the resource as bytes
     * @param author
     *     the author
     * @return the response
     */
    public Response upload(final byte[] resourceAsBytes, final String author, final List<SearchQueryFilter> roles) {
        final RecordInfoBean bean;

        try {
            bean = this.recordDataService.fromFile(null, null, resourceAsBytes, author, roles, RecordInfoBean.class);
        } catch (final FunctionalException ex) {
            LOGGER.warn("Unable to create resource from uploaded content", ex);
            return Response.serverError().entity("Unable to create resource from uploaded content").build();
        }

        if (null == bean) {
            return Response.noContent().build();
        }

        final IProvider provider = RecordDataServiceProvider.create(this.dataService, bean.getCode());
        final SpecificationLoader loader = this.engine.loader(provider);

        loader.getEventBus().post(new ScriptEvent(loader).setType(EventTypeEnum.ON_CREATE_RECORD));

        final FormSpecificationDescription description = Optional.of(loader) //
                .map(SpecificationLoader::description) //
                .orElse(new FormSpecificationDescription()) //
        ;

        if (StringUtils.isNotEmpty(description.getAuthor())) {
            loader.getEventBus().post( //
                    new MessageEvent(loader).setMessage(MessageFormat.format("Le dossier a été créé par \"{0}\"", description.getAuthor())), //
                    new RecordOpenEvent(loader) //
            );
        }

        return Response.created(URI.create("/v1/Record/code/" + bean.getCode())).entity(bean.getCode()).header("code", bean.getCode()).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Expose-Headers", "code").build();
    }

    /**
     * Upload with context.
     *
     * @param reference
     *     the reference
     * @param author
     *     the author
     * @param context
     *     the context
     * @param lang
     *     the lang
     * @return the response
     * @throws JsonParseException
     *     the json parse exception
     * @throws JsonMappingException
     *     the json mapping exception
     * @throws IOException
     *     Signals that an I/O exception has occurred.
     */
    public Response uploadWithContext(final String reference, final String author, final List<SearchQueryFilter> roles, final byte[] context, final String lang)
            throws JsonParseException, JsonMappingException, IOException {
        final FormContentData contextData = FormContentReader.read(context);
        final SpecificationBean spec = this.specificationDataService.loadByReference(reference);
        final byte[] specContent = Optional.ofNullable(spec).map(SpecificationBean::getUid).map(this.specificationDataService::resource).map(FileEntry::asBytes).orElse(null);

        final IProvider provider = new ZipProvider(specContent);
        final SpecificationLoader loader = this.engine.loader(provider);

        /*
         * Default formality language setting
         */
        if (StringUtils.isNotEmpty(lang)) {
            loader.description().setLang(lang);
            final TranslationsElement translationElement = Optional.ofNullable(loader.description()).map(FormSpecificationDescription::getTranslations).orElse(null);
            if (null != translationElement) {
                translationElement.setDefaultLang(lang);
            }
            loader.flush();
        }

        /*
         * Inject context into record
         */
        try {
            for (int i = 0; i < loader.stepsMgr().findAll().size(); i++) {
                loader.updateStepStatus(i, StepStatusEnum.IN_PROGRESS, author);

                final StepElement step = loader.stepsMgr().find(i);
                if (Optional.ofNullable(loader.processes().preExecute(step)).map(IProcessResult::isInterrupting).orElse(false)) {
                    break;
                }

                final EngineContext<FormSpecificationData> engineContext = loader.buildEngineContext(i);
                final DataModelRequestUpdater updater = new DataModelRequestUpdater(engineContext).setClearMissingValues(false);
                updater.update(contextData.withPath(engineContext.getElement().getId()));

                loader.save(i, engineContext.getElement());

                try {
                    loader.validate(engineContext.getElement());
                } catch (final TechnicalException ex) {
                    LOGGER.debug("Missing mandatory data in step #{} {}", i, step.getId());
                    break;
                }

                final StepElement next = loader.stepsMgr().find(i + 1);
                if (null == next || !next.getUser().equals(step.getUser())) {
                    break;
                } else {
                    if (Optional.ofNullable(loader.processes().postExecute(step)).map(IProcessResult::isInterrupting).orElse(false)) {
                        break;
                    } else {
                        loader.updateStepStatus(step.getPosition(), StepStatusEnum.DONE, author);
                    }
                }
            }
        } finally {
            loader.flush();
        }

        return this.upload(provider.asBytes(), author, roles);
    }

}
