/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl.dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.support.i18n.NashMessageReader;
import fr.ge.common.nash.ws.data.bean.RecordPropertiesBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.provider.RecordDataServiceProvider;
import fr.ge.record.ws.v1.model.LocalizedLabelBO;
import fr.ge.record.ws.v1.model.LocalizedStringBO;
import fr.ge.record.ws.v1.model.RecordActionBO;
import fr.ge.record.ws.v1.model.RecordActionTypeBOEnum;
import fr.ge.record.ws.v1.model.RecordBODisplay;
import fr.ge.record.ws.v1.model.RecordResultBO;
import fr.ge.record.ws.v1.rest.IRecordBOService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 *
 * @author Christian Cougourdan
 */
public class RecordBackOfficeServiceImpl extends AbstractDashboardServiceImpl<RecordResultBO> implements IRecordBOService {

    /** Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordBackOfficeServiceImpl.class);

    private static final Map<RecordActionTypeBOEnum, LocalizedLabelBO> LABELS_ACTION;

    private static final Map<String, LocalizedLabelBO> LABELS_STEP;

    private static final String EXEC = "exec:";

    private static final String CONFIRM = "confirm:";

    private static final String TO_PROCESS = "toProcess";

    static {
        final Map<RecordActionTypeBOEnum, LocalizedLabelBO> m = new EnumMap<>(RecordActionTypeBOEnum.class);
        m.put(RecordActionTypeBOEnum.VIEW, getLocalizedLabel("View"));
        m.put(RecordActionTypeBOEnum.DOWNLOAD, getLocalizedLabel("Download"));
        m.put(RecordActionTypeBOEnum.EXEC_PROCESSED, getLocalizedLabel("Process"));
        m.put(RecordActionTypeBOEnum.EXEC_ARCHIVED, getLocalizedLabel("Archive"));
        m.put(RecordActionTypeBOEnum.DELETE, getLocalizedLabel("Delete"));
        m.put(RecordActionTypeBOEnum.EXPORT, getLocalizedLabel("Export"));
        LABELS_ACTION = Collections.unmodifiableMap(m);

        final Map<String, LocalizedLabelBO> stepLabelsRaw = new HashMap<String, LocalizedLabelBO>();
        stepLabelsRaw.put(RecordActionTypeBOEnum.VIEW.getTypeActionName(), getLocalizedLabel("To process"));
        stepLabelsRaw.put(RecordActionTypeBOEnum.EXEC_PROCESSED.getTypeActionId(), getLocalizedLabel("Processed"));
        stepLabelsRaw.put(RecordActionTypeBOEnum.EXEC_ARCHIVED.getTypeActionId(), getLocalizedLabel("Archived"));
        stepLabelsRaw.put(TO_PROCESS, getLocalizedLabel("To process"));
        LABELS_STEP = Collections.unmodifiableMap(stepLabelsRaw);
    }

    /** Data service. */
    @Autowired
    private IRecordDataService dataService;

    @Value("${nash.record.additional.actions:#{null}}")
    private String[] additionalActions;

    @Autowired
    private Engine engine;

    @Override
    @ApiOperation("Gets records to display a dashboard with full text search on record title for BACK OFFICE.")
    public RecordResultBO searchRecordsBO(@ApiParam("the index of the element to display") final int startIndex, @ApiParam("the maximum number of results per page") final int maxResults,
            @ApiParam("the sort orders") final String orders, @ApiParam("the full text search query") final String fullTextSearchCriteria,
            @ApiParam("the begining date for search") final String beginDate, @ApiParam("the end date for search") final String endDate, @ApiParam("the full text search query") final Boolean archived,
            @ApiParam("the engine user type") final String engineUserType) {

        return this.search(null, startIndex, maxResults, orders, fullTextSearchCriteria, beginDate, endDate, archived, engineUserType);
    }

    @Override
    protected RecordResultBO buildResult(final String startIndex, final String maxResults, final String totalResults, final Collection<RecordInfoBean> beans, final String engineUserType) {
        final RecordResultBO searchResult = new RecordResultBO();
        searchResult.setStartIndex(startIndex);
        searchResult.setNbResults(maxResults);
        searchResult.setTotalResults(totalResults);

        searchResult.setRecords( //
                beans //
                        .stream() //
                        .map(nfo -> this.map(nfo, engineUserType)) //
                        .collect(Collectors.toList()) //
        );

        return searchResult;
    }

    /**
     * Map a RecordInfoBean to a RecordBODisplay.
     *
     * @param recordInfo
     *            The input RecordInfoBean
     * @param engineUserType
     *            The engine type
     * @return The RecordBODisplay bean
     */
    private RecordBODisplay map(final RecordInfoBean recordInfo, final String engineUserType) {
        final RecordPropertiesBean properties = this.readProperties(recordInfo.getProperties());
        final String recordCode = recordInfo.getCode();

        final RecordBODisplay record = new RecordBODisplay();
        record.setAuthorId(recordInfo.getAuthor());
        record.setRecordId(recordInfo.getCode());
        record.setTitle(recordInfo.getTitle());
        record.setCreationDate(recordInfo.getCreated().getTime());
        record.setUpdateDate(recordInfo.getUpdated().getTime());
        record.setCompletedStep(String.valueOf(properties.getNbCompletedSteps()));
        record.setTotalStep(String.valueOf(properties.getNbSteps()));
        record.setError(false);

        final SpecificationLoader loader = this.engine.loader(RecordDataServiceProvider.create(this.dataService, recordCode));
        final StepElement lastStepDone = loader.lastStepDone();

        // Set Record Status
        if (lastStepDone != null) {
            final String lastStepDoneID = lastStepDone.getId();
            LOGGER.debug("Last step in status done : {}", lastStepDoneID);
            final LocalizedLabelBO localizedLabelLastStepDone = LABELS_STEP.get(lastStepDoneID);
            if (null != lastStepDoneID && null != localizedLabelLastStepDone) {
                record.setStatus(localizedLabelLastStepDone.getActualLabel());
            }
        } else {
            record.setStatus(LABELS_STEP.get(TO_PROCESS).getActualLabel());
        }
        LOGGER.debug("Current record status : {}", record.getStatus());

        // Set Record description : Type For the GE Record (CREATION /
        // MODIFICATION / CESSATION / REGULARISATION)
        final FormSpecificationDescription description = loader.description();
        if (description != null) {
            record.setDescription(description.getDescription());
        }

        record.setMetadata(this.map(loader.meta()));

        // Set Record Actions
        record.setActions(this.getActions(record, loader, engineUserType));

        return record;
    }

    /**
     * Gets an action localized label.
     *
     * @param label
     *            the input label;
     * @return the localized label
     */
    private static LocalizedLabelBO getLocalizedLabel(final String label) {
        final LocalizedLabelBO localizedLabel = new LocalizedLabelBO();
        localizedLabel.setDefaultLocale("fr");
        localizedLabel.setValues( //
                Arrays.asList( //
                        new LocalizedStringBO("fr", NashMessageReader.getReader(Locale.FRENCH).getFormatter().format(label)), //
                        new LocalizedStringBO("en", NashMessageReader.getReader(Locale.ENGLISH).getFormatter().format(label)) //
                ) //
        );

        return localizedLabel;
    }

    /**
     * Get Record BO actions.
     *
     * @param recordCode
     *            the record identifier
     * @param loader
     *            the specification loader
     * @param engineUserType
     *            the engine user type
     * @return a list of actions to display
     */
    public List<RecordActionBO> getActions(final RecordBODisplay record, final SpecificationLoader loader, final String engineUserType) {
        // A record has 2 actions by default : View and Download .
        final List<RecordActionBO> actions = new ArrayList<>();

        actions.add(getAction(RecordActionTypeBOEnum.VIEW));

        // First Step Todo for an user BO
        final StepElement firstStepToDo = loader.firstStepToDo(engineUserType);
        // Add action of the first step Todo for an User BO if it exists
        if (null != firstStepToDo) {
            LOGGER.debug("First step in status todo : {}", firstStepToDo.getId());
            final RecordActionTypeBOEnum actionType = RecordActionTypeBOEnum.get(EXEC + firstStepToDo.getId());
            if (null != actionType) {
                actions.add(getAction(actionType));
                LOGGER.debug("Adding {} action record", actionType.getTypeActionName());
            }
        }

        // Last Action Todo For an user BO
        final StepElement lastStepToDo = loader.lastStepToDo(engineUserType);
        // Add action of the first step Todo for an User BO if it exists
        if (null != lastStepToDo && null != firstStepToDo && !firstStepToDo.getId().equals(lastStepToDo.getId())) {
            LOGGER.debug("Last step in status todo : {}", lastStepToDo.getId());
            final RecordActionTypeBOEnum actionType = RecordActionTypeBOEnum.get(EXEC + lastStepToDo.getId());
            if (null != actionType) {
                actions.add(getAction(actionType));
                LOGGER.debug("Adding {} action record", actionType.getTypeActionName());
            }
        }

        actions.add(this.getDocumentsDownloadAction(record, loader).orElseGet(() -> getAction(RecordActionTypeBOEnum.DOWNLOAD)));

        // -->TODO : Add additional actions
        // if (ArrayUtils.isNotEmpty(this.additionalActions)) {
        // Arrays.asList(this.additionalActions).forEach(action -> {
        // RecordActionTypeBOEnum actionType = RecordActionTypeBOEnum.get(EXEC +
        // action);
        // if (null != actionType) {
        // actions.add(getAction(actionType));
        // LOGGER.debug("Adding {} action record",
        // actionType.getTypeActionName());
        // }
        // actionType = RecordActionTypeBOEnum.get(CONFIRM + action);
        // if (null != actionType) {
        // actions.add(getAction(actionType));
        // LOGGER.debug("Adding {} action record",
        // actionType.getTypeActionName());
        // }
        // });
        // }
        // <--

        return actions;

    }

    private Optional<RecordActionBO> getDocumentsDownloadAction(final RecordBODisplay record, final SpecificationLoader loader) {
        final FormSpecificationMeta recordMetas = loader.meta();
        if (null != recordMetas && null != recordMetas.getMetas() && recordMetas.getMetas().stream().map(MetaElement::getName).anyMatch("document"::equals)) {
            return Optional.ofNullable(
                    getAction(RecordActionTypeBOEnum.DOCUMENTS.getTypeActionName(), RecordActionTypeBOEnum.DOWNLOAD.getTypeActionName(), LABELS_ACTION.get(RecordActionTypeBOEnum.DOWNLOAD)));
        }

        final int nbCompletedSteps = Integer.parseInt(record.getCompletedStep());
        final int nbTotalSteps = Integer.parseInt(record.getTotalStep());

        if (nbCompletedSteps == nbTotalSteps) {
            for (int stepIndex = nbTotalSteps - 1; stepIndex >= 0; stepIndex -= 1) {
                if (loader.addDocumentMeta(stepIndex)) {
                    return Optional.ofNullable(
                            getAction(RecordActionTypeBOEnum.DOCUMENTS.getTypeActionName(), RecordActionTypeBOEnum.DOWNLOAD.getTypeActionName(), LABELS_ACTION.get(RecordActionTypeBOEnum.DOWNLOAD)));
                }
            }
        }

        return Optional.empty();
    }

    /**
     * Get action bean from action type.
     *
     * @param actionType
     *            action type
     * @return action bean
     */
    private static RecordActionBO getAction(final RecordActionTypeBOEnum actionType) {
        return getAction(actionType.getTypeActionName(), actionType.getTypeActionId(), LABELS_ACTION.get(actionType));
    }

    /**
     * Gets an action.
     *
     * @param actionCode
     *            the action code
     * @param actionType
     *            action Type
     * @param localizedLabelBO
     *            the localized label
     * @return action bean
     */
    private static RecordActionBO getAction(final String actionCode, final String actionType, final LocalizedLabelBO localizedLabelBO) {
        final RecordActionBO action = new RecordActionBO();

        action.setLabel(localizedLabelBO);
        action.setCode(actionCode);
        action.setType(actionType);

        return action;
    }

}
