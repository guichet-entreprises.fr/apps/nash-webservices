/**
 *
 */
package fr.ge.common.nash.ws.v1.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.support.context.AclContext;
import fr.ge.common.nash.ws.v1.bean.GroupInfoSpecificationBean;
import fr.ge.common.nash.ws.v1.bean.ListOfSpecificationGroupe;
import fr.ge.common.nash.ws.v1.bean.search.SpecificationSearchResult;
import fr.ge.common.nash.ws.v1.service.IAclService;
import fr.ge.common.nash.ws.v1.service.ISpecificationPublicService;
import fr.ge.common.nash.ws.v1.service.internal.SpecificationInternalService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author bsadil
 *
 */

@Path("/public/v1/Specification")
@Api("Public Specification Services")
public class SpecificationPublicServiceImpl implements ISpecificationPublicService, IAclService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificationPublicServiceImpl.class);

    /** The dozer. */
    @Autowired
    private Mapper dozerMapper;

    /** The data service. */
    @Autowired
    private ISpecificationDataService dataService;

    @Autowired
    private SpecificationInternalService internalService;

    private AclContext acls;

    /**
     * @param acls
     *            the acls to set
     */
    @Context
    @Override
    public void setAcls(final AclContext acls) {
        this.acls = acls;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for records", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SpecificationSearchResult fullTextSearch(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, //
            final String searchTerms) {
        return this.internalService.fullTextSearch(startIndex, maxResults, filters, orders, this.acls.asList(), searchTerms);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ListOfSpecificationGroupe findAllGroupe() {
        final ListOfSpecificationGroupe groups = new ListOfSpecificationGroupe();

        final List<GroupInfoSpecificationBean> grp = new ArrayList<>();
        this.dataService.findAllGroupe().forEach(g -> grp.add(this.dozerMapper.map(g, GroupInfoSpecificationBean.class)));

        groups.setGroups(grp);
        return groups;
    }

}
