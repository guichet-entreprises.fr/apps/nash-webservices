/**
 *
 */
package fr.ge.common.nash.ws.watcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.ws.v1.service.internal.RecordInternalService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.watcher.IWatcher;

/**
 * The watcher to upload a record from a directory.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class RecordWatcher implements IWatcher {

    /** Le logger fonctionnel. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordWatcher.class);

    /** Buffer size. */
    protected static final int BUFFER_SIZE = 8192;

    /** the ZIP extension of record. */
    protected static final String ZIP_EXTENSION = ".zip";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String SHA1_EXTENSION = ".sha1";

    /** THE DIGEST_ALGORITHM_SHA1 extension of record. */
    protected static final String MD5_EXTENSION = ".md5";

    /** THE DIGEST_ALGORITHM_SHA1 algorithm of record. */
    protected static final String DIGEST_ALGORITHM_SHA1 = "sha1";

    /** the input directory to watch. */
    @Value("${nash.support.directory.input:#{null}}")
    private String inputDirectory;

    /** The record service. */
    @Autowired
    private RecordInternalService recordService;

    /** the default role when importing record into db. */
    @Value("${nash.support.default.role:*}")
    private String defaultRole;

    @Value("${engine.user.type:#{null}}")
    private String engineUserType;

    @Autowired
    private Engine engine;

    /**
     * Read the file and calculate checksum with a specific algorithm.
     *
     * @param file
     *            the file to read
     * @param algorithm
     *            algorithm
     * @return the SHA-1 using byte
     * @throws TechnicalException
     *             the technical exception
     */
    public byte[] createChecksum(final File file, final String algorithm) throws TechnicalException {
        try (InputStream fis = new FileInputStream(file)) {
            final MessageDigest digest = MessageDigest.getInstance(algorithm);
            int n = 0;
            final byte[] buffer = new byte[BUFFER_SIZE];

            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            return digest.digest();
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException(String.format("Unable to retrieve %s digest algorithm", algorithm), ex);
        } catch (final FileNotFoundException ex) {
            throw new TechnicalException(String.format("File '%s' not found", file), ex);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to read '%s' file", file), ex);
        }
    }

    public boolean sendRecordToNash(final File f) {
        if (null == f || !f.isDirectory()) {
            return false;
        }

        final Collection<File> files = FileUtils.listFiles(f, new String[] { "sha1" }, true);

        if (!files.isEmpty()) {
            for (final File file : files) {
                this.processZipFile(file.toPath());
            }
        }
        return true;
    }

    /**
     * Process all events for keys queued to the watchService.
     *
     * @throws TechnicalException
     *             the technical exception
     */
    @Override
    public boolean processEvents() throws TechnicalException {
        return this.sendRecordToNash(new File(this.inputDirectory));
    }

    /**
     * upload Zip to Nash.
     *
     * @param sha1Path
     *            the path of record
     */
    public void processZipFile(final Path sha1Path) {

        final String basePath = FilenameUtils.removeExtension(sha1Path.toString());

        final Path resourcePath = Paths.get(basePath + ZIP_EXTENSION);
        final Path md5Path = Paths.get(basePath + MD5_EXTENSION);

        try {
            final byte[] expectedSha1AsBytes = this.readHashFile(sha1Path);

            // calculate the SHA1 value of Zip File
            final byte[] actualSha1AsBytes = new HexBinaryAdapter().marshal(this.createChecksum(resourcePath.toFile(), DIGEST_ALGORITHM_SHA1)).getBytes();

            // compare SHA1 file already exist in FS and the
            // DIGEST_ALGORITHM_SHA1
            // value of Zip File
            if (Arrays.equals(expectedSha1AsBytes, actualSha1AsBytes)) {
                this.upload(resourcePath);
                md5Path.toFile().delete();
                sha1Path.toFile().delete();
                resourcePath.toFile().delete();
            } else {
                md5Path.toFile().delete();
                sha1Path.toFile().delete();
            }

        } catch (final TechnicalException ex) {
            LOGGER.warn("Upload file '{}' : {}", resourcePath, ex.getMessage(), ex);
        }
    }

    /**
     * Uploa a record.
     *
     * @param resourcePath
     *            the resource path
     */
    private void upload(final Path resourcePath) {
        try (InputStream inputStream = new FileInputStream(resourcePath.toFile())) {
            final byte[] resourceAsBytes = IOUtils.toByteArray(inputStream);

            final IProvider provider = new ZipProvider(resourceAsBytes);
            final SpecificationLoader loader = this.engine.loader(provider);
            final String recordUid = loader.description().getRecordUid();

            this.recordService.remove(recordUid);

            final StepElement step = Optional.ofNullable(loader.description()) //
                    .map(FormSpecificationDescription::getSteps) //
                    .map(StepsElement::getElements) //
                    .orElseGet(Collections::emptyList) //
                    .stream() //
                    .filter(s -> StepStatusEnum.TO_DO.getStatus().equals(s.getStatus()) || StepStatusEnum.IN_PROGRESS.getStatus().equals(s.getStatus())) //
                    .findFirst() //
                    .orElse(null);

            if (StringUtils.isNotEmpty(this.engineUserType) && null != step) {
                step.setUser(this.engineUserType);
                loader.description().getSteps().getElements().set(step.getPosition(), step);
                loader.flush();
            }

            this.recordService.upload(provider.asBytes(), loader.description().getAuthor(), Arrays.asList(new SearchQueryFilter("GE:" + this.defaultRole)));
        } catch (final FileNotFoundException ex) {
            throw new TechnicalException(String.format("File '%s' not found", resourcePath), ex);
        } catch (final IOException ex) {
            throw new TechnicalException(String.format("Unable to read '%s' file", resourcePath), ex);
        }
    }

    /**
     * Reads a hash file.
     *
     * @param path
     *            the path
     * @return the byte[]
     * @throws TechnicalException
     *             the technical exception
     */
    private byte[] readHashFile(final Path path) throws TechnicalException {
        try (InputStream stream = new FileInputStream(path.toString())) {
            // get the value of DIGEST_ALGORITHM_SHA1 file already exist in FS
            return IOUtils.toByteArray(stream);
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read SHA1 from file " + path, ex);
        }
    }

    /**
     * Sets the input directory.
     *
     * @param inputDirectory
     *            the inputDirectory to set
     * @return the directory watcher
     */
    public RecordWatcher setInputDirectory(final String inputDirectory) {
        this.inputDirectory = inputDirectory;
        return this;
    }

    /**
     * Sets the record REST service.
     *
     * @param recordService
     *            the recordService to set
     * @return the directory watcher
     */
    public RecordWatcher setRecordService(final RecordInternalService recordService) {
        this.recordService = recordService;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive() {
        return StringUtils.isNotEmpty(this.inputDirectory);
    }

}
