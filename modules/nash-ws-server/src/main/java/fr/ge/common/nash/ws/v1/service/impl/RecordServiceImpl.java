/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.v1.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.core.util.MimeTypeUtil;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.adapter.IValueAdapter;
import fr.ge.common.nash.engine.adapter.ValueAdapterFactory;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter;
import fr.ge.common.nash.engine.adapter.v1_0.FileValueAdapter.Item;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.manager.FormContentData;
import fr.ge.common.nash.engine.manager.FormContentReader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationReferential;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.DataElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.data.IElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.IProcess;
import fr.ge.common.nash.engine.mapping.form.v1_2.process.ProcessElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.referential.ReferentialTextElement;
import fr.ge.common.nash.engine.provider.ClasspathProvider;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.script.EngineContext;
import fr.ge.common.nash.engine.util.StylesheetUtil;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.nash.ws.support.context.AclContext;
import fr.ge.common.nash.ws.util.QueryStringUtil;
import fr.ge.common.nash.ws.util.ResponseUtil;
import fr.ge.common.nash.ws.v1.bean.EntryInfoBean;
import fr.ge.common.nash.ws.v1.bean.ProxyResultBean;
import fr.ge.common.nash.ws.v1.bean.RecordInfoBean;
import fr.ge.common.nash.ws.v1.bean.search.RecordSearchResult;
import fr.ge.common.nash.ws.v1.provider.RecordDataServiceProvider;
import fr.ge.common.nash.ws.v1.service.IAclService;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.nash.ws.v1.service.internal.RecordInternalService;
import fr.ge.common.utils.ResourceUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.record.ws.v1.model.RecordActionTypeBOEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Class RecordServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Api("Record services")
@Path("/v1/Record")
public class RecordServiceImpl implements IRecordService, IAclService {

    /** La constante LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordServiceImpl.class);

    /** the constant REPORT_STEP_LABEL_START. */
    private static final String REPORT_STEP_LABEL_START = "Step name ";

    /** the constant REPORT_STEP_LABEL_STATUS. */
    private static final String REPORT_STEP_LABEL_STATUS = " with status ";

    /** the constant REPORT_RETURN_LINE. */
    private static final String REPORT_RETURN_LINE = "\n";

    /** data service. */
    @Autowired
    private IRecordDataService dataService;

    /** spec data service. */
    @Autowired
    private ISpecificationDataService specDataService;

    /** dozer. */
    @Autowired
    private Mapper dozerMapper;

    @Autowired
    private RecordInternalService internalService;

    private AclContext acls;

    /** The constant DATE_FORMATTER. */
    private static final DateTimeFormatter SPEC_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    /** The engine. */
    @Autowired
    private Engine engine;

    /**
     * @param acls
     *            the acls to set
     */
    @Context
    @Override
    public void setAcls(final AclContext acls) {
        this.acls = acls;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Create a record.")
    public Response upload(@ApiParam("the uploaded record") final byte[] resourceAsBytes, @ApiParam("the author identifier provided by the \"Account\" service header") final String author,
            @ApiParam("ACLs information using \"[entity path]:[role]\" format") final List<SearchQueryFilter> roles) {
        return this.internalService.upload(resourceAsBytes, author, roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Create a record with a context.")
    public Response upload(@ApiParam(name = "record", value = "the uploaded record") final byte[] recordAsBytes,
            @ApiParam("the author identifier provided by the \"Account\" service header") final String author,
            @ApiParam("ACLs information using \"[entity path]:[role]\" format") final List<SearchQueryFilter> roles, @ApiParam(name = "context", value = "the context") final byte[] context) {
        final FormContentData contextData = FormContentReader.read(context);
        final IProvider provider = new ZipProvider(recordAsBytes);
        final SpecificationLoader loader = this.engine.loader(provider);

        final Map<String, Integer> formSpecDataIdToIndex = new HashMap<>();
        final Map<String, FormSpecificationData> formSpecDataIdToForm = new HashMap<>();
        final Map<String, IProvider> formSpecDataIdToProvider = new HashMap<>();

        final List<String> modifiedForms = new ArrayList<>();
        for (int i = 0; i < loader.stepsMgr().findAll().size(); ++i) {
            final StepElement step = loader.stepsMgr().find(i);
            final FormSpecificationData formSpecData = loader.data(step);
            if (formSpecData != null) {
                formSpecDataIdToIndex.put(formSpecData.getId(), i);
                formSpecDataIdToForm.put(formSpecData.getId(), formSpecData);
                formSpecDataIdToProvider.put(formSpecData.getId(), loader.provider(step.getData()));
            }
        }
        for (final Entry<String, String[]> entry : contextData.asMap().entrySet()) {
            final String[] pathSplit = entry.getKey().split("\\.");
            final String formSpecDataId = pathSplit[0];
            final String absolutePath = StringUtils.join(pathSplit, ".", 1, pathSplit.length);
            modifiedForms.add(formSpecDataId);
            final FormSpecificationData formSpecData = formSpecDataIdToForm.get(formSpecDataId);
            final IElement<?> element = SpecificationLoader.find(formSpecData, absolutePath);
            if (element instanceof DataElement) {
                final DataElement dataElement = (DataElement) element;
                ValueAdapterFactory.type(formSpecData, dataElement).set(loader, formSpecDataIdToProvider.get(formSpecDataId), dataElement, entry.getValue()[0]);
            }
        }
        for (final String modifiedForm : modifiedForms) {
            final FormSpecificationData formSpecData = formSpecDataIdToForm.get(modifiedForm);
            final Integer stepIndex = formSpecDataIdToIndex.get(modifiedForm);
            loader.save(stepIndex, formSpecData);
        }
        return this.upload(provider.asBytes(), author, roles);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Updates or creates a file in a record.")
    public Response upload(@ApiParam("the record identifier") final String code, @ApiParam("the path of the file to update within the record") final String resourcePath,
            @ApiParam("the uploaded file") final byte[] resourceAsBytes) {

        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        if (!this.dataService.exists(code)) {
            return Response.noContent().build();
        }

        Status status;
        if (this.dataService.resourceExists(code, resourcePath)) {
            LOGGER.debug("Updating resource '{}' on record '{}'", resourcePath, code);
            status = Status.OK;
        } else {
            LOGGER.debug("Adding new resource '{}' on record '{}'", resourcePath, code);
            status = Status.CREATED;
        }

        final FileBean file = this.dataService.addOrUpdate(code, resourcePath, MimeTypeUtil.type(resourcePath), resourceAsBytes);
        return Response.status(status).location(URI.create("/v1/Record/code/" + code + "/" + QueryStringUtil.encodeResourcePath(resourcePath))).lastModified(file.getUpdated().getTime()).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Retrieve record metadata.")
    public RecordInfoBean load(@ApiParam("the record identifier") final String code) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        final RecordBean bean = this.dataService.load(code, RecordBean.class);

        if (null == bean) {
            return null;
        }

        final RecordInfoBean infoBean = this.dozerMapper.map(bean, RecordInfoBean.class);

        final Collection<FileBean> files = this.dataService.resources(bean.getUid());
        if (null != files) {
            infoBean.setEntries( //
                    files.stream() //
                            .map(elm -> this.dozerMapper.map(elm, EntryInfoBean.class)) //
                            .collect(Collectors.toList()) //
            );
        }

        return infoBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download specific record complete file.")
    public Response download(@ApiParam("the record identifier") final String code) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        final RecordBean record = this.dataService.load(code, RecordBean.class);
        if (null == record) {
            return Response.noContent().build();
        }

        byte[] asBytes = null;
        try {
            asBytes = this.getZipAsBytes(record);
        } catch (final TechnicalException ex) {
            return Response.serverError().entity(ex.getMessage()).build();
        }

        try {
            return Response.ok(asBytes) //
                    .header(HttpHeaders.CONTENT_TYPE, MimeTypeUtil.type("zip")) //
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + record.getUid() + ".zip") //
                    .lastModified(record.getUpdated().getTime()) //
                    .build();
        } catch (final TechnicalException ex) {
            LOGGER.warn("Error while building ZIP file for record {}", record.getUid(), ex);

            return Response.serverError() //
                    .entity(ex.getMessage()) //
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Download a specific file from a record.")
    public Response download(@ApiParam("the record identifier") final String code, final String resourcePath) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        final Response response = ResponseUtil.defaultResponse(resourcePath);
        if (Status.OK.getStatusCode() == response.getStatus()) {
            return response;
        }

        final FileEntry entry = this.dataService.resource(code, resourcePath);

        if (null == entry) {
            LOGGER.info("Required resource '{}' for record '{}' : resource not found", resourcePath, code);
            return Response.noContent().build();
        }

        if (null == entry.asBytes()) {
            LOGGER.info("Required resource '{}' for record '{}' : resource is empty", resourcePath, code);
            return Response.noContent().build();
        }

        return Response.ok(entry.asBytes()) //
                .header(HttpHeaders.CONTENT_TYPE, entry.type()) //
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + entry.name()) //
                .lastModified(Date.from(entry.lastModified())) //
                .build();
    }

    @Override
    @ApiOperation(value = "Search for records", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public RecordSearchResult search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        return this.search(startIndex, maxResults, filters, orders, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for records using metas", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public RecordSearchResult search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders, //
            @ApiParam("terms as string") final String searchTerms) {

        final RecordSearchResult searchResult = new RecordSearchResult(startIndex, maxResults);

        final SearchResult<RecordInfoBean> rawSearchResult = this.dataService.search(startIndex, maxResults, filters, orders, this.acls.asList(), searchTerms, RecordInfoBean.class);

        searchResult.setTotalResults(rawSearchResult.getTotalResults());
        searchResult.setContent(rawSearchResult.getContent());

        return searchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Removes a file in a record.")
    public Response remove(@ApiParam("the record identifier") final String code, @ApiParam("the path of the file to remove within the record") final String resourcePath) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        final long deleted = this.dataService.remove(code, resourcePath);

        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Removes  a record.")
    public Response remove(@ApiParam("the record identifier") final String code) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        final long deleted = this.dataService.remove(code);

        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

    /**
     * {@inheritDoc}
     *
     * @throws FunctionalException
     */
    @Override
    @ApiOperation("Transform forms record to nash record")
    public String transform(@ApiParam("the name of FORMS record") final String formsRecordName, @ApiParam("the type of FORMS record") final String formsRecordType, //
            @ApiParam("the uploaded  forms record") final byte[] resourceAsBytes, //
            @ApiParam("the author of forms record") final String author, //
            @ApiParam("ACLs information using \"[entity path]:[role]\" format") final List<SearchQueryFilter> roles //
    ) throws FunctionalException {

        final RecordBean record = this.buildRecordFromRegent(formsRecordName, formsRecordType, author, roles, resourceAsBytes);

        if (record != null) {
            // -->Execute the first step to be done
            final String code = record.getUid();
            final IProvider recordDataProvider = RecordDataServiceProvider.create(this.dataService, code);
            final SpecificationLoader loader = this.engine.loader(recordDataProvider);
            final int stepIndex = loader.currentStepPosition();

            this.processStep(code, loader, loader.description().getSteps().getElements().iterator().next());

            // -->Reset the first step status
            loader.updateStepStatus(stepIndex, StepStatusEnum.TO_DO, author);
            loader.flush();

            return code;
        }

        return null;
    }

    private RecordBean buildRecordFromRegent(final String title, final String type, final String author, final List<SearchQueryFilter> roles, final byte[] resourceAsBytes) throws FunctionalException {
        LOGGER.debug("start transformation of form record to nash records");

        final ClasspathProvider provider = new ClasspathProvider(this.getClass(), ".");
        final byte[] nashResourceAsBytes = provider.asBytes("nash.zip");
        final RecordBean bean = this.dataService.fromFile(title, type, nashResourceAsBytes, author, roles, RecordBean.class);

        final List<String> entries = new ArrayList<>();
        try (InputStream resourceStream = new ByteArrayInputStream(resourceAsBytes)) {
            ZipUtil.scan(resourceStream, entry -> {
                if (StringUtils.endsWith(entry.name(), ".xml") && !StringUtils.contains(entry.name(), "XMLTC")) {
                    LOGGER.debug("add this file {} to nash record", entry.name());

                    this.dataService.addOrUpdate(bean.getUid(), "1-data/regent.xml", "xml", entry.asBytes());
                    LOGGER.info("add this regent file {} to nash record", entry.name());

                    this.dataService.addOrUpdate(bean.getUid(), "1-data/generated/" + "dossierGroup.dossier.liasse-1-" + entry.name(), "xml", entry.asBytes());
                    LOGGER.info("add this regent file {} to nash pjs", entry.name());
                } else {
                    this.dataService.addOrUpdate(bean.getUid(), "1-data/generated/" + "dossierGroup.dossier.formulaire-1-" + entry.name(), "pdf", entry.asBytes());
                    LOGGER.info("add this file {} to nash pjs", entry.name());
                }
                entries.add(entry.name());
            });
        } catch (final IOException ex) {
            LOGGER.warn("", ex);
        }

        this.dataService.addOrUpdate(bean.getUid(), "referentials/nash_pjs.xml", "xml", this.createReferentials(entries));

        return bean;
    }

    /**
     * create referential from list of entries
     *
     * @param entries
     * @return byte [] of referential file
     */
    private byte[] createReferentials(final List<String> entries) {
        final FormSpecificationReferential refs = new FormSpecificationReferential();
        final List<ReferentialTextElement> referentialTextElements = new ArrayList<>();
        for (final String entryName : entries) {
            if (!StringUtils.contains(entryName, "XMLTC")) {
                final ReferentialTextElement ref = new ReferentialTextElement();
                ref.setId(entryName);
                ref.setLabel(entryName);
                referentialTextElements.add(ref);
            }
        }
        refs.setReferentialTextElements(referentialTextElements);

        return JaxbFactoryImpl.instance().asByteArray(refs);
    }

    @Override
    @ApiOperation("Execute an action to a specific record file")
    public Response executeAction(@ApiParam("the record identifier") final String code, @ApiParam("the action identifier") final String action,
            @ApiParam("the engine user identifier") final String engineUser) {

        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        if (StringUtils.isBlank(code) || StringUtils.isBlank(action) || StringUtils.isBlank(engineUser)) {
            return Response.noContent().build();
        }

        if (RecordActionTypeBOEnum.isExecAction(action)) {
            // -->Getting step id for execution
            final String stepId = RecordActionTypeBOEnum.getExecStepId(action);
            return this.executeStep(code, stepId, engineUser);
        }
        return this.redirectAction(code, action);
    }

    /**
     * Calculate the URL for redirection.
     *
     * @param code
     *            The record identifier
     * @param action
     *            The action identifier
     * @param engineUser
     *            The engine user
     * @return
     * @throws URISyntaxException
     */
    private Response redirectAction(final String code, final String action) {
        String relativePath = null;
        if (RecordActionTypeBOEnum.VIEW.getTypeActionName().equals(action)) {
            relativePath = "/record/" + code;
        } else if (RecordActionTypeBOEnum.DOWNLOAD.getTypeActionName().equals(action)) {
            relativePath = "/record/" + code + "/download.zip";
        } else if (RecordActionTypeBOEnum.DOCUMENTS.getTypeActionName().equals(action)) {
            relativePath = "/record/" + code + "/documents.zip";
        } else if (RecordActionTypeBOEnum.EXPORT.getTypeActionName().equals(action)) {
            relativePath = "/record/" + code + "/export";
        } else if (RecordActionTypeBOEnum.DELETE.getTypeActionName().equals(action)) {
            relativePath = "/record/" + code + "/remove";
        } else {
            return Response.serverError().build();
        }
        LOGGER.debug("Relative path for redirection : {}", relativePath);
        return Response.ok(relativePath).build();
    }

    /**
     * Execute a step for a record.
     *
     * @param code
     *            The record identifier
     * @param stepName
     *            The step identifier
     * @param engineUser
     *            The engine user
     * @return The response execution
     */
    private Response executeStep(final String code, final String stepId, final String engineUser) {
        LOGGER.debug("Trying to execute step name '{}'", stepId);
        // -->Execution report
        final IProvider recordDataProvider = RecordDataServiceProvider.create(this.dataService, code);
        final SpecificationLoader loader = this.engine.loader(recordDataProvider);

        // -->Execute previous step assigned to engine user with todo status
        // -->Stop executing previous steps if one of them has todo status and
        // assigned to different step user
        for (int i = loader.currentStepPosition(); i <= loader.stepsMgr().find(stepId).getPosition(); i++) {
            final StepElement step = loader.stepsMgr().find(i);
            if (!StringUtils.equals(StepStatusEnum.DONE.getStatus(), step.getStatus())) {
                if (engineUser.equals(step.getUser())) {
                    this.processStep(code, loader, step);
                    LOGGER.debug("The step '{}' has status '{}'", step.getId(), step.getStatus());
                }
            }
        }
        // for (final StepElement step : loader.stepsMgr().findAll()) {
        // if (step.getId().equals(stepId) &&
        // !StringUtils.equals(StepStatusEnum.DONE.getStatus(),
        // step.getStatus())) {
        // if (engineUser.equals(step.getUser())) {
        // this.processStep(code, loader, step);
        // }
        // }
        //
        // if (!step.getId().equals(stepId) &&
        // !StringUtils.equals(StepStatusEnum.DONE.getStatus(),
        // step.getStatus())) {
        // LOGGER.debug("The step '{}' has status '{}' and is assigned to user
        // '{}'", step.getId(), step.getStatus(), step.getUser());
        // if (engineUser.equals(step.getUser())) {
        // this.processStep(code, loader, step);
        // }
        // }
        // }

        return Response.ok("/record/" + code + "/" + loader.stepsMgr().find(stepId).getPosition() + "/validate").build();
    }

    /**
     * Execute a step for a record.
     *
     * @param code
     *            The record identifier
     * @param loader
     *            The loader
     * @param step
     *            The step to execute
     */
    private String processStep(final String code, final SpecificationLoader loader, final StepElement step) {
        LOGGER.info("Begin execute {} step of record with uid {}", step.getId(), code);
        loader.processes().preExecute(step);
        loader.validate(loader.data(step));
        loader.processes().postExecute(step);
        loader.updateStepStatus(loader.currentStepPosition(), StepStatusEnum.DONE, loader.description().getAuthor());
        loader.flush();
        LOGGER.info("Success execution of {} step for Record {} ", step.getId(), code);
        final StringBuilder report = new StringBuilder();
        report.append(REPORT_RETURN_LINE).append(REPORT_STEP_LABEL_START).append(step.getId()).append(REPORT_STEP_LABEL_STATUS).append(StepStatusEnum.DONE.getStatus());
        final String reportContent = report.toString();
        LOGGER.info(reportContent);
        return reportContent;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response uploadProxyResult(final String recordUid, final String stepId, final String processPhase, final String processId, final String resultFileName, final ProxyResultBean proxyResult) {
        final IProvider recordDataProvider = RecordDataServiceProvider.create(this.dataService, recordUid);
        final SpecificationLoader loader = this.engine.loader(recordDataProvider);

        FormSpecificationProcess processes = null;
        final StepElement stepElement = loader.stepsMgr().find(stepId);
        if ("pre".equals(processPhase)) {
            processes = loader.processes().findPreprocesses(stepElement);
        } else if ("post".equals(processPhase)) {
            processes = loader.processes().findPostprocesses(stepElement);
        } else {
            throw new TechnicalException();
        }

        final IProvider stepProvider = loader.provider(processes.getResourceName());
        final FormSpecificationData data = this.createData(proxyResult, loader, stepProvider);

        final ProcessElement processElement = processes.getProcesses().stream() //
                .filter(ProcessElement.class::isInstance) //
                .map(ProcessElement.class::cast) //
                .filter(prc -> processId.equals(prc.getId())) //
                .findFirst() //
                .orElse(null);

        if (processElement != null) {
            data.setId(processElement.getId());
            stepProvider.save(resultFileName, data);
        } else {
            LOGGER.error("id {} of process doesn't exist in {} process ", processId, processes.getResourceName());
            throw new TechnicalException();
        }

        // -->Execute all processes from process phase
        if ("pre".equals(processPhase)) {
            loader.processes().preExecuteAfter(stepElement, processId);
            loader.validate(loader.data(stepElement));
            LOGGER.debug("Execute all pre processes from step identifier ", stepId);
        } else if ("post".equals(processPhase)) {
            loader.processes().postExecuteAfter(stepElement, processId);
            LOGGER.debug("Execute all post processes from step identifier ", stepId);
        }

        // No need to update the step status here.
        loader.flush();

        return Response.ok().build();
    }

    private FormSpecificationData createData(final ProxyResultBean proxy, final SpecificationLoader loader, final IProvider stepProvider) {

        final Map<String, byte[]> documents = proxy.getDocuments();

        final ClasspathProvider provider = new ClasspathProvider(this.getClass(), ".");
        final FormSpecificationData data = provider.asBean("proxy-result.xml", FormSpecificationData.class);

        final DataElement statusDataElement = (DataElement) SpecificationLoader.find(data, "proxyResult.status");
        ValueAdapterFactory.type(data, statusDataElement).set(statusDataElement, proxy.getStatus());

        final DataElement messageDataElement = (DataElement) SpecificationLoader.find(data, "proxyResult.message");
        ValueAdapterFactory.type(data, messageDataElement).set(messageDataElement, proxy.getMessage());

        final DataElement filesDataElement = (DataElement) SpecificationLoader.find(data, "proxyResult.files");
        final IValueAdapter<?> type = ValueAdapterFactory.type(data, filesDataElement);
        if (null != documents) {
            final List<Item> items = documents.entrySet().stream() //
                    .map(entry -> FileValueAdapter.createItem(-1, entry.getKey(), MimeTypeUtil.type(entry.getKey()), () -> entry.getValue())) //
                    .collect(Collectors.toList());

            type.set(loader, stepProvider, filesDataElement, items);
        }

        return data;
    }

    /**
     * {@inheritDoc}
     *
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @Override
    @ApiOperation("Create a record with a context using reference of a published model.")
    public Response uploadWithContext(final String reference, final String author, final byte[] context, final String lang) throws JsonParseException, JsonMappingException, IOException {
        return this.internalService.uploadWithContext(reference, author, Arrays.asList(new SearchQueryFilter(this.defaultPrefixAuthor + author, "*")), context, lang);
    }

    @Override
    @ApiOperation("Assign all records from an owner to an assignee author")
    public Response assign(@ApiParam("the owner identifier") final String owner, @ApiParam("the assignee identifier") final String assignee) {
        LOGGER.info("Assign all records from owner {} to {} ", owner, assignee);
        if (StringUtils.isEmpty(owner) || StringUtils.isEmpty(assignee)) {
            return Response.noContent().build();
        }

        final SearchResult<RecordInfoBean> searchResult = this.dataService.search(0, Integer.MAX_VALUE, Arrays.asList(new SearchQueryFilter("author", ":", owner)), null, null, null,
                RecordInfoBean.class);

        LOGGER.info("Assign total of {} records to {} ", searchResult.getTotalResults(), assignee);

        if (null != searchResult.getContent()) {
            for (final RecordInfoBean bean : searchResult.getContent()) {
                final FileEntry entry = this.dataService.resource(bean.getCode(), Engine.FILENAME_DESCRIPTION);
                if (null == entry) {
                    continue;
                }

                final FormSpecificationDescription desc = JaxbFactoryImpl.instance().unmarshal(entry.getContentAsStream(), FormSpecificationDescription.class);
                desc.setAuthor(assignee);

                this.dataService.addOrUpdate( //
                        bean.getCode(), //
                        Engine.FILENAME_DESCRIPTION, entry.type(), //
                        JaxbFactoryImpl.instance().asByteArray(desc, StylesheetUtil.resolve(Engine.FILENAME_DESCRIPTION, desc.getClass())) //
                );

                this.dataService.assignRoles(bean.getCode(), assignee, Arrays.asList(new SearchQueryFilter(this.defaultPrefixAuthor + assignee, "*")));
            }
        }

        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean own(final String code, final String author) {
        final SearchResult<RecordInfoBean> rawSearchResult = this.dataService.search(0L, 1L, Arrays.asList(new SearchQueryFilter("uid", ":", code)), null,
                Arrays.asList(new SearchQueryFilter(author, ":", "*")), null, RecordInfoBean.class);
        return null != rawSearchResult && rawSearchResult.getTotalResults() == 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Deletes all records whose update date is less than X days")
    public Response purge(@ApiParam("the number of days before removing") final long days) {
        this.dataService.remove(Arrays.asList(new SearchQueryFilter("updated", "<=", LocalDateTime.now().minusDays(days).format(SPEC_DATE_FORMATTER))));
        return Response.ok().build();
    }

    /**
     * Build specified record zip resource, filtering "cache" resources.
     *
     * @param record
     *            record bean
     * @return zip resource
     */
    private byte[] getZipAsBytes(final RecordBean record) {

        final Map<String, Function<String, byte[]>> providers = new HashMap<>();

        this.dataService.resources(record.getUid()) //
                .forEach(file -> providers.put(file.getName(), name -> Optional.ofNullable(this.dataService.resource(record.getUid(), name)).map(FileEntry::asBytes).orElse(null)));

        /*
         * Remove merge with record model
         */
        // final byte[] specContent =
        // Optional.ofNullable(this.specDataService.resource(record.getSpec())).map(FileEntry::asBytes).orElse(null);
        // ZipUtil.entries(specContent) //
        // .forEach(resourcePath -> providers.putIfAbsent(resourcePath, name ->
        // ZipUtil.entryAsBytes(name, specContent)));

        StylesheetUtil.stylesheets() //
                .forEach(resourcePath -> providers.put(resourcePath, name -> ResourceUtil.resourceAsBytes("/" + resourcePath)));

        try {
            return ZipUtil.create( //
                    providers.keySet().stream() //
                            .filter(name -> !name.startsWith("__cache__/")) //
                            .collect(Collectors.toList()), //
                    name -> providers.get(name).apply(name) //
            );
        } catch (final TechnicalException ex) {
            LOGGER.warn("Error while building ZIP file for record {}", record.getUid(), ex);
            throw ex;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response lock(final String code) {
        final IProvider recordDataProvider = RecordDataServiceProvider.create(this.dataService, code);
        final SpecificationLoader loader = this.engine.loader(recordDataProvider);
        loader.updateStepStatus(loader.currentStepPosition(), StepStatusEnum.ERROR, loader.description().getAuthor());
        loader.flush();
        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response unlock(final String code) {
        final IProvider recordDataProvider = RecordDataServiceProvider.create(this.dataService, code);
        final SpecificationLoader loader = this.engine.loader(recordDataProvider);

        final StepElement lockedStep = loader.stepsMgr().findFirst(step -> StepStatusEnum.ERROR.getStatus().equals(step.getStatus()));

        if (null == lockedStep || StringUtils.isEmpty(lockedStep.getStatus())) {
            return Response.noContent().build();
        }

        // -->Generate a data file if necessary
        loader.save(lockedStep.getData(), new ClasspathProvider(this.getClass(), ".").asBean("data-unlock-step.xml", FormSpecificationData.class));

        loader.updateStepStatus(lockedStep.getPosition(), StepStatusEnum.DONE, loader.description().getAuthor());
        loader.flush();
        loader.processes().postExecute(lockedStep);

        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response reset(final String code) {
        this.dataService.checkIfExistsAndHasAccess(code, this.acls.asList());

        if (!this.dataService.exists(code)) {
            return Response.noContent().build();
        }

        final SpecificationLoader loader = this.engine.loader(RecordDataServiceProvider.create(this.dataService, code));

        // -->Find the step from its identifier
        final StepElement stepReset = loader.stepsMgr().current();
        if (null == stepReset) {
            return Response.noContent().build();
        }

        // -->Find output files into (pre/post)process files to be removed
        final EngineContext<SpecificationLoader> engineContext = loader.buildEngineContext();
        final IProvider stepProvider = loader.provider(stepReset.getData());
        Arrays.asList(stepReset.getPreprocess(), stepReset.getPostprocess()) //
                .stream() //
                .filter(StringUtils::isNotEmpty) //
                .map(processFile -> engineContext.target(processFile, FormSpecificationProcess.class)) //
                .map(context -> context.getElement()) //
                .filter(elm -> CollectionUtils.isNotEmpty(elm.getProcesses())) //
                .map(FormSpecificationProcess::getProcesses) //
                .forEach(listProcesses -> {
                    if (CollectionUtils.isNotEmpty(listProcesses)) {
                        listProcesses.forEach(iProcess -> {
                            final EngineContext<IProcess> engineContextProcess = engineContext.target(iProcess);
                            final IProcess root = engineContextProcess.getElement();

                            if (root instanceof ProcessElement) {
                                final ProcessElement elm = (ProcessElement) root;

                                if (StringUtils.isNotEmpty(elm.getOutput())) {
                                    final String resourcePathOutput = Optional.ofNullable(elm.getOutput()).map(input -> stepProvider.getAbsolutePath(input)).orElse(null);
                                    final String resourcePathInput = Optional.ofNullable(elm.getInput()).map(input -> stepProvider.getAbsolutePath(input)).orElse(null);

                                    if (!stepReset.getData().equals(resourcePathOutput) && !resourcePathOutput.equals(resourcePathInput)) {
                                        engineContextProcess.getBaseProvider().remove(resourcePathOutput);
                                        loader.getProvider().remove(resourcePathOutput);
                                        LOGGER.debug("Remove file from step {} : {}", stepReset.getId(), resourcePathOutput);
                                    } else {
                                        LOGGER.debug("Cannot remove data file from step {} : {}", stepReset.getId(), resourcePathOutput);
                                    }
                                }
                            }
                        });
                    }
                });

        loader.flush();

        return Response.ok().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Response remove(final List<SearchQueryFilter> filters) {
        if (CollectionUtils.isEmpty(filters)) {
            return Response.noContent().build();
        }

        final long deleted = this.dataService.remove(filters);
        if (deleted > 0) {
            return Response.ok().build();
        }

        return Response.status(Status.NOT_MODIFIED).build();
    }

}