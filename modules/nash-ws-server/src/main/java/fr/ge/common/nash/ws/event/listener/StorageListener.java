/**
 *
 */
package fr.ge.common.nash.ws.event.listener;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.TemporaryException;
import fr.ge.common.nash.core.storage.IRecordStorage;
import fr.ge.common.nash.engine.loader.event.StorageEvent;
import fr.ge.common.nash.engine.loader.event.Subscribe;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.CoreUtil;

/**
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Component
public class StorageListener {

    /** The storage record root. */
    @Value("${storage.record.root:#{null}}")
    private String storageRecordRoot;

    /** The record service. */
    @Autowired
    private IRecordService recordService;

    /** The record service. */
    @Autowired
    private IRecordStorage recordStorage;

    /**
     * Mutateur sur l'attribut {@link #storageRecordRoot}.
     *
     * @param storageRecordRoot
     *     la nouvelle valeur de l'attribut storageRecordRoot
     */
    public void setStorageRecordRoot(final String storageRecordRoot) {
        this.storageRecordRoot = storageRecordRoot;
    }

    /**
     * Mutateur sur l'attribut {@link #recordService}.
     *
     * @param recordService
     *     la nouvelle valeur de l'attribut recordService
     */
    public void setRecordService(final IRecordService recordService) {
        this.recordService = recordService;
    }

    /**
     * Mutateur sur l'attribut {@link #recordStorage}.
     *
     * @param recordStorage
     *     la nouvelle valeur de l'attribut recordStorage
     */
    public void setRecordStorage(final IRecordStorage recordStorage) {
        this.recordStorage = recordStorage;
    }

    /**
     * Store record on file system, only if folder defined.
     *
     * @param event
     *     storage event
     */
    @Subscribe
    public void onStorageEvent(final StorageEvent event) {
        if (StringUtils.isBlank(this.storageRecordRoot)) {
            return;
        }

        // store on the file system only if the folder is defined
        CoreUtil.time("Store record", () -> {
            final String code = event.getLoader().description().getRecordUid();
            final Response response = this.recordService.download(code);
            if (response != null && Status.OK.getStatusCode() == response.getStatus()) {
                try {
                    this.recordStorage.storeRecord(code, response.readEntity(byte[].class), event.getLoader().description().getFormUid());
                } catch (final TemporaryException e) {
                    throw new TechnicalException(e);
                }
            }
        });
    }

}
