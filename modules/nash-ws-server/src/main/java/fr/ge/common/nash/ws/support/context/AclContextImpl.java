/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.support.context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.cxf.jaxrs.utils.JAXRSUtils;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

public class AclContextImpl implements AclContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(AclContext.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String HEADER_ROLES = "X-Roles";

    private final Message message;

    public AclContextImpl(final Message message) {
        this.message = message;
    }

    @Override
    public List<SearchQueryFilter> asList() {
        final Message msg = this.getCurrentMessage();
        final Map<String, List<String>> headers = CoreUtil.cast(msg.get(Message.PROTOCOL_HEADERS));

        if (null == headers) {
            return new ArrayList<>();
        }

        final List<String> rolesHeader = headers.get(HEADER_ROLES);
        if (null == rolesHeader) {
            return new ArrayList<>();
        }

        final Map<String, List<SearchQueryFilter>> roles = new HashMap<>();

        for (final String asString : rolesHeader) {
            try {
                final Map<String, List<String>> asObj = MAPPER.readValue(asString, new TypeReference<Map<String, List<String>>>() {
                });

                for (final Entry<String, List<String>> entry : asObj.entrySet()) {
                    final String entityName = entry.getKey();
                    final List<String> entityRoleNames = Optional.ofNullable(entry.getValue()).orElseGet(() -> Arrays.asList("*"));

                    List<SearchQueryFilter> entityRoles = roles.get(entityName);
                    if (null == entityRoles) {
                        roles.put(entityName, entityRoles = new ArrayList<>());
                    }

                    entityRoles.add(new SearchQueryFilter(entityName, ":", entityRoleNames));
                }
            } catch (final IOException ex) {
                LOGGER.info("Unable to read {} header : {}", HEADER_ROLES, ex.getMessage());
            }

        }

        return roles.values().stream().flatMap(lst -> lst.stream()).collect(Collectors.toList());
    }

    private Message getCurrentMessage() {
        final Message msg = JAXRSUtils.getCurrentMessage();
        if (null == msg) {
            return this.message;
        } else {
            return msg;
        }
    }

}
