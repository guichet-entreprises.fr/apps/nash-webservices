/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.core.util.UidFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.VersionHandler;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationEntities;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.ReferenceElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.ws.data.bean.AclEntityRoleBean;
import fr.ge.common.nash.ws.data.bean.GroupSpecificationBean;
import fr.ge.common.nash.ws.data.bean.ReferenceBean;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.SpecificationDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.SpecificationEntityRoleDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.SpecificationMetaDao;
import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class SpecificationDataServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Service
public class SpecificationDataServiceImpl extends AbstractSearchDataServiceImpl<SpecificationBean> implements ISpecificationDataService {

    /** The Constant PATTERN_SPECIFICATION_FILENAME. */
    private static final String PATTERN_SPECIFICATION_FILENAME = "%s.zip";

    /** The dao. */
    @Autowired
    private SpecificationDao dao;

    /** The specification meta dao. */
    @Autowired
    private SpecificationMetaDao specificationMetaDao;

    /** The reference service. */
    @Autowired
    private IReferenceDataService referenceService;

    @Autowired
    private SpecificationEntityRoleDao aclDao;

    /** Default colors. **/
    @Value("${specification.color.default.list:#0092BC}")
    private String[] colors;

    /** Default font awesome icon. **/
    @Value("${specification.icon.default:desktop}")
    private String icon;

    /**
     * Load.
     *
     * @param uid
     *            the uid
     * @return the specification bean
     */
    @Override
    @Transactional(readOnly = true)
    public SpecificationBean load(final String uid) {
        return this.dao.findByUid(uid);
    }

    /**
     * Load by reference.
     *
     * @param reference
     *            the reference
     * @return the specification bean
     */
    @Override
    @Transactional(readOnly = true)
    public SpecificationBean loadByReference(final String reference) {
        return this.dao.findByReference(reference);
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     */
    @Override
    @Transactional
    public void remove(final String uid) {
        this.dao.remove(uid);
    }

    /** The uid factory. */
    @Autowired
    private UidFactoryImpl uidFactory;

    /**
     * From file.
     *
     * @param code
     *            the code
     * @param resourceAsBytes
     *            the resource as bytes
     * @param published
     *            the published
     * @param author
     *            the author
     * @param group
     *            the group
     * @return the specification bean
     * @throws FunctionalException
     *             the functional exception
     */
    @Override
    @Transactional
    public SpecificationBean fromFile(final String code, final byte[] resourceAsBytes, final boolean published, final String author, final String group) throws FunctionalException {

        final IProvider provider = new ZipProvider(resourceAsBytes);
        final byte[] entryAsBytes = provider.asBytes(Engine.FILENAME_DESCRIPTION);

        if (ArrayUtils.isEmpty(entryAsBytes)) {
            throw new TechnicalException("No description found (bad file format ?)");
        }

        final FormSpecificationDescription description = JaxbFactoryImpl.instance().unmarshal(entryAsBytes, FormSpecificationDescription.class);

        final byte[] resourceMeta = provider.asBytes(Engine.FILENAME_META);

        // prepare spec to persist
        SpecificationBean spec = this.dao.findByUid(code);
        if (null == spec) {
            spec = new SpecificationBean();
            if (published && !StringUtils.isEmpty(code)) {
                spec.setUid(code);
            } else {
                spec.setUid(this.uidFactory.uid());
            }
        }

        if (StringUtils.isEmpty(description.getIcon())) {
            description.setIcon(this.icon);
        }

        if (StringUtils.isEmpty(description.getColor())) {
            description.setColor(this.colors[(int) Math.floor(Math.random() * this.colors.length)]);
        }

        spec.setColor(description.getColor());
        spec.setIcon(description.getIcon());
        spec.setName(description.getTitle());
        spec.setReference(Optional.ofNullable(description.getReference()).map(ref -> ref.getCode()).orElse(null));
        spec.setVersion(VersionHandler.getVersion(entryAsBytes, "description"));
        spec.setAuthor(author);
        spec.setGroupe(Optional.ofNullable(group).filter(StringUtils::isNotEmpty).orElse("all"));
        spec.setRevision(this.dao.nextRevision(spec));

        // define uid
        description.setFormUid(spec.getUid());
        description.setRecordUid(null);

        Optional.ofNullable(description.getReference()) //
                .map(ReferenceElement::getCode) //
                .map(this.referenceService::create) //
                .map(ReferenceBean::getId) //
                .ifPresent(spec::setReferenceId);

        // Define reference revision
        description.getReference().setRevision(spec.getRevision());

        provider.save(Engine.FILENAME_DESCRIPTION, description);

        // this.sanitizePdfResources(provider, content);

        final byte[] resourceRoles = provider.asBytes(Engine.FILENAME_ROLES);

        // -->Deleting file "roles.xml"
        if (ArrayUtils.isNotEmpty(resourceRoles)) {
            provider.remove(Engine.FILENAME_ROLES);
        }

        // -->Create specification
        spec = this.dao.createOrUpdate(spec);

        try (FileEntry entry = new FileEntry(String.format(PATTERN_SPECIFICATION_FILENAME, spec.getUid()), "application/zip").asBytes(provider.asBytes())) {
            this.dao.createOrUpdateResource( //
                    spec.getUid(), //
                    entry //
            );
            // spec.setSourceId(sourceId);
        } catch (final IOException ex) {
            LoggerFactory.getLogger(SpecificationDataServiceImpl.class).warn("Persisting specification resource : unable to close resource stream", ex);
        }

        if (description.getDescription() != null) {
            final Map<String, String> details = new HashMap<>();
            details.put("description", description.getDescription());
            spec.setDetails(new ObjectMapper().convertValue(details, ObjectNode.class));
        }

        FormSpecificationMeta metas = new FormSpecificationMeta();
        if (ArrayUtils.isNotEmpty(resourceMeta)) {
            metas = JaxbFactoryImpl.instance().unmarshal(resourceMeta, FormSpecificationMeta.class);
        }

        metas.getMetas().addAll(this.buildMetas(description));

        this.specificationMetaDao.create(spec.getUid(), metas.getMetas().toArray(new MetaElement[] {}));

        /*
         * FIXME determinate default entity/role for specification, must be the
         * same as for records
         */

        // Retrieve roles from roles.xml
        List<AclEntityRoleBean> acls = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(resourceRoles)) {
            final FormSpecificationEntities rolesXML = JaxbFactoryImpl.instance().unmarshal(resourceRoles, FormSpecificationEntities.class);
            rolesXML.getEntities().forEach(entity -> {
                entity.getRoles().forEach(role -> acls.add(new AclEntityRoleBean().setEntity(entity.getFullPath()).setRole(role.getRole())));
            });
        }

        if (CollectionUtils.isEmpty(acls)) {
            acls.add(new AclEntityRoleBean().setEntity("all").setRole("all"));
        }

        this.aclDao.create(spec.getUid(), acls.toArray(new AclEntityRoleBean[] {}));

        spec = this.dao.createOrUpdate(spec);

        if (published) {
            this.dao.tag(spec.getUid());
            spec.setTagged(true);
        }

        return spec;
    }

    /**
     * Publish.
     *
     * @param uid
     *            the uid
     */
    @Override
    @Transactional
    public void publish(final String uid) {
        this.dao.tag(uid);
    }

    /**
     * Unpublish.
     *
     * @param uid
     *            the uid
     */
    @Override
    @Transactional
    public void unpublish(final String uid) {
        this.dao.untag(uid);
    }

    /**
     * Find all groupe.
     *
     * @return the list
     */
    @Override
    public List<GroupSpecificationBean> findAllGroupe() {
        throw new TechnicalException("Not yet implemented");
    }

    /**
     * Resource.
     *
     * @param uid
     *            the uid
     * @return the file entry
     */
    @Override
    @Transactional(readOnly = true)
    public FileEntry resource(final String uid) {
        return this.dao.findResource(uid);
    }

    /**
     * Resource.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return the file entry
     */
    @Override
    @Transactional(readOnly = true)
    public FileEntry resource(final String uid, final String resourcePath) {
        return this.dao.findResourceEntry(uid, resourcePath);
    }

    /**
     * Builds the metas.
     *
     * @param description
     *            the description
     * @return the collection
     */
    private Collection<MetaElement> buildMetas(final FormSpecificationDescription description) {
        final Collection<MetaElement> metas = new ArrayList<>();

        Optional.ofNullable(description.getReference()) //
                .map(value -> value.getCode()) //
                .map(value -> StringUtils.substring(value, 0, 254)) //
                .map(value -> StringUtils.replace(value, "/", StringUtils.SPACE)) //
                .map(value -> this.buildMetaElement("reference", value)) //
                .ifPresent(metas::add);

        Optional.ofNullable(description.getTitle()) //
                .map(value -> StringUtils.substring(value, 0, 254)) //
                .map(value -> this.buildMetaElement("title", value)) //
                .ifPresent(metas::add);

        Optional.ofNullable(description.getDescription()) //
                .map(value -> StringUtils.substring(value, 0, 254)) //
                .map(value -> this.buildMetaElement("description", value)) //
                .ifPresent(metas::add);

        return metas;
    }

    /**
     * Builds the meta element.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @return the meta element
     */
    private MetaElement buildMetaElement(final String name, final String value) {
        if (null == value) {
            return null;
        }

        final MetaElement meta = new MetaElement();
        meta.setName(name);
        meta.setValue(value);

        return meta;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.ge.common.nash.ws.data.jdbc.service.impl.AbstractSearchDataServiceImpl
     * #getDao()
     */
    @Override
    protected AbstractSearchDao<SpecificationBean> getDao() {
        return this.dao;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * fr.ge.common.nash.ws.data.jdbc.service.impl.AbstractSearchDataServiceImpl
     * #getSearchClass()
     */
    @Override
    protected Class<? extends SpecificationBean> getSearchClass() {
        return SpecificationBean.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public SpecificationBean updatePriorityByReference(final String reference, final int priority) {
        this.dao.updatePriorityByReference(reference, priority);
        return this.dao.findByReference(reference);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public SpecificationBean updatePriorityByCode(final String code, final int priority) {
        this.dao.updatePriorityByCode(code, priority);
        return this.dao.findByUid(code);
    }

}
