/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao.query;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * The Class AbstractSqlQuery.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public abstract class AbstractSqlQuery<T extends AbstractSqlQuery<T>> {

    /** The table name. */
    private final String tableName;

    /** The sequence name. */
    private final String sequenceName;

    /** The key. */
    private SqlQueryField<Number> key;

    /** The fields. */
    private final List<SqlQueryField<?>> fields = new ArrayList<>();

    /**
     * Instantiates a new abstract sql query.
     *
     * @param tableName
     *            the table name
     */
    public AbstractSqlQuery(final String tableName) {
        this(tableName, "sq_" + tableName);
    }

    /**
     * Instantiates a new abstract sql query.
     *
     * @param tableName
     *            the table name
     * @param sequenceName
     *            the sequence name
     */
    public AbstractSqlQuery(final String tableName, final String sequenceName) {
        this.tableName = tableName;
        this.sequenceName = sequenceName;
    }

    /**
     * Adds the key.
     *
     * @param name
     *            the name
     * @return the t
     */
    public T addKey(final String name) {
        return this.addKey(name, null);
    }

    /**
     * Adds the key.
     *
     * @param name
     *            the name
     * @param supplier
     *            the supplier
     * @return the t
     */
    public final T addKey(final String name, final Supplier<Number> supplier) {
        this.key = new SqlQueryField<>(name, supplier, Number.class);
        return (T) this;
    }

    /**
     * Adds the field.
     *
     * @param name
     *            the name
     * @param supplier
     *            the supplier
     * @param expected
     *            the expected
     * @return the t
     */
    public final T addField(final String name, final Supplier<?> supplier, final Class<?> expected) {
        return this.addField(name, supplier, -1, expected);
    }

    public final T addField(final String name, final Supplier<?> supplier, final int type, final Class<?> expected) {
        this.fields.add(new SqlQueryField<>(name, supplier, type, expected));
        return (T) this;
    }

    /**
     * Gets the table name.
     *
     * @return the table name
     */
    protected final String getTableName() {
        return this.tableName;
    }

    /**
     * Gets the sequence name.
     *
     * @return the sequence name
     */
    protected final String getSequenceName() {
        return this.sequenceName;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    protected final SqlQueryField<Number> getKey() {
        return this.key;
    }

    /**
     * Gets the fields.
     *
     * @return the fields
     */
    protected final List<SqlQueryField<?>> getFields() {
        return this.fields;
    }

    /**
     * Execute.
     *
     * @param jdbcTemplate
     *            the jdbc template
     * @return the long
     */
    public abstract Long execute(JdbcTemplate jdbcTemplate);

}
