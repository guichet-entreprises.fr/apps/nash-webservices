/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.ws.data.bean.MetaBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class MetaDao.
 *
 * @author Christian Cougourdan
 */
public abstract class MetaDao extends AbstractSearchDao<MetaBean> {

    /** The bean mapper. */
    private final RowMapper<MetaBean> beanMapper = (rs, rowNum) -> new MetaBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
            .setUid(SqlAccessorFactory.get(String.class).get(rs, "uid")) //
            .setKey(SqlAccessorFactory.get(String.class).get(rs, "key")) //
            .setValue(SqlAccessorFactory.get(String.class).get(rs, "value")) //
    ;

    /** The Constant DATE_FORMATTER. */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /** The Constant SEARCH_FILTERS. */
    private static final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> SEARCH_FILTERS;

    static {
        final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> map = new HashMap<>();

        map.put("uid", buildQueryPredicate(GROUP_FILTER_AND, "m", v -> v));
        map.put("key", buildQueryPredicate(GROUP_FILTER_AND, "m", v -> v));
        map.put("value", buildQueryPredicate(GROUP_FILTER_AND, "m", v -> v));
        map.put("created", buildQueryPredicate(GROUP_FILTER_AND, "m", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("updated", buildQueryPredicate(GROUP_FILTER_AND, "m", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("author", buildQueryPredicate(GROUP_FILTER_AND, "m", v -> v));

        SEARCH_FILTERS = Collections.unmodifiableMap(map);
    }

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Display the meta.
     * 
     * @return
     */
    protected abstract String getMetaTableName();

    /**
     * Creates the.
     *
     * @param uid
     *            the uid
     * @param metas
     *            the metas
     * @return the long
     */
    public long create(final String uid, final MetaElement... metas) {
        final Instant now = Instant.now();

        final int[] cnts = jdbcTemplate.batchUpdate( //
                String.format("INSERT INTO %s (id, created, updated, author, uid, key, value) VALUES (nextval('sq_meta'), ?, ?, ?, ?, ?, ?)", this.getMetaTableName()), //
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(final PreparedStatement ps, final int i) throws SQLException {
                        SqlAccessorFactory.get(Instant.class).set(ps, 1, now);
                        SqlAccessorFactory.get(Instant.class).set(ps, 2, now);
                        SqlAccessorFactory.get(String.class).set(ps, 3, null);
                        SqlAccessorFactory.get(String.class).set(ps, 4, uid);
                        SqlAccessorFactory.get(String.class).set(ps, 5, metas[i].getName());
                        SqlAccessorFactory.get(String.class).set(ps, 6, metas[i].getValue());
                    }

                    @Override
                    public int getBatchSize() {
                        return metas.length;
                    }

                });

        return Arrays.stream(cnts).sum();
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchQuery()
     */
    @Override
    protected SqlQuery getSearchQuery() {
        return new SqlQuery("m.*", String.format("%s m", this.getMetaTableName()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchFilter(java.lang.String)
     */
    @Override
    protected BiConsumer<SqlQuery, SearchQueryFilter> getSearchFilter(final String column) {
        return SEARCH_FILTERS.get(column);
    }

    /*
     * (non-Javadoc)
     * 
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchRowMapper()
     */
    @Override
    protected RowMapper<MetaBean> getSearchRowMapper() {
        return beanMapper;
    }

}
