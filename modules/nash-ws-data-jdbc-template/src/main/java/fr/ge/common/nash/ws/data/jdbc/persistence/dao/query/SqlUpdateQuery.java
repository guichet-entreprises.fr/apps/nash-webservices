/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao.query;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.ISqlAccessor;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;

/**
 * The Class SqlUpdateQuery.
 *
 * @author Christian Cougourdan
 */
public class SqlUpdateQuery extends AbstractSqlQuery<SqlUpdateQuery> {

    /**
     * Instantiates a new sql update query.
     *
     * @param tableName
     *            the table name
     */
    public SqlUpdateQuery(final String tableName) {
        super(tableName);
    }

    /**
     * Instantiates a new sql update query.
     *
     * @param tableName
     *            the table name
     * @param sequenceName
     *            the sequence name
     */
    public SqlUpdateQuery(final String tableName, final String sequenceName) {
        super(tableName, sequenceName);
    }

    @Override
    public Long execute(final JdbcTemplate jdbcTemplate) {
        return (long) jdbcTemplate.update(cx -> {
            final PreparedStatement ps = cx.prepareStatement(this.buildQuery());

            final List<SqlQueryField<?>> lst = new ArrayList<>(this.getFields());
            lst.add(this.getKey());

            for (int idx = 0; idx < lst.size(); idx++) {
                final SqlQueryField<?> fld = lst.get(idx);
                final ISqlAccessor<?> accessor = fld.getType() >= 0 ? SqlAccessorFactory.get(fld.getType(), fld.getExpected()) : SqlAccessorFactory.get(fld.getExpected());
                if (null != accessor) {
                    accessor.set(ps, idx + 1, fld.getValue());
                }
            }

            return ps;
        });
    }

    /**
     * Builds the query.
     *
     * @return the string
     */
    private String buildQuery() {
        final List<String> lstFields = new ArrayList<>();
        this.getFields().forEach(fld -> {
            lstFields.add(fld.getName() + " = ?");
        });

        return String.format("UPDATE %s SET %s WHERE %s = ?", this.getTableName(), String.join(", ", lstFields), this.getKey().getName());
    }

}
