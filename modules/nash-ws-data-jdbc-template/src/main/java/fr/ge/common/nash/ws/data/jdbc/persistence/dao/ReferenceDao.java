/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import fr.ge.common.nash.ws.data.bean.ReferenceBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.SqlInsertQuery;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class ReferenceDao.
 *
 * @author Christian Cougourdan
 */
@Component
public class ReferenceDao extends AbstractSearchDao<ReferenceBean> {

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The bean mapper. */
    private final RowMapper<ReferenceBean> beanMapper = (rs, rowNum) -> new ReferenceBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
            .setParent(SqlAccessorFactory.get(Long.class).get(rs, "parent")) //
            .setCategory(SqlAccessorFactory.get(String.class).get(rs, "category")) //
            .setContent(SqlAccessorFactory.get(String.class).get(rs, "content")) //
            .setTotalSpecifications(SqlAccessorFactory.get(Long.class).get(rs, "total")) //
            .setNbSpecificationsPossible(SqlAccessorFactory.get(Long.class).get(rs, "nb_specs")) //
    ;

    /** The Constant SEARCH_FILTERS. */
    private static final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> SEARCH_FILTERS;

    static {
        final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> map = new HashMap<>();

        map.put("parent", buildQueryPredicate(GROUP_FILTER_AND, "r", v -> v));

        map.put("nb_specs", buildQueryPredicate(GROUP_FILTER_AND, "r", v -> v));
        map.put("groupe", (query, filter) -> {

            final List<String> groupeClause = new ArrayList<>();
            final List<String> groupeValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(groupe -> StringUtils.isNotEmpty(groupe)) //
                    .forEach(groupe -> {
                        groupeClause.add("?");
                        groupeValues.add(groupe);
                    });

            if (!groupeClause.isEmpty()) {
                query.addFilter(GROUP_FILTER_AND,
                        "r.content IN (SELECT SUBSTRING(sp.reference, 1, LENGTH(r.content)) FROM specification sp WHERE sp.groupe IN (" + String.join(", ", groupeClause) + "))",
                        groupeValues.toArray(new Object[] {}));
            }
        });

        SEARCH_FILTERS = Collections.unmodifiableMap(map);
    }

    /**
     * Find by id.
     *
     * @param id
     *            the id
     * @return the reference bean
     */
    public ReferenceBean findById(final long id) {
        /*-
         * A more reliable method with this query, for root references in example (parent is null) :
         *     WITH ref_aggr AS (
         *         WITH RECURSIVE ref1 AS (
         *             SELECT r.id AS parent_id, r.id, (SELECT COUNT(1) FROM specification s WHERE s.reference_id = r.id) AS nb
         *             FROM reference r
         *             WHERE r.parent IS NULL
         *                 UNION ALL
         *             SELECT p.parent_id, r.id, (SELECT COUNT(1) FROM specification s WHERE s.reference_id = r.id) AS nb
         *             FROM reference r
         *                 INNER JOiN ref1 p ON p.id = r.parent
         *         )
         *         SELECT r.parent_id AS id, SUM(nb) AS total
         *         FROM ref1 r
         *         GROUP BY 1
         *     )
         *     SELECT s.id, r.created, r.updated, r.category, r.content, s.total
         *     FROM ref_aggr s
         *         INNER JOIN reference r ON r.id = s.id
         *     ORDER BY 1;
         */
        final List<ReferenceBean> entries = this.jdbcTemplate.query( //
                "SELECT r.*, (SELECT COUNT(1) FROM specification s WHERE s.reference_id = r.id) AS total, (SELECT COUNT(1) FROM specification s WHERE s.reference LIKE CONCAT(r.content, '%')) AS nb_specs FROM reference r WHERE r.id = ? LIMIT 1", //
                new Object[] { id }, //
                this.beanMapper);

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Find by category and parent.
     *
     * @param category
     *            the category
     * @param parent
     *            the parent
     * @return the reference bean
     */
    public ReferenceBean findByCategoryAndParent(final String category, final Long parent) {
        Object[] values;
        String parentFilter;
        if (null == parent) {
            parentFilter = "parent IS NULL";
            values = new Object[] { category };
        } else {
            parentFilter = "parent = ?";
            values = new Object[] { category, parent };
        }

        final List<ReferenceBean> entries = this.jdbcTemplate.query( //
                "SELECT r.*, (SELECT COUNT(1) FROM specification s WHERE s.reference_id = r.id) AS total, (SELECT COUNT(1) FROM specification s WHERE s.reference LIKE CONCAT(r.content, '%')) AS nb_specs FROM reference r WHERE r.category = ? AND "
                        + parentFilter + " LIMIT 1", //
                values, //
                this.beanMapper);

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Creates the.
     *
     * @param entry
     *            the entry
     * @return the reference bean
     */
    public ReferenceBean create(final ReferenceBean entry) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        final Long id = new SqlInsertQuery("reference") //
                .addKey("id") //
                .addField("created", () -> now, Instant.class) //
                .addField("updated", () -> now, Instant.class) //
                .addField("category", entry::getCategory, String.class) //
                .addField("content", entry::getContent, String.class) //
                .addField("parent", () -> entry.getParent(), Long.class) //
                .execute(this.jdbcTemplate) //
        ;

        return entry //
                .setId(id) //
                .setCreated(nowAsCalendar) //
                .setUpdated(nowAsCalendar);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchQuery()
     */
    @Override
    protected SqlQuery getSearchQuery() {
        // return new SqlQuery("r.*, (SELECT COUNT(1) FROM specification s WHERE
        // s.reference_id = r.id) AS total", "reference r");
        return new SqlQuery("r.*",
                "(SELECT i.*, (SELECT COUNT(1) FROM specification s WHERE s.reference_id = i.id) AS total, (SELECT COUNT(1) FROM specification s WHERE s.reference LIKE CONCAT(i.content, '%')) AS nb_specs FROM reference i) AS r");
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchFilter(java.lang.String)
     */
    @Override
    protected BiConsumer<SqlQuery, SearchQueryFilter> getSearchFilter(final String column) {
        return SEARCH_FILTERS.get(column);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao#
     * getSearchRowMapper()
     */
    @Override
    protected RowMapper<ReferenceBean> getSearchRowMapper() {
        return this.beanMapper;
    }

}
