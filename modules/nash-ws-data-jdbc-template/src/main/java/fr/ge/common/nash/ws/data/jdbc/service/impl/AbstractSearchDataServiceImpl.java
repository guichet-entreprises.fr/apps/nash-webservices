/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao;
import fr.ge.common.nash.ws.data.service.ISearchable;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Class AbstractSearchDataServiceImpl.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public abstract class AbstractSearchDataServiceImpl<T> implements ISearchable {

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * Search.
     *
     * @param <R>
     *            the generic type
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param searchTerms
     *            the search terms
     * @param expected
     *            the expected
     * @return the search result
     */
    @Override
    public <R> SearchResult<R> search(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders, final List<SearchQueryFilter> roles,
            final String searchTerms, final Class<? extends R> expected) {

        final SearchResult<T> entries = this.getDao().find(startIndex, maxResults, filters, orders, roles, searchTerms);

        if (expected.isAssignableFrom(this.getSearchClass())) {
            return CoreUtil.cast(entries);
        } else {
            final SearchResult<R> searchResult = new SearchResult<>(startIndex, maxResults);
            searchResult.setTotalResults(entries.getTotalResults());

            searchResult.setContent(entries.getContent().stream().map(e -> this.dozer.map(e, expected)).collect(Collectors.toList()));

            return searchResult;
        }

    }

    /**
     * Search.
     *
     * @param <R>
     *            the generic type
     * @param query
     *            the query
     * @param expected
     *            the expected
     * @return the search result
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery query, final Class<? extends R> expected) {
        return this.search(query.getStartIndex(), query.getMaxResults(), query.getFilters(), query.getOrders(), null, null, expected);
    }

    /**
     * Gets the dao.
     *
     * @return the dao
     */
    protected abstract AbstractSearchDao<T> getDao();

    /**
     * Gets the search class.
     *
     * @return the search class
     */
    protected abstract Class<? extends T> getSearchClass();

}
