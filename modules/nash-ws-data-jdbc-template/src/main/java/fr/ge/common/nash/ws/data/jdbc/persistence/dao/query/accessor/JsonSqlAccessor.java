/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collection;

import org.postgresql.util.PGobject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class JsonSqlAccessor.
 *
 * @author Christian Cougourdan
 */
public class JsonSqlAccessor implements ISqlAccessor<JsonNode> {

    /** The mapper. */
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Class<? extends JsonNode> accept() {
        return JsonNode.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Integer> types() {
        return Arrays.asList(Types.JAVA_OBJECT);
    }

    @Override
    public void set(final PreparedStatement ps, final int idx, final Object value) throws SQLException {
        if (null == value) {
            ps.setObject(idx, null);
        } else {
            final PGobject obj = new PGobject();
            obj.setType("json");
            obj.setValue(this.toString(value));

            ps.setObject(idx, obj);
        }
    }

    @Override
    public JsonNode get(final ResultSet rs, final String columnName) throws SQLException {

        final PGobject obj = rs.getObject(columnName, PGobject.class);
        if (null != obj) {
            final String value = obj.getValue();
            if (null != value) {
                try {
                    return this.mapper.readTree(value);
                } catch (final IOException ex) {
                    throw new TechnicalException("Unable to unserialize JSON object", ex);
                }
            }
        }

        return null;
    }

    /**
     * To string.
     *
     * @param value
     *            the value
     * @return the string
     */
    private String toString(final Object value) {
        try {
            return this.mapper.writeValueAsString(value);
        } catch (final JsonProcessingException ex) {
            throw new TechnicalException("Unable to serialize JSON object", ex);
        }
    }

}
