/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Collection;

import org.postgresql.PGConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;

import fr.ge.common.utils.exception.TechnicalException;

public class BlobSqlAccessor implements ISqlAccessor<byte[]> {

    private static final int WRITE_BUFFER_SIZE = 2048;

    @Override
    public Class<? extends byte[]> accept() {
        return byte[].class;
    }

    @Override
    public Collection<Integer> types() {
        return Arrays.asList(Types.BINARY, Types.BLOB, Types.LONGVARBINARY, Types.VARBINARY);
    }

    @Override
    public void set(final PreparedStatement ps, final int idx, final Object value) throws SQLException {
        if (null == value) {
            ps.setNull(idx, ps.getParameterMetaData().getParameterType(idx));
        } else if (value instanceof byte[]) {
            ps.setLong(idx, this.buildLargeObject(ps, (byte[]) value));
        } else if (value instanceof InputStream) {
            ps.setLong(idx, this.buildLargeObject(ps, (InputStream) value));
        }
    }

    @Override
    public byte[] get(final ResultSet rs, final String columnName) throws SQLException {
        final long oid = rs.getLong(columnName);
        if (rs.wasNull()) {
            return null;
        } else {
            final LargeObjectManager lom = this.getPGConnection(rs.getStatement().getConnection()).getLargeObjectAPI();
            final LargeObject lo = lom.open(oid, LargeObjectManager.READ);

            final byte[] asBytes = lo.read(lo.size());
            lo.close();

            return asBytes;
        }
    }

    public Long size(final ResultSet rs, final String columnName) throws SQLException {
        final long oid = rs.getLong(columnName);
        if (rs.wasNull()) {
            return null;
        } else {
            final LargeObjectManager lom = this.getPGConnection(rs.getStatement().getConnection()).getLargeObjectAPI();
            final LargeObject lo = lom.open(oid, LargeObjectManager.READ);

            final long sz = lo.size64();
            lo.close();

            return sz;
        }
    }

    private long buildLargeObject(final PreparedStatement ps, final byte[] bytes) throws SQLException {
        final LargeObject lo = this.createLargeObject(ps);

        lo.write(bytes);
        final long oid = lo.getLongOID();
        lo.close();

        return oid;
    }

    private long buildLargeObject(final PreparedStatement ps, final InputStream bytes) throws SQLException {
        final LargeObject lo = this.createLargeObject(ps);

        final byte[] buffer = new byte[WRITE_BUFFER_SIZE];
        int ln;
        try {
            while ((ln = bytes.read(buffer, 0, WRITE_BUFFER_SIZE)) > 0) {
                lo.write(buffer, 0, ln);
            }
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read binary stream", ex);
        }

        final long oid = lo.getLongOID();
        lo.close();

        return oid;
    }

    private LargeObject createLargeObject(final PreparedStatement ps) throws SQLException {
        final LargeObjectManager lom = this.getPGConnection(ps.getConnection()).getLargeObjectAPI();
        final long oid = lom.createLO();
        return lom.open(oid, LargeObjectManager.WRITE);
    }

    private PGConnection getPGConnection(final Connection cx) throws SQLException {
        if (cx.isWrapperFor(PGConnection.class)) {
            return cx.unwrap(PGConnection.class);
        } else {
            return (PGConnection) cx;
        }
    }

}
