/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.nash.ws.data.bean.ReferenceBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.ReferenceDao;
import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.utils.CoreUtil;

/**
 * The Class ReferenceDataServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Service
public class ReferenceDataServiceImpl extends AbstractSearchDataServiceImpl<ReferenceBean> implements IReferenceDataService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReferenceDataServiceImpl.class);

    /** The Constant SEPARATOR. */
    private static final String SEPARATOR = "/";

    /** The dao. */
    @Autowired
    private ReferenceDao dao;

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * Creates the.
     *
     * @param code
     *            the code
     * @return the reference bean
     */
    @Override
    @Transactional
    public ReferenceBean create(final String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }

        LOGGER.debug("the reference is refernce{}", code);

        final String[] list = StringUtils.split(code, SEPARATOR);

        final List<ReferenceBean> references = new ArrayList<>();
        Long parent = null;

        final StringBuilder content = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            content.append(list[i]);

            final String path = content.toString();

            final ReferenceBean existingBean = this.dao.findByCategoryAndParent(list[i], parent);
            if (null == existingBean) {
                LOGGER.info("the reference with category {} and empty parent doesn't exist in database", path);
                final ReferenceBean ref = this.dao.create( //
                        new ReferenceBean() //
                                .setContent(path) //
                                .setCategory(list[i]) //
                                .setParent(parent));
                parent = ref.getId();
                references.add(ref);
            } else {
                LOGGER.info("the reference with category {} and empty parent already exist in database", path);
                parent = existingBean.getId();
                references.add(existingBean);
            }

            content.append(SEPARATOR);
        }

        return references.get(references.size() - 1);

    }

    /**
     * Load.
     *
     * @param <T>
     *            the generic type
     * @param id
     *            the id
     * @param expected
     *            the expected
     * @return the t
     */
    @Override
    @Transactional(readOnly = true)
    public <T> T load(final long id, final Class<T> expected) {
        final ReferenceBean bean = this.dao.findById(id);

        if (expected.isAssignableFrom(ReferenceBean.class)) {
            return CoreUtil.cast(bean);
        } else {
            return this.dozer.map(bean, expected);
        }
    }

    /* (non-Javadoc)
     * @see fr.ge.common.nash.ws.data.jdbc.service.impl.AbstractSearchDataServiceImpl#getDao()
     */
    @Override
    protected AbstractSearchDao<ReferenceBean> getDao() {
        return this.dao;
    }

    /* (non-Javadoc)
     * @see fr.ge.common.nash.ws.data.jdbc.service.impl.AbstractSearchDataServiceImpl#getSearchClass()
     */
    @Override
    protected Class<? extends ReferenceBean> getSearchClass() {
        return ReferenceBean.class;
    }

}
