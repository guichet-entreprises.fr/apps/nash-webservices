/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Class AbstractSearchDao.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            the generic type
 */
public abstract class AbstractSearchDao<T> {

    /** The Constant OPERATORS. */
    private static final Map<String, String> OPERATORS;

    static {
        final Map<String, String> m = new HashMap<>();

        m.put(":", "=");
        m.put("!", "<>");
        m.put("~", "LIKE");
        m.put("in", "=");
        Arrays.asList(">", "<", ">=", "<=").forEach(key -> m.put(key, key));

        OPERATORS = Collections.unmodifiableMap(m);
    }

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** Operation AND. */
    public static final String GROUP_FILTER_AND = "AND";

    /** Operation OR. */
    public static final String GROUP_FILTER_OR = "OR";

    /**
     * Builds the query predicate.
     *
     * @param alias
     *            the alias
     * @param accessor
     *            the accessor
     * @return the bi consumer
     */
    protected static BiConsumer<SqlQuery, SearchQueryFilter> buildQueryPredicate(final String operator, final CharSequence alias, final Function<String, Object> accessor) {
        return (query, filter) -> {
            final String op = OPERATORS.get(filter.getOperator());
            if (null != op) {
                final String column = (null == alias ? "" : alias + ".") + filter.getColumn();
                if ("=".equals(op)) {
                    final List<Object> values = filter.getValues().stream().filter(StringUtils::isNotEmpty).map(accessor).collect(Collectors.toList());
                    if (values.isEmpty()) {
                        query.addFilter(operator, column + " IS NULL");
                    } else if (values.size() == 1) {
                        query.addFilter(operator, column + " = ?", values.get(0));
                    } else {
                        query.addFilter(operator, //
                                column + " IN (" + values.stream().map(value -> "?").collect(Collectors.joining(", ")) + ")", //
                                values.toArray(new Object[] {}) //
                        );
                    }
                } else if ("<>".equals(op)) {
                    final List<Object> values = filter.getValues().stream().filter(StringUtils::isNotEmpty).map(accessor).collect(Collectors.toList());
                    if (values.isEmpty()) {
                        query.addFilter(operator, column + " IS NOT NULL");
                    } else if (values.size() == 1) {
                        query.addFilter(operator, //
                                column + " <> ?", //
                                values.get(0) //
                        );
                    } else {
                        query.addFilter(operator, //
                                column + " NOT IN (" + values.stream().map(value -> "?").collect(Collectors.joining(", ")) + ")", //
                                values.toArray(new Object[] {}) //
                        );
                    }
                } else if ("LIKE".equals(op)) {
                    if (StringUtils.isNotEmpty(filter.getValue())) {
                        query.addFilter(operator, "LOWER(" + column + ") LIKE ?", accessor.apply(filter.getValue().toLowerCase().replace('*', '%')));
                    }
                } else {
                    query.addFilter(operator, column + ' ' + op + " ?", accessor.apply(filter.getValue()));
                }
            }
        };
    }

    /**
     * Find.
     *
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param filters
     *            the filters
     * @param orders
     *            the orders
     * @param searchTerms
     *            the search terms
     * @return the search result
     */
    @Transactional(readOnly = true)
    public SearchResult<T> find(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders, final List<SearchQueryFilter> roles,
            final String searchTerms) {
        final SqlQuery query = this.getSearchQuery();

        if (null != filters) {
            for (final SearchQueryFilter filter : filters) {
                Optional.ofNullable(this.getSearchFilter(filter.getColumn())).ifPresent(builder -> {
                    builder.accept(query, filter);
                });
            }
        }

        if (CollectionUtils.isNotEmpty(roles)) {
            Optional.ofNullable(this.getSearchFilter("roles")).ifPresent(builder -> {
                for (final SearchQueryFilter role : roles) {
                    builder.accept(query, role);
                }
            });
        } else {

            Optional.ofNullable(this.getSearchFilter("roles")).ifPresent(builder -> {
                builder.accept(query, new SearchQueryFilter(null, null));
            });
        }

        if (StringUtils.isNotEmpty(searchTerms)) {
            Optional.ofNullable(this.getSearchFilter("searchTerms")).ifPresent(builder -> {
                builder.accept(query, new SearchQueryFilter("searchTerms", StringUtils.stripEnd(StringUtils.stripStart(searchTerms, null), null).replaceAll("\\s+", " & ")));
            });
        }

        if (null != orders) {
            for (final SearchQueryOrder order : orders) {
                query.addOrder(order.getColumn() + " " + order.getOrder());
            }
        }

        return query.execute(this.jdbcTemplate, startIndex, maxResults, this.getSearchRowMapper());
    }

    /**
     * Gets the search query.
     *
     * @return the search query
     */
    protected abstract SqlQuery getSearchQuery();

    /**
     * Gets the search filter.
     *
     * @param column
     *            the column
     * @return the search filter
     */
    protected abstract BiConsumer<SqlQuery, SearchQueryFilter> getSearchFilter(String column);

    /**
     * Gets the search row mapper.
     *
     * @return the search row mapper
     */
    protected abstract RowMapper<T> getSearchRowMapper();

}
