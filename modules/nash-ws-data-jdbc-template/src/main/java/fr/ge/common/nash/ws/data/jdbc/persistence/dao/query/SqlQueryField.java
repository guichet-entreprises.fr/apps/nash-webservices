/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao.query;

import java.util.function.Supplier;

/**
 * The Class SqlQueryField.
 *
 * @author Christian Cougourdan
 * @param <T>
 *            value type returned by supplier
 */
public class SqlQueryField<T> {

    /** The name. */
    private final String name;

    /** The supplier. */
    private final Supplier<T> supplier;

    private final int type;

    /** The expected. */
    private final Class<?> expected;

    /**
     * Instantiates a new sql query field.
     *
     * @param name
     *            the name
     * @param supplier
     *            the supplier
     * @param expected
     *            the expected
     */
    public SqlQueryField(final String name, final Supplier<T> supplier, final Class<?> expected) {
        this(name, supplier, -1, expected);
    }

    public SqlQueryField(final String name, final Supplier<T> supplier, final int type, final Class<?> expected) {
        this.name = name;
        this.supplier = supplier;
        this.type = type;
        this.expected = expected;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public T getValue() {
        return this.supplier.get();
    }

    protected int getType() {
        return this.type;
    }

    /**
     * Gets the expected.
     *
     * @return the expected
     */
    public Class<?> getExpected() {
        return this.expected;
    }
}
