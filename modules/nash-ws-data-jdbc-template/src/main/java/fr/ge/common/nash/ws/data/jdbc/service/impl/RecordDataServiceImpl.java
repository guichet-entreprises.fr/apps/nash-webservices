/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.bean.StepUserTypeEnum;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.core.util.UidFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.description.StepsElement;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.util.StylesheetUtil;
import fr.ge.common.nash.ws.data.bean.AclEntityRoleBean;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.bean.RecordPropertiesBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractEntityRoleDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.AbstractSearchDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.RecordDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.RecordEntityRoleDao;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.RecordMetaDao;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.utils.CoreUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * The Class RecordDataServiceImpl.
 *
 * @author Christian Cougourdan
 */
@Service
public class RecordDataServiceImpl extends AbstractSearchDataServiceImpl<RecordBean> implements IRecordDataService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RecordDataServiceImpl.class);

    /** The dao. */
    @Autowired
    private RecordDao dao;

    /** The meta dao. */
    @Autowired
    private RecordMetaDao recordMetaDao;

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /** The uid factory. */
    @Autowired
    private UidFactoryImpl uidFactory;

    @Autowired
    private RecordEntityRoleDao aclDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public void checkIfExistsAndHasAccess(final String uid, final List<SearchQueryFilter> roles) {
        this.dao.checkIfExistsAndHasAccess(uid, AbstractEntityRoleDao.buildEntityRoles(null, roles));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = true)
    public <T> T load(final String uid, final Class<? extends T> expected) {
        final RecordBean bean = this.dao.findByUid(uid);

        if (expected.isAssignableFrom(RecordBean.class)) {
            return CoreUtil.cast(bean);
        } else {
            return this.dozer.map(bean, expected);
        }
    }

    /**
     * Adds the or update.
     *
     * @param uid
     *            the uid
     * @param entryName
     *            the entry name
     * @param type
     *            the type
     * @param entryAsBytes
     *            the entry as bytes
     * @return the file bean
     */
    @Override
    @Transactional
    public FileBean addOrUpdate(final String uid, final String entryName, final String type, final byte[] entryAsBytes) {
        final RecordBean record = this.dao.findByUid(uid);

        if (null == record) {
            return null;
        }

        byte[] updatedResourceAsBytes = Optional.ofNullable(entryAsBytes).map(bytes -> bytes.clone()).orElse(null);
        if (Engine.FILENAME_DESCRIPTION.equals(entryName)) {
            final FormSpecificationDescription desc = JaxbFactoryImpl.instance().unmarshal(entryAsBytes, FormSpecificationDescription.class);

            desc.setRecordUid(record.getUid());

            this.bind(desc, record);

            final String stylesheet = StylesheetUtil.resolve(Engine.FILENAME_DESCRIPTION, desc.getClass());
            updatedResourceAsBytes = JaxbFactoryImpl.instance().asByteArray(desc, stylesheet);
        } else if (Engine.FILENAME_META.equals(entryName)) {
            final FormSpecificationMeta metas = JaxbFactoryImpl.instance().unmarshal(entryAsBytes, FormSpecificationMeta.class);
            if (CollectionUtils.isNotEmpty(metas.getMetas())) {
                this.recordMetaDao.create(record.getUid(), metas.getMetas().toArray(new MetaElement[] {}));
            }
        }

        final FileBean bean = this.dao.createOrUpdateResourceEntry(uid, new FileEntry(entryName, type).asBytes(updatedResourceAsBytes));
        this.dao.createOrUpdate(record);

        return bean;
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     * @param name
     *            the name
     * @return the long
     */
    @Override
    @Transactional
    public long remove(final String uid, final String name) {
        return this.dao.removeResourceEntry(uid, name);
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     * @return the long
     */
    @Override
    @Transactional
    public long remove(final String uid) {
        this.aclDao.remove(uid);
        return this.dao.remove(uid);
    }

    /**
     * From file.
     *
     * @param <T>
     *            the generic type
     * @param formsRecordName
     *            the forms record name
     * @param formsRecordType
     *            the forms record type
     * @param resourceAsBytes
     *            the resource as bytes
     * @param author
     *            the author
     * @param expected
     *            the expected
     * @return the t
     * @throws FunctionalException
     *             the functional exception
     */
    @Override
    @Transactional
    public <T> T fromFile(final String formsRecordName, final String formsRecordType, final byte[] resourceAsBytes, final String author, final List<SearchQueryFilter> roles,
            final Class<? extends T> expected) throws FunctionalException {

        // correctif sur la collision du UID lors de la création
        String uidGenere = this.uidFactory.uid();
        int nbreTentativeGenUid = 0;
        while (exists(uidGenere) && nbreTentativeGenUid < 10) {
            uidGenere = this.uidFactory.uid();
            nbreTentativeGenUid++;
        }
        if (nbreTentativeGenUid == 10) {
            LOGGER.info("Nombre de tentative de génération de  l'UID atteinte 10/10.");
        }
        final RecordBean record = this.dao.createOrUpdate(new RecordBean() //
                .setAuthor(author) //
                .setUid(uidGenere) //
                .setSpec("<unknown>") //
        );

        final Collection<String> entryNames = new HashSet<>();

        try (InputStream resourceAsStream = new ByteArrayInputStream(resourceAsBytes)) {
            final Iterator<FileEntry> entriesIterator = ZipUtil.iterator(resourceAsStream, entry -> {
                entryNames.add(entry.name());
                if (Engine.FILENAME_DESCRIPTION.equals(entry.name())) {
                    final FormSpecificationDescription desc = JaxbFactoryImpl.instance().unmarshal(entry.asBytes(), FormSpecificationDescription.class);

                    desc.setAuthor(author);

                    if (StringUtils.isEmpty(desc.getRecordUid())) {
                        desc.setRecordUid(record.getUid());
                    }

                    if (StringUtils.isNotEmpty(formsRecordName)) {
                        desc.setTitle(formsRecordName);
                    }

                    if (StringUtils.isNotEmpty(formsRecordType)) {
                        desc.setDescription(formsRecordType);
                    }

                    if (StringUtils.isNotEmpty(author)) {
                        desc.setAuthor(author);
                    }

                    this.bind(desc, record);

                    return new FileEntry(Engine.FILENAME_DESCRIPTION, "text/xml") //
                            .asBytes(JaxbFactoryImpl.instance().asByteArray(desc, StylesheetUtil.resolve(Engine.FILENAME_DESCRIPTION, desc.getClass()))) //
                            .lastModified(record.getUpdated());
                } else {
                    final byte[] entryAsBytes = entry.asBytes();
                    if (Engine.FILENAME_META.equals(entry.name())) {
                        final FormSpecificationMeta metas = JaxbFactoryImpl.instance().unmarshal(entryAsBytes, FormSpecificationMeta.class);
                        if (CollectionUtils.isNotEmpty(metas.getMetas())) {
                            this.recordMetaDao.create(record.getUid(), metas.getMetas().toArray(new MetaElement[] {}));
                        }
                    }
                    return entry.asBytes(entryAsBytes);
                }
            });

            final long ts = System.currentTimeMillis();
            this.dao.createResources(record.getUid(), entriesIterator);
            LOGGER.info("*** Record resources persistence delay : {} ms", System.currentTimeMillis() - ts);
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read specification resource", ex);
        }

        if (!entryNames.contains(Engine.FILENAME_DESCRIPTION)) {
            throw new FunctionalException("No description.xml file found in record");
        }

        this.dao.createOrUpdate(record);

        this.aclDao.create(record.getUid(), AbstractEntityRoleDao.buildEntityRoles(author, roles).toArray(new AclEntityRoleBean[] {}));

        this.recordMetaDao.create(record.getUid(), new MetaElement().setName("uid").setValue(record.getUid()), new MetaElement().setName("title").setValue(record.getTitle()));

        if (expected.isAssignableFrom(RecordBean.class)) {
            return CoreUtil.cast(record);
        } else {
            return this.dozer.map(record, expected);
        }
    }

    /**
     * Bind.
     *
     * @param desc
     *            the desc
     * @param record
     *            the record
     */
    private void bind(final FormSpecificationDescription desc, final RecordBean record) {
        RecordPropertiesBean properties = null;
        if (record.getProperties() != null) {
            try {
                properties = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false) //
                        .readValue(record.getProperties(), RecordPropertiesBean.class);
            } catch (final IOException e) {
                LOGGER.warn("Unable to parse record properties", e);
            }
        }
        if (properties == null) {
            properties = new RecordPropertiesBean();
        }

        int nbSteps = 0;
        int nbCompletedSteps = 0;

        final List<StepElement> steps = Optional.ofNullable(desc) //
                .map(FormSpecificationDescription::getSteps) //
                .map(StepsElement::getElements) //
                .orElseGet(Collections::emptyList);

        for (final StepElement step : steps) {
            if (step.getUser() == null || StepUserTypeEnum.USER.getName().equals(step.getUser())) {
                if (StepStatusEnum.DONE.getStatus().equals(step.getStatus())) {
                    ++nbCompletedSteps;
                }
                ++nbSteps;
            }
        }

        properties.setNbSteps(nbSteps);
        properties.setNbCompletedSteps(nbCompletedSteps);

        // update properties
        try {
            final byte[] propertiesAsBytes = new ObjectMapper().writeValueAsBytes(properties);
            record.setProperties(propertiesAsBytes);
        } catch (final JsonProcessingException e) {
            LOGGER.warn("Unable to serialize record properties", e);
        }

        record.setSpec(desc.getFormUid());
        record.setUid(desc.getRecordUid());
        record.setAuthor(desc.getAuthor());
        record.setTitle(desc.getTitle());

        final ObjectNode details = new ObjectMapper().valueToTree(desc);
        details.remove("flow");
        record.setDetails(details);
    }

    /**
     * Exists.
     *
     * @param uid
     *            the uid
     * @return true, if successful
     */
    @Override
    @Transactional(readOnly = true)
    public boolean exists(final String uid) {
        return this.dao.exists(uid);
    }

    /**
     * Resource exists.
     *
     * @param uid
     *            the uid
     * @param name
     *            the name
     * @return true, if successful
     */
    @Override
    @Transactional(readOnly = true)
    public boolean resourceExists(final String uid, final String name) {
        return this.dao.isResourceEntryExist(uid, name);
    }

    /**
     * Resource.
     *
     * @param uid
     *            the uid
     * @param name
     *            the name
     * @return the file entry
     */
    @Override
    @Transactional(readOnly = true)
    public FileEntry resource(final String uid, final String name) {
        return this.dao.findResourceEntry(uid, name);
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.ge.common.nash.ws.data.jdbc.service.impl.
     * AbstractSearchDataServiceImpl# getDao()
     */
    @Override
    protected AbstractSearchDao<RecordBean> getDao() {
        return this.dao;
    }

    /*
     * (non-Javadoc)
     *
     * @see fr.ge.common.nash.ws.data.jdbc.service.impl.
     * AbstractSearchDataServiceImpl# getSearchClass()
     */
    @Override
    protected Class<? extends RecordBean> getSearchClass() {
        return RecordBean.class;
    }

    /**
     * Resources.
     *
     * @param uid
     *            the uid
     * @return the collection
     */
    @Override
    @Transactional(readOnly = true)
    public Collection<FileBean> resources(final String uid) {
        return this.dao.findResourceEntries(uid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false)
    public void assignRoles(final String uid, final String assignee, final List<SearchQueryFilter> roles) {
        this.aclDao.remove(uid);
        this.aclDao.create(uid, AbstractEntityRoleDao.buildEntityRoles(assignee, roles).toArray(new AclEntityRoleBean[] {}));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(readOnly = false)
    public long remove(final List<SearchQueryFilter> filters) {
        return this.dao.removeAll(filters);
    }

}
