/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.InterruptibleBatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.TechnicalException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;
import fr.ge.common.nash.ws.data.bean.AclEntityRoleBean;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.SqlInsertQuery;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.SqlUpdateQuery;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class RecordDao.
 *
 * @author Christian Cougourdan
 */
@Component
public class RecordDao extends AbstractSearchDao<RecordBean> {

    /** The bean mapper. */
    private final RowMapper<RecordBean> beanMapper = (rs, rowNum) -> new RecordBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
            .setAuthor(SqlAccessorFactory.get(String.class).get(rs, "author")) //
            .setTitle(SqlAccessorFactory.get(String.class).get(rs, "title")) //
            .setUid(SqlAccessorFactory.get(String.class).get(rs, "uid")) //
            .setSpec(SqlAccessorFactory.get(String.class).get(rs, "spec")) //
            .setProperties(SqlAccessorFactory.get(Types.BLOB, byte[].class).get(rs, "properties")) //
            .setDetails(SqlAccessorFactory.get(ObjectNode.class).get(rs, "details")) //
    ;

    /** The Constant DATE_FORMATTER. */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /** The Constant SEARCH_FILTERS. */
    private static final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> SEARCH_FILTERS;

    static {
        final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> map = new HashMap<>();

        map.put("uid", buildQueryPredicate(GROUP_FILTER_AND, "r", v -> v));
        map.put("author", buildQueryPredicate(GROUP_FILTER_OR, "r", v -> v));
        map.put("title", buildQueryPredicate(GROUP_FILTER_AND, "r", v -> v));
        map.put("spec", buildQueryPredicate(GROUP_FILTER_AND, "r", v -> v));
        map.put("created", buildQueryPredicate(GROUP_FILTER_AND, "r", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("updated", buildQueryPredicate(GROUP_FILTER_AND, "r", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("searchTerms", (query, filter) -> {
            query.addFilter(GROUP_FILTER_AND, "r.uid IN (SELECT m.uid FROM meta m WHERE to_tsvector('fr', m.value) @@ to_tsquery('fr', ?))", filter.getValue());
        });
        map.put("roles", (query, filter) -> {
            final List<String> rolesClause = new ArrayList<>();
            final List<String> rolesValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(role -> StringUtils.isNotEmpty(role)) //
                    .forEach(role -> {
                        rolesClause.add("?");
                        rolesValues.add(role);
                    });

            if (!rolesClause.isEmpty()) {
                rolesValues.add(0, filter.getColumn() + "%");
                query.addFilter(GROUP_FILTER_OR,
                        "r.uid IN (SELECT rer.uid FROM record_entity_role rer WHERE (rer.entity = '*' OR rer.entity like ?) AND rer.role IN ('*', " + String.join(", ", rolesClause) + "))",
                        rolesValues.toArray(new Object[] {}));
            }
        });
        map.put("stepId", (query, filter) -> {
            query.addFilter(GROUP_FILTER_AND, "steps->>'id' = ?", filter.getValue());
        });
        map.put("stepStatus", (query, filter) -> {

            final List<String> statusClause = new ArrayList<>();
            final List<String> statusValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(status -> StringUtils.isNotEmpty(status)) //
                    .forEach(status -> {
                        statusClause.add("?");
                        statusValues.add(status);
                    });

            if (!statusClause.isEmpty()) {
                query.addFilter(GROUP_FILTER_AND, "steps->>'status' IN (" + String.join(", ", statusClause) + ")", statusValues.toArray(new Object[] {}));
            }
        });
        map.put("stepUser", (query, filter) -> {

            final List<String> statusClause = new ArrayList<>();
            final List<String> statusValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(status -> StringUtils.isNotEmpty(status)) //
                    .forEach(status -> {
                        statusClause.add("?");
                        statusValues.add(status);
                    });

            if (!statusClause.isEmpty()) {
                query.addFilter(GROUP_FILTER_AND, "steps->>'user' IN (" + String.join(", ", statusClause) + ")", statusValues.toArray(new Object[] {}));
            }
        });
        /*
         * map.put("stepStatus", (query, filter) -> {
         * query.addFilter(GROUP_FILTER_AND, "steps->>'status' = ?",
         * filter.getValue()); });
         */
        map.put("completed", (query, filter) -> {
            query.addFilter(GROUP_FILTER_AND, "((c.\"nbSteps\" = c.\"nbCompletedSteps\") = ?)", filter.getValue());
        });
        map.put("payment", (query, filter) -> {
            query.addFilter(GROUP_FILTER_AND,
                    "(EXISTS (SELECT 1 FROM json_to_recordset(r.details->'steps'->'elements') as s(\"id\" text, \"user\" text, \"status\" text) WHERE s.user = 'user' AND s.id = 'payment' AND status IN ('in_progress', 'done'))) = ?",
                    filter.getValue());
        });
        SEARCH_FILTERS = Collections.unmodifiableMap(map);
    }

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Find by uid.
     *
     * @param uid
     *            the uid
     * @return the record bean
     */
    public RecordBean findByUid(final String uid) {
        final List<RecordBean> entries = this.jdbcTemplate.query( //
                "SELECT r.* FROM record r WHERE r.uid = ?", //
                new Object[] { uid }, //
                this.beanMapper);

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Exists.
     *
     * @param uid
     *            the uid
     * @return true, if successful
     */
    public boolean exists(final String uid) {
        final long cnt = this.jdbcTemplate.queryForObject( //
                "SELECT COUNT(1) FROM record r WHERE r.uid = ?", //
                new Object[] { uid }, //
                Long.class);

        return cnt > 0;
    }

    /**
     * Checks if is resource entry exist.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return true, if is resource entry exist
     */
    public boolean isResourceEntryExist(final String uid, final String resourcePath) {
        final long cnt = this.jdbcTemplate.queryForObject( //
                "SELECT COUNT(1) FROM record_file rf INNER JOIN record r ON r.id = rf.record_id WHERE r.uid = ? AND rf.name = ?", //
                new Object[] { uid, resourcePath }, //
                Long.class);

        return cnt > 0;
    }

    /**
     * Find resource entries.
     *
     * @param uid
     *            the uid
     * @return the collection
     */
    public Collection<FileBean> findResourceEntries(final String uid) {
        return this.jdbcTemplate.query( //
                "SELECT rf.id, rf.created, rf.updated, rf.name, rf.type FROM record_file rf INNER JOIN record r ON r.id = rf.record_id WHERE r.uid = ?", //
                (rs, rowNum) -> {
                    return new FileBean() //
                            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
                            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
                            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
                            .setName(rs.getString("name")) //
                            .setType(rs.getString("type")) //
                    ;
                }, //
                new Object[] { uid } //
        );
    }

    /**
     * Find resource entry.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return the file entry
     */
    public FileEntry findResourceEntry(final String uid, final String resourcePath) {
        final List<FileEntry> entries = this.jdbcTemplate.query( //
                "SELECT rf.name, rf.type, rf.updated, rf.content FROM record_file rf INNER JOIN record r ON r.id = rf.record_id WHERE r.uid = ? AND rf.name = ? LIMIT 1", //
                (rs, rowNum) -> {
                    return new FileEntry(rs.getString("name"), rs.getString("type")) //
                            .lastModified(rs.getTimestamp("updated")) //
                            .asBytes(SqlAccessorFactory.get(Types.BLOB, byte[].class).get(rs, "content")) //
                    ;
                }, //
                new Object[] { uid, resourcePath } //
        );

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     * @return the long
     */
    public long remove(final String uid) {
        int cnt = this.jdbcTemplate.update( //
                "DELETE FROM record_file WHERE record_id IN (SELECT r.id FROM record r WHERE r.uid = ?)", //
                new Object[] { uid } //
        );

        cnt += this.jdbcTemplate.update( //
                "DELETE FROM record WHERE uid = ?", //
                new Object[] { uid } //
        );

        return cnt;
    }

    /**
     * Removes the resource entry.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return the long
     */
    public long removeResourceEntry(final String uid, final String resourcePath) {
        return this.jdbcTemplate.update( //
                "DELETE FROM record_file WHERE record_id IN (SELECT r.id FROM record r WHERE r.uid = ?) AND name = ?", //
                new Object[] { uid, resourcePath } //
        );
    }

    /**
     * Creates the or update.
     *
     * @param entry
     *            the entry
     * @return the record bean
     */
    public RecordBean createOrUpdate(final RecordBean entry) {
        if (null == entry.getId()) {
            return this.create(entry);
        } else {
            return this.update(entry);
        }
    }

    /**
     * Creates the.
     *
     * @param entry
     *            the entry
     * @return the record bean
     */
    private RecordBean create(final RecordBean entry) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        final Long id = new SqlInsertQuery("record") //
                .addKey("id", entry::getId) //
                .addField("created", () -> now, Instant.class) //
                .addField("updated", () -> now, Instant.class) //
                .addField("author", entry::getAuthor, String.class) //
                .addField("spec", entry::getSpec, String.class) //
                .addField("uid", entry::getUid, String.class) //
                .addField("title", entry::getTitle, String.class) //
                .addField("properties", entry::getProperties, Types.BLOB, byte[].class) //
                .addField("details", entry::getDetails, JsonNode.class) //
                .execute(this.jdbcTemplate) //
        ;

        return entry //
                .setId(id) //
                .setCreated(nowAsCalendar) //
                .setUpdated(nowAsCalendar);
    }

    /**
     * Update.
     *
     * @param entry
     *            the entry
     * @return the record bean
     */
    private RecordBean update(final RecordBean entry) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        new SqlUpdateQuery("record") //
                .addKey("id", entry::getId) //
                .addField("updated", () -> now, Instant.class) //
                .addField("author", entry::getAuthor, String.class) //
                .addField("spec", entry::getSpec, String.class) //
                .addField("uid", entry::getUid, String.class) //
                .addField("title", entry::getTitle, String.class) //
                .addField("properties", entry::getProperties, Types.BLOB, byte[].class) //
                .addField("details", entry::getDetails, JsonNode.class) //
                .execute(this.jdbcTemplate) //
        ;

        return entry.setUpdated(nowAsCalendar);
    }

    public long createResources(final String uid, final Iterator<FileEntry> entries) {
        final Instant now = Instant.now();

        final Long recordId = this.jdbcTemplate.queryForObject( //
                "SELECT r.id FROM record r WHERE r.uid = ?", //
                new Object[] { uid }, //
                Long.class //
        );

        final int[] cnts = this.jdbcTemplate.batchUpdate( //
                "INSERT INTO record_file (id, created, updated, record_id, name, type, size, content) VALUES (nextval('sq_record_file'), ?, ?, ?, ?, ?, 0, ?)", //
                new InterruptibleBatchPreparedStatementSetter() {

                    private boolean toInterrupt = false;

                    @Override
                    public boolean isBatchExhausted(final int i) {
                        return this.toInterrupt;
                    }

                    @Override
                    public void setValues(final PreparedStatement ps, final int i) throws SQLException {
                        if (entries.hasNext()) {
                            FileEntry entry = entries.next();

                            ps.setTimestamp(1, Timestamp.from(now));
                            ps.setTimestamp(2, Timestamp.from(now));
                            ps.setLong(3, recordId);
                            ps.setString(4, entry.name());
                            ps.setString(5, Optional.ofNullable(entry.type()).orElse("application/octet-stream"));
                            SqlAccessorFactory.get(Types.BLOB, byte[].class).set(ps, 6, entry.asBytes());

                            try {
                                entry.close();
                            } catch (final IOException ex) {
                                throw new TechnicalException("Unable to close [" + entry.name() + "] resource stream", ex);
                            }

                            entry = null;
                        } else {
                            this.toInterrupt = true;
                        }
                    }

                    @Override
                    public int getBatchSize() {
                        return Integer.MAX_VALUE;
                    }

                });

        return Arrays.stream(cnts).sum();
    }

    /**
     * Creates the or update resource entry.
     *
     * @param uid
     *            the uid
     * @param entry
     *            the entry
     * @return the file bean
     */
    public FileBean createOrUpdateResourceEntry(final String uid, final FileEntry entry) {
        final Object[] ids = this.jdbcTemplate.queryForObject( //
                "SELECT r.id, rf.id, rf.updated FROM record r LEFT OUTER JOIN record_file rf ON rf.record_id = r.id AND rf.name = ? WHERE r.uid = ?", //
                new Object[] { entry.name(), uid }, //
                (rs, rowNum) -> new Object[] { rs.getLong(1), rs.getLong(2), Optional.ofNullable(rs.getTimestamp(3)).map(Timestamp::toInstant).orElse(null) } //
        );

        if (null == ids[1] || null == ids[2]) {
            return this.createResourceEntry((Long) ids[0], entry);
        } else {
            return this.updateResourceEntry((Long) ids[1], entry).setCreated(GregorianCalendar.from(ZonedDateTime.ofInstant((Instant) ids[2], ZoneId.systemDefault())));
        }
    }

    /**
     * Creates the resource entry.
     *
     * @param recordId
     *            the record id
     * @param entry
     *            the entry
     * @return the file bean
     */
    private FileBean createResourceEntry(final long recordId, final FileEntry entry) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        final KeyHolder keyHolder = new GeneratedKeyHolder();

        this.jdbcTemplate.update(cx -> {
            final PreparedStatement ps = cx.prepareStatement( //
                    "INSERT INTO record_file (id, created, updated, record_id, name, type, size, content) VALUES (nextval('sq_record_file'), ?, ?, ?, ?, ?, 0, ?)", //
                    new String[] { "id" } //
            );

            ps.setTimestamp(1, Timestamp.from(now));
            ps.setTimestamp(2, Timestamp.from(now));
            ps.setLong(3, recordId);
            ps.setString(4, entry.name());
            ps.setString(5, Optional.ofNullable(entry.type()).orElse("application/octet-stream"));
            SqlAccessorFactory.get(Types.BLOB, byte[].class).set(ps, 6, entry.getContentAsStream());

            return ps;
        }, keyHolder);

        return new FileBean() //
                .setId(Optional.ofNullable(keyHolder.getKeys().get("id")).map(Number.class::cast).map(Number::longValue).orElse(-1L)) //
                .setName(entry.name()) //
                .setType(entry.type()) //
                .setCreated(nowAsCalendar) //
                .setUpdated(nowAsCalendar);
    }

    /**
     * Update resource entry.
     *
     * @param entryId
     *            the entry id
     * @param entry
     *            the entry
     * @return the file bean
     */
    private FileBean updateResourceEntry(final long entryId, final FileEntry entry) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        this.jdbcTemplate.update(cx -> {
            final PreparedStatement ps = cx.prepareStatement("UPDATE record_file SET updated = ?, type = ?, size = 0, content = ? WHERE id = ?");

            ps.setTimestamp(1, Timestamp.from(now));
            ps.setString(2, Optional.ofNullable(entry.type()).orElse("application/octet-stream"));
            SqlAccessorFactory.get(Types.BLOB, byte[].class).set(ps, 3, entry.getContentAsStream());
            ps.setLong(4, entryId);

            return ps;
        });

        return new FileBean() //
                .setId(entryId) //
                .setName(entry.name()) //
                .setType(entry.type()) //
                .setUpdated(nowAsCalendar);
    }

    private static class AclBean {

        private final String uid;

        private final long nbAllow;

        public AclBean(final String uid, final long nbAllow) {
            this.uid = uid;
            this.nbAllow = nbAllow;
        }

        /**
         * @return the uid
         */
        public String getUid() {
            return this.uid;
        }

        /**
         * @return the nbAllow
         */
        public long getNbAllow() {
            return this.nbAllow;
        }

    }

    public boolean checkIfExistsAndHasAccess(final String uid, final Collection<AclEntityRoleBean> roles) {
        final StringBuilder builder = new StringBuilder();
        final List<String> roleCriterias = new ArrayList<>();
        final List<Object> params = new ArrayList<>();
        final boolean hasAllAccess = null == roles ? false : roles.stream().anyMatch(roleBean -> "*".equals(roleBean.getEntity()) && "*".equals(roleBean.getRole()));
        final boolean hasRoles = CollectionUtils.isNotEmpty(roles);

        if (hasAllAccess) {
            builder.append("SELECT r.uid, 1 AS nb FROM record r WHERE r.uid = ?");
        } else {
            builder.append("SELECT r.uid, COUNT(acl.id) AS nb FROM record r LEFT OUTER JOIN record_entity_role acl ON acl.uid = r.uid");

            if (hasRoles) {
                roles.forEach(roleBean -> {
                    if ("*".equals(roleBean.getRole())) {
                        roleCriterias.add("acl.entity = ? OR acl.entity LIKE ?");
                        params.add(roleBean.getEntity());
                        params.add(roleBean.getEntity() + "/%");
                    } else {
                        roleCriterias.add("(acl.entity = ? OR acl.entity LIKE ?) AND acl.role in ('*', ?)");
                        params.add(roleBean.getEntity());
                        params.add(roleBean.getEntity() + "/%");
                        params.add(roleBean.getRole());
                    }
                });

                builder.append(" AND (") //
                        .append(String.join(" OR ", roleCriterias)) //
                        .append(")");
            }

            builder.append(" WHERE r.uid = ? GROUP BY r.uid");
        }

        params.add(uid);

        final List<AclBean> beans = this.jdbcTemplate.query( //
                builder.toString(), //
                (rs, idx) -> new AclBean(rs.getString("uid"), rs.getLong("nb")), //
                params.toArray(new Object[params.size()]) //
        );

        final AclBean bean = beans.stream().findFirst().orElse(null);
        if (null == bean || null == bean.getUid()) {
            throw new RecordNotFoundException("Record [" + uid + "] not found");
        } else if (bean.getNbAllow() < 1 || !hasRoles) {
            throw new UnauthorizedAccessException("Unauthorized access to record [" + uid + "]");
        }

        return true;
    }

    @Override
    protected SqlQuery getSearchQuery() {
        return new SqlQuery( //
                "r.*, (c.\"nbSteps\" = c.\"nbCompletedSteps\") as \"completed\"", //
                "record r, json_array_elements(r.details->'steps'->'elements') steps, json_to_record(encode(lo_get(r.properties), 'escape')::json) as c(\"nbSteps\" int, \"nbCompletedSteps\" int)", //
                "2, 3, c.\"nbSteps\", c.\"nbCompletedSteps\"");
    }

    @Override
    protected BiConsumer<SqlQuery, SearchQueryFilter> getSearchFilter(final String column) {
        return SEARCH_FILTERS.get(column);
    }

    @Override
    protected RowMapper<RecordBean> getSearchRowMapper() {
        return this.beanMapper;
    }

    /**
     * Remove all records based on criteria.
     * 
     * @param uids
     * @param fromDate
     * @param filters
     */
    public long removeAll(final List<String> uids, final String fromDate, final List<SearchQueryFilter> filters) {
        final List<SearchQueryFilter> recordFilters = Optional.ofNullable(filters) //
                .filter(f -> null != f && !f.isEmpty()) //
                .map(f -> f) //
                .orElse(new ArrayList<SearchQueryFilter>());

        // -->Prepare search records
        final SqlQuery sqlQueryRecord = new SqlQuery(null, "record r");
        if (CollectionUtils.isNotEmpty(uids)) {
            recordFilters.add(new SearchQueryFilter("uid" + ":" + uids.stream().collect(Collectors.joining(","))));
        }

        if (StringUtils.isNotEmpty(fromDate)) {
            recordFilters.add(new SearchQueryFilter("updated<=" + fromDate));
        }

        if (CollectionUtils.isEmpty(recordFilters)) {
            return 0;
        }

        recordFilters //
                .stream() //
                .forEach(filter -> this.getSearchFilter(filter.getColumn()).accept(sqlQueryRecord, new SearchQueryFilter(filter.getColumn(), filter.getOperator(), filter.getValues())));

        return sqlQueryRecord.removeAll(this.jdbcTemplate);
    }

    /**
     * Remove all records based on criteria.
     * 
     * @param filters
     */
    public long removeAll(final List<SearchQueryFilter> filters) {
        final List<SearchQueryFilter> recordFilters = Optional.ofNullable(filters) //
                .filter(f -> null != f && !f.isEmpty()) //
                .map(f -> f) //
                .orElse(new ArrayList<SearchQueryFilter>());

        // -->Prepare search records
        final SqlQuery sqlQueryRecord = new SqlQuery(null, "record r");
        if (CollectionUtils.isEmpty(recordFilters)) {
            return 0;
        }

        if (recordFilters //
                .stream() //
                .filter(filter -> null != filter && //
                        StringUtils.isNotEmpty(filter.getOperator()) && //
                        StringUtils.isNotEmpty(filter.getColumn()) && //
                        CollectionUtils.isNotEmpty(filter.getValues()) && //
                        null != this.getSearchFilter(filter.getColumn()) //
                ) //
                .count() == 0L) {
            return 0;
        }

        recordFilters //
                .stream() //
                .filter(filter -> null != filter && //
                        StringUtils.isNotEmpty(filter.getOperator()) && //
                        StringUtils.isNotEmpty(filter.getColumn()) && //
                        CollectionUtils.isNotEmpty(filter.getValues()) && //
                        null != this.getSearchFilter(filter.getColumn()) //
                ) //
                .forEach(filter -> this.getSearchFilter(filter.getColumn()).accept(sqlQueryRecord, new SearchQueryFilter(filter.getColumn(), filter.getOperator(), filter.getValues())));
        return sqlQueryRecord.removeAll(this.jdbcTemplate);
    }
}
