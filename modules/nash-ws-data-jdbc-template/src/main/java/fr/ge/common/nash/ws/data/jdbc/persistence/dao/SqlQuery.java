/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.sql.ParameterMetaData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.ISqlAccessor;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Class SqlQuery.
 */
public class SqlQuery {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SqlQuery.class);

    /** The select clause. */
    private final String selectClause;

    /** The from clause. */
    private final String fromClause;

    /** The filters. */
    private final Map<String, List<SqlQueryFilter>> filters = new HashMap<>();

    /** The orders. */
    private final List<String> orders = new ArrayList<>();

    /** The group by clause. */
    private final String groupByClause;

    /**
     * Instantiates a new sql query.
     *
     * @param selectClause
     *            the select clause
     * @param fromClause
     *            the from clause
     */
    public SqlQuery(final String selectClause, final String fromClause) {
        this(selectClause, fromClause, null);
    }

    /**
     * Instantiates a new sql query.
     *
     * @param selectClause
     *            the select clause
     * @param fromClause
     *            the from clause
     */
    public SqlQuery(final String selectClause, final String fromClause, final String groupByClause) {
        this.selectClause = selectClause;
        this.fromClause = fromClause;
        this.groupByClause = groupByClause;
    }

    /**
     * Adds the filter.
     *
     * @param filter
     *            the filter
     * @param parameters
     *            the parameters
     * @return the sql query
     */
    public SqlQuery addFilter(final String operator, final String filter, final Object... parameters) {
        List<SqlQueryFilter> sqf = new ArrayList<>();
        if (null != this.filters.get(operator)) {
            sqf = this.filters.get(operator);
        }
        sqf.add(new SqlQueryFilter(filter, parameters));
        this.filters.put(operator, sqf);
        return this;
    }

    /**
     * Adds the order.
     *
     * @param order
     *            the order
     * @return the sql query
     */
    public SqlQuery addOrder(final String order) {
        this.orders.add(order);
        return this;
    }

    /**
     * Execute.
     *
     * @param <R>
     *            the generic type
     * @param jdbcTemplate
     *            the jdbc template
     * @param startIndex
     *            the start index
     * @param maxResults
     *            the max results
     * @param rowMapper
     *            the row mapper
     * @return the list
     */
    public <R> SearchResult<R> execute(final JdbcTemplate jdbcTemplate, final long startIndex, final long maxResults, final RowMapper<R> rowMapper) {
        final StringBuilder builder = new StringBuilder("SELECT COUNT(1) OVER() __totalResults, ") //
                .append(this.selectClause) //
                .append(" FROM ") //
                .append(this.fromClause);

        final Object[] parameters = this.buildWhereClause(builder);

        if (StringUtils.isNotEmpty(this.groupByClause)) {
            builder.append(" GROUP BY ").append(this.groupByClause);
        }

        if (!this.orders.isEmpty()) {
            builder.append(" ORDER BY ") //
                    .append(this.orders.stream().collect(Collectors.joining(", ")));
        }

        if (maxResults > 0) {
            builder.append(" LIMIT ").append(maxResults);
        }

        if (startIndex > 0) {
            builder.append(" OFFSET ").append(startIndex);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Query : {}", builder.toString());
            LOGGER.debug("Using : {}", Arrays.stream(parameters).map(Object::toString).collect(Collectors.joining(", ")));
        }

        final SearchResult<R> searchResult = new SearchResult<>(startIndex, maxResults);

        final List<R> items = jdbcTemplate.query(builder.toString(), ps -> {
            final ParameterMetaData meta = ps.getParameterMetaData();

            for (int idx = 0; idx < parameters.length; idx++) {
                final Object value = parameters[idx];
                if (null == value) {
                    ps.setNull(idx, meta.getParameterType(idx));
                } else {
                    final String parameterClassName = meta.getParameterClassName(idx + 1);
                    try {
                        final Class<?> parameterClass = Class.forName(parameterClassName);
                        final ISqlAccessor<?> accessor = SqlAccessorFactory.get(parameterClass);
                        accessor.set(ps, idx + 1, value);
                    } catch (final ClassNotFoundException ex) {
                        LOGGER.warn("Expected prepared statement column class not found : " + parameterClassName + " (" + ex.getMessage() + ")");
                    }
                }
            }
        }, (rs, rowNum) -> {
            searchResult.setTotalResults(rs.getLong(1));
            return rowMapper.mapRow(rs, rowNum);
        });

        searchResult.setContent(items);
        return searchResult;
    }

    /**
     * Builds the where clause.
     *
     * @param builder
     *            the builder
     * @return the object[]
     */
    private Object[] buildWhereClause(final StringBuilder builder) {
        final List<Object> parameters = new ArrayList<>();

        if (!this.filters.isEmpty()) {
            final List<String> builderClause = new ArrayList<>();
            this.filters.forEach((operator, filters) -> {
                final List<String> clauses = new ArrayList<>();
                for (final SqlQueryFilter filter : filters) {
                    clauses.add(filter.getClause());
                    if (CollectionUtils.isNotEmpty(filter.getValues())) {
                        parameters.addAll(filter.getValues());
                    }
                }
                builderClause.add("(" + clauses.stream().collect(Collectors.joining(" " + operator + " ")) + ")");
            });

            builder.append(" WHERE ");
            builder.append(builderClause.stream().collect(Collectors.joining(" AND ")));
        }

        return parameters.toArray(new Object[] {});
    }

    /**
     * Remove all.
     *
     * @param <R>
     *            the generic type
     * @param jdbcTemplate
     *            the jdbc template
     * @return the number of rows removed
     */
    public int removeAll(final JdbcTemplate jdbcTemplate) {
        final StringBuilder builder = new StringBuilder() //
                .append("DELETE FROM ") //
                .append(this.fromClause);

        final Object[] parameters = this.buildWhereClause(builder);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Query : {}", builder.toString());
            LOGGER.debug("Using : {}", Arrays.stream(parameters).map(Object::toString).collect(Collectors.joining(", ")));
        }

        return jdbcTemplate.update(builder.toString(), ps -> {
            final ParameterMetaData meta = ps.getParameterMetaData();
            for (int idx = 0; idx < parameters.length; idx++) {
                final Object value = parameters[idx];
                if (null == value) {
                    ps.setNull(idx, meta.getParameterType(idx));
                } else {
                    final String parameterClassName = meta.getParameterClassName(idx + 1);
                    try {
                        final Class<?> parameterClass = Class.forName(parameterClassName);
                        final ISqlAccessor<?> accessor = SqlAccessorFactory.get(parameterClass);
                        accessor.set(ps, idx + 1, value);
                    } catch (final ClassNotFoundException ex) {
                        LOGGER.warn("Expected prepared statement column class not found : " + parameterClassName + " (" + ex.getMessage() + ")");
                    }
                }
            }
        });
    }
}
