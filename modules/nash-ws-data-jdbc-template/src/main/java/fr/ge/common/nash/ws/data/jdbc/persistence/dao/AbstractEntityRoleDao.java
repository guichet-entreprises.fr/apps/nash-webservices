/**
 *
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import fr.ge.common.nash.ws.data.bean.AclEntityRoleBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class AbstractEntityRoleDao.
 *
 * @author $Author: ijijon $
 * @version $Revision: 0 $
 */
public abstract class AbstractEntityRoleDao {

    /** The jdbc template. */
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    /** The bean mapper. */
    protected final RowMapper<AclEntityRoleBean> beanMapper = (rs, rowNum) -> new AclEntityRoleBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setUid(SqlAccessorFactory.get(String.class).get(rs, "uid")) //
            .setEntity(SqlAccessorFactory.get(String.class).get(rs, "entity")) //
            .setRole(SqlAccessorFactory.get(String.class).get(rs, "role")) //
    ;

    /**
     * Creates the specification rights.
     *
     * @param uid
     *            the specification identifier
     * @param roles
     *            the roles
     * @return the long
     */
    public long create(final String uid, final AclEntityRoleBean... roles) {
        final int[] cnts = this.jdbcTemplate.batchUpdate( //
                "INSERT INTO " + this.getTableName() + " (id, uid, entity, role) VALUES (nextval('" + this.getSequenceName() + "'), ?, ?, ?)", //
                new BatchPreparedStatementSetter() {

                    @Override
                    public void setValues(final PreparedStatement ps, final int i) throws SQLException {
                        SqlAccessorFactory.get(String.class).set(ps, 1, uid);
                        SqlAccessorFactory.get(String.class).set(ps, 2, roles[i].getEntity());
                        SqlAccessorFactory.get(String.class).set(ps, 3, roles[i].getRole());
                    }

                    @Override
                    public int getBatchSize() {
                        return roles.length;
                    }

                });

        return Arrays.stream(cnts).sum();
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     */
    public void remove(final String uid) {
        this.jdbcTemplate.update("DELETE FROM " + this.getTableName() + " WHERE uid = ?", new Object[] { uid });
    }

    /**
     * Gets the sequence name.
     *
     * @return the sequence name
     */
    protected abstract String getSequenceName();

    /**
     * Gets the table name.
     *
     * @return the table name
     */
    protected abstract String getTableName();

    /**
     * Builds the entity roles list. If input roles are empty, return default
     * author role.
     *
     * @param author
     *            the author
     * @param roles
     *            the roles
     * @return the collection
     */
    public static Collection<AclEntityRoleBean> buildEntityRoles(final String author, final List<SearchQueryFilter> roles) {
        if (CollectionUtils.isNotEmpty(roles)) {
            return roles.stream() //
                    .filter(item -> StringUtils.isNotEmpty(item.getColumn()) && CollectionUtils.isNotEmpty(item.getValues())) //
                    .map(item -> item.getValues().stream().map(role -> new AclEntityRoleBean().setEntity(item.getColumn()).setRole(role))) //
                    .flatMap(stream -> stream) //
                    .collect(Collectors.toList());
        } else if (StringUtils.isNotEmpty(author)) {
            return Arrays.asList(new AclEntityRoleBean().setEntity(author).setRole("*"));
        } else {
            return Arrays.asList(new AclEntityRoleBean().setEntity("*").setRole("*"));
        }
    }

}
