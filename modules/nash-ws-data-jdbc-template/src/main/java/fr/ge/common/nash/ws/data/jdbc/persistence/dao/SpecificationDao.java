/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import java.io.InputStream;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.SqlInsertQuery;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.SqlUpdateQuery;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.BlobSqlAccessor;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.query.accessor.SqlAccessorFactory;
import fr.ge.common.utils.HashUtil;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Class SpecificationDao.
 *
 * @author Christian Cougourdan
 */
@Component
public class SpecificationDao extends AbstractSearchDao<SpecificationBean> {

    /** The jdbc template. */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /** The bean mapper. */
    private final RowMapper<SpecificationBean> beanMapper = (rs, rowNum) -> new SpecificationBean() //
            .setId(SqlAccessorFactory.get(Long.class).get(rs, "id")) //
            .setCreated(SqlAccessorFactory.get(Calendar.class).get(rs, "created")) //
            .setUpdated(SqlAccessorFactory.get(Calendar.class).get(rs, "updated")) //
            .setAuthor(SqlAccessorFactory.get(String.class).get(rs, "author")) //
            .setName(SqlAccessorFactory.get(String.class).get(rs, "name")) //
            .setReference(SqlAccessorFactory.get(String.class).get(rs, "reference")) //
            .setReferenceId(SqlAccessorFactory.get(Long.class).get(rs, "reference_id")) //
            .setRevision(SqlAccessorFactory.get(Long.class).get(rs, "revision")) //
            .setTagged(SqlAccessorFactory.get(Boolean.class).get(rs, "tagged")) //
            .setUid(SqlAccessorFactory.get(String.class).get(rs, "uid")) //
            .setVersion(SqlAccessorFactory.get(String.class).get(rs, "version")) //
            .setDetails(SqlAccessorFactory.get(ObjectNode.class).get(rs, "details")) //
            .setGroupe(SqlAccessorFactory.get(String.class).get(rs, "groupe")) //
            .setColor(SqlAccessorFactory.get(String.class).get(rs, "color")) //
            .setIcon(SqlAccessorFactory.get(String.class).get(rs, "icon")) //
            .setSize(((BlobSqlAccessor) SqlAccessorFactory.get(Types.BLOB, byte[].class)).size(rs, "content")) //
            .setPriority(SqlAccessorFactory.get(Long.class).get(rs, "priority")) //
    ;

    /** The Constant DATE_FORMATTER. */
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /** The Constant SEARCH_FILTERS. */
    private static final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> SEARCH_FILTERS;

    static {
        final Map<String, BiConsumer<SqlQuery, SearchQueryFilter>> map = new HashMap<>();

        map.put("uid", buildQueryPredicate(GROUP_FILTER_AND, "s", v -> v));
        map.put("tagged", buildQueryPredicate(GROUP_FILTER_AND, "s", BooleanUtils::toBoolean));
        map.put("created", buildQueryPredicate(GROUP_FILTER_AND, "s", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("updated", buildQueryPredicate(GROUP_FILTER_AND, "s", str -> DATE_FORMATTER.parse(str, LocalDate::from)));
        map.put("author", buildQueryPredicate(GROUP_FILTER_AND, "s", v -> v));
        map.put("reference", buildQueryPredicate(GROUP_FILTER_AND, "s", v -> v));
        map.put("reference_id", buildQueryPredicate(GROUP_FILTER_AND, "s", Long::parseLong));
        map.put("searchTerms", (query, filter) -> {
            query.addFilter(GROUP_FILTER_AND, "s.uid IN (SELECT m.uid FROM specification_meta m WHERE to_tsvector('french', m.value) @@ to_tsquery('french', ?))", filter.getValue());
        });
        map.put("groupe", (query, filter) -> {
            final List<String> rolesClause = new ArrayList<>();
            final List<String> rolesValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(role -> StringUtils.isNotEmpty(role)) //
                    .forEach(role -> {
                        rolesClause.add("?");
                        rolesValues.add(role);
                    });

            query.addFilter(GROUP_FILTER_AND, "s.groupe IN ('all', " + String.join(", ", rolesClause) + ")", rolesValues.toArray(new Object[] {}));
        });
        map.put("roles", (query, filter) -> {
            final List<String> rolesClause = new ArrayList<>();
            final List<String> rolesValues = new ArrayList<>();

            Optional.ofNullable(filter) //
                    .map(SearchQueryFilter::getValues) //
                    .orElse(new ArrayList<String>()) //
                    .stream() //
                    .filter(role -> StringUtils.isNotEmpty(role)) //
                    .forEach(role -> {
                        rolesClause.add("?");
                        rolesValues.add(role);
                    });

            if (!rolesClause.isEmpty()) {
                rolesValues.add(0, filter.getColumn());
                query.addFilter(GROUP_FILTER_OR, "s.uid IN (SELECT ser.uid FROM specification_entity_role ser WHERE (ser.entity = 'all' OR ? like ser.entity||'%') AND ser.role IN ('all', "
                        + String.join(", ", rolesClause) + "))", rolesValues.toArray(new Object[] {}));
            } else {
                query.addFilter(GROUP_FILTER_OR, "s.uid IN (SELECT ser.uid FROM specification_entity_role ser WHERE ser.entity = ? AND ser.role = ?)", new Object[] { "all", "all" });
            }
        });

        SEARCH_FILTERS = Collections.unmodifiableMap(map);
    }

    /**
     * Find by uid.
     *
     * @param uid
     *            the uid
     * @return the specification bean
     */
    public SpecificationBean findByUid(final String uid) {
        final List<SpecificationBean> entries = this.jdbcTemplate.query( //
                "SELECT s.*, sf.content, r.priority FROM specification s LEFT OUTER JOIN specification_file sf ON sf.specification_id = s.id LEFT OUTER JOIN reference r ON r.id = s.reference_id WHERE s.uid = ?", //
                new Object[] { uid }, //
                this.beanMapper);

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Find by reference.
     *
     * @param reference
     *            the reference
     * @return the specification bean
     */
    public SpecificationBean findByReference(final String reference) {
        final List<SpecificationBean> entries = this.jdbcTemplate.query( //
                "SELECT s.*, sf.content, r.priority FROM specification s LEFT OUTER JOIN specification_file sf ON sf.specification_id = s.id LEFT OUTER JOIN reference r ON r.id = s.reference_id WHERE s.hash = ? AND s.tagged = TRUE", //
                new Object[] { HashUtil.hashAsBytes(reference) }, //
                this.beanMapper);

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Removes the.
     *
     * @param uid
     *            the uid
     */
    public void remove(final String uid) {
        this.jdbcTemplate.update("DELETE FROM specification WHERE uid = ?", new Object[] { uid });
    }

    /**
     * Find resource.
     *
     * @param uid
     *            the uid
     * @return the file entry
     */
    public FileEntry findResource(final String uid) {
        final List<FileEntry> entries = this.jdbcTemplate.query( //
                "SELECT sf.name, sf.type, sf.updated, sf.content FROM specification_file sf INNER JOIN specification s ON sf.specification_id = s.id WHERE s.uid = ? LIMIT 1", //
                (rs, rowNum) -> {
                    return new FileEntry(rs.getString("name"), rs.getString("type")) //
                            .lastModified(rs.getTimestamp("updated")) //
                            .asBytes(SqlAccessorFactory.get(Types.BLOB, byte[].class).get(rs, "content")) //
                    ;
                }, uid //
        );

        if (entries.isEmpty()) {
            return null;
        } else {
            return entries.get(0);
        }
    }

    /**
     * Find resource entry.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return the file entry
     */
    public FileEntry findResourceEntry(final String uid, final String resourcePath) {
        final FileEntry entry = this.findResource(uid);

        if (null == entry) {
            return null;
        }

        return ZipUtil.entry(entry.getContentAsStream(), resourcePath);
    }

    /**
     * Creates the or update resource.
     *
     * @param uid
     *            the uid
     * @param entry
     *            the entry
     * @return the long
     */
    public long createOrUpdateResource(final String uid, final FileEntry entry) {
        final Long specId = this.jdbcTemplate.queryForObject( //
                "SELECT s.id FROM specification s WHERE s.uid = ?", //
                new Object[] { uid }, //
                Long.class //
        );

        final List<Long> ids = this.jdbcTemplate.queryForList( //
                "SELECT sf.id FROM specification_file sf WHERE sf.specification_id = ?", //
                new Object[] { specId }, //
                Long.class //
        );

        if (ids.isEmpty()) {
            return this.createResource(specId, entry);
        } else {
            return this.updateResource(ids.get(0), entry);
        }
    }

    /**
     * Creates the resource.
     *
     * @param entry
     *            the entry
     * @return the long
     */
    private long createResource(final Long specId, final FileEntry entry) {
        final Instant now = Instant.now();

        final Long id = new SqlInsertQuery("specification_file") //
                .addKey("id") //
                .addField("created", () -> now, Instant.class) //
                .addField("updated", () -> now, Instant.class) //
                .addField("name", entry::name, String.class) //
                .addField("type", entry::type, String.class) //
                .addField("specification_id", () -> specId, Long.class) //
                .addField("size", () -> 0, Long.class) //
                .addField("content", entry::getContentAsStream, Types.BLOB, byte[].class) //
                .execute(this.jdbcTemplate) //
        ;

        return Optional.ofNullable(id).map(Number::longValue).orElse(-1L);
    }

    /**
     * Update resource.
     *
     * @param id
     *            the id
     * @param entry
     *            the entry
     * @return the long
     */
    private long updateResource(final long id, final FileEntry entry) {
        final Instant now = Instant.now();

        new SqlUpdateQuery("specification_file") //
                .addKey("id", () -> id) //
                .addField("updated", () -> now, Instant.class) //
                .addField("name", entry::name, String.class) //
                .addField("type", entry::type, String.class) //
                .addField("size", () -> 0, Long.class) //
                .addField("content", entry::getContentAsStream, Types.BLOB, byte[].class) //
                .execute(this.jdbcTemplate) //
        ;

        return id;
    }

    /**
     * Tag.
     *
     * @param uid
     *            the uid
     */
    public void tag(final String uid) {
        this.jdbcTemplate.update( //
                "UPDATE specification SET tagged = (uid = ?) WHERE hash IN (SELECT s.hash FROM specification s WHERE s.uid = ?)", //
                new Object[] { uid, uid } //
        );
    }

    /**
     * Untag.
     *
     * @param uid
     *            the uid
     */
    public void untag(final String uid) {
        this.jdbcTemplate.update( //
                "UPDATE specification SET tagged = FALSE WHERE uid = ?", //
                new Object[] { uid } //
        );
    }

    /**
     * Next revision.
     *
     * @param entity
     *            the entity
     * @return the long
     */
    public Long nextRevision(final SpecificationBean entity) {
        final List<Long> lst = this.jdbcTemplate.queryForList( //
                "SELECT revision FROM specification WHERE hash = ?", //
                new Object[] { HashUtil.hashAsBytes(entity.getReference()) }, //
                Long.class //
        );

        if (lst.isEmpty()) {
            return 1L;
        } else {
            return lst.get(0) + 1L;
        }
    }

    /**
     * Creates the or update.
     *
     * @param entity
     *            the entity
     */
    public SpecificationBean createOrUpdate(final SpecificationBean entity) {
        if (null == entity.getId()) {
            return this.create(entity);
        } else {
            return this.update(entity);
        }
    }

    /**
     * Creates the.
     *
     * @param entity
     *            the entity
     */
    private SpecificationBean create(final SpecificationBean entity) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        final Long id = new SqlInsertQuery("specification") //
                .addKey("id", entity::getId) //
                .addField("created", () -> now, Instant.class) //
                .addField("updated", () -> now, Instant.class) //
                .addField("author", entity::getAuthor, String.class) //
                .addField("hash", () -> HashUtil.hashAsBytes(entity.getReference()), InputStream.class) //
                .addField("name", entity::getName, String.class) //
                .addField("reference", entity::getReference, String.class) //
                .addField("reference_id", entity::getReferenceId, Long.class) //
                .addField("revision", entity::getRevision, Long.class) //
                .addField("tagged", entity::isTagged, Boolean.class) //
                .addField("uid", entity::getUid, String.class) //
                .addField("version", entity::getVersion, String.class) //
                .addField("groupe", entity::getGroupe, String.class) //
                .addField("details", entity::getDetails, ObjectNode.class) //
                .addField("color", entity::getColor, String.class) //
                .addField("icon", entity::getIcon, String.class) //
                .execute(this.jdbcTemplate);

        entity.setUpdated(nowAsCalendar);
        entity.setCreated(nowAsCalendar);
        entity.setId(id);

        return entity //
                .setId(id) //
                .setCreated(nowAsCalendar) //
                .setUpdated(nowAsCalendar);
    }

    /**
     * Update.
     *
     * @param entity
     *            the entity
     */
    private SpecificationBean update(final SpecificationBean entity) {
        final Instant now = Instant.now();
        final Calendar nowAsCalendar = GregorianCalendar.from(ZonedDateTime.ofInstant(now, ZoneId.systemDefault()));

        new SqlUpdateQuery("specification") //
                .addKey("id", entity::getId) //
                .addField("updated", () -> now, Instant.class) //
                .addField("author", entity::getAuthor, String.class) //
                .addField("hash", () -> HashUtil.hashAsBytes(entity.getReference()), InputStream.class) //
                .addField("name", entity::getName, String.class) //
                .addField("reference", entity::getReference, String.class) //
                .addField("reference_id", entity::getReferenceId, Long.class) //
                .addField("revision", entity::getRevision, Long.class) //
                .addField("tagged", entity::isTagged, Boolean.class) //
                .addField("uid", entity::getUid, String.class) //
                .addField("version", entity::getVersion, String.class) //
                .addField("groupe", entity::getGroupe, String.class) //
                .addField("details", entity::getDetails, ObjectNode.class) //
                .addField("color", entity::getColor, String.class) //
                .addField("icon", entity::getIcon, String.class) //
                .execute(this.jdbcTemplate);

        entity.setUpdated(nowAsCalendar);
        return entity;
    }

    @Override
    protected SqlQuery getSearchQuery() {
        return new SqlQuery("s.*, sf.content, r.priority ", "specification s LEFT OUTER JOIN specification_file sf ON sf.specification_id = s.id LEFT OUTER JOIN reference r ON r.id = s.reference_id");
    }

    @Override
    protected BiConsumer<SqlQuery, SearchQueryFilter> getSearchFilter(final String column) {
        return SEARCH_FILTERS.get(column);
    }

    @Override
    protected RowMapper<SpecificationBean> getSearchRowMapper() {
        return this.beanMapper;
    }

    /**
     * Update specification priority by reference.
     *
     * @param reference
     *            the item identifier
     * @param priority
     *            the priority
     */
    public void updatePriorityByReference(final String reference, final int priority) {
        this.jdbcTemplate.update( //
                "UPDATE reference SET priority = ? FROM specification s WHERE reference.id = s.reference_id AND reference.content = ? ", //
                new Object[] { priority, reference } //
        );
    }

    /**
     * Update specification priority by code.
     *
     * @param code
     *            the item identifier
     * @param priority
     *            the priority
     */
    public void updatePriorityByCode(final String code, final int priority) {
        this.jdbcTemplate.update( //
                "UPDATE reference SET priority = ? FROM specification s WHERE reference.id = s.reference_id AND s.uid = ? ", //
                new Object[] { priority, code } //
        );
    }

}
