/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.data.bean.AclEntityRoleBean;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 * Junit for SpecificationDao class;
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class SpecificationEntityRoleDaoTest extends AbstractPostgreSqlDbTest {

    private static final String UID_SPEC_1 = "2018-11-AAA-AAA-99";

    private static final String UID_SPEC_2 = "2018-11-AAA-AAA-98";

    private static final String UID_ENTITY_CA = "GE/CA";

    private static final String USER_ROLE = "user";

    private static final String REFERENT_ROLE = "referent";

    @Autowired
    private SpecificationEntityRoleDao dao;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Test create.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testCreate() {
        final AclEntityRoleBean specEntityRoleReferent = new AclEntityRoleBean().setEntity(UID_ENTITY_CA).setRole(REFERENT_ROLE);
        final AclEntityRoleBean specEntityRoleUser = new AclEntityRoleBean().setEntity(UID_ENTITY_CA).setRole(USER_ROLE);
        final long cnts = this.dao.create(UID_SPEC_1, specEntityRoleReferent, specEntityRoleUser);
        assertEquals(2L, cnts);
    }

    /**
     * Test remove.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testRemove() throws Exception {
        this.dao.remove(UID_SPEC_2);
    }

    /**
     * Test getTableName.
     */
    @Test
    public void testGetTableName() {

        assertEquals(this.dao.getTableName(), "specification_entity_role");
    }

    /**
     * Test getSequenceName.
     */
    @Test
    public void testGetSequenceName() {

        assertEquals(this.dao.getSequenceName(), "sq_specification_entity_role");
    }
}