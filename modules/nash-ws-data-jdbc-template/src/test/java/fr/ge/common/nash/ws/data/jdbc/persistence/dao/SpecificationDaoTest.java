package fr.ge.common.nash.ws.data.jdbc.persistence.dao;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class SpecificationDaoTest extends AbstractPostgreSqlDbTest {

    private static final String UID_SPEC_1 = "2019-02-ZZZ-ZZZ-01";

    @Autowired
    private SpecificationDao dao;

    /**
     * Sets the up.
     *
     * @throws Exception
     *             the exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Test untag.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testUntag() {
        this.dao.untag(UID_SPEC_1);

        SpecificationBean spec = this.dao.findByUid(UID_SPEC_1);
        assertFalse(spec.isTagged());
    }

}
