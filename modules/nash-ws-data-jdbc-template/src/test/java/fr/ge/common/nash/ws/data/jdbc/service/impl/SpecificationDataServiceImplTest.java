/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigInteger;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.DefaultDataSet;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.DefaultTableMetaData;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.datatype.DataType;
import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationData;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.nash.ws.data.jdbc.persistence.dao.SpecificationEntityRoleDao;
import fr.ge.common.nash.ws.data.service.ISpecificationDataService;
import fr.ge.common.utils.ZipUtil;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class SpecificationDataServiceImplTest extends AbstractPostgreSqlDbTest {

    private static final String UID_SPEC_1 = "2016-03-AAA-AAA-99";

    private static final String UID_SPEC_2 = "2016-03-BBB-BBB-99";

    private static final String UID_SPEC_3 = "2016-03-CCC-CCC-99";

    private static final String UID_SPEC_UNKNOWN = "2016-03-XXX-XXX-99";

    private static final String UID_AUTHOR = "2016-11-JON-DOE-42";

    @Autowired
    private ISpecificationDataService service;

    @Autowired
    private SpecificationEntityRoleDao aclDao;

    @Test
    public void testLoad() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SpecificationBean actual = this.service.load("2016-02-XXX-XXX-01");

        assertNotNull(actual);
        assertThat(actual, //
                allOf( //
                        hasProperty("name", equalTo("Form Four")), //
                        hasProperty("id", equalTo(11111114L)) //
                ) //
        );
    }

    @Test
    public void testLoadUnknown() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SpecificationBean actual = this.service.load("2016-02-XXX-XXX-05");

        assertNull(actual);
    }

    @Test
    public void testLoadByReference() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SpecificationBean actual = this.service.loadByReference("GQ/Path/Path2");

        assertNotNull(actual);
        assertThat(actual, //
                allOf( //
                        hasProperty("name", equalTo("Form Five")), //
                        hasProperty("id", equalTo(11111116L)) //
                ) //
        );
    }

    @Test
    public void testLoadByReferenceUnknown() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SpecificationBean actual = this.service.loadByReference("Form seven");

        assertNull(actual);
    }

    @Test
    public void testLoadByReferenceUntagged() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SpecificationBean actual = this.service.loadByReference("Form Six");

        assertNull(actual);
    }

    @Test
    public void testRemove() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.service.remove("2016-02-XXX-XXX-01");

        final ITable tbl = this.extract("specification");
        assertEquals(3, tbl.getRowCount());
    }

    @Test
    public void testRemoveUnknown() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.service.remove("2016-02-XXX-XXX-42");

        final ITable tbl = this.extract("specification");
        assertEquals(4, tbl.getRowCount());
    }

    @Test
    public void testResource() throws Exception {
        final SpecificationBean bean = this.service.fromFile(null, this.resourceAsBytes("form.zip"), false, UID_AUTHOR, null);

        final FileEntry entry = this.service.resource(bean.getUid());
        assertThat(entry, notNullValue());

        final List<String> entries = ZipUtil.entries(entry.asBytes());
        assertThat(entries, //
                containsInAnyOrder( //
                        "description.xml", //
                        "0-context/context.xml", //
                        "1-data/data.xml") //
        );
    }

    @Test
    public void testResourceUnknown() throws Exception {
        final FileEntry entry = this.service.resource(UID_SPEC_UNKNOWN);
        assertThat(entry, nullValue());
    }

    @Test
    public void testResourceFile() throws Exception {
        final SpecificationBean bean = this.service.fromFile(null, this.resourceAsBytes("form.zip"), false, UID_AUTHOR, null);

        final FileEntry entry = this.service.resource(bean.getUid(), "1-data/data.xml");
        assertThat(entry, notNullValue());

        final FormSpecificationData actual = JaxbFactoryImpl.instance().unmarshal(entry.getContentAsStream(), FormSpecificationData.class);
        assertThat(actual, notNullValue());
    }

    @Test
    public void testResourceFileUnknown() throws Exception {
        final FileEntry entry = this.service.resource(UID_SPEC_UNKNOWN, "1-data/data.xml");
        assertThat(entry, nullValue());
    }

    @Test
    public void testResourceFileUnknownPath() throws Exception {
        final SpecificationBean bean = this.service.fromFile(null, this.resourceAsBytes("form.zip"), false, UID_AUTHOR, null);

        final FileEntry entry = this.service.resource(bean.getUid(), "2-attachment/data.xml");
        assertThat(entry, nullValue());
    }

    @Test
    public void testFromFile() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final byte[] content = this.resourceAsBytes("form-with-roles.zip");
        final SpecificationBean actual = this.service.fromFile(null, content, false, UID_AUTHOR, null);
        final ITable tbl = this.extract("specification_entity_role", "role", "role like 'test-role-user-1'");
        assertEquals(tbl.getRowCount(), 1);

        final byte[] contentNoRoles = this.resourceAsBytes("form.zip");
        this.service.fromFile(null, contentNoRoles, false, UID_AUTHOR, null);
        final ITable tbl2 = this.extract("specification_entity_role", "role", "role like 'all'");
        assertTrue(0 < tbl2.getRowCount());

        assertThat(actual, allOf( //
                hasProperty("id", notNullValue()), //
                hasProperty("revision", equalTo(1L)), //
                hasProperty("tagged", equalTo(false)), //
                hasProperty("author", equalTo(UID_AUTHOR)) //
        ) //
        );
    }

    @Test
    public void testFromFileBadFormat() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        try {
            this.service.fromFile(null, "This is a file content".getBytes(), false, UID_AUTHOR, null);
            fail("Has to return a TechnicalException");
        } catch (final TechnicalException ex) {
            assertNotNull(ex);
            assertEquals("No description found (bad file format ?)", ex.getMessage());
        }
    }

    @Test
    public void testFromFileAndPublish() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final byte[] content = this.resourceAsBytes("form.zip");
        final SpecificationBean actual = this.service.fromFile("2016-02-XXX-XXX-01", content, true, UID_AUTHOR, null);

        assertThat(actual, allOf( //
                hasProperty("id", notNullValue()), //
                hasProperty("revision", equalTo(1L)), //
                hasProperty("tagged", equalTo(true)), //
                hasProperty("author", equalTo(UID_AUTHOR)) //
        ) //
        );
    }

    @Test
    public void testFromFileUpdate() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final byte[] content = this.resourceAsBytes("form.zip");
        final SpecificationBean actual = this.service.fromFile("2016-02-XXX-XXX-01", content, true, UID_AUTHOR, null);

        assertThat(actual, hasProperty("id", equalTo(11111114L)));

        final ITable tbl = this.extract("specification");
        assertEquals(4, tbl.getRowCount());
    }

    @Test
    public void testPublish() throws Exception {
        this.initDb(this.resourceName("tagged-dataset.xml"));

        this.service.publish("2016-02-ZZZ-ZZZ-02");

        final ITable tbl = this.extract("specification", "name = 'Form Tagged 01' order by revision asc");
        assertEquals(3, tbl.getRowCount());

        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, ((Number) tbl.getValue(i, "revision")).intValue());
            assertEquals(i == 1, ((Boolean) tbl.getValue(i, "tagged")).booleanValue());
        }
    }

    @Test
    public void testPublishTaggedOne() throws Exception {
        this.initDb(this.resourceName("tagged-dataset.xml"));

        this.service.publish("2016-02-ZZZ-ZZZ-03");

        final ITable tbl = this.extract("specification", "name = 'Form Tagged 01' order by revision asc");
        assertEquals(3, tbl.getRowCount());

        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, ((Number) tbl.getValue(i, "revision")).intValue());
            assertEquals(i == 2, ((Boolean) tbl.getValue(i, "tagged")).booleanValue());
        }
    }

    @Test
    public void testPublishFromUid() throws Exception {
        this.initDb(this.resourceName("tagged-dataset.xml"));

        this.service.publish("2016-02-ZZZ-ZZZ-02");

        final ITable tbl = this.extract("specification", "name = 'Form Tagged 01' order by revision asc");
        assertEquals(3, tbl.getRowCount());

        for (int i = 0; i < 3; i++) {
            assertEquals(i + 1, ((Number) tbl.getValue(i, "revision")).intValue());
            assertEquals(i == 1, ((Boolean) tbl.getValue(i, "tagged")).booleanValue());
        }
    }

    @Test
    public void testSearch() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.insertSpecifications();
        // The fourth already exists with DBUnit

        SearchResult<SpecificationBean> searchResult = this.search(0, 3, null, null, null, null, SpecificationBean.class);
        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(7L)), //
                        hasProperty("content", hasSize(3)) //
                ) //
        );

        searchResult = this.search(3, 3, null, null, null, null, SpecificationBean.class);
        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(3L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(7L)), //
                        hasProperty("content", hasSize(3)) //
                ) //
        );

        searchResult = this.search(6, 3, null, null, null, null, SpecificationBean.class);
        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(6L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(7L)), //
                        hasProperty("content", hasSize(1)) //
                ) //
        );
    }

    @Test
    public void testSearchEmpty() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.service.remove("2016-02-XXX-XXX-01");
        final SearchResult<SpecificationBean> searchResult = this.search(0, 5, null, null, null, null, SpecificationBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(3L)), //
                        hasProperty("content", hasSize(3)) //
                ) //
        );
    }

    @Test
    public void testSearchWithOrders() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.insertSpecifications();

        SearchResult<SpecificationBean> searchResult = this.search(0, 4, null, Arrays.asList("name:desc"), null, null, SpecificationBean.class);
        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Six")), //
                                hasProperty("name", equalTo("Form Four")), //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Five")) //
                        ) //
                ) //
        );

        searchResult = this.search(0, 3, null, Arrays.asList("name:asc"), null, null, SpecificationBean.class);
        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form 01")), //
                                hasProperty("name", equalTo("Form 02")), //
                                hasProperty("name", equalTo("Form 03")) //
                        ) //
                ) //
        );

        searchResult = this.search(0, 3, null, Arrays.asList("name"), null, null, SpecificationBean.class);
        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form 01")), //
                                hasProperty("name", equalTo("Form 02")), //
                                hasProperty("name", equalTo("Form 03")) //
                        ) //
                ) //
        );

    }

    @Test
    public void testSearchFilteredByTaggedField() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("tagged:yes"), Arrays.asList("name:desc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form 02")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByCreatedFieldEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("created:2016-02-16"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Four")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByCreatedFieldGreaterOrEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("created>=" + now.toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form 03")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByCreatedFieldGreater() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("created>" + now.toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        hasProperty("name", equalTo("Form 03")) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByCreatedFieldLessOrEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("created<=" + now.minusDays(3).toString("yyyy-MM-dd")), Arrays.asList("name:asc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form 01")), //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Five")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByCreatedFieldLess() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("created<" + now.minusDays(4).toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Six")), //
                                hasProperty("name", equalTo("Form Four")), //
                                hasProperty("name", equalTo("Form Five")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByUpdatedFieldEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("updated:2016-02-16"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Four")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByUpdatedFieldGreaterOrEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("updated>=" + now.toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        hasProperty("name", equalTo("Form 03")) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByUpdatedFieldGreater() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("updated>" + now.toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        hasProperty("name", equalTo("Form 03")) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByUpdatedFieldLessOrEqual() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("updated<=" + now.minusDays(3).toString("yyyy-MM-dd")), Arrays.asList("name:desc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form Six")), //
                                hasProperty("name", equalTo("Form Four")), //
                                hasProperty("name", equalTo("Form Five")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByReference() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("reference:cerfa14004"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        hasProperty("name", equalTo("Form Four")) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByReferenceNone() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("reference:unknown"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), hasSize(0));
    }

    @Test
    public void testSearchFilteredByReferenceId() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("reference_id:111111145"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        hasProperty("name", equalTo("Form Four")) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByReferenceIdNone() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("reference_id:16384"), Arrays.asList("name:asc"), null, null, SpecificationBean.class);

        assertThat(searchResult.getContent(), hasSize(0));
    }

    /**
     * Test search filtered by updated field less.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchFilteredByUpdatedFieldLess() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        final DateTime now = this.insertSpecifications();

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("updated<" + now.minusDays(4).toString("yyyy-MM-dd")), Arrays.asList("name:asc"), null, null,
                SpecificationBean.class);

        assertThat(searchResult.getContent(), //
                contains( //
                        Arrays.asList( //
                                hasProperty("name", equalTo("Form 01")), //
                                hasProperty("name", equalTo("Form Five")), //
                                hasProperty("name", equalTo("Form Five")) //
                        ) //
                ) //
        );
    }

    @Test
    public void testSearchFilteredByUnknownField() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        this.insertSpecifications();
        // The fourth already exists with DBUnit

        final SearchResult<SpecificationBean> searchResult = this.search(0, 3, Arrays.asList("unknown:xxx"), null, null, null, SpecificationBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(7L)), //
                        hasProperty("content", hasSize(3)) //
                ) //
        );
    }

    @Test
    public void testSearchFullText() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<SpecificationBean> actual = this.service.search(0, 10, null, null, null, "guide", SpecificationBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );
    }

    public void testFindAllGroups() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        fail("To implement");
    }

    private SearchResult<SpecificationBean> search(final long startIndex, final long maxResults, final List<String> filters, final List<String> orders, final List<String> roles,
            final String searchTerms, final Class<SpecificationBean> expected) {
        return this.service.search(startIndex, //
                maxResults, //
                Optional.ofNullable(filters).orElse(new ArrayList<>()).stream().map(SearchQueryFilter::new).collect(Collectors.toList()), //
                Optional.ofNullable(orders).orElse(new ArrayList<>()).stream().map(SearchQueryOrder::new).collect(Collectors.toList()), //
                Optional.ofNullable(roles).orElse(new ArrayList<>()).stream().map(SearchQueryFilter::new).collect(Collectors.toList()), //
                searchTerms, //
                expected //
        );
    }

    private DateTime insertSpecifications() throws Exception {
        return this.insertSpecifications(DateTime.now().withSecondOfMinute(17));
    }

    /**
     * Insert specifications.
     *
     * @param now
     *            now
     * @return date time
     */
    private DateTime insertSpecifications(final DateTime now) throws Exception {
        this.insertSpecifications( //
                new SpecificationBean() //
                        .setId(2001L) //
                        .setReference("Form 01") //
                        .setName("Form 01") //
                        .setUid(UID_SPEC_1) //
                        .setVersion("1.0") //
                        .setCreated(now.minusDays(7).toCalendar(Locale.getDefault())) //
                        .setUpdated(now.minusDays(7).toCalendar(Locale.getDefault())) //
                        .setRevision(1L) //
                        .setAuthor(UID_AUTHOR) //
                        .setTagged(false) //
                        .setReferenceId(111111145L), //
                new SpecificationBean() //
                        .setId(2002L) //
                        .setReference("Form 02") //
                        .setName("Form 02") //
                        .setUid(UID_SPEC_2) //
                        .setVersion("1.0") //
                        .setCreated(now.minusDays(2).toCalendar(Locale.getDefault())) //
                        .setUpdated(now.minusDays(2).toCalendar(Locale.getDefault())) //
                        .setRevision(1L) //
                        .setAuthor(UID_AUTHOR) //
                        .setTagged(true) //
                        .setReferenceId(111111145L), //
                new SpecificationBean().setId(2003L) //
                        .setReference("Form 03") //
                        .setName("Form 03") //
                        .setUid(UID_SPEC_3) //
                        .setVersion("1.0") //
                        .setCreated(now.toCalendar(Locale.getDefault())) //
                        .setUpdated(now.toCalendar(Locale.getDefault())) //
                        .setRevision(1L) //
                        .setAuthor(UID_AUTHOR) //
                        .setTagged(false) //
                        .setReferenceId(111111145L) //
        );

        return now;
    }

    private void insertSpecifications(final SpecificationBean... beans) throws Exception {
        Column[] columns = Arrays.stream(new String[] { "id", "reference", "name", "uid", "version", "created", "updated", "revision", "author", "tagged", "reference_id" }) //
                .map(name -> new Column(name, DataType.UNKNOWN)) //
                .toArray(Column[]::new);

        ITableMetaData metaData = new DefaultTableMetaData("specification", columns);

        DefaultTable tbl = new DefaultTable(metaData);
        for (final SpecificationBean bean : beans) {
            tbl.addRow(new Object[] { //
                    bean.getId(), //
                    bean.getReference(), //
                    bean.getName(), //
                    bean.getUid(), //
                    bean.getVersion(), //
                    Optional.ofNullable(bean.getCreated()).map(Calendar::getTimeInMillis).orElse(null), //
                    Optional.ofNullable(bean.getUpdated()).map(Calendar::getTimeInMillis).orElse(null), //
                    bean.getRevision(), //
                    bean.getAuthor(), //
                    bean.isTagged(), //
                    bean.getReferenceId() //
            });
        }

        DefaultDataSet dataSet = new DefaultDataSet(tbl);

        try (Connection cx = this.getDataSource().getConnection()) {
            DatabaseOperation.INSERT.execute(new DatabaseConnection(cx), dataSet);
            cx.commit();
        }

        columns = Arrays.stream(new String[] { "id", "uid", "entity", "role" }) //
                .map(name -> new Column(name, DataType.UNKNOWN)) //
                .toArray(Column[]::new);
        metaData = new DefaultTableMetaData("specification_entity_role", columns);
        tbl = new DefaultTable(metaData);
        for (final SpecificationBean bean : beans) {
            tbl.addRow(new Object[] { //
                    bean.getId(), //
                    bean.getUid(), //
                    "all", //
                    "all" //
            });
        }

        dataSet = new DefaultDataSet(tbl);

        try (Connection cx = this.getDataSource().getConnection()) {
            DatabaseOperation.INSERT.execute(new DatabaseConnection(cx), dataSet);
            cx.commit();
        }
    }

    @Test
    public void testUpdatePriorityByCode() throws Exception {
        this.initDb(this.resourceName("priority-dataset.xml"));

        final BigInteger priority = new BigInteger("10");
        this.service.updatePriorityByCode("2016-02-ZZZ-ZZZ-02", priority.intValue());

        final ITable tbl = this.extract("reference", "id = 611");
        assertEquals(priority, ((BigInteger) tbl.getValue(0, "priority")));
    }

    @Test
    public void testUpdatePriorityByReference() throws Exception {
        this.initDb(this.resourceName("priority-dataset.xml"));

        final BigInteger priority = new BigInteger("10");
        this.service.updatePriorityByReference("cerfa14004", priority.intValue());

        final ITable tbl = this.extract("reference", "id = 611");
        assertEquals(priority, ((BigInteger) tbl.getValue(0, "priority")));
    }

    @Test
    public void testSearchWithTerms() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        this.insertSpecifications();
        Arrays.asList("Guide de haute", " Guide de haute ", " Guide de haute ").forEach(searchTerms -> {
            SearchResult<SpecificationBean> searchResult = this.search(0, 1, null, null, null, searchTerms, SpecificationBean.class);
            assertThat(searchResult, //
                    allOf( //
                            hasProperty("startIndex", equalTo(0L)), //
                            hasProperty("maxResults", equalTo(1L)), //
                            hasProperty("totalResults", equalTo(1L)), //
                            hasProperty("content", hasSize(1)) //
            ) //
            );
        });
    }
}
