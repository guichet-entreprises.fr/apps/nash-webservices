/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.dbunit.dataset.ITable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.common.nash.core.bean.StepStatusEnum;
import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;
import fr.ge.common.nash.core.util.JaxbFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.nash.ws.data.bean.RecordBean;
import fr.ge.common.nash.ws.data.bean.RecordPropertiesBean;
import fr.ge.common.nash.ws.data.service.IRecordDataService;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class RecordDataServiceImplTest extends AbstractPostgreSqlDbTest {

    private static final String UID_FAKE = "fake-uid";

    private static final String UID_AUTHOR = "2016-11-USR-AAA-42";

    private static final String UID_SPEC = "2016-03-AAA-AAA-99";

    private static final String UID_RECORD_A = "2016-10-REC-AAA-99";

    private static final String UID_RECORD_B = "2016-10-REC-BBB-99";

    private static final String UID_RECORD_C = "2016-10-REC-CCC-99";

    private static final String CONTENT = "1100100111100001";

    private static final String UID_RECORD_UNKNOWN = "2016-10-XXX-XXX-99";

    private static final String FILENAME_DATA = "1-data/data.xml";

    private static final String FILENAME_UNKNOWN = "x-unknown/unknown.xml";

    private static final String XML_DATA_EMPTY = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<form>\n</form>\n";

    @Autowired
    private IRecordDataService service;

    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testCheckIfExists() throws Exception {
        this.service.checkIfExistsAndHasAccess(UID_RECORD_A, Arrays.asList(new SearchQueryFilter("A/B/2018-12-ENT1", ":", "ROL1")));
    }

    @Test
    public void testCheckIfExistsWildcard() throws Exception {
        this.service.checkIfExistsAndHasAccess(UID_RECORD_A, Arrays.asList(new SearchQueryFilter("A/B/2018-12-ENT1", ":", "*")));
    }

    @Test
    public void testCheckIfExistsNoAccess() throws Exception {
        try {
            this.service.checkIfExistsAndHasAccess(UID_RECORD_A, Arrays.asList(new SearchQueryFilter("A/B/2018-12-ENT2", ":", "ROL1")));
            fail("UnauthorizedAccessException expected");
        } catch (final UnauthorizedAccessException ex) {
            assertThat(ex.getMessage(), equalTo("Unauthorized access to record [" + UID_RECORD_A + "]"));
        }
    }

    @Test
    public void testCheckIfExistsNoRoles() throws Exception {
        this.service.checkIfExistsAndHasAccess(UID_RECORD_C, null);
    }

    @Test
    public void testLoad() throws Exception {
        final RecordBean actual = this.service.load(UID_RECORD_A, RecordBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("spec", equalTo(UID_SPEC)), //
                        hasProperty("author", equalTo(UID_AUTHOR)) //
                ) //
        );
    }

    @Test
    public void testLoadUnknown() throws Exception {
        final RecordBean bean = this.service.load(UID_SPEC, RecordBean.class);
        assertThat(bean, nullValue());
    }

    @Test
    public void testAddOrUpdate() throws Exception {
        final FileBean actual = this.service.addOrUpdate(UID_RECORD_A, FILENAME_DATA, null, XML_DATA_EMPTY.getBytes());

        assertNotNull(actual);
        assertEquals(1002L, (long) actual.getId());
    }

    @Test
    public void testAddOrUpdateMeta() throws Exception {
        final FileBean actual = this.service.addOrUpdate(UID_RECORD_A, Engine.FILENAME_META, null, this.resourceAsBytes("spec/meta.xml"));

        assertNotNull(actual);

        final ITable tbl = this.extract("meta", "uid = '" + UID_RECORD_A + "'");
        assertEquals(5, tbl.getRowCount());

        for (int rowId = 0; rowId < tbl.getRowCount(); rowId++) {
            assertEquals(UID_RECORD_A, tbl.getValue(rowId, "uid"));
        }
    }

    /**
     * Test update unknown record.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAddOrUpdateUnknownRecord() throws Exception {
        final FileBean actual = this.service.addOrUpdate(UID_RECORD_UNKNOWN, FILENAME_DATA, null, XML_DATA_EMPTY.getBytes());

        assertNull(actual);
    }

    /**
     * Test update unknown file.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testAddOrUpdateUnknownFile() throws Exception {
        final FileBean actual = this.service.addOrUpdate(UID_RECORD_A, "unknown.xml", null, XML_DATA_EMPTY.getBytes());

        assertNotNull(actual);
    }

    /**
     * Test update root file.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testAddOrUpdateRootFile() throws Exception {
        final FormSpecificationDescription spec = this.resourceAsBean("spec/" + Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        spec.setStatus(StepStatusEnum.DONE.getStatus());
        spec.setAuthor("2018-10-JON-DOE-42");
        final byte[] resourceAsBytes = JaxbFactoryImpl.instance().asByteArray(spec);

        this.service.addOrUpdate(UID_RECORD_A, Engine.FILENAME_DESCRIPTION, null, resourceAsBytes);

        final ITable tbl = this.extract("record", "title, author, details, lo_get(properties) as properties", "uid = '" + UID_RECORD_A + "'");
        assertEquals(1, tbl.getRowCount());
        assertEquals("Cerfa 14004*02", tbl.getValue(0, "title"));
        assertEquals("2018-10-JON-DOE-42", tbl.getValue(0, "author"));

        final RecordPropertiesBean properties = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false) //
                .readValue((byte[]) tbl.getValue(0, "properties"), RecordPropertiesBean.class);
        assertEquals(1, properties.getNbCompletedSteps());
        assertEquals(2, properties.getNbSteps());

        final ObjectNode details = (ObjectNode) new ObjectMapper().readTree((String) tbl.getValue(0, "details"));
        assertThat(details.get("title").asText(), equalTo("Cerfa 14004*02"));
        assertThat(details.get("author").asText(), equalTo("2018-10-JON-DOE-42"));
    }

    @Test
    public void testRemoveResource() throws Exception {
        assertThat( //
                this.extract("record_file", "record_id in (select id from record where uid = '" + UID_RECORD_A + "') and name = '" + Engine.FILENAME_DESCRIPTION + "'").getRowCount(), //
                equalTo(1) //
        );

        final long actual = this.service.remove(UID_RECORD_A, Engine.FILENAME_DESCRIPTION);

        // assertEquals(1, actual);

        final ITable tbl = this.extract("record_file", "record_id in (select id from record where uid = '" + UID_RECORD_A + "')");
        assertEquals(2, tbl.getRowCount());
        assertNotEquals(101, ((Number) tbl.getValue(1, "id")).intValue());
    }

    @Test
    public void testRemoveResourceUnknownRecord() throws Exception {
        final long actual = this.service.remove(UID_RECORD_UNKNOWN, Engine.FILENAME_DESCRIPTION);

        assertEquals(0, actual);

        final ITable tbl = this.extract("record_file");
        assertEquals(6, tbl.getRowCount());
    }

    @Test
    public void testRemoveResourceUnknownPath() throws Exception {
        final long actual = this.service.remove(UID_RECORD_A, "unknown.xml");

        assertEquals(0, actual);

        final ITable tbl = this.extract("record_file", "record_id in (select id from record where uid = '" + UID_RECORD_A + "')");
        assertEquals(3, tbl.getRowCount());
    }

    @Test
    public void testRemoveRecord() throws Exception {
        final long actual = this.service.remove(UID_RECORD_A);

        assertThat(actual, equalTo(4L));
    }

    @Test
    public void testRemoveRecordUnknown() throws Exception {
        final long actual = this.service.remove(UID_RECORD_UNKNOWN);

        assertThat(actual, equalTo(0L));
    }

    @Test
    public void testFromFile() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("spec.zip");
        final String formsRecordName = "Dossier FORMS de test";
        final String formsRecordType = "Dossier de création";

        final List<SearchQueryFilter> roles = Arrays.asList(new SearchQueryFilter("A/B/2018-ENTITY:ROL1"), new SearchQueryFilter("A/B/2018-ENTITY:ROL2"));

        final RecordBean actual = this.service.fromFile(formsRecordName, formsRecordType, resourceAsBytes, UID_AUTHOR, roles, RecordBean.class);
        assertThat(actual, //
                allOf( //
                        hasProperty("spec", equalTo(UID_SPEC)), //
                        hasProperty("author", equalTo(UID_AUTHOR)) //
                ) //
        );
        ITable tbl = this.extract("record", "id not in (select id from record where uid in ('" + UID_RECORD_A + "', '" + UID_RECORD_B + "', '" + UID_RECORD_C + "'))");
        assertEquals(1, tbl.getRowCount());
        assertEquals(formsRecordName, tbl.getValue(0, "title"));

        final Object recordId = tbl.getValue(0, "id");

        final ObjectNode details = (ObjectNode) new ObjectMapper().readTree((String) tbl.getValue(0, "details"));
        assertThat(details.get("title").asText(), equalTo("Dossier FORMS de test"));
        assertThat(details.get("author").asText(), equalTo(UID_AUTHOR));

        tbl = this.extract("record_file", "record_id not in (select id from record where uid in ('" + UID_RECORD_A + "', '" + UID_RECORD_B + "', '" + UID_RECORD_C + "'))");
        assertEquals(6, tbl.getRowCount());

        for (int rowId = 0; rowId < tbl.getRowCount(); rowId++) {
            assertEquals(recordId, tbl.getValue(rowId, "record_id"));
        }

        tbl = this.extract("meta", "uid = '" + actual.getUid() + "'");
        assertEquals(6, tbl.getRowCount());

        for (int rowId = 0; rowId < tbl.getRowCount(); rowId++) {
            assertEquals(actual.getUid(), tbl.getValue(rowId, "uid"));
        }

        tbl = this.extract("record_entity_role", "uid = '" + actual.getUid() + "'");
        assertEquals(2, tbl.getRowCount());
        for (int idx = 0; idx < roles.size(); idx++) {
            assertThat(tbl.getValue(idx, "entity"), equalTo(roles.get(idx).getColumn()));
            assertThat(tbl.getValue(idx, "role"), equalTo(roles.get(idx).getValue()));
        }
    }

    @Test
    public void testFromFileWithoutRoles() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("spec.zip");
        final String formsRecordName = "Dossier FORMS de test";
        final String formsRecordType = "Dossier de création";

        final RecordBean actual = this.service.fromFile(formsRecordName, formsRecordType, resourceAsBytes, UID_AUTHOR, null, RecordBean.class);
        assertThat(actual, //
                allOf( //
                        hasProperty("spec", equalTo(UID_SPEC)), //
                        hasProperty("author", equalTo(UID_AUTHOR)) //
                ) //
        );
        ITable tbl = this.extract("record", "id not in (select id from record where uid in ('" + UID_RECORD_A + "', '" + UID_RECORD_B + "', '" + UID_RECORD_C + "'))");
        assertEquals(1, tbl.getRowCount());
        assertEquals(formsRecordName, tbl.getValue(0, "title"));

        final Object recordId = tbl.getValue(0, "id");

        final ObjectNode details = (ObjectNode) new ObjectMapper().readTree((String) tbl.getValue(0, "details"));
        assertThat(details.get("title").asText(), equalTo("Dossier FORMS de test"));
        assertThat(details.get("author").asText(), equalTo(UID_AUTHOR));

        tbl = this.extract("record_file", "record_id not in (select id from record where uid in ('" + UID_RECORD_A + "', '" + UID_RECORD_B + "', '" + UID_RECORD_C + "'))");
        assertEquals(6, tbl.getRowCount());

        for (int rowId = 0; rowId < tbl.getRowCount(); rowId++) {
            assertEquals(recordId, tbl.getValue(rowId, "record_id"));
        }

        tbl = this.extract("meta", "uid = '" + actual.getUid() + "'");
        assertEquals(6, tbl.getRowCount());

        for (int rowId = 0; rowId < tbl.getRowCount(); rowId++) {
            assertEquals(actual.getUid(), tbl.getValue(rowId, "uid"));
        }

        tbl = this.extract("record_entity_role", "uid = '" + actual.getUid() + "'");
        assertThat(tbl.getValue(0, "entity"), equalTo(UID_AUTHOR));
        assertThat(tbl.getValue(0, "role"), equalTo("*"));
    }

    @Test
    public void testFromFileNoDescription() throws Exception {
        final byte[] resourceAsBytes = this.resourceAsBytes("no-description.zip");
        final String formsRecordName = "Dossier FORMS";
        final String formsRecordType = "Dossier de création";

        try {
            this.service.fromFile(formsRecordName, formsRecordType, resourceAsBytes, UID_AUTHOR, null, RecordBean.class);
            fail("FunctionException expected");
        } catch (final FunctionalException ex) {
            assertThat(ex.getMessage(), equalTo("No description.xml file found in record"));
        }
    }

    @Test
    public void testRecordExists() throws Exception {
        final boolean actual = this.service.exists(UID_RECORD_A);
        assertThat(actual, equalTo(true));
    }

    @Test
    public void testRecordNotExists() throws Exception {
        final boolean actual = this.service.exists(UID_RECORD_UNKNOWN);
        assertThat(actual, equalTo(false));
    }

    @Test
    public void testResourceExists() throws Exception {
        final boolean actual = this.service.resourceExists(UID_RECORD_A, FILENAME_DATA);
        assertThat(actual, equalTo(true));
    }

    @Test
    public void testResourceNotExists() throws Exception {
        final boolean actual = this.service.resourceExists(UID_RECORD_A, FILENAME_UNKNOWN);
        assertThat(actual, equalTo(false));
    }

    @Test
    public void testResourceRecordNotExists() throws Exception {
        final boolean actual = this.service.resourceExists(UID_RECORD_UNKNOWN, FILENAME_DATA);
        assertThat(actual, equalTo(false));
    }

    @Test
    public void testResource() throws Exception {
        final FileEntry actual = this.service.resource(UID_RECORD_A, FILENAME_DATA);

        assertNotNull(actual);
    }

    @Test
    public void testResourceRoot() throws Exception {
        final FileEntry actual = this.service.resource(UID_RECORD_A, Engine.FILENAME_DESCRIPTION);

        assertEquals(Engine.FILENAME_DESCRIPTION, actual.name());
        assertEquals("text/xml", actual.type());
        assertEquals("2016-03-01 17:45:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.systemDefault()).format(actual.lastModified()));

        final Object content = this.extract("record_file", "lo_get(content) as content", "id = 1000").getValue(0, "content");
        assertEquals(new String((byte[]) content, StandardCharsets.UTF_8), new String(actual.asBytes(), StandardCharsets.UTF_8));
    }

    @Test
    public void testResourceUnknownRecord() throws Exception {
        final FileEntry actual = this.service.resource(UID_RECORD_UNKNOWN, Engine.FILENAME_DESCRIPTION);

        assertNull(actual);
    }

    @Test
    public void testResourceEmptyRecord() throws Exception {
        final FileEntry actual = this.service.resource(UID_RECORD_C, Engine.FILENAME_DESCRIPTION);

        assertNull(actual);
    }

    @Test
    public void testSearchOrder() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(new SearchQuery(0, 10).addOrder("created", "desc"), RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(3L)), //
                hasProperty("content", hasSize(3)) //
        ) //
        );

        assertThat(actual.getContent(), contains( //
                Arrays.asList( //
                        hasProperty("uid", equalTo(UID_RECORD_C)), //
                        hasProperty("uid", equalTo(UID_RECORD_B)), //
                        hasProperty("uid", equalTo(UID_RECORD_A)) //
                ) //
        ) //
        );
    }

    /**
     * Test search by author.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByAuthor() throws Exception {
        final List<String> authors = new ArrayList<String>();
        authors.add(UID_AUTHOR);
        authors.add(UID_RECORD_A);
        authors.add(UID_RECORD_B);
        authors.add(UID_RECORD_C);
        final SearchResult<RecordBean> actual = this.service.search(new SearchQuery(0, 10).addFilter("author", "in", authors), RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(1L)), //
                hasProperty("content", hasSize(1)) //
        ) //
        );

        assertThat(actual.getContent().get(0), allOf( //
                hasProperty("uid", equalTo(UID_RECORD_A)), //
                hasProperty("spec", equalTo(UID_SPEC)), //
                hasProperty("title", equalTo("Gîte ou meublé de tourisme")) //
        ) //
        );
    }

    /**
     * Test search by uid.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByUid() throws Exception {

        final SearchQuery query = new SearchQuery(0, 10);
        query.addFilter("uid", "~", UID_RECORD_A);

        final SearchResult<RecordBean> actual = this.service.search(query, RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(1L)), //
                hasProperty("content", hasSize(1)) //
        ) //
        );

        assertThat(actual.getContent().get(0), //
                hasProperty("uid", equalTo(UID_RECORD_A)) //
        );
    }

    /**
     * Test search by uid.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByUidNoResults() throws Exception {

        final SearchQuery query = new SearchQuery(0, 10);
        query.addFilter("uid", "~", UID_FAKE);

        final SearchResult<RecordBean> actual = this.service.search(query, RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(0L)), //
                hasProperty("content", hasSize(0)) //
        ) //
        );
    }

    /**
     * Test search by title.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitle() throws Exception {
        this.testSearchWildcards("Gîte ou meublé de tourisme", 1, UID_RECORD_A, UID_AUTHOR, UID_SPEC, "Gîte ou meublé de tourisme");
    }

    /**
     * Test search by title with prefix wildcard.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitleWildcardStart() throws Exception {
        this.testSearchWildcards("*meublé de tourisme", 1, UID_RECORD_A, UID_AUTHOR, UID_SPEC, "Gîte ou meublé de tourisme");
    }

    /**
     * Test search by title with suffix wildcard.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitleWildcardEnd() throws Exception {
        this.testSearchWildcards("Organisateur de*", 1, UID_RECORD_C, "2016-11-USR-BBB-42", "2016-03-SPC-CCC-99", "Organisateur de foires et salons");
    }

    /**
     * Test search by title with wildcard.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitleWildcards() throws Exception {
        this.testSearchWildcards("*de haute*", 1, UID_RECORD_B, "2016-11-USR-BBB-42", "2016-03-SPC-BBB-99", "Guide de haute montagne");
    }

    /**
     * Test search by empty title.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitleEmpty() throws Exception {
        this.testSearchWildcards("", 3);
    }

    @Test
    public void testSearchByUnknownWildcards() throws Exception {
        this.testSearchWildcards("*alambic*", 0);
    }

    @Test
    public void testSearchByNoFullTextCriteria() throws Exception {
        this.testSearchWildcards(null, 3);
    }

    public void testSearchWildcards(final String fullTextCriteria, final int count) {
        final SearchQuery query = new SearchQuery(0, 10);
        if (null != fullTextCriteria) {
            query.addFilter("title", "~", fullTextCriteria);
        }

        final SearchResult<RecordBean> actual = this.service.search(query, RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo((long) count)), //
                hasProperty("content", hasSize(count)) //
        ) //
        );

    }

    public void testSearchWildcards(final String fullTextCriteria, final int count, final String uid, final String author, final String spec, final String title) {
        final SearchQuery query = new SearchQuery(0, 10);
        if (null != fullTextCriteria) {
            query.addFilter("title", "~", fullTextCriteria);
        }

        final SearchResult<RecordBean> actual = this.service.search(query, RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo((long) count)), //
                hasProperty("content", hasSize(count)) //
        ) //
        );

        assertThat(actual.getContent().get(0), allOf( //
                hasProperty("uid", equalTo(uid)), //
                hasProperty("author", equalTo(author)), //
                hasProperty("spec", equalTo(spec)), //
                hasProperty("title", equalTo(title)) //
        ) //
        );
    }

    /**
     * Test search by updated after.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByUpdatedAfter() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(new SearchQuery(0, 10).addFilter("updated", ">", "2016-07-01"), RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(2L)), //
                hasProperty("content", hasSize(2)) //
        ) //
        );

        assertThat(actual.getContent(), //
                containsInAnyOrder( //
                        Arrays.asList( //
                                hasProperty("uid", equalTo(UID_RECORD_B)), //
                                hasProperty("uid", equalTo(UID_RECORD_C)) //
                        ) //
                ) //
        );
    }

    /**
     * Test search no item found.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchNoItemFound() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(new SearchQuery(0, 10).addFilter("updated", ">", "2017-03-01"), RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(0L)), //
                hasProperty("content", hasSize(0)) //
        ) //
        );
    }

    /**
     * Test search by created before.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByCreatedBefore() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(new SearchQuery(0, 10).addFilter("created", "<", "2016-07-01"), RecordBean.class);

        assertThat(actual, allOf( //
                hasProperty("startIndex", equalTo(0L)), //
                hasProperty("maxResults", equalTo(10L)), //
                hasProperty("totalResults", equalTo(1L)), //
                hasProperty("content", hasSize(1)) //
        ) //
        );

    }

    /**
     * Test search by title with case insensitive.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearchByTitleIgnoreCase() throws Exception {
        this.testSearchWildcards("*TOURISME*", 1, UID_RECORD_A, UID_AUTHOR, UID_SPEC, "Gîte ou meublé de tourisme");
        this.testSearchWildcards("*TOuRiSMe*", 1, UID_RECORD_A, UID_AUTHOR, UID_SPEC, "Gîte ou meublé de tourisme");
    }

    private <T> T resourceAsBean(final String name, final Class<T> clazz) {
        return JaxbFactoryImpl.instance().unmarshal(this.resourceAsStream(name), clazz);
    }

    @Test
    public void testSearchFullText() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(0, 10, null, null, null, "guide", RecordBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );
    }

    @Test
    public void testSearchFullTextWithAccents() throws Exception {
        final SearchResult<RecordBean> actual = this.service.search(0, 10, null, null, null, "Gîte ou meublé de tourisme", RecordBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );
    }

    @Test
    public void testSearchFullTextNoAccents() throws Exception {
        // Must find the same result as testSearchFullTextWithAccents

        final SearchResult<RecordBean> actual = this.service.search(0, 10, null, null, null, "Gite ou meuble de tourisme", RecordBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );
    }

    @Test
    public void testResources() throws Exception {
        Collection<FileBean> resources = this.service.resources(UID_RECORD_A);

        assertThat(resources, hasSize(3));

        resources.forEach(file -> assertThat(file, anyOf( //
                hasProperty("name", equalTo("0-context/context.xml")), //
                hasProperty("name", equalTo("1-data/data.xml")), //
                hasProperty("name", equalTo("description.xml")) //
        )));
    }

    @Test
    public void testAssignRoles() throws Exception {

        List<SearchQueryFilter> roles = new ArrayList<SearchQueryFilter>();
        roles.add(new SearchQueryFilter("entity:role"));

        this.service.assignRoles(UID_RECORD_A, UID_AUTHOR, roles);

        ITable tbl = this.extract("record_entity_role", "uid like'" + UID_RECORD_A + "'");
        assertEquals(1, tbl.getRowCount());
        assertEquals("entity", tbl.getValue(0, "entity"));
        assertEquals("role", tbl.getValue(0, "role"));
        assertEquals(UID_RECORD_A, tbl.getValue(0, "uid"));

    }
}
