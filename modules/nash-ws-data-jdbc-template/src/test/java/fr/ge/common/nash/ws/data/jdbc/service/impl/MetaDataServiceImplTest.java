/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.data.bean.MetaBean;
import fr.ge.common.nash.ws.data.service.IMetaDataService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class MetaDataServiceImplTest extends AbstractPostgreSqlDbTest {

    @Autowired
    private IMetaDataService service;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testSearchByAuthor() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("author:2018-05-JON-DOE-42"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-AAA-01")), //
                        hasProperty("key", equalTo("key1")), //
                        hasProperty("value", equalTo("val1")) //
                ) //
        );
    }

    @Test
    public void testSearchByCreatedLessThan() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("created<2018-04-25"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-AAA-01")), //
                        hasProperty("key", equalTo("key1")), //
                        hasProperty("value", equalTo("val1")) //
                ) //
        );
    }

    @Test
    public void testSearchByCreatedGreaterThan() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("created>2018-05-01"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1003L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-BBB-02")), //
                        hasProperty("key", equalTo("key2")), //
                        hasProperty("value", equalTo("val2.2")) //
                ) //
        );
    }

    @Test
    public void testSearchByUpdatedLessThan() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("updated<2018-05-04"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-AAA-01")), //
                        hasProperty("key", equalTo("key1")), //
                        hasProperty("value", equalTo("val1")) //
                ) //
        );
    }

    @Test
    public void testSearchByUpdatedGreaterThan() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("updated>2018-05-10"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1003L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-BBB-02")), //
                        hasProperty("key", equalTo("key2")), //
                        hasProperty("value", equalTo("val2.2")) //
                ) //
        );
    }

    @Test
    public void testSearchByKey() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("key:key1"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-AAA-01")), //
                        hasProperty("key", equalTo("key1")), //
                        hasProperty("value", equalTo("val1")) //
                ) //
        );
    }

    @Test
    @Ignore
    public void testSearchByValue() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<MetaBean> searchResult = this.service.search(new SearchQuery().addFilter("value:val2.1"), MetaBean.class);

        final List<MetaBean> actual = searchResult.getContent();
        assertThat(actual, hasSize(1));
        assertThat(actual.get(0), //
                allOf( //
                        hasProperty("id", equalTo(1003L)), //
                        hasProperty("uid", equalTo("2018-05-SPC-BBB-02")), //
                        hasProperty("key", equalTo("key2")), //
                        hasProperty("value", equalTo("val2.1")) //
                ) //
        );
    }

}
