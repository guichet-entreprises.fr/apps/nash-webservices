/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.jdbc.service.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.ws.data.bean.ReferenceBean;
import fr.ge.common.nash.ws.data.service.IReferenceDataService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 *
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class ReferenceDataServiceImplTest extends AbstractPostgreSqlDbTest {

    @Autowired
    private IReferenceDataService service;

    @Test
    public void testCreate() throws Exception {
        this.initDb(this.resourceName("empty-dataset.xml"));

        final ReferenceBean entity = this.service.create("R&D/Test/PE42");

        assertThat(entity, //
                allOf( //
                        hasProperty("category", equalTo("PE42")), //
                        hasProperty("content", equalTo("R&D/Test/PE42")) //
                ) //
        );

        final SearchResult<ReferenceBean> searchResult = this.search(0, 10, null, null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(3L)) //
                ) //
        );
    }

    @Test
    public void testCreateIntermediateNode() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final ReferenceBean entity = this.service.create("SCN/GPART/INT/PE01");

        assertThat(entity, //
                allOf( //
                        hasProperty("category", equalTo("PE01")), //
                        hasProperty("content", equalTo("SCN/GPART/INT/PE01")) //
                ) //
        );

        final SearchResult<ReferenceBean> searchResult = this.search(0, 10, null, null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(14L)) //
                ) //
        );
    }

    @Test
    public void testCreateFinalNode() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final ReferenceBean entity = this.service.create("SCN/GE/PE42");

        assertThat(entity, //
                allOf( //
                        hasProperty("category", equalTo("PE42")), //
                        hasProperty("content", equalTo("SCN/GE/PE42")) //
                ) //
        );

        final SearchResult<ReferenceBean> searchResult = this.search(0, 10, null, null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(10L)), //
                        hasProperty("totalResults", equalTo(12L)) //
                ) //
        );
    }

    @Test
    public void testLoad() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final ReferenceBean actual = this.service.load(1005, ReferenceBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("category", equalTo("PE03")), //
                        hasProperty("content", equalTo("SCN/GQ/PE03")), //
                        hasProperty("parent", equalTo(1002L)), //
                        hasProperty("totalSpecifications", equalTo(2L)) //
                ) //
        );
    }

    @Test
    public void testLoadUnknown() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final ReferenceBean actual = this.service.load(2004, ReferenceBean.class);

        assertThat(actual, nullValue());
    }

    @Test
    public void testSearch() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, null, null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(11L)) //
                ) //
        );
    }

    @Test
    public void testSearchRoot() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, Arrays.asList("parent:"), null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );
    }

    @Test
    public void testSearchDepthOne() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, Arrays.asList("parent:1001"), null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(2L))) //
        );

        List<ReferenceBean> references = searchResult.getContent();

        for (ReferenceBean ref : references) {
            if (ref.getId() == 1002) {
                assertThat(ref, //
                        allOf( //
                                hasProperty("category", equalTo("GQ")), //
                                hasProperty("content", equalTo("SCN/GQ")), //
                                hasProperty("parent", equalTo(1001L)), //
                                hasProperty("nbSpecificationsPossible", equalTo(2L)), //
                                hasProperty("totalSpecifications", equalTo(0L)) //
                        ) //
                );
            }
        }

    }

    @Test
    public void testSearchDepthTwo() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, Arrays.asList("parent:1002"), null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(5L)) //
                ) //
        );
    }

    @Test
    public void testSearchDepthTwoWithoutFantomId() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, Arrays.asList("parent:1002", "nb_specs>0"), null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );

        ReferenceBean reference = searchResult.getContent().get(0);

        assertThat(reference, //
                allOf( //
                        hasProperty("content", equalTo("SCN/GQ/PE03")), //
                        hasProperty("id", equalTo(1005L)), //
                        hasProperty("nbSpecificationsPossible", equalTo(2L))//
                )//
        );
    }

    @Test
    public void testSearchWithRoleGroup() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
        Set<String> groups = new HashSet<>();
        groups.add("referent");
        SearchQuery query = new SearchQuery();
        query.addFilter(new SearchQueryFilter("parent:1002"));
        query.addFilter(new SearchQueryFilter("nb_specs>0"));
        query.addFilter(new SearchQueryFilter("groupe", ":", groups));
        query.addOrder("id", "desc");

        SearchResult<ReferenceBean> searchResult = this.service.search(0L, 3L, query.getFilters(), query.getOrders(), null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(1L)) //
                ) //
        );

        ReferenceBean reference = searchResult.getContent().get(0);

        assertThat(reference, //
                allOf( //
                        hasProperty("content", equalTo("SCN/GQ/PE03")), //
                        hasProperty("id", equalTo(1005L)), //
                        hasProperty("nbSpecificationsPossible", equalTo(2L))//
                )//
        );
    }

    @Test
    public void testSearchByCategory() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));

        final SearchResult<ReferenceBean> searchResult = this.search(0, 3, Arrays.asList("category:PE01"), null, null, null, ReferenceBean.class);

        assertThat(searchResult, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(3L)), //
                        hasProperty("totalResults", equalTo(11L)) //
                ) //
        );
    }

    private SearchResult<ReferenceBean> search(final long startIndex, final long maxResults, final List<String> filters, final List<String> orders, final List<String> roles, final String searchTerms,
            final Class<ReferenceBean> expected) {
        return this.service.search(startIndex, //
                maxResults, //
                Optional.ofNullable(filters).orElse(new ArrayList<>()).stream().map(SearchQueryFilter::new).collect(Collectors.toList()), //
                Optional.ofNullable(orders).orElse(new ArrayList<>()).stream().map(SearchQueryOrder::new).collect(Collectors.toList()), //
                Optional.ofNullable(roles).orElse(new ArrayList<>()).stream().map(SearchQueryFilter::new).collect(Collectors.toList()), //
                searchTerms, //
                expected //
        );
    }

}
