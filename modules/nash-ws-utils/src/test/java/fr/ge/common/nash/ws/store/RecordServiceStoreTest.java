package fr.ge.common.nash.ws.store;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.roles.EntityRole;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RecordServiceStoreTest.TestConfiguration.class })
public class RecordServiceStoreTest extends AbstractTest {

    @Configuration
    @ImportResource(locations = { "classpath:spring/test-context.xml" })
    public static class TestConfiguration {
        @Bean
        public IStore getStore() {
            return new RecordServiceStore();
        }
    }

    @Autowired
    private RecordServiceStore store;

    @Autowired
    private IRecordService service;

    @Autowired
    private ITrackerFacade tracker;

    @Captor
    private ArgumentCaptor<List<SearchQueryFilter>> rolesCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        reset(this.tracker, this.service);
    }

    @Test
    public void testSimple() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String author = desc.getAuthor();
        final String code = desc.getRecordUid();

        when(this.service.upload(any(byte[].class), eq(author), any())).thenReturn(Response.created(URI.create("/v1/Record/code/" + code)).entity(code).build());

        this.store.upload(provider, null);

        verify(this.tracker, never()).link(any(), any());

        verify(this.service).upload(any(), eq(author), this.rolesCaptor.capture());

        assertThat(this.rolesCaptor.getValue(), containsInAnyOrder( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("column", equalTo("GE/2020-07-JON-DOE-42")), //
                                hasProperty("value", equalTo("*")) //
                        ) //
                ) //
        ));
    }

    @Test
    public void testNewUid() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String author = desc.getAuthor();
        final String code = desc.getRecordUid();
        desc.setRecordUid(null);
        desc.setOrigin(code);

        when(this.service.upload(any(byte[].class), eq(author), any())).thenReturn(Response.created(URI.create("/v1/Record/code/" + code)).entity(code).build());

        this.store.upload(provider, null);

        final ArgumentCaptor<String> newCodeCaptor = ArgumentCaptor.forClass(String.class);

        verify(this.tracker, never()).link(newCodeCaptor.capture(), eq(code));

        verify(this.service).upload(any(), eq(author), this.rolesCaptor.capture());

        assertThat(this.rolesCaptor.getValue(), containsInAnyOrder( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("column", equalTo("GE/2020-07-JON-DOE-42")), //
                                hasProperty("value", equalTo("*")) //
                        ) //
                ) //
        ));
    }

    @Test(expected = TechnicalException.class)
    public void testServiceError() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String author = desc.getAuthor();

        when(this.service.upload(any(byte[].class), eq(author), any())).thenReturn(Response.serverError().build());

        this.store.upload(provider, null);
    }

    @Test
    public void testWithRoles() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String author = desc.getAuthor();
        final String code = desc.getRecordUid();

        when(this.service.upload(any(byte[].class), eq(author), any())).thenReturn(Response.created(URI.create("/v1/Record/code/" + code)).entity(code).build());

        final Collection<EntityRole> roles = Arrays.asList(new EntityRole("GE", "admin"), new EntityRole("GE", "tester"));

        this.store.upload(provider, roles);

        verify(this.tracker, never()).link(any(), any());

        verify(this.service).upload(any(), eq(author), this.rolesCaptor.capture());

        assertThat(this.rolesCaptor.getValue(), containsInAnyOrder( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("column", equalTo("GE")), //
                                hasProperty("value", equalTo("admin")) //
                        ), //
                        allOf( //
                                hasProperty("column", equalTo("GE")), //
                                hasProperty("value", equalTo("tester")) //
                        ) //
                ) //
        ));
    }

}
