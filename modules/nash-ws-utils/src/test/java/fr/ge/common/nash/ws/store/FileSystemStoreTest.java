package fr.ge.common.nash.ws.store;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { FileSystemStoreTest.TestConfiguration.class })
public class FileSystemStoreTest extends AbstractTest {

    @Autowired
    private FileSystemStore store;

    @Autowired
    private ITrackerFacade tracker;

    @Configuration
    @ImportResource(locations = { "classpath:spring/test-context.xml" })
    public static class TestConfiguration {
        @Bean
        public IStore getStore() {
            return new FileSystemStore();
        }
    }

    @Before
    public void setUp() throws Exception {
        this.tearDown();
        reset(this.tracker);
    }

    @After
    public void tearDown() throws Exception {
        for (final String path : new String[] { "target/records", "target/readonly-records" }) {
            this.remove(path);
        }
    }

    @Test
    public void testSimple() throws Exception {
        final Temporal now = LocalDateTime.now();
        this.store.setRootPath("target/records");
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));

        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final Path basePath = Paths.get( //
                "target/records", //
                DateTimeFormatter.ofPattern("yyyy-MM/dd").format(now), //
                desc.getReference().getCode() //
        );

        final IProvider actual = this.store.upload(provider, null);

        assertThat(actual, nullValue());

        for (final String ext : new String[] { "zip", "sha1", "md5" }) {
            assertTrue(basePath.resolve(String.format("%s.%s", desc.getRecordUid(), ext)).toFile().exists());
        }
    }

    @Test
    public void testNewCode() throws Exception {
        final Temporal now = LocalDateTime.now();
        this.store.setRootPath("target/records");
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));

        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String code = desc.getRecordUid();
        desc.setOrigin(code);
        desc.setRecordUid(null);

        provider.save(Engine.FILENAME_DESCRIPTION, desc);

        final Path basePath = Paths.get( //
                "target/records", //
                DateTimeFormatter.ofPattern("yyyy-MM/dd").format(now), //
                desc.getReference().getCode() //
        );

        this.store.upload(provider, null);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(this.tracker).link(captor.capture(), eq(code));

        for (final String ext : new String[] { "zip", "sha1", "md5" }) {
            assertTrue(basePath.resolve(String.format("%s.%s", captor.getValue(), ext)).toFile().exists());
        }
    }

    @Test(expected = TechnicalException.class)
    public void testAlreadyExists() throws Exception {
        final Temporal now = LocalDateTime.now();
        this.store.setRootPath("target/records");
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));

        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final Path basePath = Paths.get( //
                "target/records", //
                DateTimeFormatter.ofPattern("yyyy-MM/dd").format(now), //
                desc.getReference().getCode() //
        );

        basePath.toFile().mkdirs();
        basePath.resolve(desc.getRecordUid() + ".zip").toFile().createNewFile();

        this.store.upload(provider, null);
    }

    @Ignore
    @Test(expected = TechnicalException.class)
    public void testPermissionError() throws Exception {
        final String baseFolder = "target/readonly-records";
        final File baseFile = Paths.get(baseFolder).toFile();
        baseFile.mkdirs();
        baseFile.setReadable(false, false);
        baseFile.setWritable(false, false);

        this.store.setRootPath(baseFolder);
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));

        this.store.upload(provider, null);
    }

}
