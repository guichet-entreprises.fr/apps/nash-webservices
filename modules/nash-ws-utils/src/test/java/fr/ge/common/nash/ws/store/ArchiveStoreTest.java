package fr.ge.common.nash.ws.store;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.provider.ZipProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.common.utils.test.AbstractTest;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author Christian Cougourdan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ArchiveStoreTest.TestConfiguration.class })
public class ArchiveStoreTest extends AbstractTest {

    @Configuration
    @ImportResource(locations = { "classpath:spring/test-context.xml" })
    public static class TestConfiguration {
        @Bean
        public IStore getStore() {
            return new ArchiveStore();
        }
    }

    @Autowired
    private ITrackerFacade tracker;

    @Autowired
    private IStorageRestService storageService;

    @Autowired
    private ArchiveStore store;

    @Captor
    private ArgumentCaptor<List<SearchQueryFilter>> metas;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        reset(this.tracker, this.storageService);

        this.store.setTray(StorageTrayEnum.ARCHIVED_PARTNERS.toString());
    }

    @Test
    public void testSimple() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String code = desc.getRecordUid();

        when(this.storageService.store(any(), any(), any(), any(), any())).thenReturn(Response.ok().build());

        this.store.upload(provider, null);

        verify(this.tracker, never()).link(any(), any());

        verify(this.storageService).store( //
                eq(StorageTrayEnum.ARCHIVED_PARTNERS.getName()), //
                any(), //
                eq(String.format("%s.zip", code)), //
                eq(code), //
                this.metas.capture() //
        );

        final List<SearchQueryFilter> values = this.metas.getValue();
        assertThat(values, containsInAnyOrder( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("column", equalTo("author")), //
                                hasProperty("value", equalTo("2020-07-JON-DOE-42")) //
                        ), //
                        allOf( //
                                hasProperty("column", equalTo("description")), //
                                hasProperty("value", nullValue()) //
                        ), //
                        allOf( //
                                hasProperty("column", equalTo("title")), //
                                hasProperty("value", equalTo("Archive Store")) //
                        ) //
                ) //
        ));

    }

    @Test
    public void testNewCode() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final String code = desc.getRecordUid();
        desc.setOrigin(code);
        desc.setRecordUid(null);

        provider.save(Engine.FILENAME_DESCRIPTION, desc);

        when(this.storageService.store(any(), any(), any(), any(), any())).thenReturn(Response.ok().build());

        this.store.upload(provider, null);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(this.tracker).link(captor.capture(), eq(code));

        final String newCode = captor.getValue();

        verify(this.storageService).store( //
                eq(StorageTrayEnum.ARCHIVED_PARTNERS.getName()), //
                any(), //
                eq(String.format("%s.zip", newCode)), //
                eq(newCode), //
                this.metas.capture() //
        );

        final List<SearchQueryFilter> values = this.metas.getValue();
        assertThat(values, containsInAnyOrder( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("column", equalTo("author")), //
                                hasProperty("value", equalTo("2020-07-JON-DOE-42")) //
                        ), //
                        allOf( //
                                hasProperty("column", equalTo("description")), //
                                hasProperty("value", nullValue()) //
                        ), //
                        allOf( //
                                hasProperty("column", equalTo("title")), //
                                hasProperty("value", equalTo("Archive Store")) //
                        ) //
                ) //
        ));

    }

    @Test(expected = TechnicalException.class)
    public void testServiceError() throws Exception {
        final IProvider provider = new ZipProvider(this.resourceAsBytes("spec.zip"));

        when(this.storageService.store(any(), any(), any(), any(), any())).thenReturn(Response.serverError().build());

        this.store.upload(provider, null);
    }

}
