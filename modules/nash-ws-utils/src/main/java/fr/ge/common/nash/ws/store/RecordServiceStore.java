package fr.ge.common.nash.ws.store;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.nash.ws.provider.RecordServiceProvider;
import fr.ge.common.nash.ws.v1.service.IRecordService;
import fr.ge.common.utils.bean.roles.EntityRole;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Store records using Nash WS Server.
 *
 * @author Christian Cougourdan
 */
public class RecordServiceStore implements IStore {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecordServiceStore.class);

    @Autowired
    private IRecordService service;

    @Autowired
    private ITrackerFacade tracker;

    @Value("${default.prefix.author:GE/}")
    private String defaultPrefixAuthor;

    @Override
    public IProvider upload(final IProvider provider, final Collection<EntityRole> roles) {
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);

        LOGGER.info("Send record {} to nash application module", Optional.ofNullable(desc.getRecordUid()).orElse(desc.getOrigin()));

        List<SearchQueryFilter> uglyRoles;
        if (CollectionUtils.isEmpty(roles)) {
            uglyRoles = Arrays.asList(new SearchQueryFilter(this.defaultPrefixAuthor + desc.getAuthor(), "*"));
        } else {
            uglyRoles = roles.stream().map(role -> new SearchQueryFilter(role.getEntity(), role.getRole())).collect(Collectors.toList());
        }

        final Response response = this.service.upload(provider.asBytes(), desc.getAuthor(), uglyRoles);

        if (null != response && response.getStatus() == Status.CREATED.getStatusCode()) {
            final String code = response.readEntity(String.class);

            if (!StringUtils.isEmpty(desc.getOrigin())) {
                try {
                    this.tracker.link(code, desc.getOrigin());
                } catch (final Exception ex) {
                    LOGGER.warn("Call to tracker : {}", ex.getMessage());
                }
            }

            return RecordServiceProvider.create(this.service, code);
        } else {
            throw new TechnicalException("Unable to send record to Nash WS Server");
        }
    }

}
