package fr.ge.common.nash.ws.store;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.util.UidFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.loader.SpecificationLoader;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationMeta;
import fr.ge.common.nash.engine.mapping.form.v1_2.meta.MetaElement;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.storage.ws.model.StorageTrayEnum;
import fr.ge.common.storage.ws.rest.IStorageRestService;
import fr.ge.common.utils.bean.roles.EntityRole;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * @author Christian Cougourdan
 */
public class ArchiveStore implements IStore {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchiveStore.class);

    private final UidFactoryImpl uidFactory = new UidFactoryImpl();

    @Autowired
    private ITrackerFacade tracker;

    @Autowired
    private IStorageRestService storageService;

    private StorageTrayEnum tray = StorageTrayEnum.ARCHIVED_PARTNERS;

    @Override
    public IProvider upload(final IProvider provider, final Collection<EntityRole> roles) {
        if (null == this.storageService) {
            LOGGER.warn("No storage service configured !!!");
            return null;
        }

        final SpecificationLoader loader = SpecificationLoader.create(provider);
        final FormSpecificationDescription desc = loader.description();

        if (StringUtils.isEmpty(desc.getRecordUid())) {
            desc.setRecordUid(this.uidFactory.uid());
            provider.save(Engine.FILENAME_DESCRIPTION, desc);

            if (!StringUtils.isEmpty(desc.getOrigin())) {
                try {
                    this.tracker.link(desc.getRecordUid(), desc.getOrigin());
                } catch (final Exception ex) {
                    LOGGER.warn("Call to tracker : {}", ex.getMessage());
                }
            }
        }

        final String code = desc.getRecordUid();

        LOGGER.info("Send record {} to storage queue '{}'", code, this.tray);

        final List<SearchQueryFilter> metas = new ArrayList<>();
        metas.addAll( //
                Arrays.asList(//
                        new SearchQueryFilter("author", desc.getAuthor()), //
                        new SearchQueryFilter("description", desc.getDescription()), //
                        new SearchQueryFilter("title", desc.getTitle()) //
                ) //
        );

        Optional.of(loader) //
                .filter(l -> null != l) //
                .map(SpecificationLoader::meta) //
                .filter(m -> null != m) //
                .map(FormSpecificationMeta::getMetas) //
                .filter(l -> null != l) //
                .orElse(new ArrayList<MetaElement>()) //
                .forEach(m -> metas.add(new SearchQueryFilter(StringUtils.join(m.getName(), ":", m.getValue()))));

        final Response response = this.storageService.store( //
                this.tray.getName(), //
                new Attachment("storageUploadFile", MediaType.APPLICATION_OCTET_STREAM, provider.asBytes()), //
                String.format("%s.zip", code), //
                code, //
                metas //
        );

        if (Status.OK.getStatusCode() == response.getStatus()) {
            LOGGER.info("Archived record {} to the storage queue {} with success", code, this.tray);
        } else {
            throw new TechnicalException(String.format("Unable to archive record %s : %s status code returned", code, response.getStatus()));
        }

        return null;
    }

    /**
     * @param tray
     *            the tray to set
     */
    public void setTray(final String tray) {
        this.tray = StorageTrayEnum.valueOf(tray);
    }

}
