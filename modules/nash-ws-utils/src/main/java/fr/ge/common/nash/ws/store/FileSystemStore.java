package fr.ge.common.nash.ws.store;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Collection;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.nash.core.util.UidFactoryImpl;
import fr.ge.common.nash.engine.Engine;
import fr.ge.common.nash.engine.mapping.form.v1_2.FormSpecificationDescription;
import fr.ge.common.nash.engine.provider.IProvider;
import fr.ge.common.nash.engine.store.IStore;
import fr.ge.common.utils.bean.roles.EntityRole;
import fr.ge.common.utils.exception.TechnicalException;
import fr.ge.tracker.facade.ITrackerFacade;

/**
 * Store record on filesytem using path pattern
 * "[yyyy-MM]/[dd]/[reference]/[uid].zip" linked with SHA1 and MD5 checksums.
 *
 * @author Christian Cougourdan
 */
public final class FileSystemStore implements IStore {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemStore.class);

    private static final int CHECKSUM_BUFFER_SIZE = 32 * 1024;

    private static final Pattern PATTERN_FILE_EXTENSION = Pattern.compile("(?<=[.])[^.]+$");

    private static final HexBinaryAdapter HEX_SERIALIZER = new HexBinaryAdapter();

    private static final String[] CHECKSUM_METHODS = new String[] { "sha1", "md5" };

    private final UidFactoryImpl uidFactory = new UidFactoryImpl();

    @Autowired
    private ITrackerFacade tracker;

    private Path rootPath;

    @Override
    public IProvider upload(final IProvider provider, final Collection<EntityRole> roles) {
        final FormSpecificationDescription desc = provider.asBean(Engine.FILENAME_DESCRIPTION, FormSpecificationDescription.class);
        final Path buildPath = this.buildBasePath(desc);

        if (StringUtils.isEmpty(desc.getRecordUid())) {
            desc.setRecordUid(this.uidFactory.uid());
            provider.save(Engine.FILENAME_DESCRIPTION, desc);

            if (!StringUtils.isEmpty(desc.getOrigin())) {
                try {
                    this.tracker.link(desc.getRecordUid(), desc.getOrigin());
                } catch (final Exception ex) {
                    LOGGER.warn("Call to tracker : {}", ex.getMessage());
                }
            }
        }

        final String code = desc.getRecordUid();
        LOGGER.info("Send record {} to file system", code);

        if (buildPath.toFile().canWrite()) {
            final Path resourcePath = buildPath.resolve(code + ".zip");
            if (Files.exists(resourcePath)) {
                LOGGER.debug("Record resource [{}] already exists", resourcePath);
                throw new TechnicalException(MessageFormat.format("Record resource [{0}] already exists", resourcePath));
            } else {
                try {
                    Files.write(resourcePath, provider.asBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
                } catch (final IOException ex) {
                    throw new TechnicalException(MessageFormat.format("Storing record {0} into {1} : {2}", code, this.rootPath, ex.getMessage()));
                }

                for (final String method : CHECKSUM_METHODS) {
                    this.uploadChecksum(resourcePath, method);
                }
            }
        } else {
            throw new TechnicalException(MessageFormat.format("Storing record {0} into {1} : {2} is not writable", code, this.rootPath, buildPath));
        }

        return null;
    }

    /**
     * Upload checksum file relative to specified resource.
     *
     * @param resourcePath
     *            targeted resource
     * @param method
     *            checksum method
     */
    private void uploadChecksum(final Path resourcePath, final String method) {
        final Path checksumPath = resourcePath.getParent().resolve(PATTERN_FILE_EXTENSION.matcher(resourcePath.getFileName().toString()).replaceFirst(method));
        final byte[] checksumContent = HEX_SERIALIZER.marshal(this.checksum(resourcePath.toFile(), method)).getBytes(StandardCharsets.UTF_8);

        try {
            Files.write(checksumPath, checksumContent, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (final IOException ex) {
            throw new TechnicalException(MessageFormat.format("Storing record checksum {0} into {1} : {2}", checksumPath.getFileName(), this.rootPath, ex.getMessage()));
        }
    }

    /**
     * Create record resource folder as {deposit date - YYYY-MM/DD}/{record
     * reference}.
     *
     * @param desc
     *            record description
     * @return record resource base path
     */
    private Path buildBasePath(final FormSpecificationDescription desc) {
        final Temporal now = LocalDateTime.now();

        final Path basePath = this.rootPath.resolve(DateTimeFormatter.ofPattern("yyyy-MM").format(now)) //
                .resolve(DateTimeFormatter.ofPattern("dd").format(now)) //
                .resolve(desc.getReference().getCode());

        basePath.toFile().mkdirs();

        return basePath;
    }

    /**
     * Read resource and calculate checksum using specified algorithm.
     *
     * @param resource
     *            resource to read
     * @param method
     *            checksum method
     * @return resource checksum
     */
    private byte[] checksum(final File resource, final String method) {
        try (InputStream in = new FileInputStream(resource)) {
            final MessageDigest digest = MessageDigest.getInstance(method);
            final byte[] buffer = new byte[CHECKSUM_BUFFER_SIZE];
            for (int n = 0; (n = in.read(buffer)) > 0;) {
                digest.update(buffer, 0, n);
            }
            return digest.digest();
        } catch (final NoSuchAlgorithmException ex) {
            throw new TechnicalException("Digest algorithm [" + method + "] not found : " + ex.getMessage());
        } catch (final IOException ex) {
            throw new TechnicalException("Unable to read file [" + resource + "] : " + ex.getMessage());
        }
    }

    /**
     * @param rootPath
     *            the root path to set
     */
    public FileSystemStore setRootPath(final String rootPath) {
        this.rootPath = null == rootPath ? Paths.get(".") : Paths.get(rootPath);
        return this;
    }

}
