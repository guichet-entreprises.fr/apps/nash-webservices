/**
 * 
 */
package fr.ge.common.nash.ws.data.bean;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class RecordBOBean.
 *
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public class RecordBOBean extends RecordBean {

  /** The denomination. */
  private String denomination;

  /** The transmission Date. */
  @DateTimeFormat(style = "S-")
  private Date transmissionDate;

  /** The type of formalite. */
  private String typeFormality;

  /** The state of the file. */
  private String fileState;

  /**
   * Accesseur sur l'attribut {@link #denomination}.
   *
   * @return String denomination
   */
  public String getDenomination() {
    return denomination;
  }

  /**
   * Mutateur sur l'attribut {@link #denomination}.
   *
   * @param denomination
   *          la nouvelle valeur de l'attribut denomination
   */
  public void setDenomination(String denomination) {
    this.denomination = denomination;
  }

  /**
   * Accesseur sur l'attribut {@link #transmissionDate}.
   *
   * @return Date transmissionDate
   */
  public Date getTransmissionDate() {
    return transmissionDate;
  }

  /**
   * Mutateur sur l'attribut {@link #transmissionDate}.
   *
   * @param transmissionDate
   *          la nouvelle valeur de l'attribut transmissionDate
   */
  public void setTransmissionDate(Date transmissionDate) {
    this.transmissionDate = transmissionDate;
  }

  /**
   * Accesseur sur l'attribut {@link #typeFormality}.
   *
   * @return String typeFormality
   */
  public String getTypeFormality() {
    return typeFormality;
  }

  /**
   * Mutateur sur l'attribut {@link #typeFormality}.
   *
   * @param typeFormality
   *          la nouvelle valeur de l'attribut typeFormality
   */
  public void setTypeFormality(String typeFormality) {
    this.typeFormality = typeFormality;
  }

  /**
   * Accesseur sur l'attribut {@link #fileState}.
   *
   * @return String fileState
   */
  public String getFileState() {
    return fileState;
  }

  /**
   * Mutateur sur l'attribut {@link #fileState}.
   *
   * @param fileState
   *          la nouvelle valeur de l'attribut fileState
   */
  public void setFileState(String fileState) {
    this.fileState = fileState;
  }

}
