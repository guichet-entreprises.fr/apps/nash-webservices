/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.bean;

/**
 * Bean representing form specification rights.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class AclEntityRoleBean {

    /** The id. */
    private Long id;

    /** The uid. */
    private String uid;

    /** The entity *. */
    private String entity;

    /** The role *. */
    private String role;

    /**
     * Default constructor.
     */
    public AclEntityRoleBean() {
        // Nothing to do
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public AclEntityRoleBean setId(final Long id) {
        this.id = id;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #uid}.
     *
     * @return String uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * Mutateur sur l'attribut {@link #uid}.
     *
     * @param uid
     *            la nouvelle valeur de l'attribut uid
     */
    public AclEntityRoleBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #entity}.
     *
     * @return String entity
     */
    public String getEntity() {
        return entity;
    }

    /**
     * Mutateur sur l'attribut {@link #entity}.
     *
     * @param entity
     *            la nouvelle valeur de l'attribut entity
     */
    public AclEntityRoleBean setEntity(final String entity) {
        this.entity = entity;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #role}.
     *
     * @return String role
     */
    public String getRole() {
        return role;
    }

    /**
     * Mutateur sur l'attribut {@link #role}.
     *
     * @param role
     *            la nouvelle valeur de l'attribut role
     */
    public AclEntityRoleBean setRole(final String role) {
        this.role = role;
        return this;
    }

}
