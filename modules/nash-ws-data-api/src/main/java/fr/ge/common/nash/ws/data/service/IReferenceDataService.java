/**
 *
 */
package fr.ge.common.nash.ws.data.service;

import fr.ge.common.nash.ws.data.bean.ReferenceBean;

/**
 * The Interface IReferenceDataService.
 *
 * @author bsadil
 */
public interface IReferenceDataService extends ISearchable {

    /**
     * Creates the reference.
     *
     * @param code
     *            the code
     * @return the reference entity
     */
    ReferenceBean create(String code);

    /**
     * Load a reference by its id.
     *
     * @param <T>
     *            the expected type
     * @param id
     *            the id
     * @param expected
     *            the expected class
     * @return the t
     */
    <T> T load(long id, Class<T> expected);

}
