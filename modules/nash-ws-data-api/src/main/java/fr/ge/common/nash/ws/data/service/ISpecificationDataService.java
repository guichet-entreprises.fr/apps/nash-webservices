/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.service;

import java.util.List;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.ws.data.bean.GroupSpecificationBean;
import fr.ge.common.nash.ws.data.bean.SpecificationBean;
import fr.ge.common.utils.bean.FileEntry;

/**
 * The Interface FormSpecificationService.
 *
 * @author Christian Cougourdan
 */
public interface ISpecificationDataService extends ISearchable {

    /**
     * Load a form specification from its UID and return it as a bean.
     *
     * @param uid
     *            form specification's UID to load
     * @return form specification as a bean
     */
    SpecificationBean load(String uid);

    /**
     * Load a form specification from its UID and return it as a bean.
     *
     * @param reference
     *            form specification's reference to load
     * @return form specification as a bean
     */
    SpecificationBean loadByReference(String reference);

    /**
     * Remove a FormSpecificationEntity from its id.
     *
     * @param uid
     *            FormSpecificationEntity's UID - must not be null
     * @throws IllegalArgumentException
     *             - in case the given id is null
     */
    void remove(String uid);

    /**
     * Create new form from specification zip file, and published it if
     * necessary.
     *
     * @param code
     *            the code
     * @param resourceAsBytes
     *            content as byte array
     * @param published
     *            the published
     * @param author
     *            the author
     * @param group
     *            the group
     * @return created form specification bean
     * @throws FunctionalException
     *             the functional exception
     */
    SpecificationBean fromFile(String code, byte[] resourceAsBytes, boolean published, String author, String group) throws FunctionalException;

    /**
     * Mark a form as published, making it available to end user.
     *
     * @param uid
     *            the uid
     */
    void publish(String uid);

    /**
     * Remove published form marker, making it no more available to end user.
     *
     * @param uid
     *            the uid
     */
    void unpublish(String uid);

    /**
     * Find all groupe.
     *
     * @return all groupe of specifications
     */
    List<GroupSpecificationBean> findAllGroupe();

    /**
     * Resource.
     *
     * @param uid
     *            the uid
     * @return the file entry
     */
    FileEntry resource(String uid);

    /**
     * Resource.
     *
     * @param uid
     *            the uid
     * @param resourcePath
     *            the resource path
     * @return the file entry
     */
    FileEntry resource(String uid, String resourcePath);

    /**
     * Update specification priority.
     *
     * @param reference
     *            the item identifier
     * @param priority
     *            the priority
     */
    SpecificationBean updatePriorityByReference(String reference, int priority);

    /**
     * Update specification priority by code.
     *
     * @param code
     *            the item identifier
     * @param priority
     *            the priority
     */
    SpecificationBean updatePriorityByCode(String code, int priority);

}
