/**
 *
 */
package fr.ge.common.nash.ws.data.bean;

/**
 * The Class ReferenceBean.
 *
 * @author bsadil
 */
public class ReferenceBean extends DatedBean<ReferenceBean> {

    /** the category. */
    private String category;

    /** the parent. */
    private Long parent;

    /** the content = parent + category. */
    private String content;

    /** The total specifications. */
    private Long totalSpecifications;

    /**
     * The number of specifications accessible through this reference on the
     * tree representation.
     */
    private Long nbSpecificationsPossible;

    /**
     * Instantiates a new reference bean.
     */
    public ReferenceBean() {
        super();
        // Nothing to do
    }

    /**
     * Gets the category.
     *
     * @return the category
     */
    public String getCategory() {
        return this.category;
    }

    /**
     * Sets the category.
     *
     * @param category
     *            the category to set
     */
    public ReferenceBean setCategory(final String category) {
        this.category = category;
        return this;
    }

    /**
     * Gets the parent.
     *
     * @return the parent
     */
    public Long getParent() {
        return this.parent;
    }

    /**
     * Sets the parent.
     *
     * @param parent
     *            the parent to set
     */
    public ReferenceBean setParent(final Long parent) {
        this.parent = parent;
        return this;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content to set
     */
    public ReferenceBean setContent(final String content) {
        this.content = content;
        return this;
    }

    /**
     * Gets the total specifications.
     *
     * @return the total specifications
     */
    public Long getTotalSpecifications() {
        return this.totalSpecifications;
    }

    /**
     * Sets the total specifications.
     *
     * @param totalSpecifications
     *            the new total specifications
     */
    public ReferenceBean setTotalSpecifications(final Long totalSpecifications) {
        this.totalSpecifications = totalSpecifications;
        return this;
    }

    /**
     * Gets the number of possible specifications accessible through this
     * reference on the tree representation.
     * 
     * @return the number of possible specifications accessible through this
     *         reference on the tree representation
     */
    public Long getNbSpecificationsPossible() {
        return nbSpecificationsPossible;
    }

    /**
     * Sets the number of possible specifications accessible through this
     * reference on the tree representation.
     * 
     * @param nbSpecificationsPossible
     *            the new number of possible specifications
     */
    public ReferenceBean setNbSpecificationsPossible(Long nbSpecificationsPossible) {
        this.nbSpecificationsPossible = nbSpecificationsPossible;
        return this;
    }

}
