/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.bean;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * The Class RecordBean.
 *
 * @author Christian Cougourdan
 */
public class RecordBean extends AuthoredBean<RecordBean> {

    /** The uid. */
    private String uid;

    /** The spec. */
    private String spec;

    /** The title. */
    private String title;

    /** The properties. */
    private byte[] properties;

    /** The details. */
    private ObjectNode details;

    /**
     * Gets the uid.
     *
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Sets the uid.
     *
     * @param uid
     *            the uid to set
     * @return the record bean
     */
    public RecordBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Gets the spec.
     *
     * @return the spec
     */
    public String getSpec() {
        return this.spec;
    }

    /**
     * Sets the spec.
     *
     * @param spec
     *            the spec to set
     * @return the record bean
     */
    public RecordBean setSpec(final String spec) {
        this.spec = spec;
        return this;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets the title.
     *
     * @param title
     *            the title
     * @return the record bean
     */
    public RecordBean setTitle(final String title) {
        this.title = title;
        return this;
    }

    /**
     * Getter on attribute {@link #properties}.
     *
     * @return byte[] properties
     */
    public byte[] getProperties() {
        return this.properties == null ? null : this.properties.clone();
    }

    /**
     * Setter on attribute {@link #properties}.
     *
     * @param properties
     *            the new value of attribute properties
     */
    public RecordBean setProperties(final byte[] properties) {
        this.properties = null == properties ? null : properties.clone();
        return this;
    }

    /**
     * @return the details
     */
    public ObjectNode getDetails() {
        return this.details;
    }

    /**
     * @param details
     *            the details to set
     */
    public RecordBean setDetails(final ObjectNode details) {
        this.details = details;
        return this;
    }

}
