/**
 * 
 */
package fr.ge.common.nash.ws.data.bean;

/**
 * @author bsadil
 *
 */
public class GroupSpecificationBean {

    /**
     * the groupName of sepcification
     */
    private String groupName;

    /**
     * the number total of specifications by group
     */
    private Long totaleByGroup;

    /**
     * @param groupName
     * @param totaleByGroup
     */
    public GroupSpecificationBean(String groupName, Long totaleByGroup) {
        super();
        this.groupName = groupName;
        this.totaleByGroup = totaleByGroup;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName
     *            the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the totaleByGroup
     */
    public Long getTotaleByGroup() {
        return totaleByGroup;
    }

    /**
     * @param totaleByGroup
     *            the totaleByGroup to set
     */
    public void setTotaleByGroup(Long totaleByGroup) {
        this.totaleByGroup = totaleByGroup;
    }

}
