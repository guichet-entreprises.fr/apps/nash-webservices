/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.bean;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Bean representing form specification.
 *
 * @author Christian Cougourdan
 */
public class SpecificationBean extends AuthoredBean<SpecificationBean> {

    /** The uid. */
    private String uid;

    /** The name. */
    private String name;

    /** The reference. */
    private String reference;

    /** The version. */
    private String version;

    /** The revision. */
    private Long revision;

    /** The source file. */
    private Long sourceId;

    /** The description. */
    private String description;

    /** The action. */
    private String action;

    /** The details. */
    private ObjectNode details;

    /** The group *. */
    private String groupe;

    /** The tagged. */
    private boolean tagged;

    /** The reference id. */
    private Long referenceId;

    /** The size. */
    private Long size;

    /** The color *. */
    private String color;

    /** The icon *. */
    private String icon;

    /** The priority *. */
    private Long priority;

    /**
     * Default constructor.
     */
    public SpecificationBean() {
        // Nothing to do
    }

    /**
     * Gets the uid.
     *
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Sets the uid.
     *
     * @param uid
     *            the uid to set
     * @return the specification bean
     */
    public SpecificationBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the name to set
     * @return the specification bean
     */
    public SpecificationBean setName(final String name) {
        this.name = name;
        return this;
    }

    /**
     * Gets the revision.
     *
     * @return the revision
     */
    public Long getRevision() {
        return this.revision;
    }

    /**
     * Sets the revision.
     *
     * @param revision
     *            the revision to set
     * @return the specification bean
     */
    public SpecificationBean setRevision(final Long revision) {
        this.revision = revision;
        return this;
    }

    /**
     * Gets the source id.
     *
     * @return the source id
     */
    public Long getSourceId() {
        return this.sourceId;
    }

    /**
     * Sets the source id.
     *
     * @param sourceId
     *            the source id
     * @return the specification bean
     */
    public SpecificationBean setSourceId(final Long sourceId) {
        this.sourceId = sourceId;
        return this;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the description to set
     * @return the specification bean
     */
    public SpecificationBean setDescription(final String description) {
        this.description = description;
        return this;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public String getAction() {
        return this.action;
    }

    /**
     * Sets the action.
     *
     * @param action
     *            the action to set
     * @return the specification bean
     */
    public SpecificationBean setAction(final String action) {
        this.action = action;
        return this;
    }

    /**
     * Checks if is tagged.
     *
     * @return the tagged
     */
    public boolean isTagged() {
        return this.tagged;
    }

    /**
     * Sets the tagged.
     *
     * @param tagged
     *            the tagged to set
     * @return the specification bean
     */
    public SpecificationBean setTagged(final boolean tagged) {
        this.tagged = tagged;
        return this;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public String getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version to set
     * @return the specification bean
     */
    public SpecificationBean setVersion(final String version) {
        this.version = version;
        return this;
    }

    /**
     * Gets the reference.
     *
     * @return the reference
     */
    public String getReference() {
        return this.reference;
    }

    /**
     * Sets the reference.
     *
     * @param reference
     *            the reference to set
     * @return the specification bean
     */
    public SpecificationBean setReference(final String reference) {
        this.reference = reference;
        return this;
    }

    /**
     * Gets the reference id.
     *
     * @return the referenceBean
     */
    public Long getReferenceId() {
        return this.referenceId;
    }

    /**
     * Sets the reference id.
     *
     * @param referenceId
     *            the reference id
     * @return the specification bean
     */
    public SpecificationBean setReferenceId(final Long referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public Long getSize() {
        return this.size;
    }

    /**
     * Gets the details.
     *
     * @return the details
     */
    public ObjectNode getDetails() {
        return this.details;
    }

    /**
     * Sets the details.
     *
     * @param details
     *            the details to set
     */
    public SpecificationBean setDetails(final ObjectNode details) {
        this.details = details;
        return this;
    }

    /**
     * Sets the size.
     *
     * @param size
     *            the size
     * @return the specification bean
     */
    public SpecificationBean setSize(final Long size) {
        this.size = size;
        return this;
    }

    /**
     * Gets the groupe.
     *
     * @return the group
     */
    public String getGroupe() {
        return this.groupe;
    }

    /**
     * Sets the groupe.
     *
     * @param groupe
     *            the groupe
     * @return the specification bean
     */
    public SpecificationBean setGroupe(final String groupe) {
        this.groupe = groupe;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #color}.
     *
     * @return String color
     */
    public String getColor() {
        return color;
    }

    /**
     * Mutateur sur l'attribut {@link #color}.
     *
     * @param color
     *            la nouvelle valeur de l'attribut color
     */
    public SpecificationBean setColor(String color) {
        this.color = color;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #icon}.
     *
     * @return String icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Mutateur sur l'attribut {@link #icon}.
     *
     * @param icon
     *            la nouvelle valeur de l'attribut icon
     */
    public SpecificationBean setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    /**
     * Accesseur sur l'attribut {@link #priority}.
     *
     * @return String priority
     */
    public Long getPriority() {
        return priority;
    }

    /**
     * Mutateur sur l'attribut {@link #priority}.
     *
     * @param priority
     *            la nouvelle valeur de l'attribut priority
     */
    public SpecificationBean setPriority(Long priority) {
        this.priority = priority;
        return this;
    }

}
