/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.bean;

import java.util.Calendar;

/**
 * The Class RecordArchiveBean.
 *
 * @author Ali AKKOU
 */
public class RecordArchiveBean extends AuthoredBean<RecordArchiveBean> {

    /** The uid. */
    private String uid;

    /** The spec. */
    private String spec;

    /** The archiving date. */
    private Calendar archived;

    /** The content. */
    private byte[] content;

    /**
     * Gets the uid.
     *
     * @return the uid
     */
    public String getUid() {
        return this.uid;
    }

    /**
     * Sets the uid.
     *
     * @param uid
     *            the uid to set
     * @return the record bean
     */
    public RecordArchiveBean setUid(final String uid) {
        this.uid = uid;
        return this;
    }

    /**
     * Gets the spec.
     *
     * @return the spec
     */
    public String getSpec() {
        return this.spec;
    }

    /**
     * Sets the spec.
     *
     * @param spec
     *            the spec to set
     * @return the record bean
     */
    public RecordArchiveBean setSpec(final String spec) {
        this.spec = spec;
        return this;
    }

    /**
     * Gets the archived.
     *
     * @return the archived
     */
    public Calendar getArchived() {
        return this.archived;
    }

    /**
     * Sets the archived.
     *
     * @param archived
     *            the archived to set
     * @return the t
     */
    @SuppressWarnings("unchecked")
    public RecordArchiveBean setArchived(final Calendar archived) {
        this.archived = archived;
        return this;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Sets the content.
     *
     * @param content
     *            the content to set
     * @return the content
     */
    public RecordArchiveBean setContent(final byte[] content) {
        this.content = null == content ? null : content.clone();
        return this;
    }

}
