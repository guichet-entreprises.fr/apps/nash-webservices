/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.nash.ws.data.service;

import java.util.Collection;
import java.util.List;

import fr.ge.common.nash.core.exception.FunctionalException;
import fr.ge.common.nash.core.exception.RecordNotFoundException;
import fr.ge.common.nash.core.exception.UnauthorizedAccessException;
import fr.ge.common.nash.ws.data.bean.FileBean;
import fr.ge.common.utils.bean.FileEntry;
import fr.ge.common.utils.bean.search.SearchQueryFilter;

/**
 * The Interface RecordService.
 *
 * @author Christian Cougourdan
 */
public interface IRecordDataService extends ISearchable {

    /**
     * Check if record specified by its UID exists and can be access with roles.
     * Throws {@link RecordNotFoundException} if not exists or
     * {@link UnauthorizedAccessException} if access is not allowed.
     *
     * @param uid
     *            record UID
     * @param roles
     *            access entity/roles
     */
    void checkIfExistsAndHasAccess(String uid, final List<SearchQueryFilter> roles);

    /**
     * Load a form record by its UID.
     *
     * @param <T>
     *            the generic type
     * @param uid
     *            form record uid
     * @param expected
     *            the expected
     * @return form record bean
     */
    <T> T load(String uid, Class<? extends T> expected);

    /**
     * Load resource list by record uid.
     *
     * @param uid
     *            record uid
     * @return record resources list
     */
    Collection<FileBean> resources(String uid);

    /**
     * Add or update a file linked to a form record by its UID.
     *
     * @param uid
     *            form record UID
     * @param entryName
     *            file name
     * @param type
     *            file mime type
     * @param entryAsBytes
     *            file content as byte array
     * @return created file bean
     */
    FileBean addOrUpdate(String uid, String entryName, String type, byte[] entryAsBytes);

    /**
     * Remove a file linked to a form record by its UID.
     *
     * @param uid
     *            form record UID
     * @param name
     *            file name
     * @return number of removed element
     */
    long remove(String uid, String name);

    /**
     * Remove a record by UID.
     *
     * @param uid
     *            form record UID
     * @return number of removed element
     */
    long remove(String uid);

    /**
     * From file.
     *
     * @param <T>
     *            the generic type
     * @param formsRecordName
     *            the FORMS record name
     * @param formsRecordType
     *            the FORMS record type
     * @param resourceAsBytes
     *            the resource as bytes
     * @param author
     *            the author
     * @param roles
     *            the roles
     * @param expected
     *            the expected
     * @return the record bean
     * @throws FunctionalException
     *             the functional exception
     */
    <T> T fromFile(String formsRecordName, final String formsRecordType, byte[] resourceAsBytes, String author, List<SearchQueryFilter> roles, Class<? extends T> expected) throws FunctionalException;

    /**
     * Check if a record exists.
     *
     * @param uid
     *            record uid
     * @return true if record exists, false otherwise
     */
    boolean exists(String uid);

    /**
     * Check if a record resource exists.
     *
     * @param uid
     *            record uid
     * @param name
     *            resource name
     * @return true if resource exists, false otherwise
     */
    boolean resourceExists(String uid, String name);

    /**
     * Retrieve resource from its name and record uid.
     *
     * @param uid
     *            record uid
     * @param name
     *            resource name
     * @return resource file entry
     */
    FileEntry resource(String uid, String name);

    /**
     * Assign access rights to a record.
     * 
     * @param uid
     *            the record identifier
     * @param assignee
     *            the assignee identifier
     * @param roles
     *            the default roles
     */
    void assignRoles(String uid, String assignee, List<SearchQueryFilter> roles);

    /**
     * Remove record based on criteria.
     * 
     * @param filters
     *            Search query filters
     */
    long remove(final List<SearchQueryFilter> filters);

}
