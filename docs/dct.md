# Nash REST API

[TOC]

# Serveur REST

## Configuration

%{properties:modules/nash-ws-server/src/main/config/application.properties}


## Services

%{openapi:modules/nash-ws-contract/target/nash-webservices-openapi-2.15.1.4-SNAPSHOT.yaml}
